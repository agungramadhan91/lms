<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Auth::check()){
    return view('home');
    }else{
    return view('auth.login');
    }
});

Route::get('/course/college/{id}', 'HomeController@getCourseByCollege');
Route::get('/course/program/{id}', 'HomeController@getCourseByProgram');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/course','HomeController@courseIndex')->name('home.course');
Route::get('/home/catalogue','CatalogueController@catalogueIndex')->name('home.catalogue');
Route::get('/home/minor','HomeController@minorIndex')->name('home.minor');


Route::match(['post','get'],'/catalogue/show','CatalogueController@index')->name('catalogue.show');
Route::get('/catalogue/pdf','CatalogueController@pdf')->name('catalogue.pdf');
Route::get('/catalogue/word','CatalogueController@word')->name('catalogue.word');

Route::get('/program','ProgramController@index')->name('program');
Route::post('/program/store','ProgramController@store')->name('program.store');
Route::get('/program/outcome','ProgramController@create')->name('program.outcome');
Route::post('/program/outcome/store','ProgramController@outcomes')->name('program.outcome.store');
Route::get('/program/assesment','ProgramController@assesmentview')->name('program.assesment');
Route::post('/program/assesment/store','ProgramController@assesment')->name('program.assesment.store');
Route::get('/program/faculty', 'ProgramController@facultyview')->name('program.faculty');
Route::post('/program/faculty/store','ProgramController@faculty')->name('program.faculty.store');
Route::get('/program/fitness', 'ProgramController@fitnessview')->name('program.fitness');
Route::post('/program/fitness/store','ProgramController@fitness')->name('program.fitness.store');
Route::get('/program/student', 'ProgramController@studentview')->name('program.student');
Route::post('/program/student/store','ProgramController@student')->name('program.student.store');
Route::get('/program/research', 'ProgramController@researchview')->name('program.research');
Route::post('/program/research/store','ProgramController@research')->name('program.research.store');
Route::get('/program/resource', 'ProgramController@resourceview')->name('program.resource');
Route::post('/program/resource/store','ProgramController@resource')->name('program.resource.store');
Route::get('/program/supportive', 'ProgramController@supportiveview')->name('program.supportive');
Route::post('/program/supportive/store','ProgramController@supportive')->name('program.supportive.store');
Route::get('/program/cost', 'ProgramController@costview')->name('program.cost');
Route::post('/program/cost/store','ProgramController@cost')->name('program.cost.store');
Route::get('/program/community', 'ProgramController@communityview')->name('program.community');
Route::post('/program/community/store','ProgramController@community')->name('program.community.store');
Route::get('/program/public', 'ProgramController@publicview')->name('program.public');
Route::post('/program/public/store','ProgramController@public')->name('program.public.store');
Route::get('/program/appendices', 'ProgramController@appendicesview')->name('program.appendices');
Route::get('/program/structure', 'ProgramController@structureview')->name('program.structure');
Route::post('/program/structure/store','ProgramController@structure')->name('program.structure.store');
Route::post('/program/all','ProgramController@all')->name('program.all');

Route::get('/program/view/choose','ProgramViewController@choose')->name('program.view.choose');
Route::post('/program/view','ProgramViewController@index')->name('program.view');
Route::get('/program/view/outcome','ProgramViewController@outcome')->name('program.view.outcome');
Route::get('/program/view/structure', 'ProgramViewController@structureview')->name('program.view.structure');
Route::get('/program/view/assesment','ProgramViewController@assesment')->name('program.view.assesment');
Route::get('/program/view/faculty','ProgramViewController@faculty')->name('program.view.faculty');
Route::get('/program/view/fitness','ProgramViewController@fitness')->name('program.view.fitness');
Route::get('/program/view/student','ProgramViewController@student')->name('program.view.student');
Route::get('/program/view/research','ProgramViewController@research')->name('program.view.research');
Route::get('/program/view/resource','ProgramViewController@resource')->name('program.view.resource');
Route::get('/program/view/supportive','ProgramViewController@supportive')->name('program.view.supportive');
Route::get('/program/view/cost','ProgramViewController@cost')->name('program.view.cost');
Route::get('/program/view/community','ProgramViewController@community')->name('program.view.community');
Route::get('/program/view/public','ProgramViewController@public')->name('program.view.public');

Route::get('/program/concentration/choose', 'ProgramSubstantiveController@chooseconcentration')->name('program.concentration.choose');
Route::match(['post','get'],'/program/concentration/edit','ProgramSubstantiveController@concentration')->name('program.concentration.edit');
Route::post('/program/concentration/update', 'ProgramSubstantiveController@concentrationstore')->name('program.concentration.update');
Route::post('/program/concentration/submit', 'ProgramSubstantiveController@concentrationsubmit')->name('program.concentration.submit');

Route::get('/program/concentration/discontinuing/choose', 'ProgramSubstantiveController@choosediscontinuing')->name('program.concentration.discontinuing.choose');
Route::match(['post','get'],'/program/concentration/discontinuing/edit','ProgramSubstantiveController@discontinuing')->name('program.concentration.discontinuing.edit');
Route::post('/program/concentration/discontinuing/update', 'ProgramSubstantiveController@discontinuingstore')->name('program.concentration.discontinuing.update');
Route::post('/program/concentration/discontinuing/submit', 'ProgramSubstantiveController@discontinuingsubmit')->name('program.concentration.discontinuing.submit');

Route::get('/program/concentration/changing/choose', 'ProgramSubstantiveController@choosechanging')->name('program.concentration.changing.choose');
Route::match(['post','get'],'/program/concentration/changing/edit','ProgramSubstantiveController@changing')->name('program.concentration.changing.edit');
Route::post('/program/concentration/changing/update', 'ProgramSubstantiveController@changingstore')->name('program.concentration.changing.update');
Route::post('/program/concentration/changing/submit', 'ProgramSubstantiveController@changingsubmit')->name('program.concentration.changing.submit');

Route::get('/college/view/choose','CollegeViewController@choose')->name('college.view.choose');
Route::match(['post','match'],'/college/view/','CollegeViewController@index')->name('college.view');

Route::get('/campus/view/choose','CampusViewController@choose')->name('campus.view.choose');
Route::match(['post','match'],'/campus/view/','campusViewController@index')->name('campus.view');

Route::get('/course', 'CourseController@index')->name('course');
Route::post('/course/store', 'CourseController@store')->name('course.store');
Route::get('/course/hour','CourseController@hour')->name('course.hour');
Route::post('/course/hour/store', 'CourseController@hourstore')->name('course.hour.store');
Route::get('/course/textbook','CourseController@textbook')->name('course.textbook');
Route::post('/course/textbook/store', 'CourseController@textbookstore')->name('course.textbook.store');
Route::get('/course/optional','CourseController@optional')->name('course.optional');
Route::post('/course/optional/store', 'CourseController@optionalstore')->name('course.optional.store');
Route::get('/course/requisite','CourseController@requisite')->name('course.requisite');
Route::post('/course/requisite/store', 'CourseController@requisitestore')->name('course.requisite.store');
Route::get('/course/restrictions','CourseController@restriction')->name('course.restriction');
Route::post('/course/restrictions/store','CourseController@restrictionstore')->name('course.restriction.store');
Route::get('/course/policies','CourseController@policies')->name('course.policies');
Route::post('/course/policies/store', 'CourseController@policiesstore')->name('course.policies.store');
Route::get('/course/special','CourseController@special')->name('course.special');
Route::post('/course/special/store', 'CourseController@specialstore')->name('course.special.store');
Route::get('/course/pedagogy','CourseController@pedagogy')->name('course.pedagogy');
Route::post('/course/pedagogy/store', 'CourseController@pedagogystore')->name('course.pedagogy.store');
Route::get('/course/outcome','CourseController@outcome')->name('course.outcome');
Route::post('/course/outcome/store', 'CourseController@outcomestore')->name('course.outcome.store');
Route::get('/course/association','CourseController@association')->name('course.association');
Route::post('/course/association/store', 'CourseController@associationstore')->name('course.association.store');
Route::get('/course/assessment','CourseController@assessment')->name('course.assessment');
Route::post('/course/assessment/store', 'CourseController@assessmentstore')->name('course.assessment.store');
Route::get('/course/grading','CourseController@grading')->name('course.grading');
Route::post('/course/grading/store', 'CourseController@gradingstore')->name('course.grading.store');

Route::get('/course/view/choose','CourseViewController@choose')->name('course.view.choose');
Route::post('/course/view', 'CourseViewController@index')->name('course.view');
Route::get('/course/view/hour','CourseViewController@hour')->name('course.view.hour');
Route::get('/course/view/requisite','CourseViewController@requisite')->name('course.view.requisite');
Route::get('/course/view/association','CourseViewController@association')->name('course.view.association');
Route::get('/course/view/outcome','CourseViewController@outcome')->name('course.view.outcome');
Route::get('/course/view/assessment','CourseViewController@assessment')->name('course.view.assessment');
Route::get('/course/view/textbook','CourseViewController@textbook')->name('course.view.textbook');
Route::get('/course/view/optional','CourseViewController@optional')->name('course.view.optional');
Route::get('/course/view/grading','CourseViewController@grading')->name('course.view.grading');
Route::get('/course/view/policies','CourseViewController@policies')->name('course.view.policies');
Route::get('/course/view/special','CourseViewController@special')->name('course.view.special');
Route::get('/course/view/pedagogy','CourseViewController@pedagogy')->name('course.view.pedagogy');

Route::get('/course/description/choose', 'CourseSubstantiveController@choosedescription')->name('course.description.choose');
Route::match(['post','get'],'/course/description', 'CourseSubstantiveController@index')->name('course.description');
Route::post('/course/description/store', 'CourseSubstantiveController@store')->name('course.description.store');
Route::post('/course/description/submit', 'CourseSubstantiveController@descriptionsubmit')->name('course.description.submit');

Route::get('/course/inactive/choose', 'CourseSubstantiveController@chooseinactive')->name('course.inactive.choose');
Route::match(['post','get'],'/course/inactive', 'CourseSubstantiveController@inactive')->name('course.inactive');
Route::post('/course/inactive/store', 'CourseSubstantiveController@inactivestore')->name('course.inactive.store');
Route::post('/course/inactive/submit', 'CourseSubstantiveController@inactivesubmit')->name('course.inactive.submit');

Route::get('/course/replacement/choose', 'CourseSubstantiveController@choosereplacement')->name('course.replacement.choose');
Route::match(['post','get'],'/course/replacement', 'CourseSubstantiveController@replacement')->name('course.replacement.edit');
Route::post('/course/replacement/store', 'CourseSubstantiveController@replacementstore')->name('course.replacement.store');
Route::post('/course/replacement/submit', 'CourseSubstantiveController@replacementsubmit')->name('course.replacement.submit');

Route::get('/course/outcome/description/choose', 'CourseSubstantiveController@chooseoutcome')->name('course.outcome.choose');
Route::match(['post','get'],'/course/outcome/description','CourseSubstantiveController@outcome')->name('course.description.outcome');
Route::post('/course/outcome/description/store', 'CourseSubstantiveController@outcomestore')->name('course.outcomedescription.store');
Route::post('/course/outcome/status','CourseSubstantiveController@outcomestatus')->name('course.outcome.status');

Route::get('/course/hour/description/choose', 'CourseSubstantiveController@choosehour')->name('course.hour.choose');
Route::match(['post','get'],'/course/hour/description','CourseSubstantiveController@hour')->name('course.description.hour');
Route::post('/course/hour/description/store', 'CourseSubstantiveController@hourstore')->name('course.hourdescription.store');

Route::get('/course/association/description','CourseSubstantiveController@association')->name('course.description.association');
Route::post('/course/association/description/store', 'CourseSubstantiveController@associationstore')->name('course.associationdescription.store');

Route::get('/course/level/choose', 'CourseNonController@chooselevel')->name('course.level.choose');
Route::match(['post','get'],'/course/level', 'CourseNonController@index')->name('course.level');
Route::post('/course/level/update', 'CourseNonController@store')->name('course.level.update');
Route::post('/course/level/submit', 'CourseNonController@levelsubmit')->name('course.level.submit');
Route::get('/course/requisite/edit','CourseNonController@requisite')->name('course.requisite.edit');
Route::post('/course/requisite/update', 'CourseNonController@requisitestore')->name('course.requisite.update');
Route::get('/course/association/level','CourseNonController@association')->name('course.association.level');
Route::match(['post','get'],'/course/association/level/update', 'CourseNonController@associationstore')->name('course.associationlevel.update');
Route::post('/course/association/level/submit', 'CourseNonController@associationsubmit')->name('course.association.submit');
Route::get('/course/outcome/level','CourseNonController@outcome')->name('course.outcome.level');
Route::post('/course/outcome/level/update', 'CourseNonController@outcomestore')->name('course.outcomelevel.update');

Route::get('/course/subject/choose', 'CourseNonController@choosesubject')->name('course.subject.choose');
Route::match(['post','get'],'/course/subject', 'CourseNonController@subject')->name('course.subject.edit');
Route::post('/course/subject/update', 'CourseNonController@subjectstore')->name('course.subject.update');
Route::post('/course/subject/submit', 'CourseNonController@subjectsubmit')->name('course.subject.submit');
Route::get('/course/requisitesubject/edit','CourseNonController@subjectrequisite')->name('course.requisitesubject.edit');
Route::post('/course/requisitesubject/update', 'CourseNonController@subjectrequisitestore')->name('course.requisitesubject.update');
Route::get('/course/associationsubject/edit','CourseNonController@subjectassociation')->name('course.subjectassociation.level');
Route::post('/course/association/level/update', 'CourseNonController@subjectassociationstore')->name('course.subjectassociation.update');
Route::post('/course/asspciation/level/submit', 'CourseNonController@subjectassociationsubmit')->name('course.subjectassociation.submit');

Route::get('/course/title/choose', 'CourseNonController@choosetitle')->name('course.title.choose');
Route::match(['post','get'],'/course/title', 'CourseNonController@title')->name('course.title');
Route::post('/course/title/update', 'CourseNonController@titlestore')->name('course.title.update');
Route::post('/course/title/submit', 'CourseNonController@titlesubmit')->name('course.title.submit');

Route::get('/course/assessment/choose', 'CourseNonController@chooseassessment')->name('course.assessment.choose');
Route::match(['post','get'], '/course/assessment/edit','CourseNonController@assessment')->name('course.assessment.edit');
Route::post('/course/assessment/update', 'CourseNonController@assessmentstore')->name('course.assessment.update');

Route::get('/course/delivery/choose', 'CourseNonController@choosedelivery')->name('course.delivery.choose');
Route::post('/course/delivery/edit', 'CourseNonController@delivery')->name('course.delivery.edit');

Route::get('/course/grading/choose', 'CourseNonController@choosegrading')->name('course.grading.choose');
Route::match(['post','get'],'/course/grading/edit','CourseNonController@grading')->name('course.grading.edit');
Route::post('/course/grading/update', 'CourseNonController@gradingstore')->name('course.grading.update');
Route::post('/course/grading/submit', 'CourseNonController@gradingsubmit')->name('course.grading.submit');

Route::get('/course/type/choose', 'CourseNonController@choosetype')->name('course.type.choose');
Route::match(['post','get'],'/course/type/edit','CourseNonController@type')->name('course.type.edit');
Route::post('/course/type/update', 'CourseNonController@typestore')->name('course.type.update');
Route::post('/course/type/submit', 'CourseNonController@typesubmit')->name('course.type.submit');

Route::get('/course/textbook/choose', 'CourseNonController@choosetextbook')->name('course.textbook.choose');
Route::match(['get','post'], '/course/textbook/edit', 'CourseNonController@textbook')->name('course.textbook.edit');
Route::post('/course/textbook/update', 'CourseNonController@textbookstore')->name('course.textbook.update');
Route::post('/course/textbook/submit', 'CourseNonController@textbooksubmit')->name('course.textbook.submit');

Route::get('/course/prerequisite/choose', 'CourseNonController@chooseprerequisite')->name('course.prerequisite.choose');
Route::match(['get','post'],'/course/prerequisite/edit', 'CourseNonController@prerequisite')->name('course.prerequisite.edit');
Route::post('/course/prerequisite/update', 'CourseNonController@prerequisitestore')->name('course.prerequisite.update');

Route::get('/course/equivalency/choose', 'CourseNonController@chooseequivalency')->name('course.equivalency.choose');
Route::match(['get','post'],'/course/equivalency/edit', 'CourseNonController@equivalency')->name('course.equivalency.edit');
Route::post('/course/equivalency/update', 'CourseNonController@equivalencystore')->name('course.equivalency.update');
Route::post('/course/equivalency/submit', 'CourseNonController@equivalencysubmit')->name('course.equivalency.submit');

Route::get('/course/corequisite/choose', 'CourseNonController@choosecorequisite')->name('course.corequisite.choose');
Route::match(['get','post'],'/course/corequisite/edit', 'CourseNonController@corequisite')->name('course.corequisite.edit');
Route::post('/course/corequisite/update', 'CourseNonController@corequisitestore')->name('course.corequisite.update');

Route::get('/course/coprerequisite/choose', 'CourseSubstantiveController@choosecoprerequisite')->name('course.coprerequisite.choose');
Route::match(['get','post'],'/course/coprerequisite/edit', 'CourseSubstantiveController@coprerequisite')->name('course.coprerequisite.edit');

Route::get('/course/restrictions/choose', 'CourseNonController@chooserestrictions')->name('course.restriction.choose');
Route::match(['get','post'],'/course/restrictions','CourseNonController@restriction')->name('course.restriction.edit');
// Route::post('/course/restrictions/store','CourseController@restrictionstore')->name('course.restriction.store');

Route::get('/admin/user',function(){
    return view ('admin.user');
})->name('admin.user');


Route::get('/program/{id}', 'HomeController@getProgram');
//============================= Agung's Work ======================================================================

Route::group(['prefix'=>'managements'], function()
{
    Route::group(['prefix'=>'role_user'], function()
    {
        //ROLE
        Route::get('index', 'Management\RoleUser\IndexController')->name('management.role_user.index');
        Route::get('create', 'Management\RoleUser\CreateController')->name('management.role_user.create');
        Route::post('store', 'Management\RoleUser\StoreController')->name('management.role_user.store');
        Route::post('update/{code}', 'Management\RoleUser\UpdateController')->name('management.role_user.update');
         
        //USER
        Route::post('store/user', 'Management\RoleUser\StoreController@store_user')->name('management.role_user.store_user');
        Route::post('update/user/{user}', 'Management\RoleUser\UpdateController@update_user')->name('management.role_user.update_user');
        
        //WORKFLOW
        Route::post('store/workflow', 'Management\RoleUser\StoreController@store_workflow')->name('management.role_user.store_workflow');
        Route::post('update/{code}/workflow', 'Management\RoleUser\UpdateController@update_workflow')->name('management.role_user.update_workflow');
    });
});

Route::group(['prefix'=>'maintenance'], function()
{
    Route::group(['prefix'=>'colleges'], function()
    {
        Route::get('index','Maintenance\College\IndexController')->name('maintenance.college.index');
        Route::post('store','Maintenance\College\StoreController')->name('maintenance.college.store');
        Route::post('update/{code}','Maintenance\College\UpdateController')->name('maintenance.college.update');
    });
    
    Route::group(['prefix'=>'campuses'], function()
    {
        Route::get('index','Maintenance\Campus\IndexController')->name('maintenance.campus.index');
        Route::post('store','Maintenance\Campus\StoreController')->name('maintenance.campus.store');
        Route::post('update/{code}','Maintenance\Campus\UpdateController')->name('maintenance.campus.update');
    });
    
    Route::group(['prefix'=>'degree_levels'], function()
    {
        Route::get('index','Maintenance\DegreeLevel\IndexController')->name('maintenance.degree_level.index');
        Route::post('store','Maintenance\DegreeLevel\StoreController')->name('maintenance.degree_level.store');
        Route::post('update/{code}','Maintenance\DegreeLevel\UpdateController')->name('maintenance.degree_level.update');
    });
    
    Route::group(['prefix'=>'degree_types'], function()
    {
        Route::get('index','Maintenance\DegreeType\IndexController')->name('maintenance.degree_type.index');
        Route::post('store','Maintenance\DegreeType\StoreController')->name('maintenance.degree_type.store');
        Route::post('update/{code}','Maintenance\DegreeType\UpdateController')->name('maintenance.degree_type.update');
    });

    Route::group(['prefix'=>'course_levels'], function()
    {
        Route::get('index','Maintenance\CourseLevel\IndexController')->name('maintenance.course_level.index');
        Route::post('store','Maintenance\CourseLevel\StoreController')->name('maintenance.course_level.store');
        Route::post('update/{code}','Maintenance\CourseLevel\UpdateController')->name('maintenance.course_level.update');
    });

    Route::group(['prefix'=>'course_types'], function()
    {
        Route::get('index','Maintenance\CourseType\IndexController')->name('maintenance.course_type.index');
        Route::post('store','Maintenance\CourseType\StoreController')->name('maintenance.course_type.store');
        Route::post('update/{code}','Maintenance\CourseType\UpdateController')->name('maintenance.course_type.update');
    });

    Route::group(['prefix'=>'grading_modes'], function()
    {
        Route::get('index','Maintenance\GradingMode\IndexController')->name('maintenance.grading_mode.index');
        Route::post('store','Maintenance\GradingMode\StoreController')->name('maintenance.grading_mode.store');
        Route::post('update/{code}','Maintenance\GradingMode\UpdateController')->name('maintenance.grading_mode.update');
    });
    
    Route::group(['prefix'=>'grading_schemas'], function()
    {
        Route::get('index','Maintenance\GradingSchema\IndexController')->name('maintenance.grading_schema.index');
        Route::post('store','Maintenance\GradingSchema\StoreController')->name('maintenance.grading_schema.store');
        Route::post('update/{code}','Maintenance\GradingSchema\UpdateController')->name('maintenance.grading_schema.update');
    });

    Route::group(['prefix'=>'departments'], function()
    {
        Route::get('index','Maintenance\Department\IndexController')->name('maintenance.department.index');
        Route::post('store','Maintenance\Department\StoreController')->name('maintenance.department.store');
        Route::post('update/{code}','Maintenance\Department\UpdateController')->name('maintenance.department.update');
    });
    
    Route::group(['prefix'=>'programs'], function()
    {
        Route::get('degree/type/{code}','Maintenance\Programs\IndexController@degreeType');

        Route::get('index','Maintenance\Programs\IndexController')->name('maintenance.program.index');
        Route::post('store','Maintenance\Programs\StoreController')->name('maintenance.program.store');
        Route::post('update/{code}','Maintenance\Programs\UpdateController')->name('maintenance.program.update');
    });

    Route::group(['prefix'=>'subjects'], function()
    {
        Route::get('index','Maintenance\Subject\IndexController')->name('maintenance.subject.index');
        Route::post('store','Maintenance\Subject\StoreController')->name('maintenance.subject.store');
        Route::post('update/{code}','Maintenance\Subject\UpdateController')->name('maintenance.subject.update');
    });
    
    Route::group(['prefix'=>'terms'], function()
    {
        Route::get('index','Maintenance\Terms\IndexController')->name('maintenance.term.index');
        Route::post('store','Maintenance\Terms\StoreController')->name('maintenance.term.store');
        Route::post('update/{code}','Maintenance\Terms\UpdateController')->name('maintenance.term.update');
    });
});

Route::group(['prefix'=>'program-management'], function()
{
    Route::group(['prefix'=>'non-substantive'], function()
    {
        Route::group(['prefix'=>'moving-courses'], function()
        {
            Route::get('',
                'ProgramManagement\NonSubstantive\MovingCourse\ChooseController')
                ->name('program.non-substantive.moving_course.choose');
            Route::get('courses',
                'ProgramManagement\NonSubstantive\MovingCourse\CourseController')
                ->name('program.non-substantive.moving_course.course');
            Route::post('save',
                'ProgramManagement\NonSubstantive\MovingCourse\SaveController')
                ->name('program.non-substantive.moving_course.save');
            Route::post('submit',
                'ProgramManagement\NonSubstantive\MovingCourse\SubmitController')
                ->name('program.non-substantive.moving_course.submit');
        });
        
        Route::group(['prefix'=>'replace-course'], function()
        {
            Route::get('',
                'ProgramManagement\NonSubstantive\ReplaceCourse\ChooseController')
                ->name('program.non-substantive.replace_course.choose');
            Route::get('replace',
                'ProgramManagement\NonSubstantive\ReplaceCourse\ReplaceController')
                ->name('program.non-substantive.replace_course.course');
            Route::post('save',
                'ProgramManagement\NonSubstantive\ReplaceCourse\SaveController')
                ->name('program.non-substantive.replace_course.save');
            Route::post('submit',
                'ProgramManagement\NonSubstantive\ReplaceCourse\SubmitController')
                ->name('program.non-substantive.replace_course.submit');
        });

        Route::group(['prefix'=>'adding-course'], function()
        {
            Route::get('',
                'ProgramManagement\NonSubstantive\AddingCourse\ChooseController')
                ->name('program.non-substantive.adding_course.choose');
            Route::get('electives',
                'ProgramManagement\NonSubstantive\AddingCourse\CourseController')
                ->name('program.non-substantive.adding_course.course');
            Route::post('save',
                'ProgramManagement\NonSubstantive\AddingCourse\SaveController')
                ->name('program.non-substantive.adding_course.save');
            Route::post('submit',
                'ProgramManagement\NonSubstantive\AddingCourse\SubmitController')
                ->name('program.non-substantive.adding_course.submit');

            // Route::get('list',
            //     'ProgramManagement\NonSubstantive\AddingCourse\ListController')
            //     ->name('program.non-substantive.adding_course.list');
        
        });

        Route::group(['prefix'=>'program-learning-outcome'], function()
        {
            Route::get('',
                'ProgramManagement\NonSubstantive\ProgramLearningOutcome\ChooseController')
                ->name('program.non-substantive.program_learning_outcome.choose');
            Route::get('outcome',
                'ProgramManagement\NonSubstantive\ProgramLearningOutcome\OutcomeController')
                ->name('program.non-substantive.program_learning_outcome.outcome');
            Route::get('main',
                'ProgramManagement\NonSubstantive\ProgramLearningOutcome\MainController')
                ->name('program.non-substantive.program_learning_outcome.main');
            Route::post('save-outcome',
                'ProgramManagement\NonSubstantive\ProgramLearningOutcome\SaveController@outcome')
                ->name('program.non-substantive.program_learning_outcome.save_outcome');
            Route::post('save-main',
                'ProgramManagement\NonSubstantive\ProgramLearningOutcome\SaveController@main')
                ->name('program.non-substantive.program_learning_outcome.save_main');
            Route::post('submit',
                'ProgramManagement\NonSubstantive\ProgramLearningOutcome\SubmitController')
                ->name('program.non-substantive.program_learning_outcome.submit');
        });
    });

    Route::group(['prefix'=>'substantive'], function()
    {
        Route::group(['prefix'=>'program-learning-outcome'], function()
        {
            Route::get('',
                'ProgramManagement\Substantive\ProgramLearningOutcome\ChooseController')
                ->name('program.substantive.program_learning_outcome.choose');
            Route::get('outcome',
                'ProgramManagement\Substantive\ProgramLearningOutcome\OutcomeController')
                ->name('program.substantive.program_learning_outcome.outcome');
            Route::post('inactivate-outcome/{code}',
                'ProgramManagement\Substantive\ProgramLearningOutcome\SaveController@inactivate')
                ->name('program.substantive.program_learning_outcome.inactivate_outcome');
            Route::post('save-outcome',
                'ProgramManagement\Substantive\ProgramLearningOutcome\SaveController@outcome')
                ->name('program.substantive.program_learning_outcome.save_outcome');
            Route::post('submit',
                'ProgramManagement\Substantive\ProgramLearningOutcome\SubmitController')
                ->name('program.substantive.program_learning_outcome.submit');
        });

        Route::group(['prefix'=>'title-academic-degree'], function()
        {
            Route::get('',
                'ProgramManagement\Substantive\TitleAcademicDegree\ChooseController')
                ->name('program.substantive.title_academic_degree.choose');
            Route::get('title',
                'ProgramManagement\Substantive\TitleAcademicDegree\MainController')
                ->name('program.substantive.title_academic_degree.main');
            Route::post('save',
                'ProgramManagement\Substantive\TitleAcademicDegree\SaveController')
                ->name('program.substantive.title_academic_degree.save');
            Route::post('update',
                'ProgramManagement\Substantive\TitleAcademicDegree\SubmitController')
                ->name('program.substantive.title_academic_degree.submit');
        });

        Route::group(['prefix'=>'program-offering-location'], function()
        {
            Route::get('',
                'ProgramManagement\Substantive\ProgramOfferingLocation\ChooseController')
                ->name('program.substantive.program_offering_location.choose');
            Route::get('campus',
                'ProgramManagement\Substantive\ProgramOfferingLocation\CampusController')
                ->name('program.substantive.program_offering_location.campus');
            Route::post('save',
                'ProgramManagement\Substantive\ProgramOfferingLocation\SaveController')
                ->name('program.substantive.program_offering_location.save');
            Route::post('update',
                'ProgramManagement\Substantive\ProgramOfferingLocation\SubmitController')
                ->name('program.substantive.program_offering_location.submit');
        });
        
        Route::group(['prefix'=>'primary-language'], function()
        {
            Route::get('',
                'ProgramManagement\Substantive\PrimaryLanguage\ChooseController')
                ->name('program.substantive.primary_language.choose');
            Route::get('language',
                'ProgramManagement\Substantive\PrimaryLanguage\MainController')
                ->name('program.substantive.primary_language.main');
            Route::post('save',
                'ProgramManagement\Substantive\PrimaryLanguage\SaveController')
                ->name('program.substantive.primary_language.save');
            Route::post('update',
                'ProgramManagement\Substantive\PrimaryLanguage\SubmitController')
                ->name('program.substantive.primary_language.submit');
        });

        Route::group(['prefix'=>'delivery-modes'], function()
        {
            Route::get('',
                'ProgramManagement\Substantive\DeliveryMode\ChooseController')
                ->name('program.substantive.delivery_modes.choose');
            Route::get('delivery',
                'ProgramManagement\Substantive\DeliveryMode\MainController')
                ->name('program.substantive.delivery_modes.main');
            Route::post('save',
                'ProgramManagement\Substantive\DeliveryMode\SaveController')
                ->name('program.substantive.delivery_modes.save');
            Route::post('update',
                'ProgramManagement\Substantive\DeliveryMode\SubmitController')
                ->name('program.substantive.delivery_modes.submit');
        });
        
        Route::group(['prefix'=>'discontinuing-degree'], function()
        {
            Route::get('',
                'ProgramManagement\Substantive\DiscontinuingDegree\ChooseController')
                ->name('program.substantive.discontinuing_degree.choose');
            Route::get('discontinuing',
                'ProgramManagement\Substantive\DiscontinuingDegree\MainController')
                ->name('program.substantive.discontinuing_degree.main');
            Route::post('save',
                'ProgramManagement\Substantive\DiscontinuingDegree\SaveController')
                ->name('program.substantive.discontinuing_degree.save');
            Route::post('update',
                'ProgramManagement\Substantive\DiscontinuingDegree\SubmitController')
                ->name('program.substantive.discontinuing_degree.submit');
        });
    });

    Route::post('minor/store', 'HomeController@minorStore')->name('minor.store');
    Route::post('minor/update/{code}', 'HomeController@minorUpdate')->name('minor.update');
    
});