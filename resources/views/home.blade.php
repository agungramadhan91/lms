@extends('layouts.app')

@section('content')
<html lang="en">
<head>
  <title>Eduval</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Styles -->
  
  <!-- BOOTSTRAP STYLES-->
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  {{-- <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" /> --}}
  <script src="{{ asset('js/app.js') }}" defer></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>


  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 1500px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, 656465 text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    /* @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;} 
    } */

    /* .btn .btn-secondary {
    background: #eee;
    border-top: 1px solid transparent;
    }

    .btn .btn-secondary:hover {
    background: #e1e1e1;
    border-top: 1px solid #d0d0d0;
    } */
  </style>
</head>
<body>

<div class="container-fluid" style="padding-top:10px;">
  <div class="row content" style="">
  <span class="border border-secondary" style="background-color: #b49b4b; margin-bottom:10px;">
    <div class="col-md-2">
      <h6 style="margin-top:10px; color:white;">Notifications</h6>
    </div>
  </span>
    <div class="col-sm-1"></div>
    <div class="col-md-9" >
        <div class="header">    
            <a href="{{ route('home') }}">
                <button type="button" class="btn" style="margin-top:-5px; width:24%; background-color: white; color:gray; box-shadow: 0 3px red; border:1px solid gray;">
                <i class="fas fa-tasks"></i>
                Program Management
                </button>
            </a>
            <a href="{{ route('home.course') }}" style ="color:white;">
                <button type="button" class="btn" style="margin-top:-5px; width:24%; background-color: #b49b4b; color:white;">
                <i class="fas fa-university"></i>
                Course Management
                </button>
            </a>
            <a href="{{ route('home.minor') }}">
            <button type="button" class="btn" style="margin-top:-5px; width:25%; background-color: #b49b4b; color:white;">
                <i class="fas fa-table"></i>
                Minor Management
            </button>
            </a>
            <a href="{{ route('home.catalogue') }}">
            <button type="button" class="btn" style="margin-top:-5px; width:25%; background-color: #b49b4b; color:white;">
            <i class="fas fa-table"></i>
            Catalogue Management
            </button>
            </a>
        </div>
        <div class="container mt-3">
        
        @foreach (['Error', 'warning', 'Confirmation', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <!-- <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p> -->
                    <div class="modal" id="popup1" style="display:block;">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header" style="background: rgb(215, 227, 191)">
                                <h4 style="font-weight:bold;">{{ $msg }}</h4>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body">
                                <p style="font-size:17px;">{{ Session::get('alert-' . $msg) }}</p>
                            </div>  
                            <!-- Modal footer -->
                            <div class="modal-footer">
                            <!-- <a href="{{url('/')}}">
                                <button type="button" class="btn btn-danger">OK</button>
                            </a> -->
                                <button type="button" id="clickmodal" style="width:190px; color:white; border:3px solid #D3D3D3;" class="btn btn-primary" data-dismiss="modal">Ok</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            <br>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#menu1">Program Non-Substantive Changes</a>
                </li>
                <li class="nav-item" style="margin-left:5px;">
                <a class="nav-link" data-toggle="tab" href="#menu2">Program Substantive Changes</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div id="menu1" class="container tab-pane fade"><br>
                <h4>Program Non-Substantive Changes</h4>
                    <br>
                    <div class="row shade">
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465 ">Moving course(s) between compulsory and elective status without changing the number of credit hours</p>
                        </div>
                        <div class="col-md-1">
                        <a href="#">
                            <a href="{{ route('program.non-substantive.moving_course.choose') }}" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">
                                Choose
                            </a>
                            {{-- <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button> --}}
                        </a>
                        </div>
                    </div>
                    <div class="row shade">
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Replacement of an existing course with another</p>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('program.non-substantive.replace_course.choose') }}" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">
                                Choose
                            </a>
                            {{-- <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button> --}}
                        </div>
                    </div>
                    <div class="row shade">
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Adding an existing course to the electives list</p>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('program.non-substantive.adding_course.choose') }}" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">
                                Choose
                            </a>
                            {{-- <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button> --}}
                        </div>
                    </div>
                    <div class="row shade">
                    <div class="col-md-11 sidenav">
                            <p style="color:#656465">Minor changes to program learning outcomes</p>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('program.non-substantive.program_learning_outcome.choose') }}" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">
                                Choose
                            </a>
                        </div>
                    </div>
                </div>
                <div id="menu2" class="container tab-pane fade"><br>
                <h4>Program Substantive Changes</h4>
                    <br>
                    <div class="row shade" >
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Establishing a new degree program</p>
                        </div>
                        <div class="col-md-1">
                        <a href="{{ route('program') }}">
                            <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                        </a>
                        </div>
                    </div>

                    <div class="row shade">
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Replacement of more than three courses</p>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('course.replacement.choose') }}">
                                <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                            </a>
                        </div>
                    </div>

                    <div class="row shade" >
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Discontinuing an existing degree program</p>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('program.substantive.discontinuing_degree.choose') }}" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">
                                Choose
                            </a>
                            {{-- <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button> --}}
                        </div>
                    </div>

                    <div class="row shade">
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Changing details of a degree program</p>
                        </div>
                        <div class="col-md-1">
                            <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                        </div>
                    </div>

                    <div class="row shade" >
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Changing the title of an academic degree</p>
                        </div>
                        <div class="col-md-1">
                            {{-- <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button> --}}
                            <a href="{{ route('program.substantive.title_academic_degree.choose') }}" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">
                                Choose
                            </a>
                        </div>
                    </div>

                    <div class="row shade">
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Major changes to program learning outcomes</p>
                        </div>
                        <div class="col-md-1">
                            {{-- <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button> --}}
                            <a href="{{ route('program.substantive.program_learning_outcome.choose') }}" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">
                                Choose
                            </a>
                        </div>
                    </div>

                    <div class="row shade" >
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Change to a program's total number of credit hours or the relative distribution of credit hours between compulsory and elective courses;</p>
                        </div>
                        <div class="col-md-1">
                            <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:18px; color:white;">Choose</button>
                        </div>
                    </div>

                    <div class="row shade">
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Adding a new concenration</p>
                        </div>
                        <div class="col-md-1">
                        <a href="{{ url('/program/concentration/choose') }}">
                            <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                        </a>
                        </div>
                    </div>

                    <div class="row shade" >
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Changing concentration hours or course structure</p>
                        </div>
                        <div class="col-md-1">
                        <a href="{{ url('/program/concentration/changing/choose') }}">
                            <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                        </a>
                        </div>
                    </div>

                    <div class="row shade">
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Discontinuing an existing concentration</p>
                        </div>
                        <div class="col-md-1">
                        <a href="{{ url('/program/concentration/discontinuing/choose') }}">
                            <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                        </a>
                        </div>
                    </div>

                    <div class="row shade" >
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Change to the primary language of instruction in a degree program</p>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('program.substantive.primary_language.choose') }}" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">
                                Choose
                            </a>
                            {{-- <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button> --}}
                        </div>
                    </div>

                    <div class="row shade">
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Changing program instructioanl delivery mode</p>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('program.substantive.delivery_modes.choose') }}" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">
                                Choose
                            </a>
                            {{-- <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button> --}}
                        </div>
                    </div>

                    <div class="row shade" >
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Change or addition of program offering location</p>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('program.substantive.program_offering_location.choose') }}" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">
                                Choose
                            </a>
                            {{-- <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button> --}}
                        </div>
                    </div>

                    <div class="row shade">
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Change program admission requirements</p>
                        </div>
                        <div class="col-md-1">
                            <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

<div class="modal" id="notif-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header {{session()->has('success') ? 'bg-success':'bg-danger'}}" id="title-modal">
                
                @if (session()->has('success'))
                    Success
                @else
                    Error
                @endif
            </div>
            
            <div class="modal-body" id="content-modal">
                @if (session()->has('success'))
                    <p>{{ session()->get('success') }}</p>
                @else
                    <p>{{ session()->get('error') }}</p>
                @endif
            </div>  
            
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width:80px; border:3px solid #D3D3D3;" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div><div class="modal" id="notif-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header {{session()->has('success') ? 'bg-success':'bg-danger'}}" id="title-modal">
                
                @if (session()->has('success'))
                    Success
                @else
                    Error
                @endif
            </div>
            
            <div class="modal-body" id="content-modal">
                @if (session()->has('success'))
                    <p>{{ session()->get('success') }}</p>
                @else
                    <p>{{ session()->get('error') }}</p>
                @endif
            </div>  
            
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    $(document).ready(function () {
        @if(session()->has('success'))
            $('#notif-modal').modal('show');
        @endif

        @if(session()->has('error'))
            $('#notif-modal').modal('show');
        @endif
    })
</script>

</body>
</html>
@endsection
