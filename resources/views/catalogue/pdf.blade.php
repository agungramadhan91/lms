<!DOCTYPE html>
<html>
<head>
	<title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
        <h4 class="h6header">Curriculum and Catalogue Management System (CCMS)</h4>
	</center>
    <hr style="width:50%;"><br>
    
    @if($all == 1)
        @foreach($college as $colleges)
        <div class="col-md-3">
            <div class="form-group">
                <h5>College Code</h5>
                <label for="">{{$colleges->college_code}}</label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <h5>College Title</h5>
                <label for="">{{$colleges->title}}</label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <h5>College Mission</h5>
                <label for="">{{$colleges->college_mission}}</label>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <h5>Effective Term</h5>
                @foreach($effterm as $effterms)
                @if($effterms->term_code == $colleges->effective_term_code)
                <label for="">{{$effterms->term_description}}</label>
                @endif
                @endforeach
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <h5>End Term</h5>
                @foreach($endterm as $endterms)
                @if($endterms->term_code == $colleges->end_term_code)
                <label for="">{{$endterms->term_description}}</label>
                @endif
                @endforeach
            </div>
        </div>
        <hr>
        @foreach($program as $data)
        @if($data->college_code == $colleges->college_code)
        <div class="col-md-3">
            <div class="form-group">
                <h5>Program Code</h5>
                <label for="">{{$data->program_code}}</label>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <h5>Program Title</h5>
                <label for="">{{$data->title}}</label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <h5>Program Mission</h5>
                <label for="">{{$data->program_mission}}</label>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <h5>Program Hour</h5>
                <label for="">{{$data->required_hours}}</label>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <h5>Campus</h5>
                @foreach($campus as $campuss)
                @if($campuss->campus_code == $data->campus_code)
                <label for="">{{$campuss->campus_description}}</label>
                @endif
                @endforeach
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <h5>Degree Type</h5>
                @foreach($type as $types)
                @if($types->degree_type_code == $data->degree_type_code)
                <label for="">{{$types->title}}</label>
                @endif
                @endforeach
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <h5>Degree Level</h5>
                @foreach($level as $levels)
                @if($levels->degree_level_code == $data->degree_level_code)
                <label for="">{{$levels->title}}</label>
                @endif
                @endforeach
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <h5>Program Admission Requirements</h5>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Program Admission Requirements Code</th>
                        <th>Type</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($par as $par2)
                    @if($par2->program_code == $data->program_code)
                        <tr>
                            <td>{{$par2->par_code}}</td>
                            <td>{{$par2->par_type}}</td>
                            <td>{{$par2->par_description}}</td>
                        </tr>
                    @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <h5>Program Graduation Requirements</h5>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Program Graduation Requirements Code</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($grad as $grad2)
                    @if($grad2->program_code == $data->program_code)
                        <tr>
                            <td>{{$grad2->graduation_code}}</td>
                            <td>{{$grad2->description}}</td>
                        </tr>
                    @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <h5>Course</h5>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Semester</th>
                        <th>Course Code</th>
                        <th>Description</th>
                        <th>Course Type</th>
                        <th>Credit Hour</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($course as $data2)
                    @if($data2->program_code_01 == $data->program_code)
                        <tr>
                            <td>{{$data2->semester_program_01}}</td>
                            <td>{{$data2->course_code}}</td>
                            <td>{{$data2->long_title}}</td>
                            <td>{{$data2->course_type_code_01}}</td>
                            <td>{{$data2->credit_hours}}</td>
                        </tr>
                    @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <br>
        @endif
        @endforeach
        <hr style="height:3px;border-width:0;color:gray;background-color:gray">
        @endforeach
    @elseif($all == 0)
        <div class="col-md-3">
            <div class="form-group">
                <h5>College Code</h5>
                <label for="">{{$college->college_code}}</label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <h5>College Title</h5>
                <label for="">{{$college->title}}</label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <h5>College Mission</h5>
                <label for="">{{$college->college_mission}}</label>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <h5>Effective Term</h5>
                <label for="">{{$effterm->term_description}}</label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <h5>End Term</h5>
                <label for="">{{$endterm->term_description}}</label>
            </div>
        </div>
        <hr>
        @foreach($program as $data)
        <div class="col-md-3">
            <div class="form-group">
                <h5>Program Code</h5>
                <label for="">{{$data->program_code}}</label>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <h5>Program Title</h5>
                <label for="">{{$data->title}}</label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <h5>Program Mission</h5>
                <label for="">{{$data->program_mission}}</label>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <h5>Program Hour</h5>
                <label for="">{{$data->required_hours}}</label>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <h5>Campus</h5>
                @foreach($campus as $campuss)
                @if($campuss->campus_code == $data->campus_code)
                <label for="">{{$campuss->campus_description}}</label>
                @endif
                @endforeach
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <h5>Degree Type</h5>
                @foreach($type as $types)
                @if($types->degree_type_code == $data->degree_type_code)
                <label for="">{{$types->title}}</label>
                @endif
                @endforeach
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <h5>Degree Level</h5>
                @foreach($level as $levels)
                @if($levels->degree_level_code == $data->degree_level_code)
                <label for="">{{$levels->title}}</label>
                @endif
                @endforeach
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <h5>Program Admission Requirements</h5>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Program Admission Requirements Code</th>
                        <th>Type</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($par as $par2)
                    @if($par2->program_code == $data->program_code)
                        <tr>
                            <td>{{$par2->par_code}}</td>
                            <td>{{$par2->par_type}}</td>
                            <td>{{$par2->par_description}}</td>
                        </tr>
                    @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <h5>Program Graduation Requirements</h5>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Program Graduation Requirements Code</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($grad as $grad2)
                    @if($grad2->program_code == $data->program_code)
                        <tr>
                            <td>{{$grad2->graduation_code}}</td>
                            <td>{{$grad2->description}}</td>
                        </tr>
                    @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <h5>Course</h5>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Semester</th>
                        <th>Course Code</th>
                        <th>Description</th>
                        <th>Course Type</th>
                        <th>Credit Hour</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($course as $data2)
                    @if($data2->program_code_01 == $data->program_code)
                        <tr>
                            <td>{{$data2->semester_program_01}}</td>
                            <td>{{$data2->course_code}}</td>
                            <td>{{$data2->long_title}}</td>
                            <td>{{$data2->course_type_code_01}}</td>
                            <td>{{$data2->credit_hours}}</td>
                        </tr>
                    @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <hr>
        @endforeach
    @endif
</body>
</html>