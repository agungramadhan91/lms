<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}

</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                            <div class="row">
                            <div class="col-md-8">
                            <a href="{{url('/home/course')}}">
                                <h5 style="text-align:center; margin-top:20px; color:#a99451;"></h5>
                                </a>
                            </div>
                            </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Catalogue Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <!-- <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="{{url('course/level')}}"  class="active">Course Main Attributes</a>
                    </li>
                    <li>
                        <a href="{{url('course/hour/description')}}" class="inactive">Course Hours</a>
                    </li>
                    <li>
                        <a href="{{url('course/description/requisite')}}" class="inactive">Course Requisite and Equivalencies</a>
                    </li>
                    <li>
                        <a href="{{url('course/association/description')}}" class="inactive">Program Assosiation</a>
                    </li>
                    <li>
                        <a href="{{url('course/description/outcome')}}" class="inactive">Course Learning Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('course/description/assessment')}}" class="inactive">Course Content and Assessments</a>
                    </li>
                    <li>
                        <a href="{{url('course/description/textbook')}}" class="inactive">Assigned Textbook and Other Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/description/research')}}" class="inactive">Optional Reading Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/description/grading')}}" class="inactive">Grading Schema</a>
                    </li>
                    <li>
                        <a href="{{url('course/description/policies')}}" class="inactive">Course Policies</a>
                    </li>
                    <li>
                        <a href="{{url('course/description/special')}}" class="inactive">Course Special Requirements</a>
                    </li>
                    <li>
                        <a href="{{url('course/description/pedagogy')}}" class="inactive">Pedagogy</a>
                    </li>
                    <li>
                        <a href="#" class="inactive">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav> -->
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div class="flash-message">
            @foreach (['Error', 'warning', 'Confirmation', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <!-- <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p> -->
                    <div class="modal" id="popup1" style="display:block;">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header {{ $msg }}">
                                <h4 style="font-weight:bold;">{{ $msg }}</h4>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body">
                                <p style="font-size:17px;">{{ Session::get('alert-' . $msg) }}</p>
                            </div>  
                            <!-- Modal footer -->
                            <div class="modal-footer">
                            <!-- <a href="{{url('/')}}">
                                <button type="button" class="btn btn-danger">OK</button>
                            </a> -->
                                <button type="button" id="clickmodal" style="width:190px; color:white; border:3px solid #D3D3D3;" class="btn btn-primary" data-dismiss="modal">Ok</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
            <div id="page-inner">
            <form action="" method="post">
            {{ csrf_field() }}
            <br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                        <a href="{{url('/home/catalogue')}}">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Back</button>
                        </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                        <a href="{{url('/catalogue/word')}}">
                            <button type="button" class="btn btn-primary" style="width:210px; color:white; border:3px solid #D3D3D3;">Convert to Word Document</button>
                        </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                        <a href="{{url('/catalogue/pdf')}}">
                            <button type="button" class="btn btn-primary" style="width:190px; color:white; border:3px solid #D3D3D3;">Convert to PDF</button>
                        </a>
                        </div>
                    </div>
                </div>
                @if($all == 1)
                    @foreach($college as $colleges)
                    <div class="row content">
                        <div class="col-md-3">
                            <div class="form-group">
                                <h4>College Code</h4>
                                <label for="">{{$colleges->college_code}}</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h4>College Title</h4>
                                <label for="">{{$colleges->title}}</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h4>College Mission</h4>
                                <label for="">{{$colleges->college_mission}}</label>
                            </div>
                        </div>
                    </div>
                    <div class="row content">
                        <div class="col-md-3">
                            <div class="form-group">
                                <h4>Effective Term</h4>
                                @foreach($effterm as $effterms)
                                @if($effterms->term_code == $colleges->effective_term_code)
                                <label for="">{{$effterms->term_description}}</label>
                                @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h4>End Term</h4>
                                @foreach($endterm as $endterms)
                                @if($endterms->term_code == $colleges->end_term_code)
                                <label for="">{{$endterms->term_description}}</label>
                                @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <hr>
                    @foreach($program as $data)
                    @if($data->college_code == $colleges->college_code)
                    <div class="row content">
                        <div class="col-md-3">
                            <div class="form-group">
                                <h4>Program Code</h4>
                                <label for="">{{$data->program_code}}</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h4>Program Title</h4>
                                <label for="">{{$data->title}}</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h4>Program Mission</h4>
                                <label for="">{{$data->program_mission}}</label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <h4>Program Hour</h4>
                                <label for="">{{$data->required_hours}}</label>
                            </div>
                        </div>
                    </div>
                    <div class="row content">
                        <div class="col-md-3">
                            <div class="form-group">
                                <h4>Campus</h4>
                                @foreach($campus as $campuss)
                                @if($campuss->campus_code == $data->campus_code)
                                <label for="">{{$campuss->campus_description}}</label>
                                @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h4>Degree Type</h4>
                                @foreach($type as $types)
                                @if($types->degree_type_code == $data->degree_type_code)
                                <label for="">{{$types->title}}</label>
                                @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h4>Degree Level</h4>
                                @foreach($level as $levels)
                                @if($levels->degree_level_code == $data->degree_level_code)
                                <label for="">{{$levels->title}}</label>
                                @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="row content">
                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>Program Admission Requirements</h4>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Program Admission Requirements Code</th>
                                        <th>Type</th>
                                        <th>Description</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($par as $par2)
                                    @if($par2->program_code == $data->program_code)
                                        <tr>
                                            <td>{{$par2->par_code}}</td>
                                            <td>{{$par2->par_type}}</td>
                                            <td>{{$par2->par_description}}</td>
                                        </tr>
                                    @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>Program Graduation Requirements</h4>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Program Graduation Requirements Code</th>
                                        <th>Description</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($grad as $grad2)
                                    @if($grad2->program_code == $data->program_code)
                                        <tr>
                                            <td>{{$grad2->graduation_code}}</td>
                                            <td>{{$grad2->description}}</td>
                                        </tr>
                                    @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row content">
                        <div class="col-md-12">
                            <div class="form-group">
                                <h4>Course</h4>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Semester</th>
                                        <th>Course Code</th>
                                        <th>Description</th>
                                        <th>Course Type</th>
                                        <th>Credit Hour</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($course as $data2)
                                    @if($data2->program_code_01 == $data->program_code)
                                        <tr>
                                            <td>{{$data2->semester_program_01}}</td>
                                            <td>{{$data2->course_code}}</td>
                                            <td>{{$data2->long_title}}</td>
                                            <td>{{$data2->course_type_code_01}}</td>
                                            <td>{{$data2->credit_hours}}</td>
                                        </tr>
                                    @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <br>
                    @endif
                    @endforeach
                    <hr style="height:3px;border-width:0;color:gray;background-color:gray">
                    @endforeach
                @elseif($all == 0)
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <h4>College Code</h4>
                            <label for="">{{$college->college_code}}</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <h4>College Title</h4>
                            <label for="">{{$college->title}}</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <h4>College Mission</h4>
                            <label for="">{{$college->college_mission}}</label>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <h4>Effective Term</h4>
                            <label for="">{{$effterm->term_description}}</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <h4>End Term</h4>
                            <label for="">{{$endterm->term_description}}</label>
                        </div>
                    </div>
                </div>
                <hr>
                @foreach($program as $data)
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <h4>Program Code</h4>
                            <label for="">{{$data->program_code}}</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <h4>Program Title</h4>
                            <label for="">{{$data->title}}</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <h4>Program Mission</h4>
                            <label for="">{{$data->program_mission}}</label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <h4>Program Hour</h4>
                            <label for="">{{$data->required_hours}}</label>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <h4>Campus</h4>
                            @foreach($campus as $campuss)
                            @if($campuss->campus_code == $data->campus_code)
                            <label for="">{{$campuss->campus_description}}</label>
                            @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <h4>Degree Type</h4>
                            @foreach($type as $types)
                            @if($types->degree_type_code == $data->degree_type_code)
                            <label for="">{{$types->title}}</label>
                            @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <h4>Degree Level</h4>
                            @foreach($level as $levels)
                            @if($levels->degree_level_code == $data->degree_level_code)
                            <label for="">{{$levels->title}}</label>
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-6">
                        <div class="form-group">
                            <h4>Program Admission Requirements</h4>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Program Admission Requirements Code</th>
                                    <th>Type</th>
                                    <th>Description</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($par as $par2)
                                @if($par2->program_code == $data->program_code)
                                    <tr>
                                        <td>{{$par2->par_code}}</td>
                                        <td>{{$par2->par_type}}</td>
                                        <td>{{$par2->par_description}}</td>
                                    </tr>
                                @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <h4>Program Graduation Requirements</h4>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Program Graduation Requirements Code</th>
                                    <th>Description</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($grad as $grad2)
                                @if($grad2->program_code == $data->program_code)
                                    <tr>
                                        <td>{{$grad2->graduation_code}}</td>
                                        <td>{{$grad2->description}}</td>
                                    </tr>
                                @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-12">
                        <div class="form-group">
                            <h4>Course</h4>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Semester</th>
                                    <th>Course Code</th>
                                    <th>Description</th>
                                    <th>Course Type</th>
                                    <th>Credit Hour</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($course as $data2)
                                @if($data2->program_code_01 == $data->program_code)
                                    <tr>
                                        <td>{{$data2->semester_program_01}}</td>
                                        <td>{{$data2->course_code}}</td>
                                        <td>{{$data2->long_title}}</td>
                                        <td>{{$data2->course_type_code_01}}</td>
                                        <td>{{$data2->credit_hours}}</td>
                                    </tr>
                                @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <hr>
                @endforeach
                @endif
            </form>
            <!-- /. PAGE INNER  -->
            </div>
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</body>
</html>
<script>
</script>