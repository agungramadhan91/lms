<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>
    
.requiredStar{
    color:red;
}

</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="assets/img/eduval-logo.png" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:192px;">
                            <div class="row">
                                <div class="col-md-8">
                                    <h5 style="text-align:center; margin-top:20px; color:#a99451;">Add New Program</h5>
                                </div>
                                <div class="col-md-4">
                                    <form action="{{ route('program.all') }}" method="post">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn-small" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3; margin-top:15px; margin-left:-30px" data-toggle="modal" data-target="#myModal2">Submit The Application</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Program Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        @if($countmajor ="0")
                            <a href="#" class="active">Main</a>
                        @else
                            <a href="#" class="done" style="border: 4px solid #0b4176;">Main</a>
                        @endif
                    </li>
                    <li>
                        @if($outcome->plo_code = "")
                            <a href="{{url('program/outcome')}}">Outcomes</a>
                        @else
                            <a href="{{url('program/outcome')}}" class="done">Outcomes</a>
                        @endif
                    </li>
                    <li>
                        <a href="{{url('program/structure')}}" class="done">Structure</a>
                    </li>
                    <li>
                    @if($assesment->pa_code = "")
                        <a href="{{url('program/assesment')}}">Assesment Methods</a>
                    @else
                        <a href="{{url('program/assesment')}}" class="done">Assesment Methods</a>
                    @endif
                    </li>
                    <li>
                    @if($faculty->pfs_code == "")
                        <a href="{{url('program/faculty')}}">Faculty & Professional Staff</a>
                    @else
                        <a href="{{url('program/faculty')}}" class="done">Faculty & Professional Staff</a>
                    @endif
                    </li>
                    <li>
                    @if($fitness->pfa_code = "")
                        <a href="{{url('program/fitness')}}">Fitness & Alignment</a>
                    @else
                        <a href="{{url('program/fitness')}}" class="done">Fitness & Alignment</a>
                    @endif
                    </li>
                    <li>
                        <a href="{{url('program/student')}}" class="done">Students</a>
                    </li>
                    <li>
                        <a href="{{url('program/research')}}" class="done">Researach & Scholarly Activities</a>
                    </li>
                    <li>
                        <a href="{{url('program/resource')}}" class="done">Resources</a>
                    </li>
                    <li>
                        <a href="{{url('program/supportive')}}" class="done">Supportive Data</a>
                    </li>
                    <li>
                        <a href="{{url('program/cost')}}" class="done">Cost & Revenue</a>
                    </li>
                    <li>
                        <a href="{{url('program/community')}}" class="done">Comunity Engagement</a>
                    </li>
                    <li>
                        <a href="{{url('program/public')}}" class="done">Public Diclosure & Intergrity</a>
                    </li>
                    <li>
                        <a href="#">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="{{ route('program.store') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row content">
                    <div class="col-md-3" style="display:flex;">
                    <div class="form-group">
                        <label for="">Campus</label>
                        <div class="" style="display:flex;">
                        <select class="form-control" name="campus" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; float:left" required>
                        @if($countprogram == "0")
                            <option value="" selected disabled> Campus</option>
                        @else
                            <option value="{{$program->campus_code}}" selected>{{$campus2->campus_code}} {{$campus2->campus_description}}</option>
                        @endif
                            @foreach($campus as $data)
                            <option value="{{$data->campus_code}}">{{$data->campus_code}} {{$data->campus_description}}</option>
                            @endforeach
                        </select>
                        <p style="color:white;">></p>
                        <span class="requiredStar">*</span>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                    <div class="form-group">
                            <label for="">Program Status</label>
                            <input type="text" class="form-control" style="border:2px solid #6fa7d9; color:#6fa7d9; width:230px; background-color:#fffbf1;" value="{{$program->status}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-1"></div >
                    <div class="col-md-3" style="display:flex;">
                    <div class="form-group">
                        <label for="">College</label>
                        <div class="" style="display:flex;">
                        <select class="form-control" name="college" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;" required>
                        @if($countprogram == "0")
                            <option value="" selected disabled>College</option>
                        @else
                            <option value="{{$program->college_code}}" selected>{{$college2->title}}</option>
                        @endif
                            @foreach($college as $data)
                            <option value="{{$data->college_code}}">{{$data->title}}</option>
                            @endforeach
                        </select>
                        <p style="color:white;">></p>
                        <span class="requiredStar">*</span>
                        </div>
                    </div>
                    </div>
                </div>        
                <br>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                    <div class="form-group">
                        <label for="">Degree Type</label>
                        <div class="" style="display:flex;">
                        <select class="form-control" name="degree_type" id="type" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;" required>
                        @if($countprogram == "0")
                            <option value="" selected disabled>Degree Type</option>
                        @else
                            <option value="{{$program->degree_type_code}}" selected>{{$type2->title}}</option>
                        @endif
                            @foreach($type as $data)
                            <option value="{{$data->degree_type_code}}">{{$data->title}}</option>
                            @endforeach
                        </select>
                        <p style="color:white;">></p>
                        <span class="requiredStar">*</span>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                    <div class="form-group">
                        <label for="">Degree Level</label>
                        <div class="" style="display:flex;">
                        <select class="form-control" name="degree_level" id="level" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;" required>
                        @if($countprogram == "0")
                            <option value="" selected disabled>Degree Level</option>
                        @else
                            <option value="{{$program->degree_level_code}}" selected>{{$level2->title}}</option>
                        @endif
                            <!-- @foreach($level as $data)
                            <option value="{{$data->degree_level_code}}">{{$data->title}}</option>
                            @endforeach -->
                        </select>
                        <p style="color:white;">></p>
                        <span class="requiredStar">*</span>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                    <div class="form-group">
                        <label for="">Department</label>
                        <div class="" style="display:flex;">
                        <select class="form-control" name="department" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;">
                            <option value="" selected disabled>Department</option>
                            @foreach($department as $data)
                            <option value="{{$data->department_code}}">{{$data->title}}</option>
                            @endforeach
                        </select>
                        <p style="color:white;">></p>
                        <span class="requiredStar" style="color:white;">*</span>
                        </div>
                    </div>
                    </div>
                </div>
                <br>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                    <div class="form-group">
                        <label for="">Effective Term</label>
                        <div class="" style="display:flex;">
                        <select class="form-control" name="effective_term" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;" required>
                        @if($countprogram == "0")    
                            <option value="" selected disabled>Effective Term</option>
                        @else
                            <option value="{{$program->effective_term_code}}" selected>{{$effterm->term_description}}</option>
                        @endif
                            @foreach($term as $data)
                            <option value="{{$data->term_code}}">{{$data->term_description}}</option>
                            @endforeach
                        </select>
                        <p style="color:white;">></p>
                        <span class="requiredStar">*</span>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                    <div class="form-group">
                        <label for="">End Term</label>
                        <div class="" style="display:flex;">
                        <select class="form-control" name="end_term" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;" required>
                        @if($countprogram == "0")    
                            <option value="" selected disabled>End Term</option>
                        @else
                            <option value="{{$program->end_term_code}}" selected>{{$endterm->term_description}}</option>
                        @endif
                            @foreach($term as $data)
                            <option value="{{$data->term_code}}">{{$data->term_description}}</option>
                            @endforeach
                        </select>
                        <p style="color:white;">></p>
                        <span class="requiredStar">*</span>
                        </div>
                    </div>
                    </div>
                </div>
                <br>
                <div class="row content" style="margin-bottom:10px;">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Code</label>
                            <input type="text" class="form-control" placeholder="Program Code" name="code" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" value="{{$program->program_code}}" required>
                        </div>
                        <p style="color:white;">></p>
                        <span class="requiredStar">*</span>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Title</label>
                            <input type="text" class="form-control" placeholder="Program Title" name="title" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" value="{{$program->title}}" required>
                        </div>
                        <p style="color:white;">></p>
                        <span class="requiredStar">*</span>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Short Title</label>
                            <input type="text" class="form-control" placeholder="Short Title" name="short_title" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" value="{{$program->title}}" required>
                        </div>
                        <p style="color:white;">></p>
                        <span class="requiredStar">*</span>
                    </div>
                </div>
                <div id="major">
                    <div class="row content">
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                            <label for="">Major Code</label>
                            @if($countmajor != "0")
                                <input type="text" class="form-control" placeholder="Major Code" name="major_Code" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" required>
                            @else
                                <input type="text" value="{{$major->major_code}}" class="form-control" placeholder="Major Code" name="major_title" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" required>
                            @endif
                            </div>
                            <p style="color:white;">></p>
                            <span class="requiredStar">*</span>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                            <label for="">Major Title</label>
                            @if($countmajor != "0")
                                <input type="text" class="form-control" placeholder="Major Title" name="major_title" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" required>
                            @else
                                <input type="text" value="{{$major_name->title}}" class="form-control" placeholder="Major Title" name="major_title" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" required>
                            @endif
                            </div>
                            <p style="color:white;">></p>
                            <span class="requiredStar">*</span>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Joint Degree</label>
                            <div style="display:flex">
                            <select class="form-control" name="degree" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%;">
                                <option value="" selected disabled>Joint Degree</option>
                                <option value="yes">Yes</option>
                                <option value="no" selected>No</option>
                            </select>
                            <p style="color:white;">></p>
                            <span class="requiredStar">*</span>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" id="addMajor" class="bttn" style="background-color: #b1cfeb; margin-top:10px;width:240px;">Insert More Majors  <i class="fas fa-plus"></i></button>
                        </div>
                    </div>
                </div>
                <br>
                <div id="concentration">
                    <div class="row content">
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Concetntration code" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" name="concentration1">
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Concetntration title" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" name="concentration1">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" id="addConcentration" class="bttn" style="background-color: #b1cfeb; margin-top:10px;width:240px;">Insert More Concentrations  <i class="fas fa-plus"></i></button>
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <div class="row content">
                    @if($countprogram = "0")
                        <div class="form-group">
                            <button type="submit" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Save and Exit</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Save and Continue</button>
                        </div>
                    </div>
                    @else
                    <div class="col-md-3">
                    <div class="form-group">
                            <button type="submit" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Update and Exit</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Update and Continue</button>
                        </div>
                    </div>
                    @endif
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="modal" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="">
                            <h2>Exit</h2>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p>Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <form>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>


</body>
</html>
<script>
$(document).ready(function () {
    $("#type").change(function () {
        var val = $(this).val();
        if (val == "BA") {
            $("#level").html("<option value='UG' selected>Undergraduate</option>");
        } else if (val == "BS") {
            $("#level").html("<option value='UG' selected>Undergraduate</option>");
        } else if (val == "MA") {
            $("#level").html("<option value='GR' selected>Graduate</option>");
        } else if (val == "MS") {
            $("#level").html("<option value='GR' selected>Graduate</option>");
        } else if (val == "") {
            $("#level").html("<option value='' selected>Degree Level</option>");
        }
    });

    var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $("#major"); //Fields wrapper
	var add_button      = $("#addMajor"); //Add button ID
	
	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append(
                '<div class="row content"><div class="col-md-3"><div class="form-group"><input type="text" class="form-control" placeholder="Major code" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" name="major[]"></div></div><div class="col-md-1"></div><div class="col-md-3"><div class="form-group"><input type="text" class="form-control" placeholder="Major title" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" name="major_title[]"></div></div><div class="col-md-1"></div><a href="#" class="remove_field">Remove</a></div>'
                ); //add input box
		}
	});
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); x--;
	})

    
    var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $("#concentration"); //Fields wrapper
	var add_button      = $("#addConcentration"); //Add button ID
	
	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append(
                '<div class="row content"><div class="col-md-3"><div class="form-group"><input type="text" class="form-control" placeholder="Concentration code" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" name="concentration[]"></div></div><div class="col-md-1"></div><div class="col-md-3"><div class="form-group"><input type="text" class="form-control" placeholder="Concentration title" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" name="concentration_title[]"></div></div><div class="col-md-1"></div><a href="#" class="remove_field">Remove</a></div>'
                ); //add input box
		}
	});
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); x--;
	})
});
</script>