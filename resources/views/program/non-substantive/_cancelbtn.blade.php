<div class="modal fade" id="exit-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color : rgb(255, 242, 204)">
                <h4 style="font-weight:bold;">Warning !</h4>
            </div>
            
            <div class="modal-body">
                <p style="font-size:17px;">Your data will be lost if you exit</p>
            </div>  
            
            <div class="modal-footer">
                <a href="{{url('/')}}">
                    <button type="button" class="btn btn-primary" style="width:80px; border:3px solid #D3D3D3;">OK</button>
                </a>
                <button type="button" class="btn" style="width:80px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-dismiss="modal">Cancel</button>
            </div> 
        </div>
    </div>
</div>