@extends('program.non-substantive.sublayout')

@section('content')
<div class="row">
    <div class="col-md-11">
        <div class="content">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Program Hour</label>
                    <input  type="number" 
                            max="999" 
                            class="form-control" 
                            name="program_hour"
                            placeholder="Program Hour"
                            disabled 
                            style="border:2px solid #6fa7d9; color:#6fa7d9;">
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-11">
        <div class="content">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Program Delivery Mode (Multiple Values Selection)</label>
                    <div class="multiselect2">
                        <div class="selectBox2" onclick="showCheckboxes2()">
                            <select class="form-control" 
                                    disabled
                                    style="border:2px solid #6fa7d9; color:#6fa7d9;">
                                <option value="" selected></option>
                            </select>
                            <div class="overSelect"></div>
                        </div>
                        <div id="checkboxes2" style="width:700px; display:none;">
                            @foreach($deliveries as $data)
                                <label for="one">
                                <input  type="checkbox" 
                                        id="one" 
                                        name="delivery" 
                                        value="{{$data->delivery_mode_code}}"/>
                                        {{$data->delivery_mode_code}} - {{$data->description}}
                                </label>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Program Language of Instruction(Multiple Values Selection)</label>
                    <div class="multiselect">
                        <div class="selectBox" onclick="showCheckboxes()">
                            <select class="form-control" 
                                    disabled
                                    style="border:2px solid #6fa7d9; color:#6fa7d9;">
                                <option></option>
                            </select>
                            <div class="overSelect"></div>
                        </div>
                        <div id="checkboxes" style="width:700px; display:none;">
                            @foreach($languages as $data)
                                <label for="one">
                                <input type="checkbox" id="one" name="language" value="{{$data->instructional_languages_code}}"/>{{$data->instructional_languages_code}} - {{$data->description}}</label>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Program Admission Requirements</label>
                    <input  type="text" 
                            class="form-control" 
                            name="admission"
                            disabled
                            placeholder="Program Admission Requirements" 
                            style="border:2px solid #6fa7d9; color:#6fa7d9;">
                </div>
            </div>
        </div>
        <div class="content">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Program Graduation Requiremenst</label>
                    <input  type="text" 
                            class="form-control" 
                            name="graduation"
                            placeholder="Program Graduation Requiremenst" 
                            disabled
                            style="border:2px solid #6fa7d9; color:#6fa7d9;">
                </div>
            </div>
        </div>
    </div>
</div><hr>

<div class="row">
    <div class="col-md-12">
        @if($count_studies == 0)
            <div class="content">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="">Semester</label>
                        <select class="form-control" 
                                name="semester" 
                                disabled
                                style="border:2px solid #6fa7d9; color:#6fa7d9;">
                            @foreach($semesters as $data)
                                <option value="{{$data->semester_code}}">{{$data->semester_code}} - {{$data->semester_description}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                    <label for="">Course</label>
                        <select class="form-control" 
                                name="course" 
                                id="course" 
                                disabled
                                style="border:2px solid #6fa7d9; color:#6fa7d9;" 
                                required>
                            <option value="" selected>Course</option>
                            @foreach($courses as $data)
                            <option value="{{$data->course_code}}">{{$data->course_code}} - {{$data->long_title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                    <label for="">Course Type</label>
                        <select class="form-control" 
                                id="type" 
                                name="course_type" 
                                style="border:2px solid #6fa7d9; color:#6fa7d9;" 
                                required>

                            <option value="">-----------</option>
                            @foreach ($course_types as $data)
                                <option value="{{ $data->course_type_code }}">
                                    {{ $data->course_type_code }} - {{ $data->type_description }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                    <label for="">Course Genre</label>
                        <select class="form-control" 
                                name="genre" 
                                id="genre" 
                                disabled
                                style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;">
                            <option value="" selected disabled>Course Genre</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                    <label for="">Credit Hour</label>
                        <select class="form-control" 
                                id="credit" 
                                name="credit" 
                                disabled
                                style="border:2px solid #6fa7d9; color:#6fa7d9;">
                            <option value="" selected disabled>Credit Hours</option>
                        </select>
                    </div>
                </div>
            </div>
        @else
            @foreach($studies as $data)
                <div class="content">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Semester</label>
                            <select class="form-control" name="semester" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" readonly>
                                <option value="{{$data->semester_code}}">{{$data->semester_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                        <label for="">Course</label>
                            <select class="form-control" name="course" id="course" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" readonly>
                                <option value="{{$data->course_code}}">{{$data->course_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                        <label for="">Course Type</label>
                            <select class="form-control" id="type" name="type" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                <option value="{{$data->course_type}}" selected>{{$data->course_type}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                        <label for="">Course Genre</label>
                            <select class="form-control" name="genre" id="genre" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                <option value="{{$data->course_genre}}">{{$data->course_genre}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                        <label for="">Credit Hour</label>
                            <select class="form-control" id="credit" name="credit" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                <option value="{{$data->credit_hour}}">{{$data->credit_hour}}</option>
                            </select>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="content">
            <div class="col-md-2">
                <div class="form-group" id="gened">
                    <label for="">Total Gened</label>
                    @if($count_studies == 0)
                        <input  type="text" 
                                id="totalGened" 
                                value="0" 
                                class="form-control" 
                                name="total_gened"
                                placeholder="Total Gened" 
                                disabled
                                style="border:2px solid #6fa7d9; color:#6fa7d9;">
                    @else
                        <input  type="text" 
                                id="totalGened" 
                                value="{{$sumgen}}" 
                                class="form-control" 
                                name="total_gened"
                                placeholder="Total Gened" 
                                style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" disabled>
                    @endif
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">Total Core</label>
                    @if($count_studies == 0)
                        <input  type="text" 
                                id="totalCore" 
                                value="0" 
                                class="form-control" 
                                name="total_core"
                                placeholder="Total Core" 
                                disabled
                                style="border:2px solid #6fa7d9; color:#6fa7d9;">
                    @else
                        <input  type="text" 
                                id="totalCore" 
                                value="{{$sumcore}}" 
                                class="form-control" 
                                name="total_core"
                                placeholder="Total Core" 
                                disabled
                                style="border:2px solid #6fa7d9; color:#6fa7d9;" >
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                <label for="">Total Concentration 1</label>
                @if($count_studies == 0)
                    <input  type="text" 
                            id="totalConcentration" 
                            value="0" 
                            class="form-control" 
                            name="total_concentration"
                            placeholder="Total Concentration 1" 
                            disabled
                            style="border:2px solid #6fa7d9; color:#6fa7d9;">
                    @else
                    <input  type="text" 
                            id="totalConcentration" 
                            value="{{$sumcon}}" 
                            class="form-control" 
                            name="total_concentration"
                            placeholder="Total Concentration 1" 
                            disabled
                            style="border:2px solid #6fa7d9; color:#6fa7d9;">
                    @endif
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for=""> Total Electives</label>
                    @if($count_studies == 0)
                    <input type="text" id="totalElect" disabled value="0" class="form-control" placeholder="Total Electives" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_elective">
                    @else
                    <input type="text" id="totalElect" disabled value="{{$sumelect}}" class="form-control" placeholder="Total Electives" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_elective" readonly>
                    @endif
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">Total Program</label>
                    @if($count_studies == 0)
                    <input type="text" name="total_program" disabled id="totalProgram" class="form-control" placeholder="Total Program" style="border:2px solid #6fa7d9; color:#6fa7d9;" >
                    @else
                    <input type="text" name="total_program" disabled id="totalProgram" value="{{$sum}}" class="form-control" placeholder="Total Program" style="border:2px solid #6fa7d9; color:#6fa7d9;" >
                    @endif
                </div>
            </div>
        </div>
    </div>
</div><hr>

<div class="row">
    <div class="col-md-6">
        <div class="content" >
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary"  style="width:190px;">Save</button>
                <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
            </div>
        </div>
    </div>
</div>
    
<div class="modal" id="myModal">
    <div class="modal-dialog">
    <div class="modal-content">
    
        <!-- Modal Header -->
        <div class="modal-header" style="">
            <h2>Exit</h2>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <p>Your data will be lost if you exit</p>
        </div>  
        <!-- Modal footer -->
        <div class="modal-footer">
        <a href="{{url('/')}}">
            <button type="button" class="btn btn-danger">OK</button>
        </a>
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
        </div>
        
    </div>
    </div>
</div>

<div class="modal fade" id="myModal1">
    <div class="modal-dialog">
    <div class="modal-content" style="width:60%;">
    
        <!-- Modal Header -->
        <div class="modal-header">
        <h4 class="modal-title">Error <li class="fa fa-exclamation-triangle"></li></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        Please Complete All Requirements to Submit The Application !
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
    </div>
    </div>
</div>

<div class="modal fade" id="myModal2">
    <div class="modal-dialog">
    <div class="modal-content" style="width:60%;">
    
        <!-- Modal Header -->
        <div class="modal-header" style="background-color:green; color:white;">
        <h4 class="modal-title">Success <li class="fa fa-check"></li></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        Application Submmited successfuly
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
    </div>
    </div>
</div>
@endsection
