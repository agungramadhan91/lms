@extends('program.non-substantive.sublayout')

@section('css')
{{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> --}}
<style>
    .form-control{
        border:2px solid #6fa7d9; 
        color:#6fa7d9; 
        width:150px;
    }
</style>
@endsection

@section('content')

    <form action="{{ route('program.non-substantive.adding_course.save') }}" method="post" id="moving_course_form">
        @csrf
        
        @php
            $tab_no = 0;
            $ctn_no = 0;
        @endphp
        <ul class="nav nav-pills" id="myTab" role="tablist">
            @foreach ($programs as $data)
                <li class="nav-item {{ $tab_no == 0 ? 'active':''}}">
                    <a class="nav-link" 
                        id="home-tab" 
                        data-toggle="tab" 
                        href="#{{ $data->program_code }}" 
                        role="tab" 
                        aria-controls="home" 
                        aria-selected="true">

                        {{ $data->program_code }}
                    </a>
                </li>

                @php
                    $tab_no++;
                @endphp
            @endforeach
        </ul><hr>
        <div class="tab-content" id="myTabContent">
            
            <input type="hidden" name="changes" id="changes" value="{{ $changes != null ? 'true':'' }}">
            <div class="row">
                <div class="content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Effective Term</label>
                            <select class="form-control" 
                                    name="effective_term"
                                    id="effective_term"
                                    {{ $changes != null ? 'disabled':'' }}
                                    onchange="changeColorMain('effective')"
                                    style="border:2px solid {{ $changes != null ? '#6fa7d9':'red' }}; 
                                        width:230px; background-color:#fffbf1;">
                                
                                <option value="">--------</option>
                                @foreach($terms->where('academic_year','<','2099') as $ef)
                                    {{-- @if ($changes != null)
                                        <option value="{{$ef->term_code}}"
                                            {{ $data->ef_tc == $ef->term_code ? 'selected':'' }}>
                                            {{$ef->term_description}}
                                        </option>
                                    @else
                                    @endif --}}
                                    <option value="{{$ef->term_code}}"
                                        {{ $ef_tc == $ef->term_code ? 'selected':'' }}>
                                        {{$ef->term_description}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
            
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">End Term</label>
                            <select class="form-control" 
                                    name="end_term" 
                                    id="end_term"
                                    {{ $changes != null ? 'disabled':'' }}
                                    onchange="changeColorMain('end')"
                                    style="border:2px solid {{ $changes != null ? '#6fa7d9':'red' }}; 
                                        width:230px; background-color:#fffbf1;">
                                
                                <option value="">--------{{$en_tc}}</option>
                                @foreach($terms as $en)
                                    {{-- @if ($changes != null)
                                        <option value="{{$en->term_code}}"
                                            {{ $data->en_tc == $en->term_code ? 'selected':'' }}>
                                            {{$en->term_description}}
                                        </option>
                                    @else
                                    @endif --}}
                                    <option value="{{$en->term_code}}"
                                        {{ $en_tc == $en->term_code ? 'selected':'' }}>
                                        {{$en->term_description}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            
            @foreach ($programs as $data)
                <div class="tab-pane fade in {{ $ctn_no == 0 ? 'active':''}}" id="{{ $data->program_code }}" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <input type="hidden" name="program_code[]" class="program_code" value="{{ $data->program_code }}">
                            {{-- <input type="hidden" name="course_code[]" value="{{ $course }}"> --}}
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="content">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Program Study Plan</label>
                                                    <select class="form-control" 
                                                            name="program"
                                                            disabled
                                                            style="border:2px solid #6fa7d9; color:#6fa7d9;width:230px;" 
                                                            required>
                                                            
                                                        <option value="">{{ $data->program_code }}</option>
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Program Status</label>
                                                    <input  type="text" 
                                                            value="{{$data->status}}" 
                                                            name="status" 
                                                            disabled
                                                            class="form-control"
                                                            style="border:2px solid #6fa7d9; color:#6fa7d9;width:230px;" 
                                                            readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div><hr>
                            
                                    <div style="overflow: scroll; height: 260px;">
                                        @php $i = 1; @endphp
                                        
                                        @foreach($data->course_list as $list)

                                            @for ($a = 1; $a < 10; $a++)
                                                @if ($list->{'program_code_0'.$a} == $data->program_code)
                                                    @php
                                                        $sp = $list->{'semester_program_0'.$a};
                                                        $pct= $list->{'course_type_code_0'.$a};
                                                        $cg = $list->{'course_genre_code_0'.$a};
                                                    @endphp
                                                @endif
                                            @endfor

                                            <div class="row content">
                                                <div class="col-md-2" style="display:flex">
                                                    <div class="form-group">
                                                        <label for="">Semester</label>
                                                        <select class="form-control"
                                                                name="semester[{{$i}}]"
                                                                style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" 
                                                                disabled>
                                                            <option value="">--------</option>

                                                            @foreach ($semesters as $smst)
                                                                <option value="{{ $smst->semester_code }}"
                                                                    {{ $smst->semester_code == $sp ? 'selected':''}}>
                                                                    {{ $smst->semester_description }}
                                                                </option>
                                                            @endforeach
                                                        </select>

                                                        @if ($list->course_code == $course)
                                                            <input type="hidden" name="semester_program[]" value="{{ $sp }}">
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="display:flex">
                                                    <div class="form-group">
                                                        <label for="">Course Code</label>
                                                        <select class="form-control" name=""
                                                            style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" disabled>
                                                            <option value="">{{ $list->course_code }}</option>
                                                        </select>
                                                        
                                                        @if ($list->course_code == $course)
                                                            <input type="hidden" name="course_code" value="{{ $list->course_code }}">
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="display:flex">
                                                    <div class="form-group">
                                                        <label for="">Course Type</label>
                                                        <select class="form-control selected types total-{{$data->program_code}}" 
                                                                id="type-{{$data->program_code}}-{{$i}}" 
                                                                disabled
                                                                name="course_type[]"
                                                                {{-- onchange="electiveList('{{$list->course_code}}')" --}}
                                                                style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;">
                        
                                                            <option value="">--------</option>
                                                            @foreach ($course_types as $t)
                                                                @if ($changes != null)
                                                                    @if ($list->course_code == $course)
                                                                        <option value="{{ $t->course_type_code }}" 
                                                                            {{$changes[$ctn_no] == $t->course_type_code ? 'selected':''}}>
                                                                            {{$t->course_type_code}} - {{$t->type_description}}
                                                                        </option>
                                                                    @else    
                                                                        <option value="{{ $t->course_type_code }}" 
                                                                            {{$pct == $t->course_type_code ? 'selected':''}}>
                                                                            {{$t->course_type_code}} - {{$t->type_description}}
                                                                        </option>
                                                                    @endif
                                                                @else
                                                                    
                                                                    <option value="{{$t->course_type_code}}"
                                                                        {{$pct == $t->course_type_code ? 'selected':''}}>
                                                                        {{$t->course_type_code}} - {{$t->type_description}}
                                                                    </option>
                                                                @endif
                    
                                                            @endforeach
                                                        </select>

                                                        @if ($list->course_code == $course)
                                                            <input type="hidden" name="old_course_type[]" value="{{ $pct }}">
                                                            <input type="hidden" id="elective-{{$data->program_code}}-{{$i}}" name="electives[]" value="">
                                                        @endif
                                                        <input type="hidden" name="ct-{{$data->program_code}}[]" value="{{ $pct }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="display:flex">
                                                    <div class="form-group">
                                                        <label for="">Course Genre</label>
                                                        <select class="form-control"
                                                                name="genre[{{$i}}]"
                                                                style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" 
                                                                required 
                                                                disabled>

                                                            @for ($x = 1; $x < 10; $x++)
                                                                @if ($list->{'course_genre_code_0'.$x} == $data->order)
                                                                    <option value="{{ $list->{'course_genre_code_0'.$x} }}" selected>{{ $list->{'course_genre_code_0'.$x} }}</option>
                                                                @else
                                                                    <option value="{{ $list->{'course_genre_code_0'.$x} }}">{{ $list->{'course_genre_code_0'.$x} }}</option>
                                                                @endif
                                                            @endfor
                                                        </select>

                                                        @if ($list->course_code == $course)
                                                            <input type="hidden" name="course_genre[]" value="{{ $cg }}">
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="display:flex">
                                                    <div class="form-group">
                                                        <label for="">Credit</label>
                                                        <select class="form-control"
                                                                name="creditgen[{{$i}}]"
                                                                style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" 
                                                                required 
                                                                disabled>

                                                            <option value="">{{ $list->credit_hours }}</option>
                                                        </select>

                                                        {{-- @if ($list->course_code == $course)
                                                        @endif --}}
                                                        <input type="hidden" name="credit[]" value="{{ $list->credit_hours }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        @if ($changes == null && $list->course_code == $course)
                                                            <button class="btn btn-warning editBtn" 
                                                                    data-id="{{ $i }}"
                                                                    data-course="{{ $list->course_code }}"
                                                                    data-code="{{$data->program_code}}"
                                                                    onclick="event.preventDefault();" 
                                                                    style="border:1px solid #D3D3D3; margin-top:25px"
                                                                    title="Inactivate">
                                                                Edit
                                                            </button>
                                                        @endif
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            @php $i++; @endphp
                                        @endforeach
                                    </div>
                            
                                    <div id="txt">
                                    </div><br>

                                    <div class="row content">
                                        <div class="col-md-2">
                                            <div class="form-group" id="gened">
                                                <label for="">Total Gened</label>
                                                <input  type="text"
                                                        value="{{ $changes != 0 ? $saved_gened[$ctn_no][$data->program_code]:$data->total_gened }}" 
                                                        class="form-control" 
                                                        placeholder="Total Gened"
                                                        readonly>
                                                        
                                                <input type="hidden" name="total_gened[]" value="{{ $changes != 0 ? $saved_gened[$ctn_no][$data->program_code]:$data->total_gened }}">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="">Total Core</label>
                                                <input  type="text"
                                                        value="{{ $changes != 0 ? $saved_core[$ctn_no][$data->program_code]:$data->total_core }}" 
                                                        class="form-control" placeholder="Total Core"
                                                        readonly>

                                                <input type="hidden" name="total_core[]" value="{{ $changes != 0 ? $saved_core[$ctn_no][$data->program_code]:$data->total_core }}">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="">Total Concentration</label>
                                                <input  type="text"
                                                        value="{{ $changes != 0 ? $saved_conc[$ctn_no][$data->program_code]:$data->total_conc }}" 
                                                        class="form-control"
                                                        readonly>
                                                <input type="hidden" name="total_conc[]" value="{{ $changes != 0 ? $saved_conc[$ctn_no][$data->program_code]:$data->total_conc }}">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for=""> Total Electives</label>
                                                <input  type="text" 
                                                        value="{{ $changes != 0 ? $saved_elec[$ctn_no][$data->program_code]:$data->total_elec }}" 
                                                        class="form-control" placeholder="Total Electives"
                                                        readonly>
                                                <input type="hidden" name="total_elec[]" value="{{ $changes != 0 ? $saved_elec[$ctn_no][$data->program_code]:$data->total_elec }}">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for=""> Total Internship</label>
                                                <input  type="text" 
                                                        value="{{ $changes != 0 ? $saved_int[$ctn_no][$data->program_code]:$data->total_int }}" 
                                                        class="form-control" placeholder="Total Electives"
                                                        readonly>
                                                <input type="hidden" name="total_int[]" value="{{ $changes != 0 ? $saved_int[$ctn_no][$data->program_code]:$data->total_int }}">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="">Total Program</label>
                                                <input  type="text" 
                                                        class="form-control" 
                                                        placeholder="Total Program" 
                                                        value="{{ $data->grand_total }}"
                                                        readonly>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @php
                    $ctn_no++;
                @endphp
            @endforeach
            <hr>
            <div class="row content">
                <div class="">
                    <div class="col-md-3">
                        <div class="form-group">
                            <a  href="#" 
                                class="btn btn-primary page" 
                                id="save" 
                                name="submit[]" 
                                style="border:3px solid #D3D3D3; width:190px;">
                                Save
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <a  href="#" 
                                class="btn page" 
                                id="submit" 
                                name="submit[]" 
                                style="width:190px; 
                                        background-color:rgb(215, 227, 191); 
                                        font-weight:bold; 
                                        color:black; 
                                        border:3px solid black;">
                                Submit The Application
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#exit-modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>

    <div class="modal" id="elective-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" id="title-elect">
                    Exit
                </div>
                
                <div class="modal-body" id="content-elect">
                    <div class="row">
                        <div id="elect-content"></div>
                    </div>
                </div>  
                
                <div class="modal-footer">
                    <button type="button" class="btn" style="width:80px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-dismiss="modal">Close</button>
                </div> 
            </div>
        </div>
    </div>

    @include('program.non-substantive._cancelbtn')
@endsection

@section('js')
    {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> --}}
    <script>
        function changeColorMain(term) {
            var eff = document.getElementById('effective_term');
            var end = document.getElementById('end_term');

            if(term == 'effective'){
                if(eff.value == ''){
                    eff.style.border = "2px solid red";
                }else{
                    eff.style.border = "2px solid #6fa7d9";
                }
            }else if(term == 'end'){
                console.log(end.value)
                if(end.value == ''){
                    end.style.border = "2px solid red";
                }else{
                    end.style.border = "2px solid #6fa7d9";
                }
            }else{
                eff.style.border = "2px solid red";
                end.style.border = "2px solid red";
            }
        }
        
        $(document).ready(function(){
            var eff = document.getElementById('effective_term');
            var end = document.getElementById('end_term');
            var changes = document.getElementById('changes').value;
            var types = document.getElementsByClassName('types').length;

            $('#submit').click(function(e) {
                e.preventDefault();
                console.log('ini submit')
                // document.getElementById('click_btn').value = "submit";

                if(eff.value != '' && end.value != ''){
                    if(changes == 'true'){
                        document.getElementById("submit_form").submit();
                    }else{
                        document.getElementById('title-modal').innerHTML = "Error"
                        document.getElementById('content-modal').innerHTML = "Please Complete All Required Fields!"
                        $("#message-modal").modal('show');
                    }
                }else{
                    document.getElementById('title-modal').innerHTML = "Error"
                    document.getElementById('content-modal').innerHTML = "Please Complete All Required Fields!"
                    $("#message-modal").modal('show');
                }

                console.log($('#submit').val())
            })

            $('#save').click(function(e) {
                e.preventDefault();
                
                if(eff.value != '' && end.value != ''){
                    if(eff.value == end.value){
                        document.getElementById('title-modal').innerHTML = "Error"
                        document.getElementById('content-modal').innerHTML = "End Term must be greater than Effective Term!"
                        $("#message-modal").modal('show');
                    }else{
                        if (changes == 'true') {
                            document.getElementById('title-modal').classList.remove("bg-danger")
                            document.getElementById('title-modal').classList.add("bg-success")
                            document.getElementById('title-modal').innerHTML = "Success"
                            document.getElementById('content-modal').innerHTML = "Changes has been saved, please submit the application!"
                            $("#message-modal").modal('show');
                        } else {
                            document.getElementById("moving_course_form").submit();
                        }
                    }
                }else{
                    if(eff.value == '' || end.value == ''){
                        document.getElementById('title-modal').innerHTML = "Error"
                        document.getElementById('content-modal').innerHTML = "Please fill the effective term and end term!"
                        $("#message-modal").modal('show');
                    }else{
                        document.getElementById('title-modal').innerHTML = "Error"
                        document.getElementById('content-modal').innerHTML = "End Term must be greater than Effective Term!"
                        $("#message-modal").modal('show');
                    }
                }
            })

            $('.editBtn').click(function(e){
                var id      = $(this).data('id')
                var code    = $(this).data('code')
                var course  = $(this).data('course')
                var type    = document.getElementById('type-'+code+'-'+id);

                if(type.disabled == true){
                    type.style.border = "2px solid red"
                    type.disabled = false;
                }else{
                    type.style.border = "2px solid #6fa7d9"
                    type.disabled = true;
                }

                $('#elect-content').append(`
                    <div class="col-md-12">
                        <p>Add `+course+` to the electives list?</p>
                    </div>
                    <div class="col-md-4">
                        <input type="checkbox" class="electives" id="elect-`+code+`-1" value="Program">
                        <label for="elective1"> Program</label>
                    </div>
                    <div class="col-md-4">
                        <input type="checkbox" class="electives" id="elect-`+code+`-2" value="College">
                        <label for="elective2"> College</label>
                    </div>
                    <div class="col-md-4">
                        <input type="checkbox" class="electives" id="elect-`+code+`-3" value="University">
                        <label for="elective3"> University</label>
                    </div>
                `);

                $('#type-'+code+'-'+id).change(function () {
                    var select = this.value;
                    
                    if(select == "ELECT"){
                        document.getElementById('title-elect').innerHTML = "Program Code "+code
                        $("#elective-modal").modal('show');
                        var elect_field = document.getElementById('elective-'+code+'-'+id);
                        
                        $(".electives").change(function() {
                            var electives = [];
                            for (let index = 1; index <= 3; index++) {
                                var ch = document.getElementById('elect-'+code+'-'+index);
                                if(ch.checked) {
                                    electives.push(ch.value);
                                }
                            }
                            
                            if (electives.length != 0 && electives.length != null) {
                                elect_field.value = electives;
                            }
                            console.log(electives)
                        });
                    }
                });
            })
        });
    </script>
@endsection