@extends('program.non-substantive.sublayout')

@section('css')
{{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> --}}
<style>
    .form-control{
        border:2px solid #6fa7d9; 
        color:#6fa7d9; 
        width:150px;
    }
</style>
@endsection

@section('content')

    <form action="{{ route('program.non-substantive.adding_course.save') }}" method="post" id="moving_course_form">
        @csrf
        
        @php
            $tab_no = 0;
            $ctn_no = 0;
        @endphp
        
        <div class="tab-content" id="myTabContent">
            
            <input type="hidden" name="changes" id="changes" value="{{ $changes != null ? 'true':'' }}">
            <div class="row">
                <div class="content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Code</label>
                            <input  type="text" 
                                    name="course_code" 
                                    class="form-control"
                                    style="border:2px solid #6fa7d9;
                                        width:230px; background-color:#fffbf1;"
                                    value="{{ $data->course_code }}"
                                    readonly>
                        </div>
                    </div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Effective Term</label>
                            <select class="form-control" 
                                    name="effective_term"
                                    id="effective_term"
                                    {{ $changes != null ? 'disabled':'' }}
                                    onchange="changeColorMain('effective')"
                                    style="border:2px solid {{ $changes != null ? '#6fa7d9':'red' }}; 
                                        width:230px; background-color:#fffbf1;">
                                
                                <option value="">--------</option>
                                @foreach($terms->where('academic_year','<','2099') as $ef)
                                    <option value="{{$ef->term_code}}"
                                        {{ $ef_tc == $ef->term_code ? 'selected':'' }}>
                                        {{$ef->term_description}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
            
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">End Term</label>
                            <select class="form-control" 
                                    name="end_term" 
                                    id="end_term"
                                    {{ $changes != null ? 'disabled':'' }}
                                    onchange="changeColorMain('end')"
                                    style="border:2px solid {{ $changes != null ? '#6fa7d9':'red' }}; 
                                        width:230px; background-color:#fffbf1;">
                                
                                <option value="">--------</option>
                                @foreach($terms as $en)
                                    <option value="{{$en->term_code}}"
                                        {{ $en_tc == $en->term_code ? 'selected':'' }}>
                                        {{$en->term_description}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div style="overflow: scroll; height: 260px;">
                        {{-- <div class="row content">
                            <div class="col-md-2" style="display:flex">
                                <div class="form-group">
                                    <label for="">Semester</label>
                                    <select class="form-control"
                                            name="semester"
                                            style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" 
                                            disabled>
                                        <option value="">--------</option>

                                        @foreach ($semesters as $smst)
                                            <option value="{{ $smst->semester_code }}"
                                                {{ $smst->semester_code == $data->semester_program_01 ? 'selected':''}}>
                                                {{ $smst->semester_description }}
                                            </option>
                                        @endforeach
                                    </select>

                                    <input type="hidden" name="semester_program[]" value="{{ $data->semester_program_01 }}">
                                </div>
                            </div>
                            <div class="col-md-2" style="display:flex">
                                <div class="form-group">
                                    <label for="">Course Code</label>
                                    <select class="form-control" name=""
                                            style="border:2px solid #6fa7d9; 
                                                    color:#6fa7d9; width:150px;" 
                                            disabled>
                                        <option value="">{{ $data->course_code }}</option>
                                    </select>
                                    
                                    <input type="hidden" name="course_code" value="{{ $data->course_code }}">
                                </div>
                            </div>
                            <div class="col-md-2" style="display:flex">
                                <div class="form-group">
                                    <label for="">Course Type</label>
                                    <select class="form-control selected types" 
                                            id="type-{{$data->course_code}}" 
                                            disabled
                                            name="course_type"
                                            style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;">
    
                                        <option value="">--------</option>
                                        <option value="ELECT">
                                            ELECT - Elective Course
                                        </option>
                                    </select>

                                    <input type="hidden" id="electives" name="electives" value="">
                                    <input type="hidden" name="old_course_type" value="{{ $data->course_type_code_01 }}">
                                </div>
                            </div>
                            <div class="col-md-2" style="display:flex">
                                <div class="form-group">
                                    <label for="">Course Genre</label>
                                    <select class="form-control"
                                            name="genre"
                                            style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" 
                                            required 
                                            disabled>

                                        <option value="{{ $data->course_genre_code_01 }}" selected>{{ $data->course_genre_code_01 }}</option>
                                    </select>

                                    <input type="hidden" name="course_genre[]" value="{{ $data->course_genre_code_01 }}">
                                </div>
                            </div>
                            <div class="col-md-2" style="display:flex">
                                <div class="form-group">
                                    <label for="">Credit</label>
                                    <select class="form-control"
                                            name="creditgen"
                                            style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" 
                                            required 
                                            disabled>

                                        <option value="">{{ $data->credit_hours }}</option>
                                    </select>

                                    <input type="hidden" name="credit[]" value="{{ $data->credit_hours }}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    @if ($changes == null)
                                        <button class="btn btn-warning editBtn" 
                                                data-id=""
                                                data-course="{{ $data->course_code }}"
                                                data-code="{{$data->program_code_01}}"
                                                onclick="event.preventDefault();" 
                                                style="border:1px solid #D3D3D3; margin-top:25px"
                                                title="Inactivate">
                                            Edit
                                        </button>
                                    @endif
                                    
                                </div>
                            </div>
                        </div> --}}
                        <div class="row">        
                            <div class="col-md-2" style="display:flex">
                                <div class="form-group">
                                    <label for="">Elect By Program</label>
                                    <select name="elect_by_program" 
                                            class="form-control"
                                            style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" 
                                            required>
                                        <option value="0" {{ $elect_by_program == '0' ? 'selected':'' }}>No</option>
                                        <option value="1" {{ $elect_by_program == '1' ? 'selected':'' }}>Yes</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2" style="display:flex">
                                <div class="form-group">
                                    <label for="">Elect By College</label>
                                    <select name="elect_by_college" 
                                            class="form-control"
                                            style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" 
                                            required>
                                        <option value="0" {{ $elect_by_college == '0' ? 'selected':'' }}>No</option>
                                        <option value="1" {{ $elect_by_college == '1' ? 'selected':'' }}>Yes</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2" style="display:flex">
                                <div class="form-group">
                                    <label for="">Elect By University</label>
                                    <select name="elect_by_univeristy" 
                                            class="form-control"
                                            style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" 
                                            required>
                                        <option value="0" {{ $elect_by_university == '0' ? 'selected':'' }}>No</option>
                                        <option value="1" {{ $elect_by_university == '1' ? 'selected':'' }}>Yes</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
            
                </div>
            </div>
            
            <hr>
            <div class="row content">
                <div class="">
                    <div class="col-md-3">
                        <div class="form-group">
                            <a  href="#" 
                                class="btn btn-primary page" 
                                id="save" 
                                name="submit[]" 
                                style="border:3px solid #D3D3D3; width:190px;">
                                Save
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <a  href="#" 
                                class="btn page" 
                                id="submit" 
                                name="submit[]" 
                                style="width:190px; 
                                        background-color:rgb(215, 227, 191); 
                                        font-weight:bold; 
                                        color:black; 
                                        border:3px solid black;">
                                Submit The Application
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#exit-modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>

    <div class="modal" id="elective-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" id="title-elect">
                    Exit
                </div>
                
                <div class="modal-body" id="content-elect">
                    <div class="row">
                        <div id="elect-content"></div>
                    </div>
                </div>  
                
                <div class="modal-footer">
                    <button type="button" class="btn" style="width:80px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-dismiss="modal">OK</button>
                </div> 
            </div>
        </div>
    </div>

    @include('program.non-substantive._cancelbtn')
@endsection

@section('js')
    {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> --}}
    <script>
        function changeColorMain(term) {
            var eff = document.getElementById('effective_term');
            var end = document.getElementById('end_term');

            if(term == 'effective'){
                if(eff.value == ''){
                    eff.style.border = "2px solid red";
                }else{
                    eff.style.border = "2px solid #6fa7d9";
                }
            }else if(term == 'end'){
                console.log(end.value)
                if(end.value == ''){
                    end.style.border = "2px solid red";
                }else{
                    end.style.border = "2px solid #6fa7d9";
                }
            }else{
                eff.style.border = "2px solid red";
                end.style.border = "2px solid red";
            }
        }
        
        $(document).ready(function(){
            var eff = document.getElementById('effective_term');
            var end = document.getElementById('end_term');
            var changes = document.getElementById('changes').value;
            var types = document.getElementsByClassName('types').length;

            $('#submit').click(function(e) {
                e.preventDefault();
                console.log('ini submit')
                // document.getElementById('click_btn').value = "submit";

                if(eff.value != '' && end.value != ''){
                    if(changes == 'true'){
                        document.getElementById("submit_form").submit();
                    }else{
                        document.getElementById('title-modal').innerHTML = "Error"
                        document.getElementById('content-modal').innerHTML = "Please Complete All Required Fields!"
                        $("#message-modal").modal('show');
                    }
                }else{
                    document.getElementById('title-modal').innerHTML = "Error"
                    document.getElementById('content-modal').innerHTML = "Please Complete All Required Fields!"
                    $("#message-modal").modal('show');
                }

                console.log($('#submit').val())
            })

            $('#save').click(function(e) {
                e.preventDefault();
                
                if(eff.value != '' && end.value != ''){
                    if(eff.value == end.value){
                        document.getElementById('title-modal').innerHTML = "Error"
                        document.getElementById('content-modal').innerHTML = "End Term must be greater than Effective Term!"
                        $("#message-modal").modal('show');
                    }else{
                        if (changes == 'true') {
                            document.getElementById('title-modal').classList.remove("bg-danger")
                            document.getElementById('title-modal').classList.add("bg-success")
                            document.getElementById('title-modal').innerHTML = "Success"
                            document.getElementById('content-modal').innerHTML = "Changes has been saved, please submit the application!"
                            $("#message-modal").modal('show');
                        } else {
                            document.getElementById("moving_course_form").submit();
                        }
                    }
                }else{
                    if(eff.value == '' || end.value == ''){
                        document.getElementById('title-modal').innerHTML = "Error"
                        document.getElementById('content-modal').innerHTML = "Please fill the effective term and end term!"
                        $("#message-modal").modal('show');
                    }else{
                        document.getElementById('title-modal').innerHTML = "Error"
                        document.getElementById('content-modal').innerHTML = "End Term must be greater than Effective Term!"
                        $("#message-modal").modal('show');
                    }
                }
            })

            $('.editBtn').click(function(e){
                var id      = $(this).data('id')
                var code    = $(this).data('code')
                var course  = $(this).data('course')
                var type    = document.getElementById('type-'+course);

                if(type.disabled == true){
                    type.style.border = "2px solid red"
                    type.disabled = false;
                }else{
                    type.style.border = "2px solid #6fa7d9"
                    type.disabled = true;
                }

                $('#elect-content').append(`
                    <div class="col-md-4">
                        <input type="checkbox" class="electives" id="elect-`+code+`-1" value="Program" {{ $data->elect_by_program == 1 ? 'checked':'' }}>
                        <label for="elective1"> Program</label>
                    </div>
                    <div class="col-md-4">
                        <input type="checkbox" class="electives" id="elect-`+code+`-2" value="College" {{ $data->elect_by_college == 1 ? 'checked':'' }}>
                        <label for="elective2"> College</label>
                    </div>
                    <div class="col-md-4">
                        <input type="checkbox" class="electives" id="elect-`+code+`-3" value="University" {{ $data->elect_by_university == 1 ? 'checked':'' }}>
                        <label for="elective3"> University</label>
                    </div>
                `);

                $('#type-'+course).change(function () {
                    var select = this.value;
                    
                    if(select == "ELECT"){
                        document.getElementById('title-elect').innerHTML = 'Add '+course+' to the electives list?'
                        $("#elective-modal").modal('show');
                        var elect_field = document.getElementById('electives');
                        
                        $(".electives").change(function() {
                            var electives = [];
                            for (let index = 1; index <= 3; index++) {
                                var ch = document.getElementById('elect-'+code+'-'+index);
                                if(ch.checked) {
                                    electives.push(ch.value);
                                }
                            }
                            
                            if (electives.length != 0 && electives.length != null) {
                                elect_field.value = electives;
                            }
                            console.log(electives)
                        });
                    }
                });
            })
        });
    </script>
@endsection