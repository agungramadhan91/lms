@extends('program.non-substantive.sublayout')

@section('content')
    <form action="{{ route('program.non-substantive.fourth.index') }}" method="get">
        {{ csrf_field() }}
        <div class="row content">
            <div class="col-md-6" style="display:flex">
                <div class="form-group">
                    <label for="">Program Code</label>
                    <select id="select-state" 
                            class="form-control" 
                            style="border:2px solid #6fa7d9; color:#6fa7d9; width:400px;" 
                            name="program_code" 
                            placeholder="Pick a state..."
                            required>
                        <option value="">Select a program...</option>
                        @foreach($programs as $data)
                        <option value="{{$data->program_code}}">{{$data->program_code}} - {{$data->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <br><br>
        <div class="row content">
            <div class="col-md-3">
                <div class="form-group">
                    <button type="submit" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Submit</button>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                </div>
            </div>
        </div>
    </form>
    <!-- /. ROW  -->
    
    <div class="modal" id="myModal">
        <div class="modal-dialog">
        <div class="modal-content">
        
            <!-- Modal Header -->
            <div class="modal-header" style="">
                <h2>Exit</h2>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">
                <p>Your data will be lost if you exit</p>
            </div>  
            <!-- Modal footer -->
            <div class="modal-footer">
            <a href="{{url('/')}}">
                <button type="button" class="btn btn-danger">OK</button>
            </a>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            </div>
            
        </div>
        </div>
    </div>
@endsection


{{-- ========================================================================================= --}}
{{-- @foreach($courses as $course)
                                            <div class="row content">
                                                <div class="col-md-2" style="display:flex">
                                                    <div class="form-group">
                                                        <label for="">Semester</label>
                                                        <select class="form-control" id="semester{{$i}}" name="semester[{{$i}}]"
                                                            style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" readonly>
                                                            <option value="{{$course->semester_code}}" selected>{{$course->semester_code}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="display:flex">
                                                    <div class="form-group">
                                                        <label for="">course</label>
                                                        <select class="form-control" id="course{{$i}}" name="course[{{$i}}]"
                                                            style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" disabled>
                                                            @foreach($courses as $data2)
                                                                @if($data2->course_code == $course->course_code)
                                                                    <option value="{{$course->course_code}}" selected>{{$course->course_code}} - {{$data2->long_title}}
                                                                    </option>
                                                                @endif
                                                            
                                                                <option value="{{$data2->course_code}}">{{$data2->course_code}} - {{$data2->long_title}}</option>
                                                            @endforeach
                                                        </select>
                        
                                                        <input type="hidden" name="course_code[]" value="{{ $course->course_code }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="display:flex">
                                                    <div class="form-group">
                                                        <label for="">Course Type</label>
                                                        <select class="form-control selected types" 
                                                                id="type{{$i}}" 
                                                                disabled
                                                                name="course_type[]"
                                                                style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;">
                        
                                                            <option value="">---------</option>
                                                            @foreach ($course_types as $t)
                                                                @if ($changes != null)
                                                                    @for ($i = 0; $i < count($changes); $i++)
                                                                        @if ($changes[$i] == $t->course_type_code)
                                                                            <option value="{{ $t->course_type_code }}" selected>
                                                                                {{$t->course_type_code}} - {{$t->type_description}}
                                                                            </option>
                                                                        @endif
                                                                    @endfor
                                                                @endif
                    
                                                                <option value="{{$t->course_type_code}}" 
                                                                    {{ $t->course_type_code == $course->course_type ? 'selected':''}}>
                                                                    {{$t->course_type_code}} - {{$t->type_description}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="display:flex">
                                                    <div class="form-group">
                                                        <label for="">Course Genre</label>
                                                        <select class="form-control" id="genre{{$i}}" name="genre[{{$i}}]"
                                                            style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                                            <option value="{{$data->course_genre}}" selected>{{$data->course_genre}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="display:flex">
                                                    <div class="form-group">
                                                        <label for="">Credit</label>
                                                        @if($course->course_type == 'GEN')
                                                        <select class="form-control" id="credit{{$i}}" name="creditgen[{{$i}}]"
                                                            style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                                            @elseif($course->course_type == 'CONC')
                                                            <select class="form-control" id="credit{{$i}}" name="creditconc[{{$i}}]"
                                                                style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                                                @elseif($course->course_type == 'ELECT')
                                                                <select class="form-control" id="credit{{$i}}" name="creditelect[{{$i}}]"
                                                                    style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                                                    @elseif($course->course_type == 'Core')
                                                                    <select class="form-control" id="credit{{$i}}" name="creditcore[{{$i}}]"
                                                                        style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                                                        @else
                                                                        <select class="form-control" id="credit{{$i}}" name="credit[{{$i}}]"
                                                                            style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                                                            @endif
                                                                            <option value="{{$course->credit_hour}}" selected>{{$course->credit_hour}}</option>
                                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <button class="btn btn-warning editBtn" 
                                                                data-code="{{$course->course_code}}" 
                                                                data-id="{{$i}}"
                                                                onclick="event.preventDefault();" 
                                                                style="border:1px solid #D3D3D3; margin-top:25px"
                                                                title="Inactivate">
                                                            Edit
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php $i++?>
                                        @endforeach --}}