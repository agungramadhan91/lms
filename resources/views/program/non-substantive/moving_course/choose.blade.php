@extends('program.non-substantive.sublayout')

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        /* span.select2-selection--multiple[aria-expanded=true] {
            border-color: #6fa7d9 !important;   
        }
        .select2-container--default .select2-selection--single{
            color:#6fa7d9;
            border:2px solid #6fa7d9; 
            width:400px;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            color:#6fa7d9;
            border:2px solid #6fa7d9; 
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered */
    </style>
@endsection

@section('content')
    <form action="{{ route('program.non-substantive.moving_course.course') }}" method="get">
        {{ csrf_field() }}
        <div class="row content">
            <div class="col-md-6" style="display:flex">
                <div class="form-group">
                    <label for="">Course Code</label>
                    <div>
                        <select class="select2-course form-control" 
                                name="course_code"
                                style="color:#6fa7d9;
                                border:2px solid #6fa7d9; 
                                width:400px;"
                                required>

                            <option value="">-----------</option>
                            @foreach($courses as $data)
                                <option value="{{$data->course_code}}">
                                    {{$data->course_code}} - {{$data->short_title}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    {{-- <input list="brow"  class="form-control" name="course_code" style = "width:400px;"> --}}
                    {{-- <datalist id="brow">
                        <option value="">---------</option>
                        @foreach($courses as $data)
                            <option value="{{$data->course_code}}">
                                {{$data->course_code}} - {{$data->short_title}}
                            </option>
                        @endforeach
                    </datalist> --}}
                    {{-- <select id="select-state" 
                            class="form-control" 
                            style="border:2px solid #6fa7d9; color:#6fa7d9; width:400px;" 
                            name="program_code" 
                            placeholder="Pick a state..."
                            required>
                        <option value="">Select a course...</option>
                        
                        @foreach($courses as $data)
                            <option value="{{$data->course_code}}">{{$data->course_code}} - {{$data->short_title}}</option>
                        @endforeach
                    </select> --}}
                </div>
            </div>
        </div>
        <div class="row content">
            <div class="col-md-8" style="display:flex">
                <div class="form-gro">
                    <label for="">Program Code</label>
                    <div class="alert alert-success">
                        <div class="" id="program_check"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6" style="display:flex">
                <div class="form-group">
                </div>
            </div>
        </div>
        <div class="row content">
            <div class="col-md-3">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary" style="width:143px; color:white; border:3px solid #D3D3D3;">Submit</button>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <button type="button" class="btn" style="width:143px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#exit-modal">Cancel</button>
                </div>
            </div>
        </div>
    </form>
    <!-- /. ROW  -->
    
    @include('program.non-substantive._cancelbtn')
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="{{ asset('js/get-data-program.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.select2-course').select2({
                width:"resolve",
                
            });
            $('.select2-program').select2({
                width:"resolve",
                
            });
        });
    </script>
@endsection