@extends('program.non-substantive.sublayout')

@section('content')

    <form action="{{ route('program.non-substantive.program_learning_outcome.save_main') }}" method="post" id="main_outcome_form">
        @csrf

        {{-- <input type="hidden" name="click_btn" id="click_btn">
        <input type="hidden" name="program_code" value="{{ $program_code }}"> --}}
        
        <div class="row">
            <div class="col-md-12">
                <div class="content">
                    <div class="col-md-3" style="display:flex;">
                        <div class="form-group">
                            <label for="">Campus</label>
                            <select class="form-control" 
                                    name="campus" 
                                    disabled
                                    style="background-color:#fffbf1; width:230px; 
                                            border:2px solid #6fa7d9; 
                                            color:#6fa7d9; float:left">
            
                                @foreach($campuses as $data)
                                    <option value="{{$data->campus_code}}" 
                                            {{ $data->campus_code == $program->campus_code ? 'selected':'' }}>
                                        {{$data->campus_code}} {{$data->campus_description}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div><div class="col-md-1"></div>
            
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Status</label>
                            <input  type="text" 
                                    class="form-control" 
                                    value="{{ $program->status }}"
                                    disabled
                                    style="border:2px solid #6fa7d9; 
                                        color:#6fa7d9; width:230px; 
                                        background-color:#fffbf1;">
                        </div>
                    </div><div class="col-md-1"></div>
            
                    <div class="col-md-3" style="display:flex;">
                        <div class="form-group">
                            <label for="">College</label>
                            <select class="form-control" 
                                    name="college" 
                                    disabled
                                    style="border:2px solid #6fa7d9; 
                                        color:#6fa7d9; width:230px; 
                                        background-color:#fffbf1;">
                                
                                @foreach($colleges as $data)
                                    <option value="{{$data->college_code}}" 
                                        {{ $program->college_code == $data->college_code ? 'selected':'' }}>
                                        {{$data->title}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div><br>
            
                <div class="content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Degree Type</label>
                            <select class="form-control" 
                                    name="degree_type" 
                                    id="type" 
                                    disabled
                                    style="border:2px solid #6fa7d9; 
                                        color:#6fa7d9; width:230px; 
                                        background-color:#fffbf1;">
                                
                                @foreach($dtypes as $data)
                                    <option value="{{$data->degree_type_code}}"
                                        {{ $program->degree_type_code == $data->degree_type_code ? 'selected':'' }}>
                                        {{$data->title}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div><div class="col-md-1"></div>
            
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Degree Level</label>
                            <select class="form-control" 
                                    name="degree_level" 
                                    id="level" 
                                    disabled
                                    style="border:2px solid #6fa7d9; 
                                        color:#6fa7d9; width:230px; 
                                        background-color:#fffbf1;">
                                
                                @foreach($dlevels as $data)
                                    <option value="{{$data->degree_level_code}}"
                                        {{ $program->degree_level_code == $data->degree_level_code ? 'selected':'' }}>
                                        {{$data->title}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div><div class="col-md-1"></div>
            
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Department</label>
                            <select class="form-control" 
                                    name="department" 
                                    id="level" 
                                    disabled
                                    style="border:2px solid #6fa7d9; 
                                        color:#6fa7d9; width:230px; 
                                        background-color:#fffbf1;">
                                <!-- <option value="" selected disabled></option> -->
                                @foreach ($departments as $data)
                                    <option value="{{$data->department_code}}"
                                        {{ $program->college_code = $data->college_code ? 'selected':'' }}>
                                        {{$data->title}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div><br>
                
                <div class="content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Effective Term</label>
                            <select class="form-control" 
                                    name="effective_term"
                                    id="effective_term"
                                    onchange="changeColorMain('effective')"
                                    style="border:2px solid {{$plo_term_ef == '' ? 'red':'#6fa7d9'}}; 
                                        width:230px; background-color:#fffbf1;">
                                
                                <option value="">--------</option>
                                @foreach($terms->where('academic_year','<','2099') as $data)
                                    <option value="{{$data->term_code}}"
                                        {{ $plo_term_ef == $data->term_code ? 'selected':'' }}>
                                        {{$data->term_description}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div><div class="col-md-1"></div>
            
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">End Term</label>
                            <select class="form-control" 
                                    name="end_term" 
                                    id="end_term"
                                    onchange="changeColorMain('end')"
                                    style="border:2px solid {{$plo_term_en == '' ? 'red':'#6fa7d9'}}; 
                                        width:230px; background-color:#fffbf1;">
                                
                                <option value="">--------</option>
                                @foreach($terms as $data)
                                    <option value="{{$data->term_code}}"
                                        {{ $plo_term_en == $data->term_code ? 'selected':'' }}>
                                        {{$data->term_description}}
                                    </option>
                                @endforeach
                            </select>
                            
                        </div>
                    </div>
                </div><br>
            </div>
        </div><hr>

        <div class="row">
            <div class="col-md-12">
                <div class="content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Code</label>
                            <input  type="text" 
                                    class="form-control" 
                                    placeholder="Program Code" 
                                    name="code" 
                                    value="{{ $program->program_code }}"
                                    readonly
                                    style="border:2px solid #6fa7d9; 
                                        background-color:#fffbf1; 
                                        color:#6fa7d9; width:230px;">
                        </div>
                    </div><div class="col-md-1"></div>

                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Title</label>
                            <input  type="text" 
                                    class="form-control" 
                                    placeholder="Title" 
                                    name="title" 
                                    value="{{ $program->title }}"
                                    readonly
                                    style="border:2px solid #6fa7d9; 
                                        background-color:#fffbf1; 
                                        color:#6fa7d9; width:230px;">
                        </div>
                    </div><div class="col-md-1"></div>

                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Short Title</label>
                            <input  type="text" 
                                    class="form-control" 
                                    placeholder="Short Title" 
                                    name="short_title" 
                                    value="{{ $program->title }}"
                                    readonly
                                    style="border:2px solid #6fa7d9; 
                                        background-color:#fffbf1; 
                                        color:#6fa7d9; width:230px;">
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-12">
                <div class="content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Major Code</label>
                            <input  type="text" 
                                    class="form-control" 
                                    placeholder="{{ $count_majors == 0 ? 'No Information Added':'Major Code' }}" 
                                    name="major_code" 
                                    value="{{ $count_majors == 0 ? '' : $major->major_code }}"
                                    readonly
                                    style="border:2px solid #6fa7d9; 
                                        background-color:#fffbf1; 
                                        color:#6fa7d9; width:230px;">
                        </div>
                    </div><div class="col-md-1"></div>

                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Major Title</label>
                            <input  type="text" 
                                    class="form-control" 
                                    placeholder="{{ $count_majors == 0 ? 'No Information Added':'Major Code' }}" 
                                    name="major_title" 
                                    value="{{ $count_majors == 0 ? '' : $major->title }}"
                                    readonly
                                    style="border:2px solid #6fa7d9; 
                                    background-color:#fffbf1; 
                                    color:#6fa7d9; width:230px;">
                        </div>
                    </div><div class="col-md-1"></div>

                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Joint Degree</label>
                            <select class="form-control" 
                                    name="degree" 
                                    readonly
                                    style="border:2px solid #6fa7d9; 
                                    background-color:#fffbf1; 
                                    color:#6fa7d9; width:230px;">

                                {{-- <option value="">Joint Degree</option> --}}
                                <option value="yes">Yes</option>
                                <option value="no" selected>No</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div><br>

            <div class="col-md-12">
                <div class="content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Concentration Code</label>
                            <input  type="text" 
                                    class="form-control" 
                                    placeholder="No Information Entered" 
                                    name="concentration_code"
                                    readonly
                                    style="border:2px solid #6fa7d9; 
                                        background-color:#fffbf1; 
                                        color:#6fa7d9; width:230px;">
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Concentration Title</label>
                            <input  type="text" 
                                    class="form-control" 
                                    placeholder="No Information Entered" 
                                    name="concentration_title"
                                    readonly
                                    style="border:2px solid #6fa7d9; 
                                        background-color:#fffbf1; 
                                        color:#6fa7d9; width:230px;">
                        </div>
                    </div>
                </div>
            </div>
        </div><hr>

        <div class="row">
            <div class="col-md-12">
                <div class="content">
                    <div class="col-md-6">
                        {{-- <button type="submit" class="btn btn-primary" name="submit[]" value="main" style="width:190px;">Save</button> --}}
                        <a href="#" class="btn btn-primary page" id="save" name="submit[]" value="" style="width:190px;">Save</a>
                        <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#exit-modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    @include('program.non-substantive._cancelbtn')
@endsection

@section('js')
    <script>
        function changeColorMain(term) {
            var eff = document.getElementById('effective_term');
            var end = document.getElementById('end_term');

            if(term == 'effective'){
                if(eff.value == ''){
                    eff.style.border = "2px solid red";
                }else{
                    eff.style.border = "2px solid #6fa7d9";
                }
            }else if(term == 'end'){
                console.log(end.value)
                if(end.value == ''){
                    end.style.border = "2px solid red";
                }else{
                    end.style.border = "2px solid #6fa7d9";
                }
            }else{
                eff.style.border = "2px solid red";
                end.style.border = "2px solid red";
            }
        }

        $(document).ready(function(){
            var eff = document.getElementById('effective_term');
            var end = document.getElementById('end_term');

            $('#submit').click(function(e) {
                e.preventDefault();
                // document.getElementById('click_btn').value = "submit";

                if(eff.value != '' && end.value != ''){
                    document.getElementById("submit_form").submit();
                }else{
                    Swal.fire({
                        title: 'Error!',
                        text: "Please Complete All Required Fields!!!",
                        icon: 'error',
                        confirmButtonText: 'Ok'
                    })
                }

                console.log($('#submit').val())
            })

            $('#save').click(function(e) {
                e.preventDefault();
                // document.getElementById('click_btn').value = "save";

                if(eff.value != '' && end.value != ''){
                    document.getElementById("main_outcome_form").submit();
                }else{
                    if(eff.value == ''){
                        Swal.fire({
                            title: 'Error!',
                            text: "Please update the effective term first!",
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        })
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: "Please update the end term first!",
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        })

                    }
                }
            })
        });
    </script>
@endsection