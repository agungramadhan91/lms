@extends('program.non-substantive.sublayout')

@section('content')
    <form action="{{ route('program.non-substantive.fourth.index') }}" method="get">
        {{ csrf_field() }}
        <div class="row content">
            <div class="col-md-6" style="display:flex">
                <div class="form-group">
                    <label for="">Program Code</label>
                    <select id="select-state" 
                            class="form-control" 
                            style="border:2px solid #6fa7d9; color:#6fa7d9; width:400px;" 
                            name="program_code" 
                            placeholder="Pick a state..."
                            required>
                        <option value="">Select a program...</option>
                        @foreach($programs as $data)
                        <option value="{{$data->program_code}}">{{$data->program_code}} - {{$data->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <br><br>
        <div class="row content">
            <div class="col-md-3">
                <div class="form-group">
                    <button type="submit" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Submit</button>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                </div>
            </div>
        </div>
    </form>
    <!-- /. ROW  -->
    
    <div class="modal" id="myModal">
        <div class="modal-dialog">
        <div class="modal-content">
        
            <!-- Modal Header -->
            <div class="modal-header" style="">
                <h2>Exit</h2>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">
                <p>Your data will be lost if you exit</p>
            </div>  
            <!-- Modal footer -->
            <div class="modal-footer">
            <a href="{{url('/')}}">
                <button type="button" class="btn btn-danger">OK</button>
            </a>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            </div>
            
        </div>
        </div>
    </div>
@endsection