@extends('program.non-substantive.sublayout')

@section('content')
    <div class="col-md-12">
        <form action="{{ route('program.non-substantive.program_learning_outcome.save_outcome') }}" method="post" id="main_outcome_form">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <p style="color:#0b4176; margin-top:25px;font-size:21px; text-weigth:bold; font-family:times new roman;">
                        Program Learning Outcomes 
                        <span class="requiredStar">*</span>
                    </p><br>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        <div class="form-group pull-right">
                            <label for="">Effective Term</label>
                            <select name="effective_term"
                                    id="effective_term" 
                                    style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:180px"
                                    class="form-control"
                                    required
                                    disabled>
                                <option value="">---------</option>
                                @foreach ($terms->where('academic_year','<','2099') as $data)
                                    <option value="{{ $data->term_code }}"
                                            {{ $plo_term_ef == $data->term_code ? 'selected':'' }}>
                                        {{ $data->term_description }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group pull-right">
                            <label for="">End Term</label>
                            <select name="end_term" 
                                    id="end_term" 
                                    style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:180px"
                                    class="form-control"
                                    onchange="checkTerm()"
                                    required
                                    disabled>
                                <option value="">---------</option>
                                @foreach ($terms as $data)
                                    <option value="{{ $data->term_code }}"
                                            {{ $plo_term_en == $data->term_code ? 'selected':'' }}>
                                        {{ $data->term_description }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="text-center">
                            <small>
                                One <i>effective term</i> and one <i>end term</i> must be chosen for all changes.<br>
                                only future terms can be used, and <i>end term</i> must be greater than <i>effective term</i>.
                            </small>
                        </div>
                        {{-- <div class="alert alert-warning" role="alert">
                        </div> --}}
                    </div>
                </div>
            </div><hr>
        
            <div id="plo">
                
                @php
                    $no = 1;    
                    $changed_no = 0;    
                    $dbl_code = array();
                @endphp
                
                @foreach($outcomes as $data)
                    @php
                        $code = '';    
                    @endphp
    
                    @if ($plo_description != '')
                        @for ($i = 0; $i < count($plo_description); $i++)
                            @if ($plo_code[$i] == $data->plo_code)     
                                @php
                                    $code = $plo_code[$i];
                                @endphp  
    
                                <div class="row content">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label  for="">ID</label>
                                            <input  type="text" 
                                                    class="form-control" 
                                                    placeholder="ID" 
                                                    value="{{ $code }}"
                                                    style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:180px" readonly>
                                        </div>
                                        <input type="hidden" name="code[]" id="code{{ $no }}" value="{{ $code }}">
                                    </div><div class="col-md-1"></div>
                
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Program Learning Outcome Description</label>
                                            <textarea   type="text" 
                                                        class="form-control outcome_description" 
                                                        placeholder="Program Learning Outcome Description" 
                                                        name="description[]"
                                                        rows="5"
                                                        readonly
                                                        style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">{{ $plo_description[$i] }}</textarea>
                                        </div>
                                    </div>
    
                                </div>
                            @endif
                        @endfor
                    @endif
                        
                    @if ($data->plo_code != $code)
                        <div class="row content">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">ID</label>
                                    <input  type="text" 
                                            class="form-control" 
                                            placeholder="ID"
                                            id="dpcode{{ $no }}"
                                            value="{{ $data->plo_code }}"
                                            style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:180px" readonly>
                                </div>
                                <input type="hidden" name="code[]" id="code{{ $no }}" >
                            </div><div class="col-md-1"></div>
        
                            <div class="col-md-6" >
                                <div class="form-group">
                                    <label for="">Program Learning Outcome Description</label>
                                    <textarea   type="text" 
                                                class="form-control outcome_description" 
                                                placeholder="Program Learning Outcome Description" 
                                                name="description[]"
                                                id="description{{ $no }}"
                                                rows="5"
                                                disabled
                                                style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">{{ $data->plo_description }}</textarea>
                                </div>
                            </div>
    
                            <div class="col-md-1" style="padding-left:40px;">
                                <div class="form-group">
                                    <label for="">Edit</label>
                                    <button class="btn btn-warning edit"
                                            onclick="event.preventDefault(); changeColor({{$no}})"
                                            data-code = "{{ $data->plo_code }}"
                                            style="border:1px solid #D3D3D3;">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    @endif    
                    @php
                        $no++;
                    @endphp
                @endforeach
                
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        {{-- <button class="btn btn-primary" id="save" name="submit[]" value="outcome" style="width:190px;">Save</button> --}}
                        <a href="#" class="btn btn-primary page" id="save" name="submit[]" value="outcome" style="width:190px; border:3px solid #D3D3D3;">Save</a>
                        <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#exit-modal">Cancel</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    @include('program.non-substantive._cancelbtn');
    
@endsection

@section('js')
    <script>
        function changeColor(no) {
            var dpcode          = document.getElementById('dpcode'+no);
            var code            = document.getElementById('code'+no);
            var description     = document.getElementById('description'+no);
            var effective_term  = document.getElementById('effective_term');
            var end_term        = document.getElementById('end_term');
            var active_desc     = document.getElementsByClassName("active_desc");
            
            if(description.disabled != true){
                description.style.border = "2px solid #6fa7d9";
                description.disabled = true;
                description.classList.remove('active_desc')
            }else{
                description.style.border = "2px solid red";
                description.disabled = false;
                description.classList.add('active_desc')
                code.value = dpcode.value
            }

            if(active_desc.length > 0){
                effective_term.style.border = "2px solid red";
                effective_term.disabled = false;
                end_term.style.border = "2px solid red";
                end_term.disabled = false;
            }else{
                effective_term.style.border = "2px solid #6fa7d9";
                effective_term.disabled = true;
                end_term.style.border = "2px solid #6fa7d9";
                end_term.disabled = true;
            }

            console.log(description);
        }

        function checkTerm(params) {
            var ef = document.getElementById('effective_term').value;
            var en = document.getElementById('end_term').value;

            if(en <= ef){
                document.getElementById('title-modal').innerHTML = "Error"
                document.getElementById('content-modal').innerHTML = "End Term must be greater than Effective Term!"
                $("#message-modal").modal('show');
            //     document.getElementById('save').style.pointerEvents = "none";
            // }else{
            //     document.getElementById('save').style.pointerEvents = "";
            }
            console.log(document.getElementById('save').disabled)
        }

        $(document).ready(function() {
            var eff = document.getElementById('effective_term');
            var end = document.getElementById('end_term');

            $('#submit').click(function(e) {
                e.preventDefault();
                
                if(eff.value != '' && end.value != ''){
                    // console.log('ef '+eff.value+' en '+end.value)
                    document.getElementById("submit_form").submit();
                }else{
                    document.getElementById('title-modal').innerHTML = "Error"
                    document.getElementById('content-modal').innerHTML = "Please Complete All Required Fields!"
                    $("#message-modal").modal('show');
                }

                console.log($('#submit').val())
            })

            $('#save').click(function(e) {
                e.preventDefault();
                // document.getElementById('click_btn').value = "save";
                var active_desc = document.getElementsByClassName("active_desc");
                var eff = document.getElementById('effective_term');
                var end = document.getElementById('end_term');

                if(active_desc.length > 0){
                    if(eff.value != '' && end.value != ''){
                        if(eff.value == end.value){
                            document.getElementById('title-modal').innerHTML = "Error"
                            document.getElementById('content-modal').innerHTML = "End Term must be greater than Effective Term!"
                            $("#message-modal").modal('show');
                        }else{
                            document.getElementById("main_outcome_form").submit();
                        }
                        // document.getElementById("main_outcome_form").submit();
                    }else{
                        document.getElementById('title-modal').innerHTML = "Error"
                        document.getElementById('content-modal').innerHTML = "Please update the Effective Term and End Term!"
                        $("#message-modal").modal('show');    
                    }
                }else{
                    document.getElementById('title-modal').innerHTML = "Error"
                    document.getElementById('content-modal').innerHTML = "Please update the description first!"
                    $("#message-modal").modal('show');
                }
            })
        });
    </script>
@endsection