<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Eduval</title>
        <!-- BOOTSTRAP STYLES-->
        <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
        <!-- FONTAWESOME STYLES-->
        <link href="{{ asset('assets/css/font-awesome.css')}}" rel="stylesheet" />
        {{-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous"> --}}
        <link rel="stylesheet" href="{{ asset('css/font-awesome-4.7.0/css/font-awesome.min.css')}}">
        <!-- CUSTOM STYLES-->
        <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
        <!-- GOOGLE FONTS-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        
        @yield('css')

        <style>
            .requiredStar{
                color:red;
            }
            .selectize-control{
                width:400px;
                color:#6fa7d9;
                background-color:#fffbf1;
            }
            .selectize-input{
                background-color:#fffbf1;
                border:1px solid #6fa7d9;
        
            }
            .select-state{
                color:#6fa7d9;
                background-color:#fffbf1;
                border:1px solid #6fa7d9;
            }

        </style>
    </head>
    <body>
        <div id="wrapper">
            <div class="header">
                <div class="row">
                    <div class="col-md-2" style="background-color:white;">
                        <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                    </div>
                    <div class="col-md-10" style=" color:white">
                        <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                    </div>
                </div>
            </div>

            <div class="row"></div><br>

            <div class="row">
                <div class="col-md-12">
                    <div class="navbar navbar-inverse tetap blue">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="adjust-nav">
                                    <div class="navbar-header" style="width:392px;">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <h5 style="text-align:center; 
                                                            padding-right:40px; 
                                                            margin-top:20px; 
                                                            color:#a99451;">
                                                    @if (Request::segment(3) == 'program-learning-outcome')
                                                        Minor Changes to Program Learning Outcomes
                                                    @elseif(Request::segment(3) == 'moving-courses')
                                                        Moving course(s) between compulsory and elective status without changing the number of credit hours
                                                    @elseif(Request::segment(3) == 'replace-course')
                                                        Replacement of an existing course with another
                                                    @elseif(Request::segment(3) == 'adding-course')
                                                        Adding an existing course to the electives list
                                                    @endif
                                                </h5>
                                            </div>
                                            <div class="col-md-4">
                                                @if (Request::segment(4) == 'outcome')
                                                    @if ($plo_description != '' && $plo_term_ef != '')
                                                        <form action="{{ route('program.non-substantive.program_learning_outcome.submit') }}" method="post" id="submit_form">
                                                            @csrf
                                                        </form>
                                                    @endif
                                                @elseif (Request::segment(4) == 'courses')
                                                    @if ($changes != '' && $changes != '')
                                                        <form action="{{ route('program.non-substantive.moving_course.submit') }}" method="post" id="submit_form">
                                                            @csrf
                                                        </form>
                                                    @endif
                                                @elseif (Request::segment(4) == 'replace')
                                                    @if ($changes != '' && $changes != '')
                                                        <form action="{{ route('program.non-substantive.replace_course.submit') }}" method="post" id="submit_form">
                                                            @csrf
                                                        </form>
                                                    @endif
                                                @elseif (Request::segment(4) == 'electives')
                                                    @if ($changes != '' && $changes != '')
                                                        <form action="{{ route('program.non-substantive.adding_course.submit') }}" method="post" id="submit_form">
                                                            @csrf
                                                        </form>
                                                    @endif
                                                @endif
                                        
                                                @if (Request::segment(4) != '' &&
                                                    Request::segment(4) != 'courses' &&
                                                    Request::segment(4) != 'replace')
                                                    <a  href="#"
                                                        {{-- id="submit" --}}
                                                        class="btn btn-small page" 
                                                        style="width:190px; 
                                                            background-color:#484648; 
                                                            color:white; 
                                                            border:3px solid #D3D3D3;
                                                            margin-top:15px; 
                                                            margin-left:-10px">
                                                        Submit The Application
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav center">
                                        <li style="margin-left:-100px;"><a href="#">Program Management</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /. NAV TOP  -->
            <nav class="navbar-default navbar-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="main-menu">
                        @php
                            if(Request::segment(4) == ''){
                                $set_colors = array(
                                    '1' => 'done','2' => 'done','3' => 'done','4' => 'done','5' => 'done','6' => 'done','7' => 'done','8' => 'done','9' => 'done','10' => 'done','11' => 'done','12' => 'done','13' => 'done', '14' => 'done'
                                );
                                
                                $set_links = array(
                                    '1' => '#','2' => '#','3' => '#','4' => '#','5' => '#','6' => '#','7' => '#','8' => '#','9' => '#','10' => '#','11' => '#','12' => '#','13' => '#', '14' => '#'
                                );
                            }elseif (Request::segment(4) == 'outcome') {
                                $main       = $plo_term_ef == '' ? 'active':'done';
                                $outcome    = $plo_description == '' ? 'active':'done';
                                $set_colors = array(
                                    '1' => 'inactive',
                                    '2' => $outcome,
                                    '3' => 'inactive','4' => 'inactive','5' => 'inactive','6' => 'inactive','7' => 'inactive','8' => 'inactive','9' => 'inactive','10' => 'inactive','11' => 'inactive','12' => 'inactive','13' => 'inactive', '14' => 'inactive'
                                );
                                
                                $set_links = array(
                                    '1' => '#', 
                                    '2' => '#', 
                                    '3' => '#','4' => '#','5' => '#','6' => '#','7' => '#','8' => '#','9' => '#','10' => '#','11' => '#','12' => '#','13' => '#', '14' => '#'
                                );
                            }elseif (Request::segment(4) == 'courses') {
                                $structure    = $changes == '' ? 'active':'done';
                                $set_colors = array(
                                    '1' => 'inactive','2' => 'inactive',
                                    '3' => $structure,
                                    '4' => 'inactive','5' => 'inactive','6' => 'inactive','7' => 'inactive','8' => 'inactive','9' => 'inactive','10' => 'inactive','11' => 'inactive','12' => 'inactive','13' => 'inactive', '14' => 'inactive'
                                );
                                $set_links = array(
                                    '1' => '#', '2' => '#', '3' => '#','4' => '#','5' => '#','6' => '#','7' => '#','8' => '#','9' => '#','10' => '#','11' => '#','12' => '#','13' => '#', '14' => '#'
                                );
                            }elseif (Request::segment(4) == 'replace') {
                                $structure    = $changes == '' ? 'active':'done';
                                $set_colors = array(
                                    '1' => 'inactive','2' => 'inactive',
                                    '3' => $structure,
                                    '4' => 'inactive','5' => 'inactive','6' => 'inactive','7' => 'inactive','8' => 'inactive','9' => 'inactive','10' => 'inactive','11' => 'inactive','12' => 'inactive','13' => 'inactive', '14' => 'inactive'
                                );
                                $set_links = array(
                                    '1' => '#', '2' => '#', '3' => '#','4' => '#','5' => '#','6' => '#','7' => '#','8' => '#','9' => '#','10' => '#','11' => '#','12' => '#','13' => '#', '14' => '#'
                                );
                            }elseif (Request::segment(4) == 'electives') {
                                $structure    = $changes == '' ? 'active':'done';
                                $set_colors = array(
                                    '1' => 'inactive','2' => 'inactive',
                                    '3' => $structure,
                                    '4' => 'inactive','5' => 'inactive','6' => 'inactive','7' => 'inactive','8' => 'inactive','9' => 'inactive','10' => 'inactive','11' => 'inactive','12' => 'inactive','13' => 'inactive', '14' => 'inactive'
                                );
                                $set_links = array(
                                    '1' => '#', '2' => '#', '3' => '#','4' => '#','5' => '#','6' => '#','7' => '#','8' => '#','9' => '#','10' => '#','11' => '#','12' => '#','13' => '#', '14' => '#'
                                );
                            }else {
                                $set_colors = array(
                                    '1' => 'inactive','2' => 'inactive','3' => 'inactive','4' => 'inactive','5' => 'inactive','6' => 'inactive','7' => 'inactive','8' => 'inactive','9' => 'inactive','10' => 'inactive','11' => 'inactive','12' => 'inactive','13' => 'inactive', '14' => 'inactive'
                                );
                                $set_links = array(
                                    '1' => '#', '2' => '#', '3' => '#','4' => '#','5' => '#','6' => '#','7' => '#','8' => '#','9' => '#','10' => '#','11' => '#','12' => '#','13' => '#', '14' => '#'
                                );
                            }

                            for ($i=1; $i <= count($set_colors); $i++) { 
                                ${'color'.$i} = $set_colors[$i];
                            }
                            
                            for ($j=1; $j <= count($set_links); $j++) { 
                                ${'link'.$j} = $set_links[$j];
                            }
                        @endphp
                        {{-- @if (Request::segment(3) == 'program-learning-outcome')
                            @if (Request::segment(4) == 'choose')
                                
                            @endif
                        @endif --}}
                        @if(Request::segment(4) != '')
                            <li><a href="{{ $link1 }}" class="{{ $color1 }}">Main</a></li>
                            <li><a href="{{ $link2 }}" class="{{ $color2 }}">Outcomes</a></li>
                            <li><a href="{{ $link3 }}" class="{{ $color3 }}">Structure</a></li>
                            <li><a href="{{ $link4 }}" class="{{ $color4 }}">Assesment Methods</a></li>
                            <li><a href="{{ $link5 }}" class="{{ $color5 }}">Faculty & Professional Staff</a></li>
                            <li><a href="{{ $link6 }}" class="{{ $color6 }}">Fitness & Alignment</a></li>
                            <li><a href="{{ $link7 }}" class="{{ $color7 }}">Students</a></li>
                            <li><a href="{{ $link8 }}" class="{{ $color8 }}">Researach & Scholarly Activities</a></li>
                            <li><a href="{{ $link9 }}" class="{{ $color9 }}">Resources</a></li>
                            <li><a href="{{ $link10 }}" class="{{ $color10 }}">Supportive Data</a></li>
                            <li><a href="{{ $link11 }}" class="{{ $color11 }}">Cost & Revenue</a></li>
                            <li><a href="{{ $link12 }}" class="{{ $color12 }}">Comunity Engagement</a></li>
                            <li><a href="{{ $link13 }}" class="{{ $color13 }}">Public Diclosure & Intergrity</a></li>
                            <li><a href="{{ $link14 }}" class="{{ $color14 }}">Appendices</a></li>
                            <li><a href="{{ route('home') }}" class="inactive">Home</a></li>
                        @endif

                    </ul>

                </div>

            </nav>
            <!-- /. NAV SIDE  -->
            <div id="page-wrapper">
                <div id="page-inner">

                    @yield('content')

                </div>
                <!-- /. PAGE INNER  -->
            </div>
            <!-- /. PAGE WRAPPER  -->

            <div class="modal" id="message-modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <h4 style="font-weight:bold;">
                                <div id="title-modal"></div>
                            </h4>
                        </div>
                        
                        <div class="modal-body">
                            <p style="font-size:17px;" id="content-modal"></p>    
                        </div>  
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" style="width:80px; border:3px solid #D3D3D3;" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal" id="notif-modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        
                        <div class="modal-header {{session()->has('success') ? 'bg-success':'bg-danger'}}">
                            <h4 style="font-weight:bold;">
                                <div id="title-modal">
                                    @if (session()->has('success'))
                                        Confirmation
                                    @else
                                        Error
                                    @endif
                                </div>
                            </h4>
                        </div>
                        
                        <div class="modal-body" id="content-modal">
                            @if (session()->has('success'))
                                <p style="font-size:17px;">{{ session()->get('success') }}</p>
                            @else
                                <p style="font-size:17px;">{{ session()->get('error') }}</p>
                            @endif
                        </div>  
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" style="width:80px; border:3px solid #D3D3D3;" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form>
        <!-- /. WRAPPER  -->
        <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
        <!-- JQUERY SCRIPTS -->
        <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <!-- BOOTSTRAP SCRIPTS -->
        <!-- METISMENU SCRIPTS -->
        <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
        <!-- CUSTOM SCRIPTS -->
        {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> --}}
        @yield('js')
        <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets/js/custom.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>


        <script>
            $(document).ready(function () {
                // $('#program').selectize({
                //     sortField: 'text'
                // });
            });

            @if(session()->has('success'))
                $("#notif-modal").modal('show');
            @endif

            @if(session()->has('error'))
                $("#notif-modal").modal('show');
            @endif
        </script>
    </body>
</html>