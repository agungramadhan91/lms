<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="{{asset('assets/css/font-awesome.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>
    
.requiredStar{
    color:red;
}

</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                        <div class="row">
                            <div class="col-md-8">
                                <h5 style="text-align:center; margin-top:20px; color:#a99451;">View Program</h5>
                            </div>
                            <div class="col-md-4">
                            </div>
                        </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Program Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="{{url('program/view')}}" class="done" style="border: 4px solid #0b4176;">Main</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/outcome')}}" class="done">Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/structure')}}" class="done">Structure</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/assesment')}}" class="done">Assesment Methods</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/faculty')}}" class="done">Faculty & Professional Staff</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/fitness')}}" class="done">Fitness & Alignment</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/student')}}" class="done">Students</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/research')}}" class="done">Researach & Scholarly Activities</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/resource')}}" class="done">Resources</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/supportive')}}" class="done">Supportive Data</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/cost')}}" class="done">Cost & Revenue</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/community')}}" class="done">Comunity Engagement</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/public')}}" class="done">Public Diclosure & Intergrity</a>
                    </li>
                    <li>
                        <a href="#">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="{{ route('program.store') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row content">
                    <div class="col-md-3" style="display:flex;">
                        <div class="form-group">
                            <label for="">Campus</label>
                            <select class="form-control" name="campus" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; float:left" readonly>
                            @if($program->campus_code == "")
                                <option value="" selected disabled>Campus</option>
                            @else
                                <option value="{{$program->campus_code}}" selected>{{$campus2->campus_code}} {{$campus2->campus_description}}</option>
                            @endif
                                @foreach($campus as $data)
                                <option value="{{$data->campus_code}}">{{$data->campus_code}} {{$data->campus_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Status</label>
                            <input type="text" class="form-control" style="border:2px solid #6fa7d9; color:#6fa7d9; width:230px; background-color:#fffbf1;" value="{{$program->status}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex;">
                        <div class="form-group">
                            <label for="">College</label>
                            <select class="form-control" name="college" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;" readonly>
                                @if($program->college_code == "")
                                    <option value="" selected disabled>College</option>
                                @else
                                    <option value="{{$program->college_code}}" selected>{{$college2->title}}</option>
                                @endif
                                    @foreach($college as $data)
                                    <option value="{{$data->college_code}}">{{$data->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>        
                <br>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Degree Type</label>
                            <select class="form-control" name="degree_type" id="type" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:230px" readonly>
                                @if($program->degree_type_code == "")
                                    <option value="" selected disabled>Degree Type</option>
                                @else
                                    <option value="{{$program->degree_type_code}}" selected>{{$type2->title}}</option>
                                @endif
                                @foreach($type as $data)
                                    <option value="{{$data->degree_type_code}}">{{$data->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Degree Level</label>
                            <select class="form-control" name="degree_level" id="level" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:230px;" readonly>
                                @if($program->degree_level_code == "")
                                    <option value="" selected disabled>Degree Level</option>
                                @else
                                    <option value="{{$program->degree_level_code}}" selected>{{$level2->title}}</option>
                                @endif
                                <!-- @foreach($level as $data)
                                <option value="{{$data->degree_level_code}}">{{$data->title}}</option>
                                @endforeach -->
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Department</label>
                            <select class="form-control" name="department" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;">
                                <!-- <option value="" selected disabled></option> -->
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Effective Term</label>
                            <select class="form-control" name="effective_term" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:230px;" readonly>
                            @if($program->effective_term_code == "")    
                                <option value="" selected disabled>Effective Term</option>
                            @else
                                <option value="{{$program->effective_term_code}}" selected>{{$effterm->term_description}}</option>
                            @endif
                                @foreach($term as $data)
                                <option value="{{$data->term_code}}">{{$data->term_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">End Term</label>
                            <select class="form-control" name="end_term" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:230px" readonly>
                            @if($program->end_term_code == "")    
                                <option value="" selected disabled>End Term</option>
                            @else
                                <option value="{{$program->end_term_code}}" selected>{{$endterm->term_description}}</option>
                            @endif
                                @foreach($term as $data)
                                <option value="{{$data->term_code}}">{{$data->term_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row content" style="margin-bottom:10px;">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Code</label>
                            <input type="text" class="form-control" placeholder="Program Code" name="code" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:270px;" value="{{$program->program_code}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Title</label>
                            <input type="text" class="form-control" placeholder="Program Title" name="title" style="border:2px solid #6fa7d9; color:#6fa7d9; background-color:#fffbf1; width:270px;" value="{{$program->title}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Short Title</label>
                            <input type="text" class="form-control" placeholder="Short Title" name="short_title" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:270px;" value="{{$program->title}}" readonly>
                        </div>
                    </div>
                </div>
                <div id="major">
                    <div class="row content">
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                            <label for="">Major Code</label>
                            @if($countmajor == 0)
                                <input type="text" class="form-control" placeholder="No Information Added" name="major_Code" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:270px;" readonly>
                            @else
                                <input type="text" value="{{$major->major_code}}" class="form-control" placeholder="Major Code" name="major_title" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:270px;" readonly>
                            @endif
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                            <label for="">Major Title</label>
                            @if($countmajor == 0)
                                <input type="text" class="form-control" placeholder="No Information Added" name="major_title" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:270px;" readonly>
                            @else
                                <input type="text" value="{{$major_name->title}}" class="form-control" placeholder="Major Title" name="major_title" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:270px;" readonly>
                            @endif
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                                <label for="">Joint Degree</label>
                                <select class="form-control" name="degree" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;" readonly>
                                    <option value="">Joint Degree</option>
                                    <option value="yes">Yes</option>
                                    <option value="no" selected>No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div id="concentration">
                    <div class="row content">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Concentration Code</label>
                                <input type="text" value="No Information Entered" class="form-control"  style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:270px;" name="concentration1"  readonly>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Concentration Title</label>
                                <input type="text" value="No Information Entered" class="form-control" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:270px;" name="concentration1"  readonly>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="">
                            <h2>Exit</h2>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p>Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <form>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>


</body>
</html>
<script>
$(document).ready(function () {
    $("#type").change(function () {
        var val = $(this).val();
        if (val == "BA") {
            $("#level").html("<option value='UG' selected>Undergraduate</option>");
        } else if (val == "BS") {
            $("#level").html("<option value='UG' selected>Undergraduate</option>");
        } else if (val == "MA") {
            $("#level").html("<option value='GR' selected>Graduate</option>");
        } else if (val == "MS") {
            $("#level").html("<option value='GR' selected>Graduate</option>");
        } else if (val == "") {
            $("#level").html("<option value='' selected>Degree Level</option>");
        }
    });

    var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $("#major"); //Fields wrapper
	var add_button      = $("#addMajor"); //Add button ID
	
	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append(
                '<div class="row content"><div class="col-md-3"><div class="form-group"><input type="text" class="form-control" placeholder="Major code" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" name="major[]"></div></div><div class="col-md-1"></div><div class="col-md-3"><div class="form-group"><input type="text" class="form-control" placeholder="Major title" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" name="major_title[]"></div></div><div class="col-md-1"></div><a href="#" class="remove_field">Remove</a></div>'
                ); //add input box
		}
	});
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); x--;
	})

    
    var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $("#concentration"); //Fields wrapper
	var add_button      = $("#addConcentration"); //Add button ID
	
	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append(
                '<div class="row content"><div class="col-md-3"><div class="form-group"><input type="text" class="form-control" placeholder="Concentration code" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" name="concentration[]"></div></div><div class="col-md-1"></div><div class="col-md-3"><div class="form-group"><input type="text" class="form-control" placeholder="Concentration title" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" name="concentration_title[]"></div></div><div class="col-md-1"></div><a href="#" class="remove_field">Remove</a></div>'
                ); //add input box
		}
	});
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); x--;
	})
});
</script>