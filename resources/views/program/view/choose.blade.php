<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}


.selectize-control{
    width:400px;
    color:#6fa7d9;
    background-color:#fffbf1;
}

.selectize-input{
    background-color:#fffbf1;
    border:1px solid #6fa7d9;

}

.select-state{
    color:#6fa7d9;
    background-color:#fffbf1;
    border:1px solid #6fa7d9;
}

</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:192px;">
                            <h5 style="text-align:center; margin-top:20px; color:#a99451">View Program</h5>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li><a href="#">Program Management</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="{{url('program/view')}}" class="done">Main</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/outcome')}}" class="done">Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/structure')}}" class="done">Structure</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/assesment')}}" class="done">Assesment Methods</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/faculty')}}" class="done">Faculty & Professional Staff</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/fitness')}}" class="done">Fitness & Alignment</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/student')}}" class="done">Students</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/research')}}" class="done">Researach & Scholarly Activities</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/resource')}}" class="done">Resources</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/supportive')}}" class="done">Supportive Data</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/cost')}}" class="done">Cost & Revenue</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/community')}}" class="done">Comunity Engagement</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/public')}}" class="done">Public Diclosure & Intergrity</a>
                    </li>
                    <li>
                        <a href="#">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
            <form action="{{ route('program.view') }}" method="post">
            {{ csrf_field() }}
                <div class="row content">
                    <div class="col-md-6" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Code</label>
                            <select id="select-state" class="form-control" style="border:2px solid #6fa7d9; color:#6fa7d9; width:400px;" name="id" placeholder="Pick a state...">
                                <option value="">Select a program...</option>
                                @foreach($program as $data)
                                <option value="{{$data->program_code}}">{{$data->program_code}} - {{$data->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Submit</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
                <!-- /. ROW  -->
                
                <div class="modal" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="">
                            <h2>Exit</h2>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p>Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <form>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
</body>
</html>
<script>
$(document).ready(function () {

    $('#program').selectize({
      sortField: 'text'
    });

});
</script>