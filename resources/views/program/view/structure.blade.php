<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}

</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                        <div class="row">
                            <div class="col-md-8">
                                <h5 style="text-align:center; margin-top:20px; color:#a99451;">View Program</h5>
                            </div>
                            <div class="col-md-4">
                            </div>
                        </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Program Management</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="{{url('program/view')}}" class="done">Main</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/outcome')}}" class="done">Outcomes</a>
                    </li>
                    <li>
                    @if($countstudy == 0)
                        <a href="{{url('program/view/structure')}}" class="active">Structure</a>
                    @else
                        <a href="{{url('program/view/structure')}}" class="done" style="border: 4px solid #0b4176;">Structure</a>
                    @endif
                    </li>
                    <li>
                        <a href="{{url('program/view/assesment')}}" class="done">Assesment Methods</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/faculty')}}" class="done">Faculty & Professional Staff</a>
                    </li>
                    <li>e
                        <a href="{{url('program/view/fitness')}}" class="done">Fitness & Alignment</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/student')}}" class="done">Students</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/research')}}" class="done">Researach & Scholarly Activities</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/resource')}}" class="done">Resources</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/supportive')}}" class ="done">Supportive Data</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/cost')}}" class="done">Cost & Revenue</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/community')}}" class="done">Comunity Engagement</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/public')}}" class="done">Public Diclosure & Intergrity</a>
                    <li>
                        <a href="{{url('program/view/appendices')}}">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="{{ route('program.structure.store') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
                @endforeach
            </div>
            <div id="page-inner">
                <div class="row content">
                    <div class="col-md-8" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Hour</label>
                            <input type="number" max="999" class="form-control" placeholder="Program Hour" style="border:2px solid #6fa7d9; color:#6fa7d9; width:200px;" name="program_hour">
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-8" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Delivery Mode (Multiple Values Selection)</label>
                            <div class="multiselect2">
                                <div class="selectBox2" onclick="showCheckboxes2()">
                                <select class="form-control" style="border:2px solid #6fa7d9; color:#6fa7d9; width:700px;">
                                    <option value="" selected></option>
                                </select>
                                <div class="overSelect"></div>
                                </div>
                                <div id="checkboxes2" style="width:700px; display:none;">
                                @foreach($delivery as $data)
                                <label for="one">
                                    <input type="checkbox" id="one" name="delivery" value="{{$data->delivery_mode_code}}"/>{{$data->delivery_mode_code}} - {{$data->description}}</label>
                                @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-8" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Language of Instruction(Multiple Values Selection)</label>
                            <div class="multiselect">
                                <div class="selectBox" onclick="showCheckboxes()">
                                <select class="form-control" style="border:2px solid #6fa7d9; color:#6fa7d9; width:700px;">
                                    <option></option>
                                </select>
                                <div class="overSelect"></div>
                                </div>
                                <div id="checkboxes" style="width:700px; display:none;">
                                @foreach($language as $data)
                                <label for="one">
                                    <input type="checkbox" id="one" name="language" value="{{$data->instructional_languages_code}}"/>{{$data->instructional_languages_code}} - {{$data->description}}</label>
                                @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-8" style="display:flex">
                        <div class="form-group">
                        <label for="">Program Admission Requirements</label>
                        <input type="text" class="form-control" placeholder="Program Admission Requirements" style="border:2px solid #6fa7d9; color:#6fa7d9; width:700px;" name="admission">
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <div class="row content">
                    <div class="col-md-8" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Graduation Requiremenst</label>
                            <input type="text" class="form-control" placeholder="Program Graduation Requiremenst" style="border:2px solid #6fa7d9; color:#6fa7d9; width:700px;" name="graduation">
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <div class="row content">
                    <!-- <div class="col-md-8" style="display:flex">
                        <div class="form-group">
                        <button type="button" id="addSmes" class="bttn" style="background-color: #b1cfeb; margin-top:10px;width:280px;">Program Study Plan (Insert Row) <i class="fas fa-plus"></i></button>
                        </div>
                    </div> -->
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                        </div>
                    </div>
                </div>
                <div id="smstr">
                @if($countstudy == 0)
                <div class="row content">
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                            <label for="">Semester</label>
                            <select class="form-control" name="semester" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>
                                @foreach($semester as $data)
                                <option value="{{$data->semester_code}}">{{$data->semester_code}} - {{$data->semester_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Course</label>
                            <select class="form-control" name="course" id="course" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>
                                <option value="" selected>Course</option>
                                @foreach($course as $data)
                                <option value="{{$data->course_code}}">{{$data->course_code}} - {{$data->long_title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Course Type</label>
                            <select class="form-control" id="type" name="type" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                <option value="">Course Type</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Course Genre</label>
                            <select class="form-control" name="genre" id="genre" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                <option value="" selected disabled>Course Genre</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Credit Hour</label>
                            <select class="form-control" id="credit" name="credit" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                <option value="" selected disabled>Credit Hours</option>
                            </select>
                        </div>
                    </div>
                </div>
                @else
                @foreach($study as $data)
                <div class="row content">
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                            <label for="">Semester</label>
                            <select class="form-control" name="semester" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" readonly>
                                <option value="{{$data->semester_code}}">{{$data->semester_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Course</label>
                            <select class="form-control" name="course" id="course" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" readonly>
                                <option value="{{$data->course_code}}">{{$data->course_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Course Type</label>
                            <select class="form-control" id="type" name="type" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                <option value="{{$data->course_type}}" selected>{{$data->course_type}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Course Genre</label>
                            <select class="form-control" name="genre" id="genre" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                <option value="{{$data->course_genre}}">{{$data->course_genre}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Credit Hour</label>
                            <select class="form-control" id="credit" name="credit" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                <option value="{{$data->credit_hour}}">{{$data->credit_hour}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
                </div>
                <br><br><br>
                <div class="row content">
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group" id="gened">
                            <label for="">Total Gened</label>
                            @if($countstudy == 0)
                            <input type="text" id="totalGened" value="0" class="form-control" placeholder="Total Gened" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_gened">
                            @else
                            <input type="text" id="totalGened" value="{{$sumgen}}" class="form-control" placeholder="Total Gened" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_gened" readonly>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Total Core</label>
                        @if($countstudy == 0)
                            <input type="text" id="totalCore" value="0" class="form-control" placeholder="Total Core" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_core">
                            @else
                            <input type="text" id="totalCore" value="{{$sumcore}}" class="form-control" placeholder="Total Core" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_core" readonly>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                        <label for="">Total Concentration 1</label>
                        @if($countstudy == 0)
                            <input type="text" id="totalConcentration" value="0" class="form-control" placeholder="Total Concentration 1" style="border:2px solid #6fa7d9; color:#6fa7d9; width:240px;" name="total_concentration">
                            @else
                            <input type="text" id="totalConcentration" value="{{$sumcon}}" class="form-control" placeholder="Total Concentration 1" style="border:2px solid #6fa7d9; color:#6fa7d9; width:240px;" name="total_concentration" readonly>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                            <label for=""> Total Electives</label>
                            @if($countstudy == 0)
                            <input type="text" id="totalElect" value="0" class="form-control" placeholder="Total Electives" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_elective">
                            @else
                            <input type="text" id="totalElect" value="{{$sumelect}}" class="form-control" placeholder="Total Electives" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_elective" readonly>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                            <label for="">Total Program</label>
                            @if($countstudy == 0)
                            <input type="text" id="totalProgram" class="form-control" placeholder="Total Program" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_program">
                            @else
                            <input type="text" id="totalProgram" value="{{$sum}}" class="form-control" placeholder="Total Program" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_program" readonly>
                            @endif
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row content" >
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <div class="modal" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="">
                            <h2>Exit</h2>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p>Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>

                <div class="modal fade" id="myModal1">
                    <div class="modal-dialog">
                    <div class="modal-content" style="width:60%;">
                    
                        <!-- Modal Header -->
                        <div class="modal-header">
                        <h4 class="modal-title">Error <li class="fa fa-exclamation-triangle"></li></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                        Please Complete All Requirements to Submit The Application !
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                        
                    </div>
                    </div>
                </div>

                <div class="modal fade" id="myModal2">
                    <div class="modal-dialog">
                    <div class="modal-content" style="width:60%;">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="background-color:green; color:white;">
                        <h4 class="modal-title">Success <li class="fa fa-check"></li></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                        Application Submmited successfuly
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <form>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
</body>
</html>
<script>
$(document).ready(function () {
    $("#course").change(function () {
        var val = $(this).val();
        $("#type").html(
            "foreach($course as $data)"+
            "if($data->course_code == '+val+')"+
            "<option value='{{$data->course_type_code}}' selected>{{$data->course_type_code}}</option>"+
            "endif"+
            "endforeach"
        );
        $("#genre").html(
            "foreach($course as $data)"+
            "if($data->course_code == '+val+')"+
            "@foreach($genre as $data2)"+
            "if($data->course_genre_code_01 == $data2->course_genre_code)"+
            "<option value='{{$data->course_genre_code_01}}' selected>{{$data->course_genre_code_01}} - {{$data2->genre_description}}</option>"+
            "endif"+
            "@endforeach"+
            "endif"+
            "endforeach"
        );
        $("#credit").html(
            "foreach($course as $data)"+
            "if($data->course_code == '+val+')"+
            "<option value='{{$data->credit_hours}}' selected>{{$data->credit_hours}}</option>"+
            "endif"+
            "endforeach"
        );



        var s = $('select[name^="credit"] option:selected').map(function() {
            return this.value
        }).get()

        var sum = s.reduce((pv, cv) => {
            return pv + (parseFloat(cv) || 0);
        }, 0);

        $("#totalCore").val(sum);

    });
    
    var max_fields      = 7; //maximum input boxes allowed
	var wrapper   		= $("#smstr"); //Fields wrapper
	var add_button      = $("#addSmes"); //Add button ID
	
	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append(
                '<div class="row content">'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<select class="form-control" name="semester['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>'+
                                '@foreach($semester as $data)'+
                                '<option value="{{$data->semester_code}}">{{$data->semester_code}} - {{$data->semester_description}}</option>'+
                                '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<select class="form-control" id="course['+x+']" name="course['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>'+
                                '<option value="" selected disabled>Course</option>'+
                                '@foreach($course as $data)'+
                                '<option value="{{$data->course_code}}">{{$data->course_code}} - {{$data->long_title}}</option>'+
                                '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<select class="form-control" id="type[]" name="type" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required disabled>'+
                                '<option value="">Course Type</option>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<select class="form-control" id="genre['+x+']" name="genre['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required disabled>'+
                                '<option value="" selected disabled>Course Genre</option>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<select class="form-control" id="credit['+x+']" name="credit['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required disabled>'+
                                '<option value="" selected disabled>Credit Hours</option>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-1"></div>'+
                    '&nbsp;'+
                    '&nbsp;'+
                    '&nbsp;'+
                    '&nbsp;'+
                    '&nbsp;'+
                    '&nbsp;'+
                    '&nbsp;'+
                    '&nbsp;'+
                    '&nbsp;'+
                    '<a href="#" class="remove_field" styel="margin-left:400px;"><i class="fas fa-times"></i></a>'+
                '</div>'
                ); //add input box
		}
	});

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); x--;
	});
 
});
    var expanded = false;

    function showCheckboxes() {
    var checkboxes = document.getElementById("checkboxes");
    if (!expanded) {
        checkboxes.style.display = "block";
        expanded = true;
    } else {
        checkboxes.style.display = "none";
        expanded = false;
    }
    }

    var expanded2 = false;

    function showCheckboxes2() {
    var checkboxes2 = document.getElementById("checkboxes2");
        if (!expanded2) {
            checkboxes2.style.display = "block";
            expanded2 = true;
        } else {
            checkboxes2.style.display = "none";
            expanded2 = false;
        }
    }

    
    // $("select[name^='credit']").change(function() {
        
    // })
</script>