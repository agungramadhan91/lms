<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}

</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:192px;">
                            <h5 style="text-align:center; margin-top:20px; color:#a99451">View Program</h5>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li><a href="#">Program Management</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="{{url('program/view')}}" class="done">Main</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/outcome')}}" class="done" style="border: 4px solid #0b4176;">Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/structure')}}" class="done">Structure</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/assesment')}}" class="done">Assesment Methods</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/faculty')}}" class="done">Faculty & Professional Staff</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/fitness')}}" class="done">Fitness & Alignment</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/student')}}" class="done">Students</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/research')}}" class="done">Researach & Scholarly Activities</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/resource')}}" class="done">Resources</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/supportive')}}" class="done">Supportive Data</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/cost')}}" class="done">Cost & Revenue</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/community')}}" class="done">Comunity Engagement</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/public')}}" class="done">Public Diclosure & Intergrity</a>
                    </li>
                    <li>
                        <a href="#">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="{{ route('program.outcome.store') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
                @endforeach
            </div>
            <div id="page-inner">
                <p style="color:#0b4176; font-size:21px; text-weigth:bold; font-family:times new roman;">Program Learning Outcomes <span class="requiredStar">*</span></p>
                <br>
                <div id="plo">
                @foreach($outcome as $data)
                    <div class="row content">
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">ID</label>
                                <input type="text" class="form-control" value="{{$data->plo_code}}" placeholder="ID" name="major_Code" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:180px" readonly>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-8" style="display:flex">
                            <div class="form-group">
                                <label for="">Program Learning Outcome Description</label>
                                <textarea type="text" class="form-control" placeholder="Program Learning Outcome Description" name="major_title" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:400px" readonly>{{$data->plo_description}}</textarea>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
                <br><br><br><br><br>
                <p style="color:#0b4176; font-size:21px; text-weigth:bold; font-family:times new roman;">Program Objectives</p>
                <div id="po">
                    <!-- <div class="row content">
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="ID" name="major_Code" style="border:2px solid #6fa7d9; color:#6fa7d9;" required>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-8" style="display:flex">
                            <div class="form-group">
                                <textarea type="text" class="form-control" placeholder="Program Objective Description" name="major_title" style="border:2px solid #6fa7d9; color:#6fa7d9; width:500px" required></textarea>
                            </div>
                        </div>
                    </div> -->
                </div> <br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <div class="modal" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="">
                            <h2>Exit</h2>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p>Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <form>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
</body>
</html>
<script>
$(document).ready(function () {
    
    var max_fields      = 99; //maximum input boxes allowed
	var wrapper   		= $("#plo"); //Fields wrapper
	var add_button      = $("#addPlo"); //Add button ID
	
	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append(
                '<div class="row content"><div class="col-md-2" style="display:flex"><div class="form-group"><input type="text" class="form-control" placeholder="ID" value="{{$program->program_code}}_PLO_'+x+'" name="plo_code['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:180px" required></div><p style="color:white;">></p><span class="requiredStar">*</span></div><div class="col-md-1"></div><div class="col-md-8" style="display:flex"><div class="form-group"><textarea type="text" class="form-control" placeholder="Program Learning Outcome Description" name="plo_description['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:400px"></textarea></div><p style="color:white;">></p><span class="requiredStar">*</span></div><a href="#" class="remove_field">Remove</a></div>'
                ); //add input box
		}
	});
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); x--;
	});

    var max_fields      = 99; //maximum input boxes allowed
	var wrapper1   		= $("#po"); //Fields wrapper
	var add_button      = $("#addPo"); //Add button ID
	
	var y = 0; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(y < max_fields){ //max input box allowed
			y++; //text box increment
			$(wrapper1).append(
                '<div class="row content"><div class="col-md-2" style="display:flex"><div class="form-group"><input type="text" value="{{$program->program_code}}_PO_'+y+'" class="form-control" placeholder="ID" name="po_code['+y+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px" required></div></div><div class="col-md-1"></div><div class="col-md-8" style="display:flex"><div class="form-group"><textarea type="text" class="form-control" placeholder="Program Objective Description" name="po_description['+y+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:400px"></textarea></div></div><a href="#" class="remove_field">Remove</a></div>'
                ); //add input box
		}
	});
	
	$(wrapper1).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); x--;
	});
});
</script>