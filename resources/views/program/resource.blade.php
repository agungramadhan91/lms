<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}

</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:192px;">
                            <h5 style="text-align:center; margin-top:20px; color:#a99451">Add New Program</h5>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li><a href="#">Program Management</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="{{url('program')}}" class="done">Main</a>
                    </li>
                    <li>
                        <a href="{{url('program/outcome')}}" class="done">Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('program/structure')}}" class="done">Structure</a>
                    </li>
                    <li>
                        <a href="{{url('program/assesment')}}" class="done">Assesment Methods</a>
                    </li>
                    <li>
                    @if($faculty->pfs_code == "")
                        <a href="{{url('program/faculty')}}">Faculty & Professional Staff</a>
                    @else
                        <a href="{{url('program/faculty')}}" class="done">Faculty & Professional Staff</a>
                    @endif
                    </li>
                    <li>
                    @if($fitness->pfa_code = "")
                        <a href="{{url('program/fitness')}}">Fitness & Alignment</a>
                    @else
                        <a href="{{url('program/fitness')}}" class="done">Fitness & Alignment</a>
                    @endif
                    </li>
                    <li>
                    @if($student->psd_code = "")
                        <a href="{{url('program/student')}}" class="active">Students</a>
                    @else
                        <a href="{{url('program/student')}}" class="done">Students</a>
                    @endif
                    </li>
                    <li>
                    @if($countresearch == "0")
                        <a href="{{url('program/research')}}">Researach & Scholarly Activities</a>
                    @else
                        <a href="{{url('program/research')}}" class="done">Researach & Scholarly Activities</a>
                    @endif
                    </li>
                    <li>
                    @if($countresource == "0")
                        <a href="{{url('program/resource')}}" class="active">Resources</a>
                    @else
                        <a href="{{url('program/resource')}}" class="done" style="border: 4px solid #0b4176;">Resources</a>
                    @endif
                    </li>
                    <li>
                    @if($countsupportive == "0")
                        <a href="{{url('program/supportive')}}">Supportive Data</a>
                    @else
                        <a href="{{url('program/supportive')}}" class ="done">Supportive Data</a>
                    @endif                        
                    </li>
                    <li>
                    @if($countcost == "0")
                        <a href="{{url('program/cost')}}">Cost & Revenue</a>
                    @else
                        <a href="{{url('program/cost')}}" class="done">Cost & Revenue</a>
                    @endif
                    </li>
                    <li>
                    @if($countcommunity == "0")
                        <a href="{{url('program/community')}}">Comunity Engagement</a>
                    @else
                        <a href="{{url('program/community')}}" class="done">Comunity Engagement</a>
                    @endif
                    </li>
                    <li>
                    @if($countpublic == "0")
                        <a href="{{url('program/public')}}">Public Diclosure & Intergrity</a>
                    @else
                        <a href="{{url('program/public')}}" class="done">Public Diclosure & Intergrity</a>
                    @endif
                    <li>
                        <a href="#">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="{{ route('program.resource.store') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row content">
                    <div class="col-md-8" style="display:flex">
                        <div class="form-group">
                            <label for="">Learning Resources</label>
                            <textarea type="text" class="form-control" placeholder="" name="learning_resource" style="border:2px solid #6fa7d9; color:#6fa7d9; width:700px; background-color:#fffbf1;" readonly>No Information Entered</textarea>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                        <button type="submit" class="btn blue" style="width:190px; color:white; border:3px solid #D3D3D3;">Attach files to this item</button>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-8" style="display:flex">
                        <div class="form-group">
                            <label for="">Physical Resource</label>
                            <textarea type="text" class="form-control" placeholder="" name="physical_resource" style="border:2px solid #6fa7d9; color:#6fa7d9; width:700px; background-color:#fffbf1;" readonly>No Information Entered</textarea>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                        <button type="submit" class="btn blue" style="width:190px; color:white; border:3px solid #D3D3D3;">Attach files to this item</button>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-8" style="display:flex">
                        <div class="form-group">
                            <label for="">Fiscal Resource</label>
                            <textarea type="text" class="form-control" placeholder="" name="fiscal_resource" style="border:2px solid #6fa7d9; color:#6fa7d9; width:700px; background-color:#fffbf1;" readonly>No Information Entered</textarea>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                        <button type="submit" class="btn blue" style="width:190px; color:white; border:3px solid #D3D3D3;">Attach files to this item</button>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-8" style="display:flex">
                        <div class="form-group">
                            <label for="">Other Pertinent Information to This Requirement</label>
                            <textarea type="text" class="form-control" placeholder="" name="other_pertinent" style="border:2px solid #6fa7d9; color:#6fa7d9; width:700px; background-color:#fffbf1;" readonly>No Information Entered</textarea>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                        <button type="submit" class="btn blue" style="width:190px; color:white; border:3px solid #D3D3D3;">Attach files to this item</button>
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Save and Exit</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Save and Continue</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <button type="submit" class="btn blue" style="width:230px; color:white; border:3px solid #D3D3D3;">Attach files to this requirement</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <div class="modal" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="">
                            <h2>Exit</h2>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p>Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <form>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
</body>
</html>
<script>
</script>