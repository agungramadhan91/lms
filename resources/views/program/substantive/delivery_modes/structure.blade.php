@extends('program.substantive.sublayout')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<style>
    .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn){
        background-color:#fffbf1; 
        /* border:2px solid #6fa7d9;  */
        color:#6fa7d9;
        width: 100%;
    }
</style>
@endsection

@section('content')

    <form action="{{ route('program.substantive.delivery_modes.save') }}" method="post" id="main_academic_form">
        @csrf

        <input type="hidden" name="changes" id="changes" value="{{ $delivery != null ? 'true':'' }}">

        <div class="row content">
            <div class="col-md-8">
                <div class="form-group">
                    <label for="">Program Hour</label>
                    <input  type="number" 
                            max="999" 
                            class="form-control" 
                            placeholder="Program Hour" 
                            disabled
                            value="{{$program->required_hours}}" 
                            style="border:2px solid #6fa7d9; color:#6fa7d9; width:220px;" 
                            name="program_hour">
                </div>
            </div>
        </div><hr>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Program Delivery Mode (Multiple Selection)</label>
                    <div style="border: 2px solid {{ $delivery != null ? '#6fa7d9':'red' }}">
                        <select class="selectpicker" 
                                multiple data-live-search="true" 
                                name="delivery[]" 
                                required
                                style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;">
    
                            @php
                                $lang = "";
                            @endphp
    
                            @foreach($deliveries as $data)
                                @if($delivery != null)
                                    @for ($i = 0; $i < count($delivery); $i++)
                                        @if ($data->delivery_mode_code == $delivery[$i])
                                            <option value="{{ $delivery[$i] }}" selected>
                                                {{$data->delivery_mode_code}} - {{$data->description}}
                                            </option>
                                        @endif
                                    @endfor
                                @else    
                                    @foreach ($program_delivery as $pl)
                                        @if ($data->delivery_mode_code == $pl->delivery_code)
                                            @php
                                                $lang = $pl->delivery_code;
                                            @endphp
    
                                            <option value="{{$data->delivery_mode_code}}" selected>
                                                {{$data->delivery_mode_code}} - {{$data->description}}
                                            </option>
                                        @endif
                                    @endforeach
                                @endif
    
                                @if ($data->delivery_mode_code != $lang)
                                    <option value="{{$data->delivery_mode_code}}">
                                        {{$data->delivery_mode_code}} - {{$data->description}}
                                    </option>
                                @endif
                                {{-- <option value="{{$data->instructional_languages_code}}">
                                    {{$data->instructional_languages_code}} - {{$data->description}}
                                </option> --}}
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Program Language of Instruction (Multiple Selection)</label>
                    <div style="border: 2px solid #6fa7d9">
                        <select class="selectpicker language_field" 
                                multiple data-live-search="true" 
                                name="language[]" 
                                disabled
                                id="language_field"
                                style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;">
        
                            @php
                                $lang = "";
                            @endphp
        
                            @foreach($languages as $data)
                                <option value="{{ $data->instructional_languages_code }}" selected>
                                    {{$data->instructional_languages_code}} - {{$data->description}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="row content">
            <div class="col-md-6">
                <div class="form-group">
                <label for="">Program Admission Requirements</label>
                <input type="text" 
                        class="form-control" 
                        disabled
                        placeholder="Program Admission Requirements" 
                        style="border:2px solid #6fa7d9; color:#6fa7d9;" 
                        name="admission">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Program Graduation Requirements</label>
                    <input type="text" 
                            class="form-control" 
                            disabled
                            placeholder="Program Graduation Requiremenst" 
                            style="border:2px solid #6fa7d9; color:#6fa7d9;" 
                            name="graduation">
                </div>
            </div>
        </div>
        
        @if($countstudy == 0)
            <div id="smstr">
                <div class="row content">
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                            <label for="">Semester</label>
                            <select class="form-control" disabled name="semester" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>
                                @foreach($semester as $data)
                                <option value="{{$data->semester_code}}">{{$data->semester_code}} - {{$data->semester_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Course</label>
                            <select class="form-control" disabled name="course" id="course" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>
                                <option value="" selected>Course</option>
                                @foreach($course as $data)
                                <option value="{{$data->course_code}}">{{$data->course_code}} - {{$data->long_title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Course Type</label>
                            <select class="form-control" id="type" name="type" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                <option value="">Course Type</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Course Genre</label>
                            <select class="form-control" name="genre" id="genre" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                <option value="" selected disabled>Course Genre</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Credit Hour</label>
                            <select class="form-control" id="credit" name="credit" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                <option value="" selected disabled>Credit Hours</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        @else
            @foreach($study as $data)
                <div class="row content">
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                            <label for="">Semester</label>
                            <select class="form-control" name="semester" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" readonly>
                                <option value="{{$data->semester_code}}">{{$data->semester_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Course</label>
                            <select class="form-control" name="course" id="course" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" readonly>
                                <option value="{{$data->course_code}}">{{$data->course_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Course Type</label>
                            <select class="form-control" id="type" name="type" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                <option value="{{$data->course_type}}" selected>{{$data->course_type}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Course Genre</label>
                            <select class="form-control" name="genre" id="genre" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                <option value="{{$data->course_genre}}">{{$data->course_genre}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Credit Hour</label>
                            <select class="form-control" id="credit" name="credit" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                <option value="{{$data->credit_hour}}">{{$data->credit_hour}}</option>
                            </select>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif

        <div class="row content">
            <div class="col-md-2" style="display:flex">
                <div class="form-group" id="gened">
                    <label for="">Total Gened</label>
                    @if($countstudy == 0)
                    <input type="text" disabled id="totalGened" value="0" class="form-control" placeholder="Total Gened" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_gened">
                    @else
                    <input type="text" disabled id="totalGened" value="{{$sumgen}}" class="form-control" placeholder="Total Gened" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_gened" readonly>
                    @endif
                </div>
            </div>
            <div class="col-md-2" style="display:flex">
                <div class="form-group">
                <label for="">Total Core</label>
                @if($countstudy == 0)
                    <input type="text" disabled id="totalCore" value="0" class="form-control" placeholder="Total Core" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_core">
                    @else
                    <input type="text" disabled id="totalCore" value="{{$sumcore}}" class="form-control" placeholder="Total Core" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_core" readonly>
                    @endif
                </div>
            </div>
            <div class="col-md-2" style="display:flex">
                <div class="form-group">
                <label for="">Total Concentration 1</label>
                @if($countstudy == 0)
                    <input type="text" disabled id="totalConcentration" value="0" class="form-control" placeholder="Total Concentration 1" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_concentration">
                    @else
                    <input type="text" disabled id="totalConcentration" value="{{$sumcon}}" class="form-control" placeholder="Total Concentration 1" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_concentration" readonly>
                    @endif
                </div>
            </div>
            <div class="col-md-2" style="display:flex">
                <div class="form-group">
                    <label for=""> Total Electives</label>
                    @if($countstudy == 0)
                    <input type="text" disabled id="totalElect" value="0" class="form-control" placeholder="Total Electives" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_elective">
                    @else
                    <input type="text" disabled id="totalElect" value="{{$sumelect}}" class="form-control" placeholder="Total Electives" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_elective" readonly>
                    @endif
                </div>
            </div>
            <div class="col-md-2" style="display:flex">
                <div class="form-group">
                    <label for="">Total Program</label>
                    @if($countstudy == 0)
                    <input type="text" disabled id="totalProgram" class="form-control" placeholder="Total Program" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_program">
                    @else
                    <input type="text" disabled id="totalProgram" value="{{$sum}}" class="form-control" placeholder="Total Program" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_program" readonly>
                    @endif
                </div>
            </div>
        </div><hr>

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Effective Term</label>
                    <select class="form-control" 
                            name="effective_term"
                            id="effective_term"
                            {{ $ef_tc != null ? 'disabled':''}}
                            onchange="changeColorMain('effective')"
                            style="border:2px solid {{ $en_tc != null ? '#6fa7d9':'red'}}; 
                                background-color:#fffbf1;">
                        
                        <option value="">--------</option>
                        @foreach($terms->where('academic_year','<','2099') as $data)
                            <option value="{{$data->term_code}}"
                                {{ $ef_tc == $data->term_code ? 'selected':''}}>
                                {{$data->term_description}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
    
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">End Term</label>
                    <select class="form-control" 
                            name="end_term" 
                            id="end_term"
                            {{ $en_tc != null ? 'disabled':''}}
                            onchange="changeColorMain('end')"
                            style="border:2px solid {{ $en_tc != null ? '#6fa7d9':'red'}}; 
                                background-color:#fffbf1;">
                        
                        <option value="">--------</option>
                        @foreach($terms as $data)
                            <option value="{{$data->term_code}}"
                                {{ $en_tc == $data->term_code ? 'selected':''}}>
                                {{$data->term_description}}
                            </option>
                        @endforeach
                    </select>
                    
                </div>
            </div>
        </div><hr>
        {{-- <br><br> --}}
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    {{-- <button type="submit" class="btn btn-primary" style="width:190px; color:white; border:3px solid #D3D3D3;">Save</button> --}}
                    <a href="#" class="btn btn-primary page" id="save" name="submit[]" value="" style="border:3px solid #D3D3D3; width:190px;">Save</a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group pull-right">
                    <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#exit-modal">Cancel</button>
                </div>
            </div>
        </div>

    </form>

    @include('program.non-substantive._cancelbtn')
@endsection

@section('js')
{{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script>
        $('#program').selectpicker();
        function changeColorMain(term) {
            var eff = document.getElementById('effective_term');
            var end = document.getElementById('end_term');

            if(term == 'effective'){
                if(eff.value == ''){
                    eff.style.border = "2px solid red";
                }else{
                    eff.style.border = "2px solid #6fa7d9";
                }
            }else if(term == 'end'){
                console.log(end.value)
                if(end.value == ''){
                    end.style.border = "2px solid red";
                }else{
                    end.style.border = "2px solid #6fa7d9";
                }
            }else{
                eff.style.border = "2px solid red";
                end.style.border = "2px solid red";
            }
        }

        $(document).ready(function(){
            var eff = document.getElementById('effective_term');
            var end = document.getElementById('end_term');
            var changes = document.getElementById('changes').value;

            $('#submit').click(function(e) {
                e.preventDefault();
                // document.getElementById('click_btn').value = "submit";

                if(eff.value != '' && end.value != ''){
                    if(changes == 'true'){
                        document.getElementById("submit_form").submit();
                    }
                }else{
                    document.getElementById('title-modal').innerHTML = "Error"
                    document.getElementById('content-modal').innerHTML = "Please complete all required fields!"
                    
                    $("#message-modal").modal('show');
                }

                console.log($('#submit').val())
            })

            $('#save').click(function(e) {
                e.preventDefault();
                // document.getElementById('click_btn').value = "save";

                if(eff.value != '' && end.value != ''){
                    if(eff.value == end.value){
                        document.getElementById('title-modal').innerHTML = "Error"
                        document.getElementById('content-modal').innerHTML = "End Term must be greater than Effective Term!"
                        $("#message-modal").modal('show');
                    }else{
                        if(changes == 'true'){
                            // console.log(document.getElementById('title-modal'));
                            document.getElementById('title-modal').classList.remove("bg-danger")
                            document.getElementById('title-modal').classList.add("bg-success")
                            document.getElementById('title-modal').innerHTML = "Success"
                            document.getElementById('content-modal').innerHTML = "Changes has been saved, please submit the application!"
                            $("#message-modal").modal('show');
                        }else{
                            document.getElementById("main_academic_form").submit();
                        }
                    }
                }else{
                    if(eff.value == '' || end.value == ''){
                        document.getElementById('title-modal').innerHTML = "Error"
                        document.getElementById('content-modal').innerHTML = "Please fill the effective term and end term!"
                        // document.getElementById('title-modal-notif').classList.add = "error" 
                        $("#message-modal").modal('show');
                    }else{
                        document.getElementById('title-modal').innerHTML = "Error"
                        document.getElementById('content-modal').innerHTML = "End Term must be greater than Effective Term!"
                        // document.getElementById('title-modal-notif').classList.add = "error" 
                        $("#message-modal").modal('show');
                    }
                }
            })
        });
    </script>
@endsection