@extends('program.substantive.sublayout')

@section('content')
    <form action="{{ route('program.substantive.delivery_modes.main') }}" method="get">
        {{ csrf_field() }}
        <div class="row content">
            <div class="col-md-6" style="display:flex">
                <div class="form-group">
                    <label for="">Program Code</label>
                    <select id="select-state" 
                            class="form-control" 
                            style="border:2px solid #6fa7d9; color:#6fa7d9; width:400px;" 
                            name="program_code" 
                            placeholder="Pick a state..."
                            required>
                        <option value="">Select a program...</option>
                        @foreach($programs as $data)
                        <option value="{{$data->program_code}}">{{$data->program_code}} - {{$data->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row content">
            <div class="col-md-3">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary" style="width:143px; color:white; border:3px solid #D3D3D3;">Submit</button>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <button type="button" class="btn" style="width:143px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#exit-modal">Cancel</button>
                </div>
            </div>
        </div>
    </form>
    <!-- /. ROW  -->
    
    @include('program.non-substantive._cancelbtn')
@endsection