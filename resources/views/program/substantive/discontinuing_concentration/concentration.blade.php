<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="{{asset('assets/css/font-awesome.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>
    
.requiredStar{
    color:red;
}

</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                        <div class="row">
                            <div class="col-md-8">
                                <h5 style="text-align:center; margin-top:20px; color:#a99451;">Discontinuing an existing concentration</h5>
                            </div>
                            <div class="col-md-4">
                            </div>
                        </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Program Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                    @if($count == 0)
                        <a href="{{url('program/concentration/discontinuing/edit')}}" class="active" style="border: 4px solid #0b4176;">Main</a>
                    @else
                        <a href="{{url('program/concentration/discontinuing/edit')}}" class="done" style="border: 4px solid #0b4176;">Main</a>
                    @endif
                    </li>
                    <li>
                        <a href="{{url('program/view/outcome')}}" class="inactive">Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/structure')}}" class="inactive">Structure</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/assesment')}}" class="inactive">Assesment Methods</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/faculty')}}" class="inactive">Faculty & Professional Staff</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/fitness')}}" class="inactive">Fitness & Alignment</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/student')}}" class="inactive">Students</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/research')}}" class="inactive">Researach & Scholarly Activities</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/resource')}}" class="inactive">Resources</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/supportive')}}" class="inactive">Supportive Data</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/cost')}}" class="inactive">Cost & Revenue</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/community')}}" class="inactive">Comunity Engagement</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/public')}}" class="inactive">Public Diclosure & Intergrity</a>
                    </li>
                    <li>
                        <a href="#" class="inactive">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="{{ route('program.concentration.discontinuing.update') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
        <div class="flash-message">
            @foreach (['Error', 'warning', 'Confirmation', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <!-- <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p> -->
                    <div class="modal" id="popup1" style="display:block;">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header {{ $msg }}">
                                <h4 style="font-weight:bold;">{{ $msg }}</h4>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body">
                                <p style="font-size:17px;">{{ Session::get('alert-' . $msg) }}</p>
                            </div>  
                            <!-- Modal footer -->
                            <div class="modal-footer">
                            <!-- <a href="{{url('/')}}">
                                <button type="button" class="btn btn-danger">OK</button>
                            </a> -->
                                <button type="button" id="clickmodal" style="width:190px; color:white; border:3px solid #D3D3D3;" class="btn btn-primary" data-dismiss="modal">Ok</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
            <div id="page-inner">
                <div class="row content">
                    <div class="col-md-3" style="display:flex;">
                        <div class="form-group">
                            <label for="">Campus</label>
                            <select class="form-control" name="campus" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; float:left" readonly>
                            @if($program->campus_code == "")
                                <option value="" selected disabled>Campus</option>
                            @else
                                <option value="{{$program->campus_code}}" selected>{{$campus2->campus_code}} {{$campus2->campus_description}}</option>
                            @endif
                                @foreach($campus as $data)
                                <option value="{{$data->campus_code}}">{{$data->campus_code}} {{$data->campus_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Status</label>
                            <input type="text" class="form-control" style="border:2px solid #6fa7d9; color:#6fa7d9; width:230px; background-color:#fffbf1;" value="{{$program->status}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex;">
                        <div class="form-group">
                            <label for="">College</label>
                            <select class="form-control" name="college" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;" readonly>
                                @if($program->college_code == "")
                                    <option value="" selected disabled>College</option>
                                @else
                                    <option value="{{$program->college_code}}" selected>{{$college2->title}}</option>
                                @endif
                                    @foreach($college as $data)
                                    <option value="{{$data->college_code}}">{{$data->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>        
                <br>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Degree Type</label>
                            <select class="form-control" name="degree_type" id="type" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:230px" readonly>
                                @if($program->degree_type_code == "")
                                    <option value="" selected disabled>Degree Type</option>
                                @else
                                    <option value="{{$program->degree_type_code}}" selected>{{$type2->title}}</option>
                                @endif
                                @foreach($type as $data)
                                    <option value="{{$data->degree_type_code}}">{{$data->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Degree Level</label>
                            <select class="form-control" name="degree_level" id="level" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:230px;" readonly>
                                @if($program->degree_level_code == "")
                                    <option value="" selected disabled>Degree Level</option>
                                @else
                                    <option value="{{$program->degree_level_code}}" selected>{{$level2->title}}</option>
                                @endif
                                <!-- @foreach($level as $data)
                                <option value="{{$data->degree_level_code}}">{{$data->title}}</option>
                                @endforeach -->
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Department</label>
                            <select class="form-control" name="department" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;">
                                <!-- <option value="" selected disabled></option> -->
                                <option value="{{$department->department_code}}">{{$department->department_code}} - {{$department->title}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row content" style="margin-bottom:10px;">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Code</label>
                            <input type="text" class="form-control" placeholder="Program Code" name="code" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:270px;" value="{{$program->program_code}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Title</label>
                            <input type="text" class="form-control" placeholder="Program Title" name="title" style="border:2px solid #6fa7d9; color:#6fa7d9; background-color:#fffbf1; width:270px;" value="{{$program->title}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Short Title</label>
                            <input type="text" class="form-control" placeholder="Short Title" name="short_title" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:270px;" value="{{$program->title}}" readonly>
                        </div>
                    </div>
                </div>
                <div id="major">
                    <div class="row content">
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                            <label for="">Major Code</label>
                            @if($countmajor == 0)
                                <input type="text" class="form-control" placeholder="No Information Added" name="major_Code" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:270px;" readonly>
                            @else
                                <input type="text" value="{{$major->major_code}}" class="form-control" placeholder="Major Code" name="major_title" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:270px;" readonly>
                            @endif
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                            <label for="">Major Title</label>
                            @if($countmajor == 0)
                                <input type="text" class="form-control" placeholder="No Information Added" name="major_title" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:270px;" readonly>
                            @else
                                <input type="text" value="{{$major_name->title}}" class="form-control" placeholder="Major Title" name="major_title" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:270px;" readonly>
                            @endif
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                                <label for="">Joint Degree</label>
                                <select class="form-control" name="degree" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;" readonly>
                                    <option value="">Joint Degree</option>
                                    <option value="yes">Yes</option>
                                    <option value="no" selected>No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                @if($countconcentration == 0)
                @else
                <?php $i=1;?>
                @foreach($concentration as $data)
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Concentration Code</label>
                            <input type="text" value="{{$data->concentration_code}}" class="form-control"  style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:270px;" name="concentration{{$i}}" id="concentration{{$i}}"  readonly>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Concentration Title</label>
                            <input type="text" value="{{$data->title}}" class="form-control" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:270px;" name="concentration_title{{$i}}" id="concentration_title{{$i}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Concentration Hour</label>
                            <input type="text" value="{{$data->concentration_hour}}" class="form-control" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:150px;" name="concentration1"  readonly>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                        <button class="btn btn-warning inactivate-btn"
                            data-code = "{{$data->concentration_code}}"
                            data-id = "{{$i}}"
                            onclick="event.preventDefault();"
                            style="border:1px solid #D3D3D3; margin-top:25px"
                            title="Inactivate">
                            Edit
                        </button>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">Effective Term</label>
                            <select class="form-control" id="effterm{{$i}}" name="eff_term{{$i}}" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;" disabled>
                            <option value="{{$data->effective_term_code}}">{{$data->effective_term_code}}</option>
                                @foreach($effterm as $data)
                                <option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">End Term</label>
                            <select class="form-control" id="endterm{{$i}}" name="end_term{{$i}}" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;">
                            @if($term3->termcode == $data->end_term_code)
                            <option value="{{$term3->term_code}}" selected>{{$term3->term_code}} - {{$term3->term_description}}</option>
                            @endif
                                @foreach($endterm as $data)
                                <option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <?php $i++?>
                @endforeach
                @endif
                <div id="concentration">
                </div>
                <br>
                <br>
                <div class="row content">
                <div class="col-md-3">
                        <div class="form-group" id="save">
                            <button type="submit" class="btn btn-primary" style="width:190px; color:white; border:3px solid #D3D3D3;">Save</button>             
                </form>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <form action="{{ route('program.concentration.discontinuing.submit') }}" method="post">
                        {{ csrf_field() }}
                        <button type="submit" class="btn" style="width:190px; background-color:rgb(215, 227, 191); font-weight:bold; color:black; border:3px solid black;" data-toggle="modal" data-target="#myModal2">Submit The Application</button>
                        </form>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="">
                            <h2>Exit</h2>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p>Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>


</body>
</html>
<script>

var i = 0;
    $('.inactivate-btn').click(function(){
        i++
        // alert(i);
        var code    = $(this).data('code');
        var id = $(this).data('id');
        var concentration = document.getElementById("effterm"+id);
        concentration.style.border = "2px solid red";
        concentration.disabled = false;
        document.getElementById('effterm'+id).setAttribute("name","enableeffterm["+i+"]")
        document.getElementById('endterm'+id).setAttribute("name","enableendterm["+i+"]")
        document.getElementById('concentration'+id).setAttribute("name","enableconcentration["+i+"]")
        document.getElementById('concentration_title'+id).setAttribute("name","enabletitle["+i+"]")
        // document.getElementById('inactive_code').value = code;
        // document.getElementById('activation_field').value = status;

        // if(status == 'Inactive'){
        //     document.getElementById('en_field').value = en_tc;
        // }else{
        //     document.getElementById('en_field').value = '999999';
        // }
    });
$(document).ready(function () {
    
    var pre = document.getElementById("popup1");

    $("#clickmodal").click(function(){
        pre.style.display = "none";
    });

    $("#type").change(function () {
        var val = $(this).val();
        if (val == "BA") {
            $("#level").html("<option value='UG' selected>Undergraduate</option>");
        } else if (val == "BS") {
            $("#level").html("<option value='UG' selected>Undergraduate</option>");
        } else if (val == "MA") {
            $("#level").html("<option value='GR' selected>Graduate</option>");
        } else if (val == "MS") {
            $("#level").html("<option value='GR' selected>Graduate</option>");
        } else if (val == "") {
            $("#level").html("<option value='' selected>Degree Level</option>");
        }
    });

    var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $("#major"); //Fields wrapper
	var add_button      = $("#addMajor"); //Add button ID
	
	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append(
                '<div class="row content"><div class="col-md-3"><div class="form-group"><input type="text" class="form-control" placeholder="Major code" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" name="major[]"></div></div><div class="col-md-1"></div><div class="col-md-3"><div class="form-group"><input type="text" class="form-control" placeholder="Major title" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" name="major_title[]"></div></div><div class="col-md-1"></div><a href="#" class="remove_field">Remove</a></div>'
                ); //add input box
		}
	});
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); x--;
	})

    
    var max_fields      = 10; //maximum input boxes allowed
	var wrapper2   		= $("#concentration"); //Fields wrapper
	var add_button2     = $("#addConcentration"); //Add button ID
	
	var y = 0; //initlal text box count
	$(add_button2).click(function(e){ //on add input button click
		e.preventDefault();
		if(y < max_fields){ //max input box allowed
			y++; //text box increment
			$(wrapper2).append(
                '<div class="row content">'+
                    '<div class="col-md-3">'+
                        '<div class="form-group">'+
                            '<input type="text" class="form-control" placeholder="Concentration code" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" name="concentration['+y+']">'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-1">'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<div class="form-group">'+
                            '<input type="text" class="form-control" placeholder="Concentration title" style="border:2px solid #6fa7d9; color:#6fa7d9; width:270px;" name="concentration_title['+y+']">'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-1">'+
                    '</div>'+
                    '<a href="#" class="remove_field">Remove</a>'+
                '</div>'
                ); //add input box
		}
	});
	
	$(wrapper2).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); y--;
	})
});
</script>