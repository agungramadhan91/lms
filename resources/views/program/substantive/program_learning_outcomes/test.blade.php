@extends('program.substantive.sublayout')

@section('content')
    <table>
        @foreach ($group_plo as $data)
            <tr>
                <td width="200px">{{ $data->plo_code }}</td>
                <td width="200px">{{ $data->program_code }}</td>
                <td width="200px">{{ $data->sequence }}</td>
                <td width="200px">{{ $data->status }}</td>
            </tr>
        @endforeach
    </table>
@endsection