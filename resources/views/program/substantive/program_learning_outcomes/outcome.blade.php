@extends('program.substantive.sublayout')

@section('content')
    <div class="col-md-12">
        <form action="{{ route('program.substantive.program_learning_outcome.save_outcome') }}" method="post" id="main_outcome_form">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <p style="color:#0b4176; font-size:21px; margin-top:5px; text-weigth:bold; font-family:times new roman;">
                        Program Learning Outcomes 
                        <span class="requiredStar">*</span>
                    </p>
                    <a  href="#" 
                        style="width:180px; border:3px solid #D3D3D3;"
                        class="btn btn-primary add_row">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        Add New Record
                    </a>
                    <input type="hidden" id="code_name" value="{{$real_code}}">
                    <input type="hidden" id="count_code" value="{{$count_code}}">
                </div>
                
                <div class="col-md-6">
                    <div class="col-md-6">
                        <div class="form-group pull-right">
                            <label for="">Effective Term</label>
                            <select name="effective_term"
                                    id="effective_term" 
                                    style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:180px"
                                    class="form-control"
                                    required
                                    disabled>
                                <option value="">---------</option>
                                @foreach ($terms->where('academic_year','<','2099') as $data)
                                    <option value="{{ $data->term_code }}"
                                            {{ $plo_term_ef == $data->term_code ? 'selected':'' }}>
                                        {{ $data->term_description }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group pull-right">
                            <label for="">End Term</label>
                            <select name="end_term" 
                                    id="end_term" 
                                    style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:180px"
                                    class="form-control"
                                    onchange="checkTerm()"
                                    required
                                    disabled>
                                <option value="">---------</option>
                                @foreach ($terms as $data)
                                    @if ($plo_term_en != '')
                                        <option value="{{ $data->term_code }}"
                                                {{ $plo_term_en == $data->term_code ? 'selected':'' }}>
                                            {{ $data->term_description }}
                                        </option>
                                    @else
                                        <option value="{{ $data->term_code }}"
                                                {{ $data->term_code == '999999' ? 'selected':'' }}>
                                            {{ $data->term_description }}
                                        </option>
                                        
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="text-center">
                            <small>
                                One <i>effective term</i> and one <i>end term</i> must be chosen for all changes.<br>
                                only future terms can be used, and <i>end term</i> must be greater than <i>effective term</i>.
                            </small>
                        </div>
                    </div>
                </div>
            </div><hr>
        
            <div id="plo">
                
                @php
                    $no = 1;    
                    $changed_no = 0;    
                    $dbl_code = array();
                @endphp
                
                @foreach($group_plo as $data)
                    @php
                        $code = '';    
                    @endphp
    
                    @if ($plo_description != '')
                        @for ($i = 0; $i < count($plo_description); $i++)
                            @if ($plo_code[$i] == $data->plo_code)     
                                @php
                                    $code = $plo_code[$i];
                                @endphp  
    
                                <div class="row content">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label  for="">ID</label>
                                            <input  type="text" 
                                                    class="form-control" 
                                                    placeholder="ID" 
                                                    value="{{ $code }}"
                                                    style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:180px" readonly>
                                        </div>
                                        <input type="hidden" name="code[]" id="code{{ $no }}" value="{{ $code }}">
                                    </div><div class="col-md-1"></div>
                
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Program Learning Outcome Description</label>
                                            <textarea   type="text" 
                                                        class="form-control outcome_description" 
                                                        placeholder="Program Learning Outcome Description" 
                                                        name="description[]"
                                                        rows="5"
                                                        readonly
                                                        style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">{{ $plo_description[$i] }}</textarea>
                                        </div>
                                    </div>
    
                                </div>
                            @endif
                        @endfor
                    @endif
                        
                    @if ($data->plo_code != $code)
                        <div class="row content">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">ID</label>
                                    <input  type="text" 
                                            class="form-control" 
                                            placeholder="ID"
                                            id="dpcode{{ $no }}"
                                            value="{{ $data->plo_code }}"
                                            style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:180px" readonly>
                                </div>
                                <input type="hidden" name="code[]" id="code{{ $no }}" >
                            </div><div class="col-md-1"></div>
        
                            <div class="col-md-6" >
                                <div class="form-group">
                                    <label for="">Program Learning Outcome Description</label>
                                    <textarea   type="text" 
                                                class="form-control outcome_description" 
                                                placeholder="Program Learning Outcome Description" 
                                                name="description[]"
                                                id="description{{ $no }}"
                                                rows="5"
                                                disabled
                                                style="border:2px solid #6fa7d9; 
                                                        background-color:{{ $data->status == 'Inactive' ? '#d3d7da':'#fffbf1'}}; 
                                                        color:#6fa7d9;">{{ $data->plo_description }}</textarea>
                                </div>
                            </div>
    
                            <div class="col-md-2" style="padding-left:40px;">
                                <div class="form-group">
                                    @if ($data->status == 'Active')
                                        <label for="">Edit / Inactivate</label>
                                    @else
                                        {{-- <label for="">Edit / Activate</label> --}}
                                    @endif

                                    
                                    @if ($data->status == 'Active')
                                        <button class="btn btn-warning edit"
                                                onclick="event.preventDefault(); changeColor({{$no}})"
                                                data-code = "{{ $data->plo_code }}"
                                                style="border:1px solid #D3D3D3;"
                                                title="Edit">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </button>
                                        <button class="btn btn-danger inactivate-btn"
                                                data-url = "{{ route('program.substantive.program_learning_outcome.inactivate_outcome', $data->plo_code) }}"
                                                data-title = "Inactivate - {{ $data->plo_code }}"
                                                data-inactive_code = "{{ $data->plo_code }}"
                                                data-status = ""
                                                data-en_tc = ""
                                                onclick="event.preventDefault();"
                                                style="border:1px solid #D3D3D3;"
                                                title="Inactivate">
                                            <i class="fa fa-remove" aria-hidden="true"></i>
                                        </button>
                                    @else
                                        {{-- <button class="btn btn-secondary edit"
                                                onclick="event.preventDefault(); changeColor({{$no}})"
                                                data-code = "{{ $data->plo_code }}"
                                                style="border:1px solid #D3D3D3;"
                                                disabled
                                                title="Edit">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </button>
                                        <button class="btn btn-success inactivate-btn"
                                                data-url = "{{ route('program.substantive.program_learning_outcome.inactivate_outcome', $data->plo_code) }}"
                                                data-title = "Inactivate - {{ $data->plo_code }}"
                                                data-inactive_code = "{{ $data->plo_code }}"
                                                data-status = "{{ $data->status }}"
                                                data-en_tc = "{{ $data->end_term_code }}"
                                                onclick="event.preventDefault();"
                                                style="border:1px solid #D3D3D3;"
                                                title="Inactivate">
                                            <i class="fa fa-remove" aria-hidden="true"></i>
                                        </button> --}}
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif    
                    @php
                        $no++;
                    @endphp
                @endforeach

                <div class="wrap_row"></div>
                
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        {{-- <button class="btn btn-primary" id="save" name="submit[]" value="outcome" style="width:190px;">Save</button> --}}
                        <a href="#" class="btn btn-primary page" id="save" name="submit[]" value="outcome" style="width:190px;border:3px solid #D3D3D3;">Save</a>
                        <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#exit-modal">Cancel</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal" id="inactivate-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" id="inactivate-form" method="post">
                    @csrf

                    <div class="modal-header" id="inactivate-title"></div>
                    
                    <div class="modal-body">
                        <div class="row">
                            <input type="hidden" name="inactive_code" id="inactive_code">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Status</label>
                                    <select name="status" 
                                            id="activation_field" 
                                            style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9;"
                                            class="form-control"
                                            required>
                                        <option value="">----------</option>
                                        <option value="Active">Activate</option>
                                        <option value="Inactive">Inactivate</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <input type="hidden" name="end_term" value="999999">
                                {{-- <div class="form-group">
                                    <label for="">End Term</label>
                                    <select name="end_term" 
                                            id="en_field" 
                                            style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9;"
                                            class="form-control"
                                            required>
                                        <option value="">----------</option>
                                        @foreach ($terms as $data)
                                            <option value="{{ $data->term_code }}"
                                                {{ $data->term_code == '999999' ? 'selected':''  }}>
                                                {{ $data->term_description }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div> --}}
                            </div>
                        </div>
                    </div>  
                    
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" form="inactivate-form" style="width:143px; color:white; border:3px solid #D3D3D3;">Save</button>
                        <button type="button" class="btn" style="width:143px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-dismiss="modal">Cancel</button>
                    </div> 
                </form>
            </div>
        </div>
    </div>

    @include('program.non-substantive._cancelbtn')
    
@endsection

@section('js')
    <script>
        function changeColor(no) {
            var dpcode          = document.getElementById('dpcode'+no);
            var code            = document.getElementById('code'+no);
            var description     = document.getElementById('description'+no);
            var effective_term  = document.getElementById('effective_term');
            var end_term        = document.getElementById('end_term');
            var active_desc     = document.getElementsByClassName("active_desc");
            
            if(description.disabled != true){
                description.style.border = "2px solid #6fa7d9";
                description.disabled = true;
                description.classList.remove('active_desc')
            }else{
                description.style.border = "2px solid red";
                description.disabled = false;
                description.classList.add('active_desc')
                code.value = dpcode.value
            }

            if(active_desc.length > 0){
                effective_term.style.border = "2px solid red";
                effective_term.disabled = false;
                end_term.style.border = "2px solid red";
                end_term.disabled = false;
            }else{
                effective_term.style.border = "2px solid #6fa7d9";
                effective_term.disabled = true;
                end_term.style.border = "2px solid #6fa7d9";
                end_term.disabled = true;
            }

            console.log(description);
        }

        function checkTerm(params) {
            var ef = document.getElementById('effective_term').value;
            var en = document.getElementById('end_term').value;

            if(en <= ef){
                document.getElementById('title-modal').innerHTML = "Error"
                document.getElementById('content-modal').innerHTML = "End Term must be greater than Effective Term!"
                $("#message-modal").modal('show');
            //     document.getElementById('save').style.pointerEvents = "none";
            // }else{
            //     document.getElementById('save').style.pointerEvents = "";
            }
            console.log(en)
        }

        $(document).ready(function() {
            var eff = document.getElementById('effective_term');
            var end = document.getElementById('end_term');

            $('#submit').click(function(e) {
                e.preventDefault();
                
                if(eff.value != '' && end.value != ''){
                    // console.log('ef '+eff.value+' en '+end.value)
                    document.getElementById("submit_form").submit();
                }else{
                    document.getElementById('title-modal').innerHTML = "Error"
                    document.getElementById('content-modal').innerHTML = "Please Complete All Required Fields!"
                    // document.getElementById('title-modal-notif').classList.add = "error" 
                    $("#message-modal").modal('show');
                }

                console.log($('#submit').val())
            })

            $('#save').click(function(e) {
                e.preventDefault();
                // document.getElementById('click_btn').value = "save";
                var active_desc = document.getElementsByClassName("active_desc");
                var eff = document.getElementById('effective_term');
                var end = document.getElementById('end_term');

                if(active_desc.length > 0){
                    if(eff.value != '' && end.value != ''){
                        if(eff.value == end.value){
                            document.getElementById('title-modal').innerHTML = "Error"
                            document.getElementById('content-modal').innerHTML = "End Term must be greater than Effective Term!"
                            $("#message-modal").modal('show');
                        }else{
                            document.getElementById("main_outcome_form").submit();
                        }

                    }else{
                        document.getElementById('title-modal').innerHTML = "Error"
                        document.getElementById('content-modal').innerHTML = "Please complete all required fields before you save!"
                        $("#message-modal").modal('show');    
                    }
                }else{
                    document.getElementById('title-modal').innerHTML = "Error"
                    document.getElementById('content-modal').innerHTML = "Please complete all required fields before you save!"
                    $("#message-modal").modal('show');
                }
            })

            $('.inactivate-btn').click(function(){
                $('#inactivate-modal').modal('show');

                var url     = $(this).data('url');
                var title   = $(this).data('title');
                var code    = $(this).data('inactive_code');
                var status  = $(this).data('status');
                var en_tc  = $(this).data('en_tc');
                // console.log(url)
                document.getElementById('inactivate-form').action = url;
                document.getElementById('inactivate-title').innerHTML = title;
                document.getElementById('inactive_code').value = code;
                document.getElementById('activation_field').value = status;

                if(status == 'Inactive'){
                    document.getElementById('en_field').value = en_tc;
                }else{
                    document.getElementById('en_field').value = '999999';
                }
            }); 
        });

        $(document).ready(function() {
            var add_row     = $(".add_row");
            var wrap_row    = $(".wrap_row");
            var code_name   = document.getElementById('code_name').value;
            var count_code  = document.getElementById('count_code').value;
            var effective_term  = document.getElementById('effective_term');
            var end_term        = document.getElementById('end_term');
            var active_desc     = document.getElementsByClassName("active_desc");
            var y = parseInt(count_code);
            
            $(add_row).click(function(e){ //on add input button click
                e.preventDefault();
                y++; 

                var add_code_name = y < 10 ? code_name+'0'+y : code_name + y;
                
                $(wrap_row).append(
                    `
                    <div class="row content">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">ID</label>
                                <input  type="text" 
                                        class="form-control" 
                                        placeholder="ID"
                                        id="dpcode"
                                        value="`+ add_code_name +`"
                                        readonly
                                        style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:180px">
                            </div>
                            <input type="hidden" name="add_code[]" id="code`+ y +`" value="`+ add_code_name +`" >
                        </div><div class="col-md-1"></div>
    
                        <div class="col-md-6" >
                            <div class="form-group">
                                <label for="">Program Learning Outcome Description</label>
                                <textarea   type="text" 
                                            class="form-control outcome_description active_desc" 
                                            placeholder="Program Learning Outcome Description" 
                                            name="add_description[]"
                                            id="description`+ y +`"
                                            rows="5"
                                            style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9;"></textarea>
                            </div>
                        </div>

                        <div class="col-md-1" style="padding-left:40px;">
                            <label for="">Remove</label>
                            <button class="btn btn-danger remove_row"
                                    onclick="event.preventDefault();"
                                    style="border:1px solid #D3D3D3;"
                                    title="Remove">
                                <i class="fa fa-remove" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                    `
                ); 

                if(active_desc.length > 0){
                    effective_term.style.border = "2px solid red";
                    effective_term.disabled = false;
                    end_term.style.border = "2px solid red";
                    end_term.disabled = false;
                }else{
                    effective_term.style.border = "2px solid #6fa7d9";
                    effective_term.disabled = true;
                    end_term.style.border = "2px solid #6fa7d9";
                    end_term.disabled = true;
                }
            });

            $(wrap_row).on("click",".remove_row", function(e){ //user click on remove text
                e.preventDefault(); 
                $(this).parent('div').parent('div').remove(); 
                y--;

                if(active_desc.length > 0){
                    effective_term.style.border = "2px solid red";
                    effective_term.disabled = false;
                    end_term.style.border = "2px solid red";
                    end_term.disabled = false;
                }else{
                    effective_term.style.border = "2px solid #6fa7d9";
                    effective_term.disabled = true;
                    end_term.style.border = "2px solid #6fa7d9";
                    end_term.disabled = true;
                }
            });
        });
    </script>
@endsection