<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous"> -->
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.bootstrap3.min.css')}}" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}

hr {
    border: none;
    height: 1px;
    /* Set the hr color */
    color: #333; /* old IE */
    background-color: #333; /* Modern Browsers */
}

table {
    padding:0px;
    border-collapse: collapse;
}

.selectize-control{
    width:400px;
    color:#6fa7d9;
    background-color:#fffbf1;
}

.selectize-input{
    background-color:#fffbf1;
    border:2px solid #6fa7d9;

}
.select-state{
    color:#6fa7d9;
    background-color:#fffbf1;
}

.selected {
    background-color:red;
}
</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                            <div class="row">
                            <div class="col-md-8">
                            <a href="{{url('/home/course')}}">
                                <h5 style="text-align:center; margin-top:20px; color:#a99451;">Discontinuing an existing concentration</h5>
                            </a>
                            </div>
                            </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Course Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="#" class="inactive" style="border: 4px solid #0b4176;">Main</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/outcome')}}" class="inactive">Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/structure')}}" class="active">Structure</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/assesment')}}" class="inactive">Assesment Methods</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/faculty')}}" class="inactive">Faculty & Professional Staff</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/fitness')}}" class="inactive">Fitness & Alignment</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/student')}}" class="inactive">Students</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/research')}}" class="inactive">Researach & Scholarly Activities</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/resource')}}" class="inactive">Resources</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/supportive')}}" class="inactive">Supportive Data</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/cost')}}" class="inactive">Cost & Revenue</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/community')}}" class="inactive">Comunity Engagement</a>
                    </li>
                    <li>
                        <a href="{{url('program/view/public')}}" class="inactive">Public Diclosure & Intergrity</a>
                    </li>
                    <li>
                        <a href="#" class="inactive">Appendices</a>
                    </li>
                </ul>
            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="{{ route('program.concentration.changing.update') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
            <div class="flash-message">
            @foreach (['Error', 'warning', 'Confirmation', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <!-- <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p> -->
                    <div class="modal" id="popup1" style="display:block;">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header {{ $msg }}">
                                <h4>{{ $msg }}</h4>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body">
                                <p>{{ Session::get('alert-' . $msg) }}</p>
                            </div>  
                            <!-- Modal footer -->
                            <div class="modal-footer">
                            <!-- <a href="{{url('/')}}">
                                <button type="button" class="btn btn-danger">OK</button>
                            </a> -->
                                <button type="button" id="clickmodal" class="btn btn-primary" data-dismiss="modal">Ok</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endif
                @endforeach

                @if($countstudy != "0")
                    @if($sumcon != $concentration)
                    <p class="alert alert-danger">The Concentration Hour must Match with Total Concentration !. Please Adjust The Required Hours on Program Site<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @else
                    <p class="alert alert-success">The Concentration Hours is Match with Total Concentration<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif
                @else    
                @endif

            </div>
            <div id="page-inner">
                <div class="row content">
                    <div class="col-md-6" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Study Plan</label>
                            <select class="select-state" id="program" class="form-control" name="program" style="border:2px solid #6fa7d9; color:#6fa7d9;" required>
                                <option value="{{$program->program_code}}" selected>{{$program->program_code}} - {{$program->title}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Status</label>
                            <input type="text" value="{{$program->status}}" id="status" class="form-control" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="status" readonly>
                        </div>
                    </div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Concentration Hour</label>
                            <input type="text" value="{{$concentration}}" id="status" class="form-control" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="conhour" disabled>
                        </div>
                    </div>
                </div>
                <div class="row content">
                <form action="{{ route('program.concentration.changing.update') }}" method="post">
                {{ csrf_field() }}
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">Effective Term</label>
                            @if($count != 0)
                            <select class="form-control" id="effterm" name="eff_term" style="border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;">
                            @else
                            <select class="form-control" id="effterm" name="eff_term" style="border:2px solid red; color:#6fa7d9; width:120%; width:230px;">
                            @endif
                                <option value="{{$term2->term_code}}">{{$term2->term_code}} - {{$term2->term_description}}</option>
                                @foreach($effectiveterm as $data)
                                <option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">End Term</label>
                            @if($count != 0)
                            <select class="form-control" name="end_term" style="border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;">
                            @else
                            <select class="form-control" name="end_term" style="border:2px solid red; color:#6fa7d9; width:120%; width:230px;">
                            @endif
                                <option value="{{$term3->term_code}}">{{$term3->term_code}} - {{$term3->term_description}}</option>
                                @foreach($endterm as $data)
                                <option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </form>
                </div>
                <br><br>
                <?php $i=1;?>
                <div style="overflow: scroll; height: 460px;">
                @if($countstudy !=0)
                @foreach($study as $data)
                <div class="row content">
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Semester</label>
                                <select class="form-control" id="semester{{$i}}" name="semester[{{$i}}]" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" readonly>
                                    <option value="{{$data->semester_program_01}}" selected>{{$data->semester_program_01}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">course</label>
                                <select class="form-control" id="course{{$i}}" name="course[{{$i}}]" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" readonly>
                                    @foreach($course3 as $data2)
                                    @if($data2->course_code == $data->course_code)
                                    <option value="{{$data->course_code}}" selected>{{$data->course_code}} - {{$data2->long_title}}</option>
                                    @endif
                                    <option value="{{$data2->course_code}}">{{$data2->course_code}} - {{$data2->long_title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Course Type</label>
                                <select class="form-control" id="type{{$i}}" name="type[{{$i}}]" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" disabled>
                                <option value="{{$data->course_type_code_01}}" selected>{{$data->course_type_code_01}}</option>
                                @foreach($type as $data2)
                                <option value="{{$data2->course_type_code}}">{{$data2->course_type_code}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Course Genre</label>
                                <select class="form-control" id="genre{{$i}}" name="genre[{{$i}}]" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                <option value="{{$data->course_genre_code_01}}" selected>{{$data->course_genre_code_01}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Credit</label>
                                @if($data->course_type_code_01 == 'GEN')
                                <input type="text" value="{{$data->credit_hours}}" class="form-control" id="credit{{$i}}" name="creditgen[{{$i}}]" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required disabled>
                                @elseif($data->course_type_code_01 == 'CONC')
                                <input type="text" value="{{$data->credit_hours}}" class="form-control" id="credit{{$i}}" name="creditconc[{{$i}}]" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required disabled>
                                @elseif($data->course_type_code_01 == 'ELECT')
                                <input type="text" value="{{$data->credit_hours}}" class="form-control" id="credit{{$i}}" name="creditelect[{{$i}}]" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required disabled>
                                @elseif($data->course_type_code_01 == 'Core')
                                <input type="text" value="{{$data->credit_hours}}" class="form-control" id="credit{{$i}}" name="creditcore[{{$i}}]" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required disabled>
                                @else
                                <input type="text" value="{{$data->credit_hours}}" class="form-control" id="credit{{$i}}" name="credit[{{$i}}]" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required disabled>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                            <button class="btn btn-warning inactivate-btn"
                                data-code = "{{$data->course_code}}"
                                data-id = "{{$i}}"
                                onclick="event.preventDefault();"
                                style="border:1px solid #D3D3D3; margin-top:25px"
                                title="Inactivate">
                            Edit
                            </button>
                            </div>
                        </div>
                    </div>
                <?php $i++?>
                @endforeach
                @endif
                </div>
                <div id="txt">
                </div>
                <br>
                <div class="row content">
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group" id="gened">
                            <label for="">Total Gened</label>
                            @if($countstudy == 0)
                            <input type="text" id="totalGened" value="0" class="form-control" placeholder="Total Gened" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_gened" readonly>
                            @else
                            <input type="text" id="totalGened" value="{{$sumgen}}" class="form-control" placeholder="Total Gened" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_gened" readonly>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Total Core</label>
                        @if($countstudy == 0)
                            <input type="text" id="totalCore" value="0" class="form-control" placeholder="Total Core" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_core" readonly>
                            @else
                            <input type="text" id="totalCore" value="{{$sumcore}}" class="form-control" placeholder="Total Core" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_core" readonly>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                        <label for="">Total Concentration 1</label>
                        @if($countstudy == 0)
                            <input type="text" id="totalConcentration" value="0" class="form-control" placeholder="Total Concentration 1" style="border:2px solid #6fa7d9; color:#6fa7d9; width:240px;" name="total_concentration" readonly>
                            @else
                            <input type="text" id="totalConcentration" value="{{$sumcon}}" class="form-control" placeholder="Total Concentration 1" style="border:2px solid #6fa7d9; color:#6fa7d9; width:240px;" name="total_concentration" readonly>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                            <label for=""> Total Electives</label>
                            @if($countstudy == 0)
                            <input type="text" id="totalElect" value="0" class="form-control" placeholder="Total Electives" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_elective" readonly>
                            @else
                            <input type="text" id="totalElect" value="{{$sumelect}}" class="form-control" placeholder="Total Electives" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_elective" readonly>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                            <label for="">Total Program</label>
                            @if($countstudy == 0)
                            <input type="text" id="totalProgram" class="form-control" placeholder="Total Program" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_program" readonly>
                            @else
                            <input type="text" id="totalProgram" value="{{$sum}}" class="form-control" placeholder="Total Program" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_program" readonly>
                            @endif
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" name="exit" value="1" class="btn btn-primary" style="width:190px; color:white; border:3px solid #D3D3D3;">Save</button>
                        </div>
                    </div>
    </form>
                    <div class="col-md-3">
                        <form action="{{ route('program.concentration.changing.submit') }}" method="post">
                        {{ csrf_field() }}
                        <button type="submit" class="btn" style="width:190px; background-color:rgb(215, 227, 191); font-weight:bold; color:black; border:3px solid black;" data-toggle="modal" data-target="#myModal2">Submit The Application</button>
                        </form>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="">
                            <h2>Exit</h2>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p>Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
    
</body>
</html>
<script>
$("#program").change(function () {
        var val = $(this).val();
        if(val == "CBBAMGT"){
            $("#status").val('Pending');    
        }else{
            $("#status").val('{{$program3->status}}');
        }

    });
    
    var all_tr = $('tr');
    $('td input[type="button"]').on('click', function () {
        alert({{$i}});
        all_tr.removeClass('selected');
        $(this).closest('').addClass('selected');
    });

    var i = 0;
    $('.inactivate-btn').click(function(){
        i++
        // alert(i);
        var code    = $(this).data('code');
        var id = $(this).data('id');
        var type = document.getElementById("type"+id);
        type.style.border = "2px solid red";
        type.disabled = false;
        var type = $("#type"+id+"").val();
        document.getElementById('course'+id).setAttribute("name","enablecourse["+i+"]")
        document.getElementById('semester'+id).setAttribute("name","enablesemester["+i+"]")
        document.getElementById('genre'+id).setAttribute("name","enablegenre["+i+"]")
        document.getElementById('type'+id).setAttribute("name","enabletype["+i+"]")
        document.getElementById('credit'+id).setAttribute("name","enablecredit["+i+"]");
        // document.getElementById('activation_field').value = status;

        // if(status == 'Inactive'){
        //     document.getElementById('en_field').value = en_tc;
        // }else{
        //     document.getElementById('en_field').value = '999999';
        // }
    });

$(document).ready(function () {

    var i = 1;

    // $("#edit"+i).click(function(){
    //     var course = document.getElementById("course"+i);
    //     var type = $("#type"+i+"").val();
    //     course.style.border = "2px solid red";
    //     course.disabled = false;
    //     document.getElementById('course'+i).setAttribute("name","enablecourse["+i+"]")
    //     document.getElementById('semester'+i).setAttribute("name","enablesemester["+i+"]")
    //     document.getElementById('genre'+i).setAttribute("name","enablegenre["+i+"]")
    //     document.getElementById('type'+i).setAttribute("name","enabletype["+i+"]")
    //     if(type == "Core"){
    //         document.getElementById('credit'+i+'').setAttribute("name","enablecreditcore["+i+"]");
    //     }else if(type == "GEN"){
    //         document.getElementById('credit'+i+'').setAttribute("name","enablecreditgen["+i+"]");
    //     }else if(type == "ELECT"){
    //         document.getElementById('credit'+i+'').setAttribute("name","enablecreditelect["+i+"]");
    //     }else if(type == "CONC"){
    //         document.getElementById('credit'+i+'').setAttribute("name","enablecreditconc["+i+"]");
    //     }
    //     // alert(i);
    //     i++
    // });

    $("#edit").on('click', function () {
        $(this).closest('#course').setAttribute("name","enablecourse["+i+"]");
        i++
    });

    var pre = document.getElementById("popup1");

    $("#clickmodal").click(function(){
        pre.style.display = "none";
    });

    $('#program').selectize({
          sortField: 'text'
      });

    var max_fields      = 99; //maximum input boxes allowed
	var wrapper   		= $("#txt"); //Fields wrapper
    var add_button      = $("#addtxt"); //Add button ID

	
	var x = 0; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append(
                '<a href="#" class="remove_field" style="margin-left:90%;">remove <i class="fas fa-times"></i></a>'+
                '<div class="row content">'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Semester</label>'+
                            '<select class="form-control" name="semester['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>'+
                                '<option value=""></option>'+
                                '@foreach($semester as $data)'+
                                '<option value="{{$data->semester_code}}">{{$data->semester_code}} - {{$data->semester_description}}</option>'+
                                '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">course</label>'+
                            '<select class="select-state" id="course'+x+'" name="course['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>'+
                                '<option value=""></option>'+
                                '@foreach($course2 as $data)'+
                                '<option value="{{$data->course_code}}">{{$data->course_code}} - {{$data->long_title}}</option>'+
                                '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Course Type</label>'+
                            '<select class="form-control" id="type'+x+'" name="type['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>'+
                            '<option value=""></option>'+
                            '@foreach($type as $data)'+
                                '<option value="{{$data->course_type_code}}">{{$data->course_type_code}} - {{$data->type_description}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Course Genre</label>'+
                            '<select class="form-control" id="genre'+x+'" name="genre['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Credit</label>'+
                            '<select class="form-control" id="credit'+x+'" name="credit['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                '</div>'
            ); //add input box

            $('#course'+x+'').selectize({
                sortField: 'text'
            });

            $(wrapper).on("change","#course"+x+"", function(e){ //user click on remove text
                var val = $(this).val();
                $("#genre"+x+"").html(
                    '<option value="{{$course->course_genre_code_01}}" selected>{{$course->course_genre_code_01}}</option>'
                );
                
                $("#credit"+x+"").html(
                    '<option value="{{$course->credit_hours}}" selected>{{$course->credit_hours}}</option>'
                );
            });
            
            $(wrapper).on("change","#type"+x+"", function(e){ //user click on remove text
                var val2 = $("#type"+x+"").val();
                // alert(val2);

                if(val2 == "Core"){
                    document.getElementById('credit'+x+'').setAttribute("name","creditcore["+x+"]");
                    var s = $('select[name^="creditcore"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum = s.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalCore").val(sum);
                }else if(val2 == "GEN"){
                    document.getElementById('credit'+x+'').setAttribute("name","creditgen["+x+"]");
                    var y = $('select[name^="creditgen"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum2 = y.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalGened").val(sum2);
                }else if(val2 == "ELECT"){
                    document.getElementById('credit'+x+'').setAttribute("name","creditelect["+x+"]");
                    var y = $('select[name^="creditelect"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum2 = y.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalElect").val(sum2);
                }else if(val2 == "CONC"){
                    document.getElementById('credit'+x+'').setAttribute("name","creditconc["+x+"]");
                    var y = $('select[name^="creditconc"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum2 = y.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalConcentration").val(sum2);
                }

                var a1 = parseInt($('#totalGened').val());
                var a2 = parseInt($('#totalCore').val());
                var a3 = parseInt($('#totalElect').val());
                var a4 = parseInt($('#totalConcentration').val());

                var sum3 = a1 + a2 + a3 + a4;

                $("#totalProgram").val(sum3);
            });
		}
	});

	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    });
});
</script>