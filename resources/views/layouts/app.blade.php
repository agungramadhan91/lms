<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  
  
</head>
<body>
    <div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel" style="color:white; background-color:#301949;">
        <img src="{{ asset('assets/img/eduval_logo_final.png') }}" style="height:80px; width:80px; background-color:white; margin-right:10px">
        <div class="navbar-collapse" style="">
            <h6 style="font-size:21px;">
            <b>Curriculum and Catalogue Management System CCMS</b>
            </h6>
        </div>
    </nav>
        <nav class="navbar navbar-expand-sm" style="background-color: #0b4176; height:40px;">
            <div class="container">
                <a class="nav-item" href="{{ url('/') }}" style ="color:white;font-size:18px;">
                @guest
                    Home
                @else
                   User {{Auth::user()->id}} {{Auth::user()->name}}
                @endif
                </a>
                
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}" style ="color:white; background-color: #b49b4b;">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}" style ="color:white; background-color: #b49b4b;">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                        <li class="dropdown">
                                <a id="navbarDropdown" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style ="color:white;">Views and Reports</a>

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="background-color: #0b4176;">
                                    <a class="dropdown-item" href="{{url('program/view/choose')}}" style ="color:white; border:1px solid gray">View a Program</a>
                                    <a class="dropdown-item" href="{{url('course/view/choose')}}" style ="color:white; border:1px solid gray">View a Course</a>
                                    <a class="dropdown-item" href="{{url('college/view/choose')}}" style ="color:white; border:1px solid gray">View a College</a>
                                    <a class="dropdown-item" href="{{url('campus/view/choose')}}" style ="color:white; border:1px solid gray">View a Campus</a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <li class="dropdown">
                                <a id="navbarDropdown" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style ="color:white;">
                                   @if (Auth::user()->name == "admin")
                                    Admin
                                    @else
                                    System Admin
                                    @endif
                                    <i class="fas fa-cogs"></i>
                                    <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="background-color: #0b4176;">
                                    <a class="dropdown-item" href="{{ route('maintenance.college.index') }}" style ="color:white; border:1px solid gray">College Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.campus.index') }}" style ="color:white; border:1px solid gray">Campus Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.degree_level.index') }}" style ="color:white; border:1px solid gray">Degree Level Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.degree_type.index') }}" style ="color:white; border:1px solid gray">Degree Type Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.course_level.index') }}" style ="color:white; border:1px solid gray">Course Level Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.course_type.index') }}" style ="color:white; border:1px solid gray">Course Type Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.department.index') }}" style ="color:white; border:1px solid gray">Department Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.grading_mode.index') }}" style ="color:white; border:1px solid gray">Grading Mode Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.grading_schema.index') }}" style ="color:white; border:1px solid gray">Grading Schema Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.subject.index') }}" style ="color:white; border:1px solid gray">Subject Code Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.term.index') }}" style ="color:white; border:1px solid gray">Term Code maintenance</a>
                                    <!-- <a class="dropdown-item" href="#" style ="color:white; border:1px solid gray">Workkflow Management</a>
                                    <a class="dropdown-item" href="#" style ="color:white; border:1px solid gray">Approvals Management</a> -->
                                    <a class="dropdown-item" href="{{ route('management.role_user.index') }}" style ="color:white; border:1px solid gray">
                                        Roles, User, and Workflow
                                    </a>
                                    <a class="dropdown-item" href="#" style ="color:white; border:1px solid gray">System Reports</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" style ="color:white;">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
