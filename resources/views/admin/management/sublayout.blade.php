<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="{{ asset('assets/css/font-awesome.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
        integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />

    @yield('css')
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>
    .requiredStar {
        color: red;
    }
</style>

<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
        <div class="row"></div>
        <br>

        <div class="row">
            <div class="col-md-12">
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5 style="text-align:center; margin-top:20px; color:#a99451;">
                                        Roles and User Management 
                                    </h5>
                                </div>
                            </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;">
                                    @if(Request::segment(3) == 'index')
                                        <a href="#">Index</a>
                                    @elseif(Request::segment(3) == 'create')
                                        <a href="#">Create Role</a>
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <!-- <li>
                        <a href="{{ route('management.role_user.index') }}" class="done" >
                            Index
                        </a>
                    </li> -->
                    <li>
                        <a href="{{ route('maintenance.college.index') }}" class="done" style="{{ Request::segment(2) == 'colleges' ? 'border: 4px solid #0b4176;':'' }}">
                            College Maintenance
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('maintenance.campus.index') }}" class="done" style="{{ Request::segment(2) == 'campuses' ? 'border: 4px solid #0b4176;':'' }}">
                            Campus Maintenance
                        </a>
                    </li>
                    <li>    
                        <a href="{{ route('maintenance.degree_level.index') }}" class="done" style="{{ Request::segment(2) == 'degree_levels' ? 'border: 4px solid #0b4176;':'' }}">
                            Degree Level Maintenance
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('maintenance.degree_type.index') }}" class="done" style="{{ Request::segment(2) == 'degree_types' ? 'border: 4px solid #0b4176;':'' }}">
                            Degree Type Maintenance
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('maintenance.course_level.index') }}" class="done" style="{{ Request::segment(2) == 'course_levels' ? 'border: 4px solid #0b4176;':'' }}">
                            Course Level Maintenance
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('maintenance.course_type.index') }}" class="done" style="{{ Request::segment(2) == 'course_types' ? 'border: 4px solid #0b4176;':'' }}">
                            Course Type Maintenance
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('maintenance.department.index') }}" class="done" style="{{ Request::segment(2) == 'departments' ? 'border: 4px solid #0b4176;':'' }}">
                            Department Maintenance
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('maintenance.grading_mode.index') }}" class="done" style="{{ Request::segment(2) == 'grading_modes' ? 'border: 4px solid #0b4176;':'' }}">
                            Grading Mode Maintenance
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('maintenance.grading_schema.index') }}" class="done" style="{{ Request::segment(2) == 'grading_schemas' ? 'border: 4px solid #0b4176;':'' }}">
                            Grading Schema Maintenance
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('maintenance.program.index') }}" class="done" style="{{ Request::segment(2) == 'programs' ? 'border: 4px solid #0b4176;':'' }}">
                            Program Code Maintenance
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('maintenance.subject.index') }}" class="done" style="{{ Request::segment(2) == 'subjects' ? 'border: 4px solid #0b4176;':'' }}">
                            Subject Code Maintenance
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('maintenance.term.index') }}" class="done" style="{{ Request::segment(2) == 'terms' ? 'border: 4px solid #0b4176;':'' }}">
                            Term Code Maintenance
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('management.role_user.index') }}" class="done" style="{{ Request::segment(2) == 'role_user' ? 'border: 4px solid #0b4176;':'' }}">
                            Roles and User Management
                        </a>
                    </li>
                    
                    <li>
                        <a href="{{ route('home') }}">Home</a>
                    </li>

                </ul>
            </div>
        </nav>

        @yield('content')
    </div>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    {{-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> --}}
    <!-- <script src="{{--asset('assets/js/jquery-1.10.2.js')--}}"></script> -->
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> -->
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js" integrity="sha512-CwHUCK55pONjDxvPZQeuwKpxos8mPyEv9gGuWC8Vr0357J2uXg1PycGDPND9EgdokSFTG6kgSApoDj9OM22ksw==" crossorigin="anonymous"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <!-- <script src="{{--asset('assets/js/bootstrap.min.js')--}}"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    
    @yield('js')
    
    <script type="text/javascript">
        @if(session()->has('success'))
            Swal.fire({
                title: 'Success!',
                text: "{{ session()->get('success') }}",
                icon: 'success',
                confirmButtonText: 'Ok'
            })
        @endif
        
        @if(session()->has('error'))
            Swal.fire({
                title: 'Error!',
                text: "{{ session()->get('error') }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            })
        @endif
    </script>

    
</body>

</html>