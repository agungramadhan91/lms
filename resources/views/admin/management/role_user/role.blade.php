<div class="row content">
    <div class="col-md-3"><br>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newRoleModal">
            <i class="fa fa-plus" aria-hidden="true"></i>
            Add a New Role
        </button>

    </div>
</div><br>

<div class="row content">
    <div class="col-md-12" style="display:flex">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Role Code</th>
                    <th>Role Description</th>
                    <th>Role Level</th>
                    <th>Role Access</th>
                    <th>Role Department</th>
                    <th>Active / Inactive</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
                @foreach($roles as $role)
                    <tr>
                        <td>{{ $role->code }}</td>
                        <td>{{ $role->name }}</td>
                        <td>{{ $role->level }}</td>
                        <td>{{ $role->status }}</td>
                        <td>{{ $role->department }}</td>
                        <td>
                            @if($role->is_active == 'true')
                                <span class="label label-success">Active</span>
                            @else
                                <span class="label label-danger">Inactive</span>
                            @endif           
                        </td>
                        <td width="150px">
                            <!-- <button type="button" class="btn btn-warning" data-toggle="modal" title="View" data-target="#exampleModal-{{ $role->code }}">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                            </button> -->
                            
                            <button type="button" class="btn btn-primary" data-toggle="modal" title="Edit" data-target="#exampleModal-{{ $role->code }}">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal-{{ $role->code }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">

                                        <form action="{{ route('management.role_user.update', $role->code) }}" method="post">
                                            @csrf
                                            
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Edit Role {{ $role->code }}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-1"></div>
                                                    <div class="col-md-3" style="display:flex">
                                                        <div class="form-group">
                                                            <label for="">Role Description <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control" placeholder="" value="{{ $role->name }}" name="name"
                                                                style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-1"></div>
                                                    <div class="col-md-3" style="display:flex">
                                                        <div class="form-group">
                                                            <label for="">Role Level <span class="text-danger">*</span></label>
                                                            <select name="level" class="form-control" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;" required>
                                                                @foreach($levels as $level)
                                                                    @if($level->role_level_code == $role->role_level_code)
                                                                        <option value="{{ $level->role_level_code }}" selected>{{ $level->role_description }}</option>
                                                                    @else
                                                                        <option value="{{ $level->role_level_code }}">{{ $level->role_description }}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="col-md-1"></div>
                                                    <div class="col-md-3" style="display:flex">
                                                        <div class="form-group">
                                                            <label for="">Role Access <span class="text-danger">*</span></label>
                                                            <select name="status" class="form-control" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;" required>
                                                                <option value="true" {{ $role->is_approver == 1 ? 'selected':'' }}>Approver</option>
                                                                <option value="false" {{ $role->is_approver == 0 ? 'selected':'' }}>Reviewers</option>
                                                                <option value="2" {{ $role->is_approver == 2 ? 'selected':'' }}>Approver and Reviewers</option>
                                                                <option value="3" {{ $role->is_approver == 3 ? 'selected':'' }}>Initiator</option>
                                                                <option value="4" {{ $role->is_approver == 4 ? 'selected':'' }}>Execution</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-1"></div>
                                                    <div class="col-md-3" style="display:flex">
                                                        <div class="form-group">
                                                            <label for="">Role Department</label>
                                                            <select name="department" class="form-control" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                                                                <option value="">-----------</option>
                                                                @foreach($departments as $dep)
                                                                    @if($role->department_code == $dep->department_code)
                                                                        <option value="{{ $dep->department_code }}" selected>
                                                                            {{ $dep->department_code }} - {{ $dep->title }}
                                                                        </option>
                                                                    @else
                                                                        <option value="{{ $dep->department_code }}">
                                                                            {{ $dep->department_code }} - {{ $dep->title }}
                                                                        </option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-1"></div>
                                                    <div class="col-md-3" style="display:flex">
                                                        <div class="form-group">

                                                            <label for="">Role Status</label>
                                                            <Select class="form-control" name="activation"  style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                                                                <option value="true" {{$role->is_active == 'true' ? 'selected':'' }}>Active</option>
                                                                <option value="false" {{$role->is_active == 'false' ? 'selected':'' }}>Inactive</option>
                                                            </Select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            
        </table>
    </div>
</div><br>