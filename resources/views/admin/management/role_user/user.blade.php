<div class="row content">
    <div class="col-md-12"><br>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newUserModal">
            <i class="fa fa-plus" aria-hidden="true"></i>
            Add a New User
        </button>
    </div>
</div><br>

<div class="row content">
    <div class="col-md-12" style="display:flex">
        <table id="users-table" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>User ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>College</th>
                    <th>Department</th>
                    <th>Active / Inactive</th>
                    <th>Role</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->firstname }}</td>
                        <td>{{ $user->lastname }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->college }}</td>
                        <td>{{ $user->department }}</td>
                        <td>
                            @if($user->is_active == TRUE)
                                <span class="label label-success">Active</span>
                            @else
                                <span class="label label-danger">Inactive</span>
                            @endif           
                        </td>
                        <td>
                            @foreach($user->roles as $ru)
                                {{ $ru->role_code }}
                            @endforeach
                        </td>
                        <td width="150px">
                            
                            <button type="button" 
                                    class="btn btn-primary editUser" 
                                    data-user_id ="{{ $user->id }}"
                                    data-user_url ="{{ route('management.role_user.update_user', $user->id) }}"
                                    title="Edit">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="editUserModal-{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">

                                        <form action="{{ route('management.role_user.update_user', $user->id) }}" method="post">
                                            @csrf
                                            
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Edit User {{ $user->id }}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-3" style="display:flex">
                                                                <div class="form-group">
                                                                    <label for="">First Name <span class="text-danger">*</span></label>
                                                                    <input type="text" class="form-control" placeholder="" value="{{ $user->firstname }}" name="firstname"
                                                                        style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-3" style="display:flex">
                                                                <div class="form-group">
                                                                    <label for="">Last Name <span class="text-danger">*</span></label>
                                                                    <input type="text" class="form-control" placeholder="" value="{{ $user->lastname }}" name="lastname"
                                                                        style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-3" style="display:flex">
                                                                <div class="form-group">
                                                                    <label for="">College <span class="text-danger">*</span></label>
                                                                    <select name="college" class="form-control" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;" required>
                                                                        @foreach($colleges as $college)
                                                                            @if($college->college_code == $user->college)
                                                                                <option value="{{ $college->college_code }}" selected>{{ $college->college_code }} - {{ $college->title }}</option>
                                                                            @else
                                                                                <option value="{{ $college->college_code }}">{{ $college->college_code }} - {{ $college->title }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-3" style="display:flex">
                                                                <div class="form-group">
                                                                    <label for="">Department</label>
                                                                    <select name="department" class="form-control" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                                                                        <option value="">-----------</option>
                                                                        @foreach($departments as $department)
                                                                            @if($department->department_code == $user->department)
                                                                                <option value="{{ $department->department_code }}" selected>{{ $department->department_code }} - {{ $department->title }}</option>
                                                                            @else
                                                                                <option value="{{ $department->department_code }}">{{ $department->department_code }} - {{ $department->title }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-3" style="display:flex">
                                                                <div class="form-group">
                                                                    <label for="">ID <span class="text-danger">*</span></label>
                                                                    <input type="text" class="form-control" placeholder="" value="{{ $user->id }}" name="id" maxlength="10"
                                                                        style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;" disabled>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-3" style="display:flex">
                                                                <div class="form-group">
                                                                    <label for="">Email <span class="text-danger">*</span></label>
                                                                    <input type="email" class="form-control" placeholder="" value="{{ $user->email }}" name="email"
                                                                        style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="row">
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-3" style="display:flex">
                                                                <div class="form-group">
                                                                    <label for="">Password <span class="text-danger">*</span></label>
                                                                    <input type="password" class="form-control" placeholder="" value="" name="password"
                                                                        style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;" required>
                                                                </div>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                </div><hr>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        @php
                                                            $count_role = count($user->roles);
                                                            $count_no = 1;
                                                        @endphp

                                                        @forelse($user->roles as $r) 
                                                            <div class="row">
                                                                <div class="col-md-1"></div>
                                                                <div class="col-md-3" style="display:flex">
                                                                    <div class="form-group">
                                                                        <label for="">
                                                                            Role {{ $count_no }}
                                                                            <!-- <button class="add_field_role_edit btn btn-sm btn-primary">+</button> -->
                                                                            <span class="text-danger">*</span>
                                                                        </label>
                                                                        <select name="role_code[]" class="form-control" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                                                                            <option value="">-----------</option>
                                                                            @foreach($roles as $role)
                                                                                @if($role->role_code == $r->role_code)
                                                                                    <option value="{{ $role->role_code }}" selected>{{ $role->role_code }} - {{ $role->role_description }}</option>
                                                                                @else
                                                                                    <option value="{{ $role->role_code }}">{{ $role->role_code }} - {{ $role->role_description }}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                @php $count_no++; @endphp

                                                            </div>
                                                        @empty
                                                            <div class="row">
                                                                <div class="col-md-1"></div>
                                                                <div class="col-md-3" style="display:flex">
                                                                    <div class="form-group">
                                                                        <label for="">
                                                                            Role {{ $count_no }}
                                                                            <button class="edit_role_field btn btn-xs btn-primary">+</button>
                                                                            <span class="text-danger">*</span>
                                                                        </label>
                                                                        <select name="role_code[]" 
                                                                                class="form-control editUser" 
                                                                                onclick="event.preventDefault()"
                                                                                onchange="checkDuplicateEditRole()" 
                                                                                id="edit_role_field{{ $user->id }}{{ $count_no }}"
                                                                                style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                                                                            <option value="">-----------</option>
                                                                            @foreach($roles as $role)
                                                                                <option value="{{ $role->role_code }}">{{ $role->role_code }} - {{ $role->role_description }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                
                                                                @php $count_no++; @endphp
                                                            </div>
                                                        @endforelse

                                                        <!-- <button class="add_field_role btn btn-sm btn-primary">+ Add Role</button> -->
                                                        <div class="wrap_edit_role_fields"></div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-3" style="display:flex">
                                                                <div class="form-group">
                                                                    <label for="">
                                                                        Activation
                                                                        <span class="text-danger">*</span>
                                                                    </label>
                                                                    <!-- <label for="">Activation</label> -->

                                                                    <select name="activation" class="form-control" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                                                                        <option value="true">Active</option>
                                                                        <option value="false">Inactive</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            
        </table>
    </div>
</div>