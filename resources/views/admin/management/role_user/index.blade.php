@extends('admin.management.sublayout')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/font-awesome-4.7.0/css/font-awesome.min.css')}}">

    <style>
        .form-control {
            border:2px solid #6fa7d9; 
            background-color:#fffbf1; 
            color:#6fa7d9;";
            width:350px;
        }
        
        .error {
            border:2px solid #ff0000; 
            background-color:#fffbf1; 
            color:#6fa7d9;";
            width:350px;
        }
        /* style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"  */
    </style>
@endsection

@section('content')
    <div id="page-wrapper">
        <div id="page-inner">
            <div id="exTab2" class="">	
                <ul class="nav nav-tabs">
                    <li class="{{ session()->has('user_error') ? '':'active' }}">
                        <a href="#1" data-toggle="tab">Roles</a>
                    </li>
                    <li class="{{ session()->has('user_error') ? 'active':'' }}">
                        <a href="#2" data-toggle="tab">Users</a>
                    </li>
                    <li>
                        <a href="#3" data-toggle="tab">Workflow</a>
                    </li>
                </ul>

                <div class="tab-content ">
                    <div class="tab-pane {{ session()->has('user_error') ? '':'active' }}" id="1"> 
                        @include('admin.management.role_user.role')
                    </div>

                    <div class="tab-pane {{ session()->has('user_error') ? 'active':'' }}" id="2">
                        @include('admin.management.role_user.user')
                    </div>
                    
                    <div class="tab-pane" id="3">
                        @include('admin.management.role_user.workflow')
                    <div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-roles">
        <!-- newRoleModal -->
        <div class="modal fade" id="newRoleModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">

                    <form action="{{ route('management.role_user.store') }}" method="post">
                        @csrf
                        
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">New Role</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                            @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    @foreach($errors->all() as $error)
                                        {{ $error }}
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">
                                            Role Code <span class="text-danger">*</span>
                                        </label>
                                        <input  type="text" 
                                                class="form-control {{$errors->has('code') ? 'error':''}}" 
                                                name="code"
                                                value="{{ old('code') }}" 
                                                required>

                                        {{--@if($errors->has('code'))
                                            <div class="error">{{ $errors->first('code') }}</div>
                                        @endif --}}
                                    </div>
                                    <div class="form-group">
                                        <label for="">
                                            Role Description <span class="text-danger">*</span>
                                        </label>
                                        <input  type="text" 
                                                class="form-control {{$errors->has('name') ? 'error':''}}"
                                                name="name"
                                                value="{{ old('name') }}" 
                                                required>

                                    </div>
                                    <div class="form-group">
                                        <label for="">
                                            Role Level <span class="text-danger">*</span>
                                        </label>
                                        <select class="form-control {{$errors->has('level') ? 'error':''}}"
                                                name="level" 
                                                required>

                                            <option value="">-----------</option>
                                            @foreach($levels as $level)
                                                <option value="{{ $level->role_level_code }}"
                                                        {{ old('level') == $level->role_level_code ? 'selected':'' }}>
                                                    {{ $level->role_description }}
                                                </option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Role Status <span class="text-danger">*</span></label>
                                        <select class="form-control {{$errors->has('status') ? 'error':''}}"
                                                name="status" 
                                                required>
                                            <option value="">-----------</option>
                                            <option value="true" {{ old('status') == 'true' ? 'selected':'' }}>Approver</option>
                                            <option value="false" {{ old('status') == 'false' ? 'selected':'' }}>Reviewers</option>
                                            <option value="2" {{ old('status') == '2' ? 'selected':'' }}>Approver and Reviewers</option>
                                            <option value="3" {{ old('status') == '3' ? 'selected':'' }}>Initiator</option>
                                            <option value="4" {{ old('status') == '4' ? 'selected':'' }}>Execution</option>
                                        </select>
    
                                    </div>
                                    <div class="form-group">
                                        <label for="">Role Department</label>
                                        <select class="form-control {{$errors->has('department') ? 'error':''}}"
                                                name="department" >

                                            <option value="">-----------</option>
                                            @foreach($departments as $dep)
                                                <option value="{{ $dep->department_code }}"
                                                        {{ old('department') == $dep->department_code ? 'selected':'' }}>
                                                    {{ $dep->department_code }} - {{ $dep->title }}
                                                </option>
                                            @endforeach
                                        </select>
    
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-users">
        
        <!-- newUserModal -->
        <div class="modal fade" id="newUserModal">
            <div class="modal-dialog">
                <div class="modal-content">
    
                    <form action="{{ route('management.role_user.store_user') }}" method="post">
                        @csrf
                        
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">New User</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                            @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    @foreach($errors->all() as $error)
                                        {{ $error }}
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="modal-body">
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">First Name <span class="text-danger">*</span></label>
                                        <input  type="text" 
                                                class="form-control {{$errors->has('firstname') ? 'error':''}}" 
                                                name="firstname"
                                                id="firstname"
                                                value="{{ old('firstname') }}"
                                                required>
    
                                    </div>
                                    <div class="form-group">
                                        <label for="">Last Name <span class="text-danger">*</span></label>
                                        <input  type="text" 
                                                class="form-control {{$errors->has('lastname') ? 'error':''}}" 
                                                name="lastname" 
                                                id="lastname" 
                                                value="{{ old('lastname') }}"
                                                required>
    
                                    </div>
                                    <div class="form-group">
                                        <label for="">College <span class="text-danger">*</span></label>
                                        <select class="form-control {{$errors->has('college') ? 'error':''}}" 
                                                name="college" 
                                                id="college"
                                                value="{{ old('college') }}"
                                                required>
    
                                            <option value="">-----------</option>
                                            @foreach($colleges as $college)
                                                @if(old('college'))
                                                    <option value="{{ $college->college_code }}" selected>
                                                        {{ $college->college_code }} - {{ $college->title }}
                                                    </option>
                                                @else
                                                    <option value="{{ $college->college_code }}">
                                                        {{ $college->college_code }} - {{ $college->title }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
    
                                    </div>
                                    <div class="form-group">
                                        <label for="">Department</label>
                                        <select name="department" 
                                                id="department"
                                                value="{{ old('department') }}"
                                                class="form-control {{$errors->has('department') ? 'error':''}}">
        
                                            <option value="">-----------</option>
                                            @foreach($departments as $department)
                                                @if(old('department'))
                                                    <option value="{{ $department->department_code }}" selected>
                                                        {{ $department->department_code }} - {{ $department->title }}
                                                    </option>
                                                @else
                                                    <option value="{{ $department->department_code }}">
                                                        {{ $department->department_code }} - {{ $department->title }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
    
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">ID <span class="text-danger">*</span></label>
                                        <input  type="text" 
                                                class="form-control {{$errors->has('id') ? 'error':''}}" 
                                                name="id" 
                                                id="id" 
                                                value="{{ old('id') }}"
                                                maxlength="15"
                                                required>
    
                                    </div>
                                    <div class="form-group">
                                        <label for="">Email <span class="text-danger">*</span></label>
                                        <input  type="email" 
                                                class="form-control {{$errors->has('email') ? 'error':''}}" 
                                                name="email"
                                                id="email"
                                                value="{{ old('email') }}"
                                                required>
    
                                    </div>
                                    <div class="form-group">
                                        <label for="">Password <span class="text-danger">*</span></label>
                                        <input  type="password" 
                                                class="form-control {{$errors->has('code') ? 'error':''}}" 
                                                name="password"
                                                id="password"
                                                value="{{ old('password') }}"
                                                required>
    
                                    </div>
                                </div>
                            </div><hr>
    
                            <div class="row">
                                <div class="col-md-6" id="role_code_form">
                                    <div class="form-group" >
                                        <strong id="role_field_title1">
                                            Role 1
                                        </strong>
                                        <button class="add_role_field btn btn-xs btn-primary">+</button>
                                        <span class="text-danger">*</span>
                                        
                                        <select name="role_code[]" 
                                                class="form-control role_field" 
                                                onchange="checkDuplicateRole()" 
                                                id="role_field1"
                                                required>
        
                                            <option value="">-----------</option>
                                            @foreach($roles as $role)
                                                <option value="{{ $role->role_code }}">
                                                    {{ $role->role_code }} - {{ $role->role_description }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
        
                                    <div class="wrap_role_fields"></div>
                                </div>
                            </div>
    
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary" id="user_submit_btn">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- EditUserModal -->
        
    </div>

    <div class="modal-workflow">
        
        <!-- new Workflow Modal -->
        <div class="modal fade" id="newWorkflowModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">

                    <form action="{{ route('management.role_user.store_workflow') }}" method="post">
                        @csrf
                        
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">New Workflow</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                            @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    @foreach($errors->all() as $error)
                                        {{ $error }}
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Workflow Code <span class="text-danger">*</span></label>
                                        <input  type="text" 
                                                class="form-control {{$errors->has('workflow_code') ? 'error':''}}" 
                                                name="workflow_code"
                                                value="{{ old('workflow_code') }}" 
                                                required>

                                    </div>
                                    <div class="form-group">
                                        <label for="">Workflow Description <span class="text-danger">*</span></label>
                                        <input  type="text" 
                                                class="form-control {{$errors->has('description') ? 'error':''}}" 
                                                name="description"
                                                value="{{ old('description') }}" 
                                                required>

                                    </div>
                                </div>
                                <div class="col-md-6" id="wf_role_code_form">
                                    <div class="form-group" >
                                        <strong id="wf_role_field_title1">
                                            Role Level 1
                                        </strong>
                                        <button class="wf_add_role_field btn btn-xs btn-primary">+</button>
                                        <span class="text-danger">*</span>
                                        
                                        <select name="role_code[]" 
                                                class="form-control wf_role_field" 
                                                onchange="checkDuplicateRole2()" 
                                                id="wf_role_field1"
                                                required>
        
                                            <option value="">-----------</option>
                                            @foreach($roles as $role)
                                                <option value="{{ $role->role_code }}">
                                                    {{ $role->role_code }} - {{ $role->role_description }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
        
                                    <div class="wf_wrap_role_fields"></div>
                                </div>
                                
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary" id="wf_submit_btn">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

         <!-- edit Workflow Modal -->
         <div class="modal fade" id="editWFModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">

                    <form action="" id="formEditWF" method="post">
                        @csrf
                        
                        <div class="modal-header">
                            <h5 class="modal-title" id="wf_mod_title">Edit Workflow</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                            @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    @foreach($errors->all() as $error)
                                        {{ $error }}
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="hidden" name="workflow_code" id="wf_code" value="">
                                    
                                    <div class="form-group">
                                        <label for="">Workflow Description <span class="text-danger">*</span></label>
                                        <input  type="text" 
                                                class="form-control {{$errors->has('description') ? 'error':''}}" 
                                                name="description"
                                                id="wf_description_field"
                                                value="{{ old('description') }}" 
                                                required>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="wf_wrap_roles"></div>
                                    
                                    <div class="form-group" >
                                        <strong id="wf_role_title"></strong>
                                        <button class="edit_wf_add_role_field btn btn-xs btn-primary">+</button>
                                        <span class="text-danger">*</span>
                                        
                                        <select name="role_code[]" 
                                                onchange="checkDuplicateRole3()" 
                                                class="form-control role_no_id {{$errors->has('role_code.*') ? 'error':''}}">
        
                                            <option value="">-----------</option>
                                            @foreach($roles as $role)
                                                <option value="{{ $role->role_code }}">
                                                    {{ $role->role_code }} - {{ $role->role_description }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
        
                                    <div class="edit_wf_wrap_role_fields"></div>
                                </div>
                                
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" form="formEditWF" id="edit_wf_submit_btn" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        var y = 1; //initlal text box count
        var x = 1; //initlal text box count

        // NOTIFIKASI
        $(document).ready(function() {
            $('#example').DataTable();
            $('#users-table').DataTable();
            $('#workflow-table').DataTable();

            @if ($errors->has('user-error'))
                Swal.fire({
                    title: 'Error!',
                    text: "The user already been taken, please choose different one!",
                    icon: 'error',
                    confirmButtonText: 'Ok'
                });
            @endif
        
            @if($errors->has('code') ||
                $errors->has('name') ||
                $errors->has('level') ||
                $errors->has('status'))

                $("#newRoleModal").modal('show');
            @endif
            
            @if($errors->has('firstname') ||
                $errors->has('lastname') ||
                $errors->has('college') ||
                $errors->has('id') ||
                $errors->has('user_error') ||
                $errors->has('email'))

                $("#newUserModal").modal('show');
            @endif

            @if($errors->has('workflow_code') ||
                $errors->has('description') ||
                $errors->has('role_code.*'))

                $("#newWorkflowModal").modal('show');
            @endif            
            
            @if($errors->has('role_code.*'))
                console.log("asdfasd fasdf")
                $("#editWFModal").modal('show');
            @endif            
        });
        
        // ADD ROLE & ADD WORKFLOW
        $(document).ready(function() {
            var max_fields      = 10; //maximum input boxes allowed
            var add_role_field  = $(".add_role_field"); //Add button ID
            var wrap_role_fields= $(".wrap_role_fields"); //Fields wrapper

            var wf_add_role_field  = $(".wf_add_role_field"); //Add button ID
            var wf_wrap_role_fields= $(".wf_wrap_role_fields"); //Fields wrapper
            
            $(add_role_field).click(function(e){ //on add input button click
                e.preventDefault();
                y++; //text box increment
                $(wrap_role_fields).append(
                    `
                    <div class="form-group role_field_filled">
                        <strong id="role_field_title`+ y +`">
                            Role `+ y +`
                        </strong>
                        <button class="remove_role_field btn btn-xs btn-danger">-</button>
                        <span class="text-danger">*</span>
                        
                        <select name="role_code[]" 
                                class="form-control role_field" 
                                onchange="checkDuplicateRole()" 
                                id="role_field`+ y +`"
                                required>

                            <option value="">-----------</option>
                            @foreach($roles as $role)
                                <option value="{{ $role->role_code }}">
                                    {{ $role->role_code }} - {{ $role->role_description }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    `
                ); 
            });

            $(wrap_role_fields).on("click",".remove_role_field", function(e){ //user click on remove text
                e.preventDefault(); 
                $(this).parent('div').remove(); 
                y--;
            });
            
            $(wf_add_role_field).click(function(e){ //on add input button click
                e.preventDefault();
                x++; //text box increment
                $(wf_wrap_role_fields).append(
                    `
                    <div class="form-group wf_role_field_filled">
                        <strong id="wf_role_field_title`+ x +`">
                            Role Level `+ x +`
                        </strong>
                        <button class="wf_remove_role_field btn btn-xs btn-danger">-</button>
                        <span class="text-danger">*</span>
                        
                        <select name="role_code[]" 
                                class="form-control wf_role_field" 
                                onchange="checkDuplicateRole2()" 
                                id="wf_role_field`+ x +`"
                                required>

                            <option value="">-----------</option>
                            @foreach($roles as $role)
                                <option value="{{ $role->role_code }}">
                                    {{ $role->role_code }} - {{ $role->role_description }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    `
                ); 
            });

            $(wf_wrap_role_fields).on("click",".wf_remove_role_field", function(e){ //user click on remove text
                e.preventDefault(); 
                $(this).parent('div').remove(); 
                x--;
            });
        });

        $(document).ready(function(){
            $('.editUser').click(function(){
                var id  = $(this).data('user_id');
                var url = $(this).data('user_url');

                
            });

            $('.editWF').click(function(){
                $("#editWFModal").modal('show');

                var url         = $(this).data('wf_url');
                var code        = $(this).data('wf_code');
                var description = $(this).data('wf_description');
                var role_codes  = document.getElementsByClassName("hidden_role_code-"+code);
                var role_descs  = document.getElementsByClassName("hidden_role_desc-"+code);
                var roles_count = role_codes.length;
                var role_title  = roles_count + 1;
                var add_count   = role_title;
                var num     = 1;

                document.getElementById("formEditWF").action   = url;
                document.getElementById('wf_mod_title').innerHTML = "Edit Workflow - "+code;
                document.getElementById('wf_code').value = code;
                document.getElementById('wf_description_field').value = description;
                document.getElementById('wf_role_title').innerHTML = 'Role Level '+role_title;

                if($(".group_roles").length > 0){
                    $(".group_roles").remove();
                    $(".wf_role_field_filled").remove();
                    // add_count = role_title;
                }

                for (let index = 0; index < role_codes.length; index++) {
                    const code = role_codes[index];
                    const desc = role_descs[index];
                    const add_id  = num + 1;

                    $('.role_no_id').attr('id', 'wf_role_field'+add_id);

                    if(num > 1){
                        $('.wf_wrap_roles').append(`
                            <div class="form-group group_roles">
                                <strong>
                                    Role Level `+ num +`
                                </strong>
                                <span class="text-danger">*</span>
                                
                                <select name="role_code[]" 
                                        onchange="checkDuplicateRole3()"
                                        class="form-control wf_role_field {{$errors->has('role_code.*') ? 'error':''}}" 
                                        id="wf_role_field`+num+`">

                                    <option value="">--REMOVE--</option>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->role_code }}" {{ $role->role_code === '`+code.value+`' ? 'selected':'' }}>
                                            {{ $role->role_code }} - {{ $role->role_description }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        `);
                    }else{
                        $('.wf_wrap_roles').append(`
                            <div class="form-group group_roles">
                                <strong>
                                    Role Level `+ num +`
                                </strong>
                                <span class="text-danger">*</span>
                                
                                <select name="role_code[]" 
                                        onchange="checkDuplicateRole3()"
                                        class="form-control wf_role_field {{$errors->has('role_code.*') ? 'error':''}}" 
                                        id="wf_role_field`+num+`">

                                    <option value="`+code.value+`">`+code.value+` - `+desc.value+`</option>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->role_code }}" {{ $role->role_code === '`+code.value+`' ? 'selected':'' }}>
                                            {{ $role->role_code }} - {{ $role->role_description }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        `);
                    }
                    
                    document.getElementById('wf_role_field'+num).value = code.value;
                    console.log(code.value+' - '+desc.value)
                    console.log('=='+document.getElementById('wf_role_field'+num).value)
                    num++;
                }

                console.log('======================')
            
                var edit_wf_add_role_field   = $(".edit_wf_add_role_field"); //Add button ID
                var edit_wf_wrap_role_fields = $(".edit_wf_wrap_role_fields"); //Fields wrapper

                $(edit_wf_add_role_field).click(function(e){ //on add input button click
                    e.preventDefault();
                    add_count++; //text box increment
                    $(edit_wf_wrap_role_fields).append(
                        `
                        <div class="form-group wf_role_field_filled">
                            <strong id="wf_role_field_title`+ add_count +`">
                                Role Level `+ add_count +`
                            </strong>
                            <button class="edit_wf_remove_role_field btn btn-xs btn-danger">-</button>
                            <span class="text-danger">*</span>
                            
                            <select name="role_code[]" 
                                    onchange="checkDuplicateRole3()"
                                    class="form-control wf_role_field"
                                    id="wf_role_field`+ add_count +`">

                                <option value="">-----------</option>
                                @foreach($roles as $role)
                                    <option value="{{ $role->role_code }}">
                                        {{ $role->role_code }} - {{ $role->role_description }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        `
                    ); 
                });

                $(edit_wf_wrap_role_fields).on("click",".edit_wf_remove_role_field", function(e){ //user click on remove text
                    e.preventDefault(); 
                    $(this).parent('div').remove(); 
                    add_count--;
                });
            });
        });

        function wf_edit_form(wf_no){
            var wf_role = document.getElementsByClassName("wf_edit_role_field"+wf_no);

            // console.log('hwo many rows? '+wf_role)
            for (let i = 1; i <= wf_role.length; i++) {
                var wf = wf_no.toString();
                
                var wf_edit_role_field  = $("#wf_edit_role_field"+wf_no+i); //Add button ID
                var wf_wrap_role_fields = $("#wf_wrap_role_fields"+wf_no+i); //Fields wrapper
                var z = 1;
                
                $(wf_edit_role_field).click(function(e){ //on add input button click
                    console.log(wf+""+i)
                    // e.preventDefault();
                    z++; //text box increment
                    $(wf_wrap_role_fields).append(
                        `
                        <div class="form-group">
                            <strong id="wf_role_field_title`+ z +`">
                                Role Level `+ z +`
                            </strong>
                            <button class="wf_remove_role_field btn btn-xs btn-danger">-</button>
                            <span class="text-danger">*</span>
                            
                            <select name="role_code[]" 
                                    class="form-control wf_role_field" 
                                    onchange="checkDuplicateRole2()" 
                                    id="wf_role_field`+ z +`"
                                    required>

                                <option value="">-----------</option>
                                @foreach($roles as $role)
                                    <option value="{{ $role->role_code }}">
                                        {{ $role->role_code }} - {{ $role->role_description }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        `
                    ); 
                });

                $(wf_wrap_role_fields).on("click",".wf_remove_role_field", function(e){ //user click on remove text
                    e.preventDefault(); 
                    $(this).parent('div').remove(); 
                    z--;
                });
            }
        }

        function checkDuplicateRole(){
            var total_role_field = document.getElementsByClassName("role_field").length
            // console.log("total role field "+total_role_field)
            for (let index = 1; index <= total_role_field; index++) {
                var role_field = document.getElementById('role_field'+index).value;

                if (index > 1 && role_field !== "")
                {
                    var before = index - 1;
                    for (let index2 = 1; index2 <= total_role_field; index2++) 
                    {
                        if(index2 !== index)
                        {
                            var role_field_before = document.getElementById('role_field'+index2).value

                            if (role_field === role_field_before) {
                                document.getElementById("user_submit_btn").disabled = true;
                                Swal.fire({
                                    title: 'Warning!',
                                    text: "The "+ role_field +" role already been taken, please choose different one!",
                                    icon: 'info',
                                    confirmButtonText: 'Ok'
                                });
                            }else{
                                document.getElementById("user_submit_btn").disabled = false;
                            }
                        }
                    } 
                }                
            }
        }
        
        function checkDuplicateRole2(){
            var total_role_field = document.getElementsByClassName("wf_role_field").length
            // console.log("total role field "+total_role_field)
            for (let index = 1; index <= total_role_field; index++) {
                var role_field = document.getElementById('wf_role_field'+index).value;

                if (index > 1 && role_field !== "")
                {
                    var before = index - 1;
                    for (let index2 = 1; index2 <= total_role_field; index2++) 
                    {
                        if(index2 !== index)
                        {
                            var role_field_before = document.getElementById('wf_role_field'+index2).value;
                            
                            if (role_field === role_field_before) {
                                document.getElementById("wf_submit_btn").disabled = true;
                                Swal.fire({
                                    title: 'Warning!',
                                    text: "The "+ role_field +" role already been taken, please choose different one!",
                                    icon: 'info',
                                    confirmButtonText: 'Ok'
                                });
                            }else{
                                document.getElementById("wf_submit_btn").disabled = false;
                            }
                        }
                    } 
                }                
            }
        }

        function checkDuplicateRole3(){
            var total_role_field = document.getElementsByClassName("wf_role_field").length
            // console.log("total role field "+total_role_field)
            for (let index = 1; index <= total_role_field; index++) {
                var role_field = document.getElementById('wf_role_field'+index).value;

                if (index > 1 && role_field !== "")
                {
                    var before = index - 1;
                    for (let index2 = 1; index2 <= total_role_field; index2++) 
                    {
                        if(index2 !== index)
                        {
                            var role_field_before = document.getElementById('wf_role_field'+index2).value;
                            
                            if (role_field === role_field_before) {
                                document.getElementById("edit_wf_submit_btn").disabled = true;
                                Swal.fire({
                                    title: 'Warning!',
                                    text: "The "+ role_field +" role already been taken, please choose different one!",
                                    icon: 'info',
                                    confirmButtonText: 'Ok'
                                });
                            }else{
                                document.getElementById("edit_wf_submit_btn").disabled = false;
                            }
                        }
                    } 
                }                
            }
        }

    </script>
@endsection
