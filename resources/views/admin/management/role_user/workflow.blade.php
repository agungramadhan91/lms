<div class="row content">
    <div class="col-md-3"><br>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newWorkflowModal">
            <i class="fa fa-plus" aria-hidden="true"></i>
            Add a New Workflow
        </button>
    </div>
</div><br>

<div class="row content">
    <div class="col-md-12" style="display:flex">
        <table id="workflow-table" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Workflow Code</th>
                    <th>Workflow Code Description</th>
                    <th>Sequence</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
                
                @foreach($workflows as $flow)
                    <tr>
                        <td>{{ $flow->workflow_code }}</td>
                        <td>{{ $flow->workflow_code_description }}</td>
                        <td>{{ $flow->sequence }}</td>

                        <td width="150px">
                            <button type="button" class="btn btn-warning" data-toggle="modal" title="View" data-target="#flowLevelModal-{{ $flow->workflow_code }}">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                            </button>
                            
                            <button type="button" 
                                    class="btn btn-primary editWF"
                                    data-wf_url = "{{ route('management.role_user.update_workflow', $flow->workflow_code) }}"
                                    data-wf_code = "{{ $flow->workflow_code }}"
                                    data-wf_description = "{{ $flow->workflow_code_description }}"
                                    title="Edit">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </button>

                            @for($x = 1; $x <=25; $x++)
                                @if($flow->{"role_code_". $x} != NULL)
                                    <input type="hidden" class="hidden_role_code-{{ $flow->workflow_code }}" name="role_code[]" value="{{ $flow->{"role_code_". $x} }}">
                                    <input type="hidden" class="hidden_role_desc-{{ $flow->workflow_code }}" name="role_description[]" value="{{ $flow->{"role_description_". $x} }}">
                                @endif
                            @endfor

                            <!-- Modal Sequence -->
                            <div class="modal fade" id="flowLevelModal-{{ $flow->workflow_code }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Workflow {{ $flow->workflow_code }}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">                                                           
                                            <table id="users-table" class="table table-striped table-bordered" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>Level</th>
                                                        <th>Role Code</th>
                                                        <th>Role Description</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @for($x = 1; $x <=25; $x++)
                                                        @if($flow->{"role_code_". $x} != NULL)
                                                            <tr>
                                                                <td>{{ $x }}</td>
                                                                <td>{{ $flow->{"role_code_". $x} }}</td>
                                                                <td>{{ $flow->{"role_description_". $x} }}</td>
                                                            </tr>
                                                        @endif
                                                    @endfor
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </td>
                    </tr>

                @endforeach
            </tbody>
            
        </table>
    </div>
</div>