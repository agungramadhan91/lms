@extends('admin.management.sublayout')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/font-awesome-4.7.0/css/font-awesome.min.css')}}">

    <style>
        .form-control {
            border:2px solid #6fa7d9; 
            background-color:#fffbf1; 
            color:#6fa7d9;";
            width:350px;
        }
        
        .error {
            border:2px solid #ff0000; 
            background-color:#fffbf1; 
            color:#6fa7d9;";
            width:350px;
        }
        /* style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"  */
    </style>
@endsection

@section('content')
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row content">
            <div class="col-md-3"><br>
                <button type="button" class="btn btn-primary" id="newTerm">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    Add a New Term
                </button>
            </div>
        </div><br>

        <div class="row content">
            <div class="col-md-12">
                <table id="terms-table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Term Code</th>
                            <th>Term Description</th>
                            <th>Academic Year</th>
                            <th>Calendar Year</th>
                            <th>Term Start Date</th>
                            <th>Term End Date</th>
                            <th>Active / Inactive</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($terms as $c)
                            <tr>
                                <td>{{ $c->term_code }}</td>
                                <td>{{ $c->term_description }}</td>
                                <td>{{ $c->academic_year }}</td>
                                <td>{{ $c->calender_year }}</td>
                                <td>{{ $c->term_start_date }}</td>
                                <td>{{ $c->term_end_date }}</td>
                                <td>
                                    @if($c->is_active == true)
                                        <span class="label label-success">Active</span>
                                    @else
                                        <span class="label label-danger">Inactive</span>
                                    @endif           
                                </td>
                                <td>
                                    <button type="button" 
                                            class="btn btn-primary editTerm"
                                            data-url = "{{ route('maintenance.term.update', $c->term_code) }}" 
                                            data-code="{{ $c->term_code }}" 
                                            data-description="{{ $c->term_description }}" 
                                            data-academic="{{ $c->academic_year }}" 
                                            data-calendar="{{ $c->calender_year }}"  
                                            data-sequence="{{ $c->sequence }}"  
                                            data-start="{{ $c->term_start_date }}" 
                                            data-end="{{ $c->term_end_date }}" 
                                            data-active="{{ $c->is_active }}"
                                            title="Edit">

                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </button>

                                </td>
                            </tr>
                        @empty
                        @endforelse
                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>
</div>

<!-- new Modal -->
<div class="modal fade" id="termModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <form action="{{ route('maintenance.term.store') }}" id="termForm" method="post">
                @csrf
                
                <div class="modal-header">
                    <h5 class="modal-title" id="term_title">New Term</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    @if($errors->any())
                        <div class="alert alert-danger" role="alert">
                            @foreach($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="code_group"></div>

                            <input type="hidden" name="sequence" id="sequence">
                            <div class="form-group">
                                <label for="">Term Description <span class="text-danger">*</span></label>
                                <input  type="text" 
                                        class="form-control {{$errors->has('description') ? 'error':''}}" 
                                        value="{{ old('description') }}" 
                                        name="description"
                                        id="description"
                                        required>
                            </div>
                            <div class="form-group">
                                <label for="">Academic Year <span class="text-danger">*</span></label>
                                <input  type="number" 
                                        class="form-control {{$errors->has('academic_year') ? 'error':''}}" 
                                        value="{{ old('academic_year') }}" 
                                        id="academic_year"
                                        name="academic_year"
                                        required>
                            </div>
                            <div class="form-group">
                                <label for="">Calendar Year <span class="text-danger">*</span></label>
                                <input  type="number" 
                                        class="form-control {{$errors->has('calendar_year') ? 'error':''}}" 
                                        value="{{ old('calendar_year') }}" 
                                        id="calendar_year"
                                        name="calendar_year"
                                        required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Term Start Date <span class="text-danger">*</span></label>
                                <input  type="date" 
                                        class="form-control {{$errors->has('term_start') ? 'error':''}}" 
                                        value="{{ old('term_start') }}" 
                                        id="term_start"
                                        name="term_start"
                                        required>
                            </div>
                            <div class="form-group">
                                <label for="">Term End Date <span class="text-danger">*</span></label>
                                <input  type="date" 
                                        class="form-control {{$errors->has('term_end') ? 'error':''}}" 
                                        value="{{ old('term_end') }}" 
                                        id="term_end"
                                        name="term_end"
                                        required>  
                            </div>

                            <div id="activation_group"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" form="termForm" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            @if($errors->has('code') ||
                $errors->has('title') ||
                $errors->has('datetime'))

                $("#termModal").modal('show');
            @endif

            $('#newTerm').click(function(){
                $("#termModal").modal('show');

                var isset_code      = $('#code_form').length;
                var isset_active    = $('#active_form').length;

                if(isset_code == 0){
                    $("#code_group").append(`
                        <div class="form-group" id="code_form">
                            <label for="">Term Code <span class="text-danger">*</span></label>
                            <input  type="text" 
                                    class="form-control {{$errors->has('code') ? 'error':''}}" 
                                    placeholder="" 
                                    id="code"
                                    value="{{ old('code') }}" 
                                    name="code"
                                    required>
                        </div>
                    `);

                    if(isset_active == 1){
                        document.getElementById("active_form").remove();
                    }
                }else{
                    document.getElementById("code").value = "";
                }

                document.getElementById("term_title").innerHTML = "Add a New Term";
                document.getElementById("description").value = "";
                document.getElementById("academic_year").value = "";
                document.getElementById("calendar_year").value = "";
                document.getElementById("term_start").value = "";
                document.getElementById("term_end").value = "";
            });

            $('.editTerm').click(function(){
                $("#termModal").modal('show');

                var isset_code      = $('#code_form').length;
                var isset_active    = $('#active_form').length;
                var url             = $(this).data('url');
                var code            = $(this).data('code');
                var description     = $(this).data('description');
                var academic        = $(this).data('academic');
                var calendar        = $(this).data('calendar');
                var sequence        = $(this).data('sequence');
                var start           = $(this).data('start');
                var end             = $(this).data('end');
                var active          = $(this).data("active");
                // console.log(url)
                if(isset_active == 0){
                    $('#activation_group').append(`
                        <div class="form-group" id="active_form">
                            <label for="">Activation <span class="text-danger">*</span></label>
                            <select name="activation" id="activation" class="form-control" required>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>   
                        </div>
                        <input type="hidden" name="code" id="code_hidden">
                    `);
                    document.getElementById("activation").value     = active;
                    document.getElementById("code_hidden").value     = code;
                        
                    if(isset_code == 1){
                        document.getElementById("code_form").remove();
                    }
                }else{
                    document.getElementById("activation").value     = active;
                }

                document.getElementById("termForm").action   = url;
                document.getElementById("term_title").innerHTML = "Edit Term - "+code;
                document.getElementById("description").value = description;
                document.getElementById("academic_year").value = academic;
                document.getElementById("calendar_year").value = calendar;
                document.getElementById("sequence").value = sequence;
                document.getElementById("term_start").value = start;
                document.getElementById("term_end").value = end;
            });

            // $(".submitForm").click(function(){
                // document.getElementById("termForm").submit();
            // });

            $('#terms-table').DataTable();
        });
    </script>
@endsection