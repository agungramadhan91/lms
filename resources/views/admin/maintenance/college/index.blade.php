@extends('admin.management.sublayout')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/font-awesome-4.7.0/css/font-awesome.min.css')}}">

    <style>
        .form-control {
            border:2px solid #6fa7d9; 
            background-color:#fffbf1; 
            color:#6fa7d9;";
            width:350px;
        }
        
        .error {
            border:2px solid #ff0000; 
            background-color:#fffbf1; 
            color:#6fa7d9;";
            width:350px;
        }
        /* style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"  */
    </style>
@endsection

@section('content')
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row content">
            <div class="col-md-3">
                <!-- <a href="{{-- route('management.role_user.create') --}}" class="btn btn-primary">New Role</a> -->
                <!-- Button trigger modal --><br>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#collegeModal">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    Add a New College
                </button>
            </div>
        </div><br>

        <div class="row content">
            <div class="col-md-12">
                <table id="colleges-table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Title</th>
                            <th>Campus Code</th>
                            <th>Effective Term Code</th>
                            <th>End Term Code</th>
                            <th>Active / Inactive</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($colleges as $c)
                            <tr>
                                <td>{{ $c->college_code }}</td>
                                <td>{{ $c->title }}</td>
                                <td>{{ $c->campus_code }}</td>
                                <td>{{ $c->effective_term_code }}</td>
                                <td>{{ $c->end_term_code }}</td>
                                <td>
                                    @if($c->is_active == TRUE)
                                        <span class="label label-success">Active</span>
                                    @else
                                        <span class="label label-danger">Inactive</span>
                                    @endif           
                                </td>
                                <td>
                                    <button type="button" 
                                            class="btn btn-primary" 
                                            data-toggle="modal" 
                                            title="Edit" 
                                            data-target="#collegeEditModal-{{ $c->college_code }}">

                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </button>

                                    <div class="modal fade" id="collegeEditModal-{{ $c->college_code }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                    
                                                <form action="{{ route('maintenance.college.update', $c->college_code) }}" method="post">
                                                    @csrf
                                                    
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Edit College - {{ $c->college_code }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                    
                                                        @if($errors->any())
                                                            <div class="alert alert-danger" role="alert">
                                                                @foreach($errors->all() as $error)
                                                                    {{ $error }}
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="modal-body">
                                                        <input type="hidden" name="code" value="{{ $c->college_code }}" >
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="">Title <span class="text-danger">*</span></label>
                                                                            <input  type="text" 
                                                                                    class="form-control {{$errors->has('title') ? 'error':''}}" 
                                                                                    value="{{ $c->title }}" 
                                                                                    name="title"
                                                                                    style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"
                                                                                    required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6" style="display:flex">
                                                                        <div class="form-group">
                                                                            <label for="">Campus Code <span class="text-danger">*</span></label>
                                                                            <select name="campus_code" 
                                                                                    class="form-control"
                                                                                    style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"
                                                                                    required>
                                                                                <option value="">------------</option>
                                                                                @foreach ($campuses as $cps)
                                                                                    <option value="{{ $cps->campus_code }}" {{ $cps->campus_code == $c->campus_code ? 'selected':'' }}>
                                                                                        {{ $cps->campus_code }} - {{ $cps->campus_description }}
                                                                                    </option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-6" style="display:flex">
                                                                        <div class="form-group">
                                                                            <label for="">Effective Term Code <span class="text-danger">*</span></label>
                                                                            <select name="effective_tc" 
                                                                                    onchange="checkTerm()"
                                                                                    required
                                                                                    class="form-control effective_tc"
                                                                                    style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                                                                                <option value="">------------</option>
                                            
                                                                                @foreach ($terms->where('academic_year','<','2099') as $t)
                                                                                    <option value="{{ $t->term_code }}" {{ $c->effective_term_code == $t->term_code ? 'selected':'' }}>
                                                                                        {{ $t->term_description }}
                                                                                    </option>
                                                                                @endforeach
                                                                            </select>   
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6" style="display:flex">
                                                                        <div class="form-group">
                                                                            <label for="">End Term Code <span class="text-danger">*</span></label>
                                                                            <select name="end_tc" 
                                                                                    onchange="checkTerm()"
                                                                                    required
                                                                                    class="form-control end_tc"
                                                                                    style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                                                                                <option value="">------------</option>
                                            
                                                                                @foreach ($terms as $t)
                                                                                    <option value="{{ $t->term_code }}" {{ $c->end_term_code == $t->term_code ? 'selected':'' }}>
                                                                                        {{ $t->term_description }}
                                                                                    </option>
                                                                                @endforeach
                                                                            </select>   
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6" style="display:flex">
                                                                        <div class="form-group">
                
                                                                            <label for="">Activation</label>
                                                                            <Select class="form-control" name="activation"  style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                                                                                <option value="1" {{$c->is_active == TRUE ? 'selected':'' }}>Active</option>
                                                                                <option value="0" {{$c->is_active == FALSE ? 'selected':'' }}>Inactive</option>
                                                                            </Select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="row">
                                                            <div class="col-md-8"></div>
                                                            <div class="col-md-4">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                <button type="submit" class="btn btn-primary">Save</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @empty
                        @endforelse
                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>
</div>

<!-- new Modal -->
<div class="modal fade" id="collegeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <form action="{{ route('maintenance.college.store') }}" method="post" id="form">
                @csrf
                
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New College</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    @if($errors->any())
                        <div class="alert alert-danger" role="alert">
                            @foreach($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">College Code <span class="text-danger">*</span></label>
                                <input  type="text" 
                                        class="form-control {{$errors->has('code') ? 'error':''}}" 
                                        placeholder="" 
                                        value="{{ old('code') }}" 
                                        name="code"
                                        required>
                            </div>
                            <div class="form-group">
                                <label for="">College Title <span class="text-danger">*</span></label>
                                <input  type="text" 
                                        class="form-control {{$errors->has('title') ? 'error':''}}" 
                                        value="{{ old('title') }}" 
                                        name="title"
                                        required>
                            </div>
                            <div class="form-group">
                                <label for="">Campus Code <span class="text-danger">*</span></label>
                                <select name="campus_code" class="form-control" required>
                                    <option value="">------------</option>
                                    @foreach ($campuses as $c)
                                        <option value="{{ $c->campus_code }}" {{ old('campus_code') == $c->campus_code ? 'selected':'' }}>
                                            {{ $c->campus_code }} - {{ $c->campus_description }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Effective Term Code <span class="text-danger">*</span></label>
                                <select name="effective_tc" id="new_effective_tc" class="form-control" onchange="checkTerm()" required>
                                    <option value="">------------</option>

                                    @foreach ($terms->where('academic_year','<','2099') as $t)
                                        <option value="{{ $t->term_code }}" {{ old('effective_tc') == $t->term_code ? 'selected':'' }}>
                                            {{ $t->term_code }} - {{ $t->term_description }}
                                        </option>
                                    @endforeach
                                </select>   
                            </div>
                            <div class="form-group">
                                <label for="">End Term Code <span class="text-danger">*</span></label>
                                <select name="end_tc" id="new_end_tc" class="form-control" onchange="checkTerm()" required>
                                    <option value="">------------</option>

                                    @foreach ($terms as $t)
                                        <option value="{{ $t->term_code }}" {{ old('end_tc') == $t->term_code ? 'selected':'' }}>
                                            {{ $t->term_code }} - {{ $t->term_description }}
                                        </option>
                                    @endforeach
                                </select>   
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" id="save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        function checkTerm() {
            var ef = document.getElementById('new_effective_tc').value;
            var en = document.getElementById('new_end_tc').value;

            if(ef != '' && en != ''){

                if(en <= ef){
                    Swal.fire({
                        title: 'Error!',
                        text: "End Term must be greater than Effective Term!",
                        icon: 'error',
                        confirmButtonText: 'Ok'
                    })
                }
            }
            console.log(ef)
        }

        $(document).ready(function() {
            $('#colleges-table').DataTable();

            @if($errors->has('code') ||
                $errors->has('title') ||
                $errors->has('datetime'))

                $("#collegeModal").modal('show');
            @endif

            $('#save').click(function(e) {
                e.preventDefault();
                // document.getElementById('click_btn').value = "save";
                var ef = document.getElementById('new_effective_tc').value;
                var en = document.getElementById('new_end_tc').value;

                if(ef != '' && en != ''){
                    if(en <= ef){
                        Swal.fire({
                            title: 'Error!',
                            text: "End Term must be greater than Effective Term!",
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        })
                    }else{
                        document.getElementById("form").submit();
                    }
                }
                // else{
                //     Swal.fire({
                //         title: 'Error!',
                //         text: "Fill all required fields",
                //         icon: 'error',
                //         confirmButtonText: 'Ok'
                //     })
                // }

                // if(active_desc.length > 0){
                //     if(eff.value != '' && end.value != ''){
                //         if(eff.value == end.value){
                //             document.getElementById('title-modal').innerHTML = "Error"
                //             document.getElementById('content-modal').innerHTML = "End Term must be greater than Effective Term!"
                //             $("#message-modal").modal('show');
                //         }else{
                //             document.getElementById("main_outcome_form").submit();
                //         }

                //     }else{
                //         document.getElementById('title-modal').innerHTML = "Error"
                //         document.getElementById('content-modal').innerHTML = "Please update the Effective Term and End Term!"
                //         $("#message-modal").modal('show');    
                //     }
                // }else{
                //     document.getElementById('title-modal').innerHTML = "Error"
                //     document.getElementById('content-modal').innerHTML = "Please update the description first!"
                //     $("#message-modal").modal('show');
                // }
            })
        });
    </script>
@endsection