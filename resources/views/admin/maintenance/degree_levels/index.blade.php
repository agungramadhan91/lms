@extends('admin.management.sublayout')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/font-awesome-4.7.0/css/font-awesome.min.css')}}">

    <style>
        .form-control {
            border:2px solid #6fa7d9; 
            background-color:#fffbf1; 
            color:#6fa7d9;";
            width:350px;
        }
        
        .error {
            border:2px solid #ff0000; 
            background-color:#fffbf1; 
            color:#6fa7d9;";
            width:350px;
        }
        /* style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"  */
    </style>
@endsection

@section('content')
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row content">
            <div class="col-md-3">
                <!-- <a href="{{-- route('management.role_user.create') --}}" class="btn btn-primary">New Role</a> -->
                <!-- Button trigger modal --><br>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#campusModal">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    Add a New Degree Level
                </button>
            </div>
        </div><br>

        <div class="row content">
            <div class="col-md-12">
                <table id="campuses-table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Level Code</th>
                            <th>Level Title</th>
                            <th>Effective Term Code</th>
                            <th>End Term Code</th>
                            <th>Active / Inactive</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($dg_levels as $dl)
                            <tr>
                                <td>{{ $dl->degree_level_code }}</td>
                                <td>{{ $dl->title }}</td>
                                <td>{{ $dl->effective_term_code }}</td>
                                <td>{{ $dl->end_term_code }}</td>
                                <td>
                                    @if($dl->is_active == TRUE)
                                        <span class="label label-success">Active</span>
                                    @else
                                        <span class="label label-danger">Inactive</span>
                                    @endif           
                                </td>
                                <td>
                                    <button type="button" 
                                            class="btn btn-primary" 
                                            data-toggle="modal" 
                                            title="Edit" 
                                            data-target="#campusEditModal-{{ $dl->degree_level_code }}">

                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </button>

                                    <div class="modal fade" id="campusEditModal-{{ $dl->degree_level_code }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                    
                                                <form action="{{ route('maintenance.degree_level.update', $dl->degree_level_code) }}" method="post">
                                                    @csrf
                                                    
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Degree Level - {{ $dl->degree_level_code }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                    
                                                        @if($errors->any())
                                                            <div class="alert alert-danger" role="alert">
                                                                @foreach($errors->all() as $error)
                                                                    {{ $error }}
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="hidden" name="code" value="{{ $dl->degree_level_code }}" >
                                                                
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="">Degree Level Title <span class="text-danger">*</span></label>
                                                                            <input  type="text" 
                                                                                    class="form-control {{$errors->has('title') ? 'error':''}}" 
                                                                                    value="{{ $dl->title }}" 
                                                                                    name="title"
                                                                                    style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"
                                                                                    required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-6" style="display:flex">
                                                                        <div class="form-group">
                                                                            <label for="">Effective Term Code <span class="text-danger">*</span></label>
                                                                            <select name="effective_tc" 
                                                                                    class="form-control"
                                                                                    required
                                                                                    style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                                                                                <option value="">------------</option>
                                            
                                                                                @foreach ($terms as $t)
                                                                                    <option value="{{ $t->term_code }}" {{ $dl->effective_term_code == $t->term_code ? 'selected':'' }}>
                                                                                        {{ $t->term_code }} - {{ $t->term_description }}
                                                                                    </option>
                                                                                @endforeach
                                                                            </select>   
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6" style="display:flex">
                                                                        <div class="form-group">
                                                                            <label for="">End Term Code <span class="text-danger">*</span></label>
                                                                            <select name="end_tc" 
                                                                                    class="form-control"
                                                                                    required
                                                                                    style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                                                                                <option value="">------------</option>
                                            
                                                                                @foreach ($terms as $t)
                                                                                    <option value="{{ $t->term_code }}" {{ $dl->end_term_code == $t->term_code ? 'selected':'' }}>
                                                                                        {{ $t->term_code }} - {{ $t->term_description }}
                                                                                    </option>
                                                                                @endforeach
                                                                            </select>   
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6" style="display:flex">
                                                                        <div class="form-group">
                
                                                                            <label for="">Activation</label>
                                                                            <Select class="form-control" name="activation"  style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                                                                                <option value="1" {{$dl->is_active == TRUE ? 'selected':'' }}>Active</option>
                                                                                <option value="0" {{$dl->is_active == FALSE ? 'selected':'' }}>Inactive</option>
                                                                            </Select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="row">
                                                            <div class="col-md-8"></div>
                                                            <div class="col-md-4">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                <button type="submit" class="btn btn-primary">Save</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @empty
                        @endforelse
                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>
</div>

<!-- new Modal -->
<div class="modal fade" id="campusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <form action="{{ route('maintenance.degree_level.store') }}" method="post">
                @csrf
                
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Degree Level</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    @if($errors->any())
                        <div class="alert alert-danger" role="alert">
                            @foreach($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Degree Level Code <span class="text-danger">*</span></label>
                                <input  type="text" 
                                        class="form-control {{$errors->has('code') ? 'error':''}}" 
                                        placeholder="" 
                                        value="{{ old('code') }}" 
                                        name="code"
                                        required>
                            </div>
                            <div class="form-group">
                                <label for="">Degree Level Title <span class="text-danger">*</span></label>
                                <input  type="text" 
                                        class="form-control {{$errors->has('title') ? 'error':''}}" 
                                        value="{{ old('title') }}" 
                                        name="title"
                                        required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Effective Term Code <span class="text-danger">*</span></label>
                                <select name="effective_tc" class="form-control" required>
                                    <option value="">------------</option>

                                    @foreach ($terms as $t)
                                        <option value="{{ $t->term_code }}" {{ old('effective_tc') == $t->term_code ? 'selected':'' }}>
                                            {{ $t->term_code }} - {{ $t->term_description }}
                                        </option>
                                    @endforeach
                                </select>   
                            </div>
                            <div class="form-group">
                                <label for="">End Term Code <span class="text-danger">*</span></label>
                                <select name="end_tc" class="form-control" required>
                                    <option value="">------------</option>

                                    @foreach ($terms as $t)
                                        <option value="{{ $t->term_code }}" {{ old('end_tc') == $t->term_code ? 'selected':'' }}>
                                            {{ $t->term_code }} - {{ $t->term_description }}
                                        </option>
                                    @endforeach
                                </select>   
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('#campuses-table').DataTable();

            @if($errors->has('code') ||
                $errors->has('title') ||
                $errors->has('datetime'))

                $("#campusModal").modal('show');
            @endif
        });
    </script>
@endsection