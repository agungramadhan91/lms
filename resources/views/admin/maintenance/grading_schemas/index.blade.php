@extends('admin.management.sublayout')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/font-awesome-4.7.0/css/font-awesome.min.css')}}">

    <style>
        .form-control {
            border:2px solid #6fa7d9; 
            background-color:#fffbf1; 
            color:#6fa7d9;";
            width:350px;
        }
        
        .error {
            border:2px solid #ff0000; 
            background-color:#fffbf1; 
            color:#6fa7d9;";
            width:350px;
        }
        /* style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"  */
    </style>
@endsection

@section('content')
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row content">
            <div class="col-md-3">
                <!-- <a href="{{-- route('management.role_user.create') --}}" class="btn btn-primary">New Role</a> -->
                <!-- Button trigger modal --><br>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#campusModal">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    Add a New Grading Schema
                </button>
            </div>
        </div><br>

        <div class="row content">
            <div class="col-md-12">
                <table id="campuses-table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Grading Mode Code</th>
                            <th>Grading Schema Code</th>
                            <th>Grade</th>
                            <th>Grading Description</th>
                            <th>Percentage From</th>
                            <th>Percentage To</th>
                            {{-- <th>Grade Point</th> --}}
                            <th>Active / Inactive</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($g_schemas as $gs)
                            <tr>
                                <td>{{ $gs->grading_mode_code }}</td>
                                <td>{{ $gs->grading_schema_details_code }}</td>
                                <td>{{ $gs->grade }}</td>
                                <td>{{ $gs->description }}</td>
                                <td>{{ $gs->percentage_from }}</td>
                                <td>{{ $gs->percentage_to }}</td>
                                <td>
                                    @if($gs->is_active == TRUE)
                                        <span class="label label-success">Active</span>
                                    @else
                                        <span class="label label-danger">Inactive</span>
                                    @endif           
                                </td>
                                <td width="80px">
                                    <div class="">
                                        <div class="form-group">

                                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" title="View" data-target="#gschemaModal-{{ $gs->grading_schema_details_code }}">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </button>
                                            <button type="button" 
                                                    class="btn btn-primary btn-sm" 
                                                    data-toggle="modal" 
                                                    title="Edit" 
                                                    data-target="#campusEditModal-{{ $gs->grading_schema_details_code }}">
        
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </div>

                                    <!-- Modal Sequence -->
                                    <div class="modal fade" id="gschemaModal-{{ $gs->grading_schema_details_code }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Grading Schema Details {{ $gs->grading_schema_details_code }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">                                                           
                                                    <table id="users-table" class="table table-striped table-bordered" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Grade Points</th>
                                                                <th>Effective Term Code</th>
                                                                <th>End Term Code</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>{{ $gs->grade_points }}</td>
                                                                <td>{{ $gs->effective_term_code }}</td>
                                                                <td>{{ $gs->end_term_code }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal fade" id="campusEditModal-{{ $gs->grading_schema_details_code }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                    
                                                <form action="{{ route('maintenance.grading_schema.update', $gs->grading_schema_details_code) }}" method="post">
                                                    @csrf
                                                    
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Grading Schema - {{ $gs->grading_schema_details_code }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                    
                                                        @if($errors->any())
                                                            <div class="alert alert-danger" role="alert">
                                                                @foreach($errors->all() as $error)
                                                                    {{ $error }}
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="hidden" name="code" value="{{ $gs->grading_schema_details_code }}" >
                                                                <div class="form-group">
                                                                    <label for="">Grading Mode Code <span class="text-danger">*</span></label>
                                                                    <select name="gm_code" 
                                                                            class="form-control" 
                                                                            style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"
                                                                            required>
                                                                        <option value="">------------</option>
                                    
                                                                        @foreach ($g_modes as $gm)
                                                                            <option value="{{ $gm->grading_mode_code }}" {{ $gs->grading_mode_code == $gm->grading_mode_code ? 'selected':'' }}>
                                                                                {{ $gm->grading_mode_code }} - {{ $gm->grading_description }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>   
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="">Percentage From <span class="text-danger">*</span></label>
                                                                    <input  type="number" 
                                                                            class="form-control {{$errors->has('percentage_from') ? 'error':''}}" 
                                                                            value="{{ $gs->percentage_from }}" 
                                                                            name="percentage_from"
                                                                            style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"
                                                                            required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="">Percentage To <span class="text-danger">*</span></label>
                                                                    <input  type="number" 
                                                                            class="form-control {{$errors->has('percentage_to') ? 'error':''}}" 
                                                                            value="{{ $gs->percentage_to }}" 
                                                                            name="percentage_to"
                                                                            style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"
                                                                            required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="">Grade <span class="text-danger">*</span></label>
                                                                    <input  type="text" 
                                                                            class="form-control {{$errors->has('grade') ? 'error':''}}" 
                                                                            value="{{ $gs->grade }}" 
                                                                            name="grade"
                                                                            style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"
                                                                            required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="">Grade Point<span class="text-danger">*</span></label>
                                                                    <input  type="text" 
                                                                            class="form-control {{$errors->has('grade_point') ? 'error':''}}" 
                                                                            value="{{ $gs->grade_points }}" 
                                                                            name="grade_point"
                                                                            style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"
                                                                            required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="">Description <span class="text-danger">*</span></label>
                                                                    <input  type="text" 
                                                                            class="form-control {{$errors->has('description') ? 'error':''}}" 
                                                                            value="{{ $gs->description }}" 
                                                                            name="description"
                                                                            style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"
                                                                            required>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6" style="display:flex">
                                                                        <div class="form-group">
                                                                            <label for="">Effective Term Code <span class="text-danger">*</span></label>
                                                                            <select name="effective_tc" 
                                                                                    class="form-control"
                                                                                    required
                                                                                    style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                                                                                <option value="">------------</option>
                                            
                                                                                @foreach ($terms as $t)
                                                                                    <option value="{{ $t->term_code }}" {{ $gs->effective_term_code == $t->term_code ? 'selected':'' }}>
                                                                                        {{ $t->term_code }} - {{ $t->term_description }}
                                                                                    </option>
                                                                                @endforeach
                                                                            </select>   
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6" style="display:flex">
                                                                        <div class="form-group">
                                                                            <label for="">End Term Code <span class="text-danger">*</span></label>
                                                                            <select name="end_tc" 
                                                                                    class="form-control"
                                                                                    required
                                                                                    style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                                                                                <option value="">------------</option>
                                            
                                                                                @foreach ($terms as $t)
                                                                                    <option value="{{ $t->term_code }}" {{ $gs->end_term_code == $t->term_code ? 'selected':'' }}>
                                                                                        {{ $t->term_code }} - {{ $t->term_description }}
                                                                                    </option>
                                                                                @endforeach
                                                                            </select>   
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6" style="display:flex">
                                                                        <div class="form-group">
                
                                                                            <label for="">Activation</label>
                                                                            <Select class="form-control" name="activation"  style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                                                                                <option value="1" {{$gs->is_active == TRUE ? 'selected':'' }}>Active</option>
                                                                                <option value="0" {{$gs->is_active == FALSE ? 'selected':'' }}>Inactive</option>
                                                                            </Select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="row">
                                                            <div class="col-md-8"></div>
                                                            <div class="col-md-4">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                <button type="submit" class="btn btn-primary">Save</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @empty
                        @endforelse
                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>
</div>

<!-- new Modal -->
<div class="modal fade" id="campusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <form action="{{ route('maintenance.grading_schema.store') }}" method="post">
                @csrf
                
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Grading Schema</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    @if($errors->any())
                        <div class="alert alert-danger" role="alert">
                            @foreach($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Grading Mode Code <span class="text-danger">*</span></label>
                                        <select name="gm_code" class="form-control" required>
                                            <option value="">------------</option>
        
                                            @foreach ($g_modes as $gm)
                                                <option value="{{ $gm->grading_mode_code }}" {{ old('gm_code') == $gm->grading_mode_code ? 'selected':'' }}>
                                                    {{ $gm->grading_mode_code }} - {{ $gm->grading_description }}
                                                </option>
                                            @endforeach
                                        </select>   
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Effective Term Code <span class="text-danger">*</span></label>
                                        <select name="effective_tc" class="form-control" required>
                                            <option value="">------------</option>
            
                                            @foreach ($terms as $t)
                                                <option value="{{ $t->term_code }}" {{ old('effective_tc') == $t->term_code ? 'selected':'' }}>
                                                    {{ $t->term_code }} - {{ $t->term_description }}
                                                </option>
                                            @endforeach
                                        </select>   
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">End Term Code <span class="text-danger">*</span></label>
                                        <select name="end_tc" class="form-control" required>
                                            <option value="">------------</option>
            
                                            @foreach ($terms as $t)
                                                <option value="{{ $t->term_code }}" {{ old('end_tc') == $t->term_code ? 'selected':'' }}>
                                                    {{ $t->term_code }} - {{ $t->term_description }}
                                                </option>
                                            @endforeach
                                        </select>   
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-md-11">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Grading Schema Code <span class="text-danger">*</span></label>
                                        <input  type="text" 
                                                class="form-control {{$errors->has('code') ? 'error':''}}" 
                                                placeholder="" 
                                                value="{{ old('code') }}" 
                                                name="code[]"
                                                required>
                                    </div>    
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Grade <span class="text-danger">*</span></label>
                                        <input  type="text" 
                                                class="form-control {{$errors->has('grade') ? 'error':''}}" 
                                                value="{{ old('grade') }}" 
                                                name="grade[]"
                                                required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Grading Description <span class="text-danger">*</span></label>
                                        <input  type="text" 
                                                class="form-control {{$errors->has('description') ? 'error':''}}" 
                                                value="{{ old('description') }}" 
                                                name="description[]"
                                                required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Percentage From <span class="text-danger">*</span></label>
                                        <input  type="number" 
                                                class="form-control {{$errors->has('percentage_from') ? 'error':''}}" 
                                                value="{{ old('percentage_from') }}" 
                                                name="percentage_from[]"
                                                required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Percentage To <span class="text-danger">*</span></label>
                                        <input  type="number" 
                                                class="form-control {{$errors->has('percentage_to') ? 'error':''}}" 
                                                value="{{ old('percentage_to') }}" 
                                                name="percentage_to[]"
                                                required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Grade Point <span class="text-danger">*</span></label>
                                        <input  type="text" 
                                                class="form-control {{$errors->has('grade_point') ? 'error':''}}" 
                                                value="{{ old('grade_point') }}" 
                                                name="grade_point[]"
                                                required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <label for="">Add</label>
                            <button class="add_row_grading_schema btn btn-primary btn-sm">+</button>
                            <div class="form-group">
                            </div>
                        </div>
                    </div>

                    <div class="wrap_row_grading_schema"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"  onclick="cancel_confirm();">Cancel</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        function cancel_confirm() {
            Swal.fire({
                title: 'Exit',
                text: "Your data will be lost if you exit",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ok!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $("#campusModal").modal('hide');
                    // data-dismiss="modal"
                    // Swal.fire(
                    //   'Deleted!',
                    //   'Your file has been deleted.',
                    //   'success'
                    // )
                }
            })
        }

        $(document).ready(function() {
            $('#campuses-table').DataTable();

            @if($errors->has('code') ||
                $errors->has('title') ||
                $errors->has('datetime'))

                $("#campusModal").modal('show');
            @endif
        });

        $(document).ready(function() {
            var y = 1;
            var add_row_grading_schema  = $(".add_row_grading_schema"); //Add button ID
            var wrap_row_grading_schema = $(".wrap_row_grading_schema"); //Fields wrapper
            var cancel = $("#cancel"); 
 
            $(add_row_grading_schema).click(function(e){ //on add input button click
                e.preventDefault();
                y++; //text box increment
                $(wrap_row_grading_schema).append(
                    `
                    <div class="row"><hr>
                        <div class="col-md-11">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Grading Schema Code <span class="text-danger">*</span></label>
                                        <input  type="text" 
                                                class="form-control {{$errors->has('code') ? 'error':''}}" 
                                                placeholder="" 
                                                value="{{ old('code') }}" 
                                                name="code[]"
                                                required>
                                    </div>    
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Grade <span class="text-danger">*</span></label>
                                        <input  type="text" 
                                                class="form-control {{$errors->has('grade') ? 'error':''}}" 
                                                value="{{ old('grade') }}" 
                                                name="grade[]"
                                                required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Grading Description <span class="text-danger">*</span></label>
                                        <input  type="text" 
                                                class="form-control {{$errors->has('description') ? 'error':''}}" 
                                                value="{{ old('description') }}" 
                                                name="description[]"
                                                required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Percentage From <span class="text-danger">*</span></label>
                                        <input  type="number" 
                                                class="form-control {{$errors->has('percentage_from') ? 'error':''}}" 
                                                value="{{ old('percentage_from') }}" 
                                                name="percentage_from[]"
                                                required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Percentage To <span class="text-danger">*</span></label>
                                        <input  type="number" 
                                                class="form-control {{$errors->has('percentage_to') ? 'error':''}}" 
                                                value="{{ old('percentage_to') }}" 
                                                name="percentage_to[]"
                                                required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Grade Point <span class="text-danger">*</span></label>
                                        <input  type="text" 
                                                class="form-control {{$errors->has('grade_point') ? 'error':''}}" 
                                                value="{{ old('grade_point') }}" 
                                                name="grade_point[]"
                                                required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <label for="">Remove</label>
                            <button class="remove_row_grading_schema btn btn-danger btn-sm">-</button>
                            <div class="form-group">
                            </div>
                        </div>
                    </div>
                    
                    `
                ); 
            });

            $(wrap_row_grading_schema).on("click",".remove_row_grading_schema", function(e){ //user click on remove text
                e.preventDefault(); 
                $(this).parent('div').parent('div').remove(); 
                y--;
            });
        });

        
    </script>
@endsection