@extends('admin.management.sublayout')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/font-awesome-4.7.0/css/font-awesome.min.css')}}">

    <style>
        .form-control {
            border:2px solid #6fa7d9; 
            background-color:#fffbf1; 
            color:#6fa7d9;";
            width:350px;
        }
        
        .error {
            border:2px solid #ff0000; 
            background-color:#fffbf1; 
            color:#6fa7d9;";
            width:350px;
        }
        /* style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"  */
    </style>
@endsection

@section('content')
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row content">
            <div class="col-md-3">
                <!-- <a href="{{-- route('management.role_user.create') --}}" class="btn btn-primary">New Role</a> -->
                <!-- Button trigger modal --><br>
                <button type="button" id="newProgram" class="btn btn-primary" data-toggle="modal" data-target="#progamModal">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    Add a New Program 
                </button>
            </div>
        </div><br>

        <div class="row content">
            <div class="col-md-12">
                <table id="colleges-table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Title</th>
                            <th>Campus Code</th>
                            <th>Collge Code</th>
                            <th>Degree Level Code</th>
                            <th>Degree Type Code</th>
                            <th>Effective Term Code</th>
                            <th>End Term Code</th>
                            <th>Active / Inactive</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($programs as $c)
                            <tr>
                                <td>{{ $c->program_code }}</td>
                                <td>{{ $c->title }}</td>
                                <td>{{ $c->campus_code }}</td>
                                <td>{{ $c->college_code }}</td>
                                <td>{{ $c->degree_level_code }}</td>
                                <td>{{ $c->degree_type_code }}</td>
                                <td>{{ $c->effective_term_code }}</td>
                                <td>{{ $c->end_term_code }}</td>
                                <td>
                                    @if($c->status == 'Active')
                                        <span class="label label-success">Active</span>
                                    @else
                                        <span class="label label-danger">Inactive</span>
                                    @endif           
                                </td>
                                <td>
                                    <button type="button" 
                                            class="btn btn-primary programEditModal" 
                                            {{-- data-toggle="modal"  --}}
                                            {{-- onclick='programEdit()' --}}
                                            title="Edit" 
                                            data-url        = "{{ route('maintenance.program.update', $c->program_code) }}"
                                            data-title      = "{{ $c->title }}"
                                            data-hours      = "{{ $c->required_hours }}"
                                            data-college    = "{{ $c->college_code }}"
                                            data-campus     = "{{ $c->campus_code }}"
                                            data-dl         = "{{ $c->degree_level_code }}"
                                            data-dt         = "{{ $c->degree_type_code }}"
                                            data-ef_tc      = "{{ $c->effective_term_code }}"
                                            data-en_tc      = "{{ $c->end_term_code }}"
                                            data-active     = "{{ $c->status }}"
                                            data-code       = "{{ $c->program_code }}">

                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </button>

                                    <div class="modal fade" id="programEditModal-{{ $c->program_code }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                    
                                                <form action="{{ route('maintenance.program.update', $c->program_code) }}" method="post">
                                                    @csrf
                                                    
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Edit College - {{ $c->program_code }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                    
                                                        @if($errors->any())
                                                            <div class="alert alert-danger" role="alert">
                                                                @foreach($errors->all() as $error)
                                                                    {{ $error }}
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="hidden" name="code" value="{{ $c->program_code }}">
                                                                <div class="form-group">
                                                                    <label for="">Program Title <span class="text-danger">*</span></label>
                                                                    <input  type="text" 
                                                                            class="form-control {{$errors->has('title') ? 'error':''}}" 
                                                                            {{-- value="{{ $c->title }}"  --}}
                                                                            id="title-{{ $c->program_code }}"
                                                                            name="title"
                                                                            style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"
                                                                            required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="">Required Hours <span class="text-danger">*</span></label>
                                                                    <input  type="number" 
                                                                            class="form-control {{$errors->has('hours') ? 'error':''}}" 
                                                                            value="{{ $c->required_hours }}" 
                                                                            name="hours"
                                                                            style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"
                                                                            required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="">College Code <span class="text-danger">*</span></label>
                                                                    <select name="college_code" 
                                                                            class="form-control" 
                                                                            style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"
                                                                            required>
                                                                        <option value="">------------</option>
                                                                        @foreach ($colleges as $co)
                                                                            <option value="{{ $co->college_code }}" {{ old('college_code') == $co->college_code ? 'selected':'' }}>
                                                                                {{ $co->college_code }} - {{ $co->title }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="">Campus Code <span class="text-danger">*</span></label>
                                                                    <select name="campus_code" 
                                                                            class="form-control" 
                                                                            style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"
                                                                            required>
                                                                        <option value="">------------</option>
                                                                        @foreach ($campuses as $c)
                                                                            <option value="{{ $c->campus_code }}" {{ old('campus_code') == $c->campus_code ? 'selected':'' }}>
                                                                                {{ $c->campus_code }} - {{ $c->campus_description }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="">Degree Level <span class="text-danger">*</span></label>
                                                                    <select name="dl_code" 
                                                                            class="form-control" 
                                                                            style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"
                                                                            required>
                                                                        <option value="">------------</option>
                                                                        @foreach ($dg_levels as $dl)
                                                                            <option value="{{ $dl->degree_level_code }}" {{ old('dl_code') == $dl->degree_level_code ? 'selected':'' }}>
                                                                                {{ $dl->degree_level_code }} - {{ $dl->title }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="">Degree Type <span class="text-danger">*</span></label>
                                                                    <select name="dt_code" 
                                                                            class="form-control" 
                                                                            style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"
                                                                            required>
                                                                        <option value="">------------</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="">Effective Term Code <span class="text-danger">*</span></label>
                                                                    <select name="effective_tc" 
                                                                            class="form-control" 
                                                                            style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"
                                                                            required>
                                                                        <option value="">------------</option>
                                    
                                                                        @foreach ($terms as $t)
                                                                            <option value="{{ $t->term_code }}" {{ old('effective_tc') == $t->term_code ? 'selected':'' }}>
                                                                                {{ $t->term_code }} - {{ $t->term_description }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>   
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="">End Term Code <span class="text-danger">*</span></label>
                                                                    <select name="end_tc" 
                                                                            class="form-control" 
                                                                            style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;"
                                                                            required>
                                                                        <option value="">------------</option>
                                    
                                                                        @foreach ($terms as $t)
                                                                            <option value="{{ $t->term_code }}" {{ old('end_tc') == $t->term_code ? 'selected':'' }}>
                                                                                {{ $t->term_code }} - {{ $t->term_description }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="row">
                                                            <div class="col-md-8"></div>
                                                            <div class="col-md-4">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                <button type="submit" class="btn btn-primary">Save</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @empty
                        @endforelse
                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>
</div>

<!-- new Modal -->
<div class="modal fade" id="progamModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <form action="{{ route('maintenance.program.store') }}" method="post" id="programForm">
                @csrf
                
                <div class="modal-header">
                    <h5 class="modal-title" id="programTitleModal">New Program</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    @if($errors->any())
                        <div class="alert alert-danger" role="alert">
                            @foreach($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="code_group"></div>
                            
                            {{-- <input type="hidden" name="code" id="code_hidden"> --}}
                            <div class="form-group">
                                <label for="">Program Title <span class="text-danger">*</span></label>
                                <input  type="text" 
                                        class="form-control {{$errors->has('title') ? 'error':''}}" 
                                        value="{{ old('title') }}" 
                                        id="title"
                                        name="title"
                                        required>
                            </div>
                            <div class="form-group">
                                <label for="">Required Hours <span class="text-danger">*</span></label>
                                <input  type="number" 
                                        class="form-control {{$errors->has('hours') ? 'error':''}}" 
                                        value="{{ old('hours') }}" 
                                        id="hours"
                                        name="hours"
                                        required>
                            </div>
                            <div class="form-group">
                                <label for="">College Code <span class="text-danger">*</span></label>
                                <select name="college_code" id="college_code" class="form-control" required>
                                    <option value="">------------</option>
                                    @foreach ($colleges as $co)
                                        <option value="{{ $co->college_code }}" {{ old('college_code') == $co->college_code ? 'selected':'' }}>
                                            {{ $co->college_code }} - {{ $co->title }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Campus Code <span class="text-danger">*</span></label>
                                <select name="campus_code" id="campus_code" class="form-control" required>
                                    <option value="">------------</option>
                                    @foreach ($campuses as $c)
                                        <option value="{{ $c->campus_code }}" {{ old('campus_code') == $c->campus_code ? 'selected':'' }}>
                                            {{ $c->campus_code }} - {{ $c->campus_description }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Degree Level <span class="text-danger">*</span></label>
                                <select name="dl_code" id="dl_code" class="form-control" required>
                                    <option value="">------------</option>
                                    @foreach ($dg_levels as $dl)
                                        <option value="{{ $dl->degree_level_code }}" {{ old('dl_code') == $dl->degree_level_code ? 'selected':'' }}>
                                            {{ $dl->degree_level_code }} - {{ $dl->title }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Degree Type <span class="text-danger">*</span></label>
                                <select name="dt_code" id="dt_code" class="form-control" required>
                                    <option value="">------------</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Effective Term Code <span class="text-danger">*</span></label>
                                <select name="effective_tc" id="effective_tc" class="form-control" required>
                                    <option value="">------------</option>

                                    @foreach ($terms as $t)
                                        <option value="{{ $t->term_code }}" {{ old('effective_tc') == $t->term_code ? 'selected':'' }}>
                                            {{ $t->term_code }} - {{ $t->term_description }}
                                        </option>
                                    @endforeach
                                </select>   
                            </div>
                            <div class="form-group">
                                <label for="">End Term Code <span class="text-danger">*</span></label>
                                <select name="end_tc" id="end_tc" class="form-control" required>
                                    <option value="">------------</option>

                                    @foreach ($terms as $t)
                                        <option value="{{ $t->term_code }}" {{ old('end_tc') == $t->term_code ? 'selected':'' }}>
                                            {{ $t->term_code }} - {{ $t->term_description }}
                                        </option>
                                    @endforeach
                                </select>   
                            </div>
                            
                            <div id="activation_group"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" form="programForm" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            @if($errors->has('code') ||
                $errors->has('title') ||
                $errors->has('datetime'))

                $("#progamModal").modal('show');
                $("#code_group").append(`
                    <div class="form-group" id="code_form">
                        <label for="">Program Code <span class="text-danger">*</span></label>
                        <input  type="text" 
                                class="form-control {{$errors->has('code') ? 'error':''}}" 
                                placeholder="" 
                                id="code"
                                value="{{ old('code') }}" 
                                name="code"
                                required>
                    </div>
                `);
            @endif

            $("#newProgram").click(function () 
            {
                var isset_code      = $('#code_form').length;
                var isset_active    = $('#active_form').length;

                console.log(isset_code)
                if(isset_code == 0){
                    $("#code_group").append(`
                        <div class="form-group" id="code_form">
                            <label for="">Program Code <span class="text-danger">*</span></label>
                            <input  type="text" 
                                    class="form-control {{$errors->has('code') ? 'error':''}}" 
                                    placeholder="" 
                                    id="code"
                                    value="{{ old('code') }}" 
                                    name="code"
                                    required>
                        </div>
                    `);

                    if(isset_active == 1){
                        document.getElementById("active_form").remove();
                    }
                }else{
                    document.getElementById("code").value = "";
                }

                // document.getElementById("programForm").action   = ;
                document.getElementById("programTitleModal").innerHTML = "Add New Program";
                document.getElementById("title").value          = "";
                document.getElementById("hours").value          = "";
                document.getElementById("college_code").value   = "";
                document.getElementById("campus_code").value    = "";
                document.getElementById("dl_code").value        = "";
                document.getElementById("dt_code").value        = "";
                document.getElementById("effective_tc").value   = "";
                document.getElementById("end_tc").value         = "";
            });

            $(".programEditModal").click(function(){
                $("#progamModal").modal('show');

                var url         = $(this).data("url");
                var code        = $(this).data("code");
                var title       = $(this).data("title");
                var hours       = $(this).data("hours");
                var college_code= $(this).data("college");
                var campus_code = $(this).data("campus");
                var dl_code     = $(this).data("dl");
                var dt_code     = $(this).data("dt");
                var ef_tc       = $(this).data("ef_tc");
                var en_tc       = $(this).data("en_tc");
                var active      = $(this).data("active");

                var isset_active    = $('#active_form').length;
                var isset_code      = $('#code_form').length;

                if(isset_active == 0){
                    $('#activation_group').append(`
                        <div class="form-group" id="active_form">
                            <label for="">Activation <span class="text-danger">*</span></label>
                            <select name="activation" id="activation" class="form-control" required>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>   
                        </div>
                        <input type="hidden" name="code" id="code_hidden">
                    `);
                    document.getElementById("activation").value     = active;
                    document.getElementById("code_hidden").value     = code;
                        
                    if(isset_code == 1){
                        document.getElementById("code_form").remove();
                    }
                }else{
                    document.getElementById("activation").value     = active;
                }

                // var dl_code = $(this).val();
                if(dl_code) {
                    $.ajax({
                        url: '/maintenance/programs/degree/type/'+dl_code,
                        type:"GET",
                        dataType:"json",
                        beforeSend: function(){
                            $('#loader').css("visibility", "visible");
                        },

                        success:function(data) {

                            $('select[name="dt_code"]').empty();
                            // $('select[name="dt_code"]').append('<option value="0">SEMUA TEMPAT</option>');
                            $.each(data, function(key, value){
                                if (key == dt_code) {
                                    $('select[name="dt_code"]').append('<option value="'+ key +'" selected>'+ key +' - ' + value + '</option>');
                                }else{
                                    $('select[name="dt_code"]').append('<option value="'+ key +'">'+ key +' - ' + value + '</option>');
                                }

                            });
                        },
                        complete: function(){
                            $('#loader').css("visibility", "hidden");
                        }
                    });
                } else {
                    $('select[name="dt_code"]').empty();
                }

                // console.log($('#code_group').length);
                // document.getElementById("activation_group").style.display = "block";
                // document.getElementById("code_hidden").value    = code;

                document.getElementById("programForm").action   = url;
                document.getElementById("programTitleModal").innerHTML = "Edit Program - " + code;
                document.getElementById("title").value          = title;
                document.getElementById("hours").value          = hours;
                document.getElementById("college_code").value   = college_code;
                document.getElementById("campus_code").value    = campus_code;
                document.getElementById("dl_code").value        = dl_code;
                document.getElementById("dt_code").value        = dt_code;
                document.getElementById("effective_tc").value   = ef_tc;
                document.getElementById("end_tc").value         = en_tc;
            });

            $(".submitForm").click(function(){
                document.getElementById("programForm").submit();
            });

            $('#colleges-table').DataTable();
        });
          
    </script>
    <script src="{{ asset('js/get-data-degree.js') }}"></script>
@endsection