<html lang="en">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Eduval</title>
	{{-- <link href="assets/css/font-awesome.css" rel="stylesheet" /> --}}
    {{-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous"> --}}
    
    {{-- <link rel="stylesheet" href="{{ asset('assets/css/selectize.bootstrap3.min.css')}}" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" /> --}}
    <!-- GOOGLE FONTS-->
	
	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	
    {{-- <link rel="stylesheet" href="{{ asset('css/font-awesome-4.7.0/css/font-awesome.min.css')}}"> --}}
    {{-- <link rel="stylesheet" href="{{ asset('css/font-awesome-4.7.0/css/font-awesome.min.css')}}"> --}}
	<link href="{{ asset('assets/css/font-awesome.css')}}" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{{ asset('css/multi-select.css') }}">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

	{{-- <script src="{{ asset('js/app.js') }}" defer></script>  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> --}}
	

	<style>
		/* Set height of the grid so .sidenav can be 100% (adjust if needed) */
		/* .row.content {
			height: 1500px
		} */
		.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn){
			background-color:#fffbf1; 
			/* border:2px solid #6fa7d9;  */
			color:#6fa7d9;
			width: 100%;
		}

		/* Set gray background color and 100% height */
		.sidenav {
			background-color: #f1f1f1;
			height: 100%;
		}

		/* Set black background color, white text and some padding */
		footer {
			background-color: #555;
			color: white;
			padding: 15px;
		}

		.Error {
			background: rgb(226, 176, 171);
		}

		.Success {
			background: rgb(215, 227, 191);
		}

		/* On small screens, set height to 'auto' for sidenav and grid */
		/* @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;} 
    } */
	</style>
</head>

<body>
	<nav class="navbar navbar-expand-md navbar-light navbar-laravel" style="color:white; background-color:#301949;">
        <img src="{{ asset('assets/img/eduval_logo_final.png') }}" style="height:80px; width:80px; background-color:white; margin-right:10px">
        <div class="navbar-collapse" style="">
            <h6 style="font-size:21px;">
            <b>Curriculum and Catalogue Management System CCMS</b>
            </h6>
        </div>
    </nav>
        <nav class="navbar navbar-expand-sm" style="background-color: #0b4176; height:40px;">
            <div class="container">
                <a class="nav-item" href="{{ url('/') }}" style ="color:white;font-size:18px;">
                @guest
                    Home
                @else
                   User {{Auth::user()->id}} {{Auth::user()->name}}
                @endif
                </a>
                
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}" style ="color:white; background-color: #b49b4b;">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}" style ="color:white; background-color: #b49b4b;">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                        <li class="dropdown">
                                <a id="navbarDropdown" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style ="color:white;">Views and Reports</a>

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="background-color: #0b4176;">
                                    <a class="dropdown-item" href="{{url('program/view/choose')}}" style ="color:white; border:1px solid gray">View a Program</a>
                                    <a class="dropdown-item" href="{{url('course/view/choose')}}" style ="color:white; border:1px solid gray">View a Course</a>
                                    <a class="dropdown-item" href="{{url('college/view/choose')}}" style ="color:white; border:1px solid gray">View a College</a>
                                    <a class="dropdown-item" href="{{url('campus/view/choose')}}" style ="color:white; border:1px solid gray">View a Campus</a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <li class="dropdown">
                                <a id="navbarDropdown" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style ="color:white;">
                                   @if (Auth::user()->name == "admin")
                                    Admin
                                    @else
                                    System Admin
                                    @endif
                                    <i class="fas fa-cogs"></i>
                                    <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="background-color: #0b4176;">
                                    <a class="dropdown-item" href="{{ route('maintenance.college.index') }}" style ="color:white; border:1px solid gray">College Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.campus.index') }}" style ="color:white; border:1px solid gray">Campus Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.degree_level.index') }}" style ="color:white; border:1px solid gray">Degree Level Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.degree_type.index') }}" style ="color:white; border:1px solid gray">Degree Type Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.course_level.index') }}" style ="color:white; border:1px solid gray">Course Level Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.course_type.index') }}" style ="color:white; border:1px solid gray">Course Type Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.department.index') }}" style ="color:white; border:1px solid gray">Department Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.grading_mode.index') }}" style ="color:white; border:1px solid gray">Grading Mode Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.grading_schema.index') }}" style ="color:white; border:1px solid gray">Grading Schema Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.subject.index') }}" style ="color:white; border:1px solid gray">Subject Code Maintenance</a>
                                    <a class="dropdown-item" href="{{ route('maintenance.term.index') }}" style ="color:white; border:1px solid gray">Term Code maintenance</a>
                                    <!-- <a class="dropdown-item" href="#" style ="color:white; border:1px solid gray">Workkflow Management</a>
                                    <a class="dropdown-item" href="#" style ="color:white; border:1px solid gray">Approvals Management</a> -->
                                    <a class="dropdown-item" href="{{ route('management.role_user.index') }}" style ="color:white; border:1px solid gray">
                                        Roles, User, and Workflow
                                    </a>
                                    <a class="dropdown-item" href="#" style ="color:white; border:1px solid gray">System Reports</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" style ="color:white;">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            {{-- @yield('content') --}}
			<div class="container-fluid" style="padding-top:10px;">
				<div class="row content" style="">
					<span class="border border-secondary" style="background-color: #b49b4b; margin-bottom:10px;">
						<div class="col-md-2">
							<h6 style="margin-top:10px; color:white;">Notification</h6>
						</div>
					</span>
					<div class="col-sm-1"></div>
					<div class="col-md-9">
						<div class="header">
							<a href="{{ route('home') }}">
								<button type="button" class="btn"
									style="margin-top:-5px; width:24%; background-color: #b49b4b; color:white;">
									<i class="fa fa-tasks"></i>
									Program Management
								</button>
							</a>
							<a href="{{ route('home.course') }}" style="color:white;">
								<button type="button" class="btn"
									style="margin-top:-5px; width:24%; background-color: #b49b4b; color:white;">
									<i class="fa fa-university"></i>
									Course Management
								</button>
							</a>
							<a href="{{ route('home.minor') }}">
								<button type="button" class="btn"
									style="margin-top:-5px; width:24%; background-color: white; color:gray; box-shadow: 0 3px red; border:1px solid gray;">
									<i class="fa fa-table"></i>
									Minor Management
								</button>
							</a>
							<a href="{{ route('home.catalogue') }}">
								<button type="button" class="btn"
									style="margin-top:-5px; width:24%; background-color: #b49b4b; color:white;">
									<i class="fa fa-newspaper"></i>
									Catalogue Management
								</button>
							</a>
						</div>
						<div class="container mt-3">
							@foreach (['Error', 'warning', 'Confirmation', 'info'] as $msg)
								@if(Session::has('alert-' . $msg))
									<!-- <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p> -->
									<div class="modal" id="popup1" style="display:block;">
										<div class="modal-dialog">
											<div class="modal-content">
		
												<!-- Modal Header -->
												<div class="modal-header" style="background: rgb(215, 227, 191)">
													<h4 style="font-weight:bold;">{{ $msg }}</h4>
												</div>
		
												<!-- Modal body -->
												<div class="modal-body">
													<p style="font-size:17px;">{{ Session::get('alert-' . $msg) }}</p>
												</div>
												<!-- Modal footer -->
												<div class="modal-footer">
													<!-- <a href="{{url('/')}}">
												<button type="button" class="btn btn-danger">OK</button>
											</a> -->
													<button type="button" id="clickmodal"
														style="width:190px; color:white; border:3px solid #D3D3D3;"
														class="btn btn-primary" data-dismiss="modal">Ok</button>
												</div>
		
											</div>
										</div>
									</div>
								@endif
							@endforeach
							<br>
							<div class="tab-content">
								@include('_minor_content')
							</div>
						</div>
					</div>
				</div>
			</div>
        </main>
    </div>

	{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js" integrity="sha512-CwHUCK55pONjDxvPZQeuwKpxos8mPyEv9gGuWC8Vr0357J2uXg1PycGDPND9EgdokSFTG6kgSApoDj9OM22ksw==" crossorigin="anonymous"></script> --}}
	{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> --}}
	{{-- <script src="{{ asset('js/app.js') }}" defer></script>   --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
	<script src="{{ asset('js/jquery.multi-select.js') }}"></script>
	<script>
		$(document).ready(function () {
			// $('#course_codes').selectpicker();
			$('#program').DataTable();

            @if($errors->has('code') ||
                $errors->has('title') ||
                $errors->has('effective_tc') ||
                $errors->has('end_tc') ||
                $errors->has('datetime'))

                $("#minorModal").modal('show');
            @endif

			$("select#college_code").change(function(){
				var selectedCountry = $(this).children("option:selected").val();
				var minor_count = $('#minor_count').val();
				var selectoption = document.getElementById('select-'+selectedCountry);
				var count = parseInt(minor_count) + 1;
				var old_select = new Array();

				if(count < 10){
					que = '00'+count;
				}else if(count < 100){
					que = '0'+count;
				}else{
					que = count;
				}
				var mcode = selectedCountry+"_MNR_"+que;
				var store = localStorage.getItem('old_select');

				document.getElementById('minor_code_display').value = mcode;
				document.getElementById('minor_code').value = mcode;
				console.log(store);

				if(store == null){
					if(selectoption.style.display == "none"){
						localStorage.setItem('old_select', selectedCountry);
						selectoption.style.display = 'block';
					}
				}else{
					var oldselectoption = document.getElementById('select-'+store);
					localStorage.setItem('old_select', selectedCountry);
					
					if(oldselectoption.style.display == "block"){
						oldselectoption.style.display = 'none';
					}

					if(selectoption.style.display == "none"){
						selectoption.style.display = 'block';
					}
				}
				
				// alert("You have selected the country - " + mcode);
			});

			$('.edit-minor').click(function(){
				$("#editMinorModal").modal('show');

				var url 	= $(this).data('url');
				var minor 	= $(this).data('minor');
				var college = $(this).data('college');
				var title 	= $(this).data('title');
				var campus 	= $(this).data('campus');
				var courses = $(this).data('courses');
				var ef_tc 	= $(this).data('ef_tc');
				var en_tc 	= $(this).data('en_tc');
				
				document.getElementById('edit_form').action = url
				document.getElementById('edit_minor_code_display').value = minor
				document.getElementById('edit_college_code').value = college
				document.getElementById('edit_title').value = title
				document.getElementById('edit_campus').value = campus
				document.getElementById('edit_ef').value = ef_tc
				document.getElementById('edit_en').value = en_tc
				document.getElementById('edit_show_courses').value = courses

				editselect = document.getElementById('editselect-'+college);
				if(editselect.style.display == "none"){
					for (let index = 0; index < courses.length; index++) {
						$('#editselect-'+college+' option[value="'+courses[index]+'"]').prop('selected',true)
					}
					
					editselect.style.display = "block";
				}
			});
		});
	</script>
	<script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
	<script src="{{asset('assets/js/custom.js')}}"></script>
	<script src="{{ asset('js/get-data-courses.js') }}"></script>

	
    {{-- <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> --}}
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" /> --}}
	
	<!-- METISMENU SCRIPTS -->
	<!-- CUSTOM SCRIPTS -->
	
</body>

</html>
