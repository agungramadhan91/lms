<hr>
<div class="row content">
    <div class="col-md-3">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#minorModal">
            <i class="fa fa-plus" aria-hidden="true"></i>
            Add a New Minor
        </button>
    </div>
</div><br>
<div class="row content">
    <div class="col-md-12">
        <table id="program" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Minor Code</th>
                    <th>Title</th>
                    <th>College Code</th>
                    <th>Campus Code</th>
                    <th>Course Code</th>
                    <th>Show / Edit</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($minors as $data)
                    <tr>
                        <td>{{ $data->minor_code }}</td>
                        <td>{{ $data->title }}</td>
                        <td>{{ $data->college_code }}</td>
                        <td>{{ $data->campus_code }}</td>
                        <td>
                            @foreach ($data->courses as $item)
                                {{ $item }}, 
                            @endforeach
                        </td>
                        <td>
                            <button type="button" class="btn btn-warning" data-toggle="modal" title="View" data-target="#minorModal-{{ $data->minor_code }}">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                            </button>
                            {{-- <button type="button" class="btn btn-primary" data-toggle="modal" title="View" data-target="#editMinorModal-{{ $data->minor_code }}">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </button> --}}
                            <button type="button" 
                                    class="btn btn-primary edit-minor"
                                    data-url = "{{ route('minor.update', $data->minor_code) }}"
                                    data-minor = "{{ $data->minor_code }}"
                                    data-title = "{{ $data->title }}"
                                    data-college = "{{ $data->college_code }}"
                                    data-campus = "{{ $data->campus_code }}"
                                    data-courses = "{{ $data->courses }}"
                                    data-ef_tc = "{{ $data->effective_term_code }}"
                                    data-en_tc = "{{ $data->end_term_code }}"
                                    title="Edit">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </button>

                            <div class="modal fade" id="minorModal-{{ $data->minor_code }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Minor {{ $data->minor_code }}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">                                                           
                                            <table id="users-table" class="table table-striped table-bordered" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>Effective Term</th>
                                                        <th>End Term</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>{{ $data->effective_term_code }}</td>
                                                        <td>{{ $data->end_term_code }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="editMinorModal-{{ $data->minor_code }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <form action="{{ route('minor.update', $data->minor_code) }}" method="post">
                                            @csrf
                                            
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Edit Minor</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                            
                                                @if($errors->any())
                                                    <div class="alert alert-danger" role="alert">
                                                        @foreach($errors->all() as $error)
                                                            {{ $error }}
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-4" style="display:flex">
                                                            <div class="row">
                                                                <label for="">College <span class="text-danger">*</span></label>
                                                                <select class="form-control {{$errors->has('college_code') ? 'error':''}}" 
                                                                        {{-- id="college_code-1" --}}
                                                                        {{-- style="width:100%;"  --}}
                                                                        name="college_code"
                                                                        disabled>
                                                                    <option value="">------------</option>
                                
                                                                    @foreach ($colleges as $clg)
                                                                        <option value="{{ $clg->college_code }}"
                                                                            {{ $clg->college_code == $data->college_code ? 'selected':'' }}>
                                                                            {{ $clg->college_code }} - {{ $clg->title }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3" style="display:flex">
                                                            <div class="row">
                                                                <label for="">Minor Code</label>
                                                                <input  type="text" 
                                                                        name="minor_code" 
                                                                        style="width:100%;" 
                                                                        value="{{ $data->minor_code }}" 
                                                                        class="form-control" 
                                                                        readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- <input type="hidden" id="minor_count" value="{{ count($minors) }}"> --}}
                                                {{-- <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="">College <span class="text-danger">*</span></label>
                                                                <select class="form-control {{$errors->has('college_code') ? 'error':''}}" 
                                                                        id="college_code-1"
                                                                        name="college_code"
                                                                        required>
                                                                    <option value="">------------</option>
                                
                                                                    @foreach ($colleges as $clg)
                                                                        <option value="{{ $clg->college_code }}"
                                                                            {{ $clg->college_code == $data->college_code ? 'selected':'' }}>
                                                                            {{ $clg->college_code }} - {{ $clg->title }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="">Minor Code</label>
                                                                <input  type="text" 
                                                                        name="minor_code" 
                                                                        value="{{ $data->minor_code }}" 
                                                                        class="form-control" 
                                                                        readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        
                                                        <div class="form-group">
                                                            <label for="">Title <span class="text-danger">*</span></label>
                                                            <input  type="text" 
                                                                    class="form-control {{$errors->has('description') ? 'error':''}}" 
                                                                    value="{{ $data->title }}" 
                                                                    name="description"
                                                                    required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Campus Code <span class="text-danger">*</span></label>
                                                            <select class="form-control {{$errors->has('campus_code') ? 'error':''}}" 
                                                                    value="{{ old('campus_code') }}" 
                                                                    name="campus_code"
                                                                    required>
                                                                <option value="">------------</option>
                            
                                                                @foreach ($campuses as $cps)
                                                                    <option value="{{ $cps->campus_code }}"
                                                                        {{ $cps->campus_code == $data->campus_code ? 'selected':'' }}>
                                                                        {{ $cps->campus_code }} - {{ $cps->campus_description }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <label for="">Courses Code <span class="text-danger">*</span></label>
                                                            <select class="form-control" 
                                                                    multiple 
                                                                    name="course_codes[]"
                                                                    id="course_codes-1" 
                                                                    style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;"
                                                                    required>
                                                                    
                                                                <option value="">------------</option>
                                                                @foreach ($data->courses as $item)
                                                                    <option value="{{ $item->course_code }}">{{ $item->course_code }}</option>
                                                                @endforeach
                                                               
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Effective Term Code <span class="text-danger">*</span></label>
                                                            <select name="effective_tc" class="form-control" required>
                                                                <option value="">------------</option>
                            
                                                                @foreach ($terms->where('academic_year','<','2099') as $t)
                                                                    <option value="{{ $t->term_code }}" {{ $data->effective_term_code == $t->term_code ? 'selected':'' }}>
                                                                        {{ $t->term_code }} - {{ $t->term_description }}
                                                                    </option>
                                                                @endforeach
                                                            </select>   
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">End Term Code <span class="text-danger">*</span></label>
                                                            <select name="end_tc" class="form-control" required>
                                                                <option value="">------------</option>
                            
                                                                @foreach ($terms as $t)
                                                                    <option value="{{ $t->term_code }}" {{ $data->end_term_code == $t->term_code ? 'selected':'' }}>
                                                                        {{ $t->term_code }} - {{ $t->term_description }}
                                                                    </option>
                                                                @endforeach
                                                            </select>   
                                                        </div>
                                                    </div>
                                                </div> --}}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                <button type="submit" class="btn btn-primary">Update</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>No Data Minor</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="minorModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <form action="{{ route('minor.store') }}" method="post">
                @csrf
                
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Minor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    @if($errors->any())
                        <div class="alert alert-danger" role="alert">
                            @foreach($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="modal-body">
                    <input type="hidden" id="minor_count" value="{{ count($minors) }}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">College <span class="text-danger">*</span></label>
                                <select class="form-control {{$errors->has('college_code') ? 'error':''}}" 
                                        value="{{ old('college_code') }}" 
                                        id="college_code"
                                        name="college_code"
                                        required>
                                    <option value="">------------</option>

                                    @foreach ($colleges as $clg)
                                        <option value="{{ $clg->college_code }}">
                                            {{ $clg->college_code }} - {{ $clg->title }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Minor Code</label>
                                <input type="text" name="minor_code" id="minor_code_display" class="form-control" readonly>
                                <input type="hidden" name="minor_code" id="minor_code">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            
                            <div class="form-group">
                                <label for="">Title <span class="text-danger">*</span></label>
                                <input  type="text" 
                                        class="form-control {{$errors->has('description') ? 'error':''}}" 
                                        value="{{ old('description') }}" 
                                        name="description"
                                        required>
                            </div>
                            <div class="form-group">
                                <label for="">Campus Code <span class="text-danger">*</span></label>
                                <select class="form-control {{$errors->has('campus_code') ? 'error':''}}" 
                                        value="{{ old('campus_code') }}" 
                                        name="campus_code"
                                        required>
                                    <option value="">------------</option>

                                    @foreach ($campuses as $cps)
                                        <option value="{{ $cps->campus_code }}">
                                            {{ $cps->campus_code }} - {{ $cps->campus_description }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <div class="">
                                <label for="">Courses Code <span class="text-danger">*</span></label>
                                @foreach ($colleges as $clg)
                                    <div id="select-{{ $clg->college_code }}" style="display: none">
                                        <select class="selectpicker" 
                                                multiple data-live-search="true" 
                                                name="course_codes[]"
                                                {{-- id="course_codes"  --}}
                                                style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;"
                                                required>
                                              
                                            <option value="">------------</option>
                                            @foreach ($courses->where('college_code', $clg->college_code) as $data)
                                                <option value="{{ $data->course_code }}">
                                                    {{ $data->course_code }} - {{ $data->short_title }}
                                                </option>
                                            @endforeach
                                           
                                        </select>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Effective Term Code <span class="text-danger">*</span></label>
                                <select name="effective_tc" class="form-control" required>
                                    <option value="">------------</option>

                                    @foreach ($terms->where('academic_year','<','2099') as $t)
                                        <option value="{{ $t->term_code }}" {{ old('effective_tc') == $t->term_code ? 'selected':'' }}>
                                            {{ $t->term_code }} - {{ $t->term_description }}
                                        </option>
                                    @endforeach
                                </select>   
                            </div>
                            <div class="form-group">
                                <label for="">End Term Code <span class="text-danger">*</span></label>
                                <select name="end_tc" class="form-control" required>
                                    <option value="">------------</option>

                                    @foreach ($terms as $t)
                                        <option value="{{ $t->term_code }}" {{ old('end_tc') == $t->term_code ? 'selected':'' }}>
                                            {{ $t->term_code }} - {{ $t->term_description }}
                                        </option>
                                    @endforeach
                                </select>   
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="editMinorModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <form action="" method="post" id="edit_form">
                @csrf
                
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Minor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">College <span class="text-danger">*</span></label>
                                <select class="form-control {{$errors->has('college_code') ? 'error':''}}" 
                                        value="{{ old('college_code') }}" 
                                        id="edit_college_code"
                                        name="college_code"
                                        disabled>
                                    <option value="">------------</option>

                                    @foreach ($colleges as $clg)
                                        <option value="{{ $clg->college_code }}">
                                            {{ $clg->college_code }} - {{ $clg->title }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Minor Code</label>
                                <input type="text" name="minor_code" id="edit_minor_code_display" class="form-control" readonly>
                                <input type="hidden" name="minor_code" id="edit_minor_code">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            
                            <div class="form-group">
                                <label for="">Title <span class="text-danger">*</span></label>
                                <input  type="text" 
                                        class="form-control {{$errors->has('description') ? 'error':''}}" 
                                        value="{{ old('description') }}" 
                                        name="description"
                                        id="edit_title"
                                        required>
                            </div>
                            <div class="form-group">
                                <label for="">Campus Code <span class="text-danger">*</span></label>
                                <select class="form-control {{$errors->has('campus_code') ? 'error':''}}" 
                                        value="{{ old('campus_code') }}" 
                                        name="campus_code"
                                        id="edit_campus"
                                        required>
                                    <option value="">------------</option>

                                    @foreach ($campuses as $cps)
                                        <option value="{{ $cps->campus_code }}">
                                            {{ $cps->campus_code }} - {{ $cps->campus_description }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <div class="">
                                <label for="">Courses Code <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="edit_show_courses" style="" readonly>
                                @foreach ($colleges as $clg)
                                    <div id="editselect-{{ $clg->college_code }}" style="display: none">
                                        <select class="selectpicker" 
                                                multiple data-live-search="true" 
                                                name="course_codes[]"
                                                id="edit_course_codes-{{ $clg->college_code }}" 
                                                style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;">
                                              
                                            <option value="">------------</option>
                                            @foreach ($courses->where('college_code', $clg->college_code) as $data)
                                                <option value="{{ $data->course_code }}">
                                                    {{ $data->course_code }} - {{ $data->short_title }}
                                                </option>
                                            @endforeach
                                           
                                        </select>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Effective Term Code <span class="text-danger">*</span></label>
                                <select name="effective_tc" class="form-control" id="edit_ef" required>
                                    <option value="">------------</option>

                                    @foreach ($terms->where('academic_year','<','2099') as $t)
                                        <option value="{{ $t->term_code }}" {{ old('effective_tc') == $t->term_code ? 'selected':'' }}>
                                            {{ $t->term_code }} - {{ $t->term_description }}
                                        </option>
                                    @endforeach
                                </select>   
                            </div>
                            <div class="form-group">
                                <label for="">End Term Code <span class="text-danger">*</span></label>
                                <select name="end_tc" class="form-control" id="edit_en" required>
                                    <option value="">------------</option>

                                    @foreach ($terms as $t)
                                        <option value="{{ $t->term_code }}" {{ old('end_tc') == $t->term_code ? 'selected':'' }}>
                                            {{ $t->term_code }} - {{ $t->term_description }}
                                        </option>
                                    @endforeach
                                </select>   
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>