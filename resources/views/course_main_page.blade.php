@extends('layouts.app')

@section('content')
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.bootstrap3.min.css')}}" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <script src="{{ asset('js/app.js') }}" defer></script>  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 1500px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    .Error{
        background: rgb(226, 176, 171);    
    }

    .Success{
        background: rgb(215, 227, 191);    
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    /* @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;} 
    } */
    
  </style>
</head>
<body>

<div class="container-fluid" style="padding-top:10px;">
  <div class="row content" style="">
  <span class="border border-secondary" style="background-color: #b49b4b; margin-bottom:10px;">
    <div class="col-md-2">
      <h6 style="margin-top:10px; color:white;">Notification</h6>
    </div>
  </span>
    <div class="col-sm-1"></div>
    <div class="col-md-9" >
        <div class="header">    
            <a href="{{ route('home') }}">
                <button type="button" class="btn" style="margin-top:-5px; width:24%; background-color: #b49b4b; color:white;">
                <i class="fas fa-tasks"></i>
                Program Management
                </button>
            </a>
            <a href="{{ route('home.course') }}" style ="color:white;">
            <button type="button" class="btn" style="margin-top:-5px; width:24%; background-color: white; color:gray; box-shadow: 0 3px red; border:1px solid gray;">
                <i class="fas fa-university"></i>
                Course Management
            </button>
            </a>
            <a href="{{ route('home.minor') }}">
            <button type="button" class="btn" style="margin-top:-5px; width:25%; background-color: #b49b4b; color:white;">
                <i class="fas fa-table"></i>
                Minor Management
            </button>
            </a>
            <a href="{{ route('home.catalogue') }}">
            <button type="button" class="btn" style="margin-top:-5px; width:25%; background-color: #b49b4b; color:white;">
                <i class="fas fa-table"></i>
                Catalogue Management
            </button>
            </a>
        </div>
        <div class="container mt-3">
                @foreach (['Error', 'warning', 'Confirmation', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <!-- <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p> -->
                    <div class="modal" id="popup1" style="display:block;">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header" style="background: rgb(215, 227, 191)">
                                <h4 style="font-weight:bold;">{{ $msg }}</h4>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body">
                                <p style="font-size:17px;">{{ Session::get('alert-' . $msg) }}</p>
                            </div>  
                            <!-- Modal footer -->
                            <div class="modal-footer">
                            <!-- <a href="{{url('/')}}">
                                <button type="button" class="btn btn-danger">OK</button>
                            </a> -->
                                <button type="button" id="clickmodal" style="width:190px; color:white; border:3px solid #D3D3D3;" class="btn btn-primary" data-dismiss="modal">Ok</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            <br>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" >
                <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#menu1">Course Non-Substantice Changes</a>
                </li>
                <li class="nav-item" style="margin-left:5px;">
                <a class="nav-link" data-toggle="tab" href="#menu2">Course Substantive Changes</a>
                </li>
            </ul>
            <!-- Tab panes -->
            
        <form action="{{ route('course.level') }}" method="post">
            {{ csrf_field() }}
            <div class="tab-content">
              
                <div id="menu1" class="container tab-pane fade"><br>
                    <div class="row content">
                        <div class="col-md-6" style="display:flex">
                            <h4>Course Non-Substantive Changes</h4>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                    <br>
                    <div class="row shade" >
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Changing the course number</p>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('course.level.choose') }}">
                                <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                            </a>
                        </div>
                    </div>
                    <div class="row shade" >
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Changing the course subject</p>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('course.subject.choose') }}">
                                <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                            </a>
                        </div>
                    </div>
                    <div class="row shade">
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Changing the course title</p>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('course.title.choose') }}">
                            <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                            </a>
                        </div>
                    </div>
                    <div class="row shade" >
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Changing course assessment methods</p>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('course.assessment.choose') }}">
                                <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                            </a>
                        </div>
                    </div>
                    <div class="row shade">
                        <div class="col-md-11 sidenav">
                                <p style="color:#656465">Changing course instructional delivery mode</p>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('course.delivery.choose') }}">
                                <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                            </a>
                        </div>
                    </div>
                    <div class="row shade">
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Changing course primary language</p>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('course.delivery.choose') }}">
                                <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                            </a>
                        </div>
                    </div>
                    <div class="row shade" >
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Changing grading mode</p>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('course.grading.choose') }}">
                                <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                            </a>
                        </div>
                    </div>
                    <div class="row shade">
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Change in course genre (Lab, Lecture, Internship, Project, Independent Study, Advising)</p>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('course.type.choose') }}">
                                <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                            </a>
                        </div>
                    </div>
                    <div class="row shade" >
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Changing the assigned textbook/reading</p>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('course.textbook.choose') }}">
                                <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                            </a>
                        </div>
                    </div>
                    <div class="row shade">
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Change in registration restrictions</p>
                        </div>
                        <div class="col-md-1">
                        <a href="{{ route('course.restriction.choose') }}">
                            <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                        </a>
                        </div>
                    </div>
                    <div class="row shade" >
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Changing a course equivalency</p>
                        </div>
                        <div class="col-md-1">
                        <a href="{{ route('course.equivalency.choose') }}">
                            <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                        </a>
                        </div>
                    </div>
                    <div class="row shade" >
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Changing a course prerequisite</p>
                        </div>
                        <div class="col-md-1">
                        <a href="{{ route('course.prerequisite.choose') }}">
                            <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                        </a>
                        </div>
                    </div>
                    <div class="row shade">
                        <div class="col-md-11 sidenav">
                            <p style="color:#656465">Change a course co-requisite</p>
                        </div>
                        <div class="col-md-1">
                        <a href="{{ route('course.corequisite.choose') }}">
                            <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                        </a>
                        </div>
                    </div>
                </div>
                <div id="menu2" class="container tab-pane fade"><br>
                    <div class="row-content">
                        <div class="col-md-6" style="display:flex">
                            <h4>Course Substantive Changes</h4>
                        </div>
                    </div>
                        <br>
                        <div class="row shade" >
                            <div class="col-md-11 sidenav">
                                <p style="color:#656465">Creating New Course</p>
                            </div>
                            <div class="col-md-1">
                                <a href="{{ route('course') }}">
                                <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                                </a>
                            </div>
                        </div>
                        <div class="row shade" >
                            <div class="col-md-11 sidenav">
                                <p style="color:#656465">Discontinuing an existing course</p>
                            </div>
                            <div class="col-md-1">
                                <a href="{{ route('course.inactive.choose') }}">
                                <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                                </a>
                            </div>
                        </div>
                        <div class="row shade">
                            <div class="col-md-11 sidenav">
                                <p style="color:#656465">Making a major change in course description</p>
                            </div>
                            <div class="col-md-1">
                            <a href="{{ route('course.description.choose') }}">
                                <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                            </a>
                            </div>
                        </div>
                        <div class="row shade" >
                            <div class="col-md-11 sidenav">
                                <p style="color:#656465">Making a major change in course outcome</p>
                            </div>
                            <div class="col-md-1">
                            <a href="{{ route('course.outcome.choose') }}">
                                <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                            </a>
                            </div>
                        </div>
                        <div class="row shade">
                            <div class="col-md-11 sidenav">
                                <p style="color:#656465">Changing course hours</p>
                            </div>
                            <div class="col-md-1">
                            <a href="{{ route('course.hour.choose') }}">
                                <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:5px; color:white;">Choose</button>
                            </a>
                            </div>
                        </div>
                        <div class="row shade" >
                            <div class="col-md-11 sidenav">
                                <p style="color:#656465">Changing a course pre/co requisite if one of the courses is offered by another College</p>
                            </div>
                            <div class="col-md-1">
                            <a href="{{ route('course.coprerequisite.choose') }}">
                                <button type="button" class="btn btn-sm" style=" background-color: #b49b4b; margin-top:15px; color:white;">Choose</button>
                            </a>
                            </div>
                        </div>
                </div>
            </div>
        </form>
        </div>
    </div>
  </div>
</div>
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
    
<script>
  $(document).ready(function () {
      $('select').selectize({
          sortField: 'text'
      });
      
    var pre = document.getElementById("popup1");

    $("#clickmodal").click(function(){
        pre.style.display = "none";
    });
  });

</script>
</body>
</html>
@endsection
