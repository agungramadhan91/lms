<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}

</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                            <div class="row">
                            <div class="col-md-8">
                            <a href="{{url('/home/course')}}">
                                <h5 style="text-align:center; margin-top:20px; color:#a99451;">Change course description</h5>
                            </a>
                            </div>
                            </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Course Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                    @if($count == 0)
                        <a href="{{url('course/description')}}"  class="active" style="border: 4px solid #0b4176;">Course Main Attributes</a>
                    @else
                        <a href="{{url('course/description')}}"  class="done" style="border: 4px solid #0b4176;">Course Main Attributes</a>
                    @endif
                    </li>
                    <li>
                        <a href="{{url('course/hour/description')}}" class="inactive">Course Hours</a>
                    </li>
                    <li>
                        <a href="{{url('course/description/requisite')}}" class="inactive">Course Requisite and Equivalencies</a>
                    </li>
                    <li>
                        <a href="{{url('course/association/description')}}" class="inactive">Program Assosiation</a>
                    </li>
                    <li>
                        <a href="{{url('course/description/outcome')}}" class="inactive">Course Learning Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('course/description/assessment')}}" class="inactive">Course Content and Assessments</a>
                    </li>
                    <li>
                        <a href="{{url('course/description/textbook')}}" class="inactive">Assigned Textbook and Other Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/description/research')}}" class="inactive">Optional Reading Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/description/grading')}}" class="inactive">Grading Schema</a>
                    </li>
                    <li>
                        <a href="{{url('course/description/policies')}}" class="inactive">Course Policies</a>
                    </li>
                    <li>
                        <a href="{{url('course/description/special')}}" class="inactive">Course Special Requirements</a>
                    </li>
                    <li>
                        <a href="{{url('course/description/pedagogy')}}" class="inactive">Pedagogy</a>
                    </li>
                    <li>
                        <a href="#" class="inactive">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="{{ route('course.description.store') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
            <div class="flash-message">
                @foreach (['Error', 'warning', 'Confirmation', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <!-- <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p> -->
                    <div class="modal" id="popup1" style="display:block;">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        
                            <!-- Modal Header -->
                          
                            <div class="modal-header {{ $msg }}">
                                <h4>{{ $msg }}</h4>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body">
                                <p>{{ Session::get('alert-' . $msg) }}</p>
                            </div>  
                            <!-- Modal footer -->
                            <div class="modal-footer">
                            <!-- <a href="{{url('/')}}">
                                <button type="button" class="btn btn-danger">OK</button>
                            </a> -->
                                <button type="button" id="clickmodal" class="btn btn-primary" data-dismiss="modal">Ok</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
            @if($course == "0")
            <div id="page-inner">
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Subject</label>    
                            <select class="form-control" name="course" id="subject" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;">
                                <option value="" selected disabled></option>
                                @foreach($subject as $data)
                                <option value="{{$data->subject_code}}">{{$data->subject_code}} - {{$data->subject_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course No</label>
                            <input type="number" class="form-control" placeholder="" name="no" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">Course Status</label>
                            <select class="form-control" name="status" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;" readonly>
                                <option value="" selected>Draft</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">College</label>
                            <select class="form-control" name="college" id="college" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;" readonly>
                                <option value="" selected disabled></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Long Title</label>
                            <input type="text" class="form-control" placeholder="" name="long_title" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">Course Genre</label>
                            <select class="form-control" name="genre" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;">
                                <option value="" selected disabled></option>
                                @foreach($genre as $data)
                                <option value="{{$data->course_genre_code}}">{{$data->course_genre_code}} - {{$data->genre_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">Department</label>
                            <select class="form-control" name="department" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;">
                                <option value="" selected disabled></option>
                                @foreach($department as $data)
                                <option value="{{$data->department_code}}">{{$data->department_code}} - {{$data->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Short Title</label>
                            <input type="text" class="form-control" placeholder="" name="short_title" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">Course Level</label>
                            <select class="form-control" name="level" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;">
                                <option value="" selected disabled></option>
                                @foreach($level as $data)
                                <option value="{{$data->course_level_code}}">{{$data->course_level_code}} - {{$data->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-7">
                        <div class="form-group">
                        <label for="">Course Description</label>
                        <textarea class="form-control" placeholder="" name="description" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:575px;"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">Effective Term</label>
                            <select class="form-control" name="eff_term" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;">
                                <option value="" selected disabled></option>
                                @foreach($effectiveterm as $data)
                                <option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">End Term</label>
                            <select class="form-control" name="end_term" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;">
                                <option value="" selected disabled></option>
                                @foreach($endterm as $data)
                                <option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Save and Exit</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Save and Continue</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="background-color : rgb(255, 242, 204)">
                            <h4 style="font-weight:bold;">Warning !</h4>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p style="font-size:17px;">Your data will be lost if you exit</p>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            @else
            <div id="page-inner">
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Subject</label>    
                            <select class="form-control" name="course" id="subject" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;" readonly>
                                <option value="{{$coursedata->subject_code}}" selected>{{$coursedata->subject_code}} - {{$subject3->subject_description}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course No</label>
                            <input type="number" class="form-control" placeholder="" value="{{$number}}" name="no" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;" readonly>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">Course Status</label>
                            <select class="form-control" name="status" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;" readonly>
                                <option value="" selected>{{$coursedata->status}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">College</label>
                            <select class="form-control" name="college" id="college" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;" readonly>
                                <option value="{{$coursedata->college_code}}" selected>{{$coursedata->college_code}} - {{$college2->title}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Long Title</label>
                            <input type="text" class="form-control" value="{{$coursedata->long_title}}" placeholder="" name="long_title" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;" readonly>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">Course Genre</label>
                            <select class="form-control" name="genre" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;" readonly>
                                <option value="{{$coursedata->course_genre_code_01}}" selected>{{$coursedata->course_genre_code_01}} - {{$genre2->genre_description}}</option>
                                @foreach($genre as $data)
                                <option value="{{$data->course_genre_code}}">{{$data->course_genre_code}} - {{$data->genre_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">Department</label>
                            <select class="form-control" name="department" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;" readonly>
                                @if($coursedata->department == "")
                                <option value="" selected>No Information Added</option>
                                @else
                                <option value="{{$coursedata->department}}" selected>{{$coursedata->department}}</option>
                                @endif
                                @foreach($department as $data)
                                <option value="{{$data->department_code}}">{{$data->department_code}} - {{$data->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Short Title</label>
                            <input type="text" class="form-control" placeholder="" value="{{$coursedata->short_title}}" name="short_title" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;" readonly>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">Course Level</label>
                            <select class="form-control" name="level" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;" readonly>
                                <option value="{{$coursedata->course_level_code}}" selected>{{$coursedata->course_level_code}} - {{$level2->title}}</option>
                                @foreach($level as $data)
                                <option value="{{$data->course_level_code}}">{{$data->course_level_code}} - {{$data->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-7">
                        <div class="form-group">
                        <label for="">Course Description</label>
                        @if($count == 0)
                        <textarea class="form-control" placeholder="" name="description" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9; width:575px;">{{$coursedata->description}}</textarea>
                        @else
                        <textarea class="form-control" placeholder="" name="description" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:575px;">{{$coursedata->description}}</textarea>
                        @endif
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">Effective Term</label>
                            @if($count == 0)
                            <select class="form-control" id="effterm" name="eff_term" style="background-color:#fffbf1; border:2px solid red; color:#6fa7d9; width:120%; width:230px;">
                            @else
                            <select class="form-control" id="effterm" name="eff_term" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;">
                            @endif
                                <option value="{{$term2->term_code}}" selected>{{$term2->term_code}} - {{$term2->term_description}}</option>
                                @foreach($effectiveterm as $data)
                                <option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">End Term</label>
                            @if($count == 0)
                            <select class="form-control" name="end_term" style="background-color:#fffbf1; border:2px solid red; color:#6fa7d9; width:120%; width:230px;">
                            @else
                            <select class="form-control" name="end_term" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;">
                            @endif
                            <option value="{{$term3->term_code}}" selected>{{$term3->term_code}} - {{$term3->term_description}}</option>
                                @foreach($endterm as $data)
                                <option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group" id="save">
                            <button type="submit" class="btn btn-primary" style="width:190px; color:white; border:3px solid #D3D3D3;">Save</button>
                        </div>              
                </form>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">                    
                        <form action="{{ route('course.description.submit') }}" method="post">
                                {{ csrf_field() }}
                            <button type="submit" class="btn" style="width:190px; background-color:rgb(215, 227, 191); font-weight:bold; color:black; border:3px solid black;" data-toggle="modal" data-target="#myModal2">Submit The Application</button>
                        </form>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="background-color : rgb(255, 242, 204)">
                            <h4 style="font-weight:bold;">Warning !</h4>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p style="font-size:17px;">Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/home/course')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            @endif
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</body>
</html>
<script>
$(document).ready(function () {
    
    var pre = document.getElementById("popup1");

    $("#clickmodal").click(function(){
        pre.style.display = "none";
    });

    $("#subject").change(function () {
        var val = $(this).val();

        if (val == "AAD" || val == "AAH" || val == "ADR" || val == "AGD" || val == "AID" || val == "ART" || val == "AVA") {
            $("#college").html("<option value='CA' selected>College of Arts and Creative Enterprise</option>");
        } else if (val == "ACC" || val == "BUS" || val == "ECN" || val == "FIN" || val == "HRM" || val == "INS" || val == "LAW" || val == "MKT" || val == "OPR") {
            $("#college").html("<option value='CB' selected>College of Business</option>");
        } else if (val == "COM" || val == "ISC" || val == "MPS" || val == "TCC") {
            $("#college").html("<option value='CM' selected>College of Communication</option>");
        } else if (val == "APL" || val == "ECE" || val == "EDC" || val == "ETC" || val == "MSE" || val == "SPE") {
            $("#college").html("<option value='ED' selected>College of Education</option>");
        } else if (val == "ANT" || val == "ARA" || val == "FLS" || val == "HIS" || val == "POL" || val == "SOC") {
            $("#college").html("<option value='HS' selected>College of Humanities and Social Sciences</option>");
        } else if (val == "BIO" || val == "CHE" || val == "PHN" || val == "PHY") {
            $("#college").html("<option value='NH' selected>College of Natural and Health Sciences</option>");
        } else if (val == "CIT" || val == "IMT" || val == "SWE") {
            $("#college").html("<option value='TI' selected>College of Technological Innovation</option>");
        } else if (val == "ADV" || val == "GEN" || val == "MTH") {
            $("#college").html("<option value='UC' selected>University College</option>");
        }
    });
});
</script>