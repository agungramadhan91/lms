<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}

</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                            <div class="row">
                            <div class="col-md-8">
                            <a href="{{url('/home/course')}}">
                                <h5 style="text-align:center; margin-top:20px; color:#a99451;">Changing Course Prerequisite</h5>
                            </a>
                            </div>
                            </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Course Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="{{url('course/level/choose')}}" class="inactive">Course Main Attributes</a>
                    </li>
                    <li>
                        <a href="{{url('course/hour')}}" class="inactive">Course Hours</a>
                    </li>
                    <li>
                        <a href="{{url('/course/requisite/edit')}}"  class="active" style="border: 4px solid #0b4176;">Course Requisite and Equivalencies</a>
                    </li>
                    <li>
                        <a href="{{url('course/association/level')}}" class="inactive">Program Assosiation</a>
                    </li>
                    <li>
                        <a href="{{url('course/outcome')}}" class="inactive">Course Learning Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('course/assessment')}}" class="inactive">Course Content and Assessments</a>
                    </li>
                    <li>
                        <a href="{{url('course/textbook')}}" class="inactive">Assigned Textbook and Other Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/optional')}}" class="inactive">Optional Reading Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/grading')}}" class="inactive">Grading Schema</a>
                    </li>
                    <li>
                        <a href="{{url('course/policies')}}" class="inactive">Course Policies</a>
                    </li>
                    <li>
                        <a href="{{url('course/cost')}}" class="inactive">Course Special Requirements</a>
                    </li>
                    <li>
                        <a href="{{url('course/community')}}" class="inactive">Pedagogy</a>
                    </li>
                    <li>
                        <a href="#" class="inactive">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="{{ route('course.prerequisite.update') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
                @endforeach
            </div>
            <div id="page-inner">
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                    <p style="color:#0b4176; font-size:17px; text-weigth:bold; font-family:times new roman;">This Course Replaces</p>
                    </div>
                </div>
                <br><br>
                <p style="color:#0b4176; font-size:17px; text-weigth:bold; font-family:times new roman;">Course Prerequisites</p>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <button type="button" id="addpre" class="bttn" style="background-color: #b1cfeb; margin-top:10px;width:240px;">Insert Prerequisites <i class="fas fa-plus"></i></button>
                    </div>
                    <div class="col-md-3" style="display:flex">
                        <button type="button" id="inactive" class="bttn" style="background-color: #b1cfeb; margin-top:10px;width:240px;">Inactivate</button>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group" style="margin-top:-20px;">
                            <label for="">Course Code</label>
                            <input type="text" class="form-control" value="{{$course->course_code}}" placeholder="" name="idpre1" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:130px;">
                        </div>
                    </div>
                </div>
                <br><br>
                @if($countprerequisite != "0")
                @foreach($prerequisite as $data)
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Prerequisite Course Code</label>
                            <select class="form-control" name="idpre" id="idpre" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$data->course_code}}">{{$data->course_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Prerequisite ID</label>
                            <input type="text" class="form-control" value="{{$data->course_prerequisite_code}}" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Course Title (only active course)</label>
                            <select class="form-control" name="titlepre" id="titlepre" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$title2->long_title}}">{{$title2->long_title}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Effective Term</label>
                            <select class="form-control" name="effpre" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;">
                            <option value="{{$data->effective_term_code}}">{{$data->effective_term_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">End Term</label>
                            <select class="form-control" id="endterm" name="endpre" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;">
                            <option value="{{$data->end_term_code}}">{{$data->end_term_code}}</option>
                            @foreach($effectiveterm as $data2)
                            @if($data2->term_code < $data->end_term_code)
                            <option value="{{$data2->term_code}}">{{$data2->term_code}} - {{$data2->term_description}}</option>
                            @endif
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
                <div id="pre">
                </div>
                <br>
                <p style="color:#0b4176; font-size:17px; text-weigth:bold; font-family:times new roman;">Course Co-requisites</p>
                <br><br>
                <br>
                <p style="color:#0b4176; font-size:17px; text-weigth:bold; font-family:times new roman;">Course Equivalency</p>
                <br><br>
                <br><br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" name="continue" id="continue" class="btn btn-primary" style="width:190px;  color:white; border:3px solid #D3D3D3;">Save</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn" style="width:190px; background-color:rgb(215, 227, 191); font-weight:bold; color:black; border:3px solid black;" data-toggle="modal" data-target="#myModal2">Submit The Application</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="">
                            <h2>Exit</h2>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p>Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <form>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</body>
</html>
<script>
function remove(){
        var row = document.getElementById("row");
         row.style.display = "none";
    }
$(document).ready(function () {
    
    $("#inactive").click(function(){    
        var pre = document.getElementById("endterm");
            pre.style.border = "2px solid red";
    });

    $("#continue").click(function(){    
        var effval = $("effectiveterm").val();
        var end = document.getElementById("endterm");
        var eff = document.getElementById("effectiveterm");
        if( effval == ""){
            eff.style.border = "2px solid red";
        }
    });

    var max_fields2      = 99; //maximum input boxes allowed
	var wrapper2   		= $("#pre"); //Fields wrapper
	var add_button2      = $("#addpre"); //Add button ID
	
	var y = 0; //initlal text box count
    var yy = 1;
	$(add_button2).click(function(e){ //on add input button click
		e.preventDefault();
		if(y < max_fields2){ //max input box allowed
			y++; //text box increment
            yy++;
			$(wrapper2).append(
                '<a href="#" class="remove_field2" style="margin-left:90%;">remove <i class="fas fa-times"></i></a>'+
                '<div class="row content">'+
                    '<div class="col-md-3">'+
                        '<div class="form-group">'+
                            '<label for="">Prerequisite Course Code</label>'+
                            '<select class="form-control" name="idpre" id="idpre" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">'+
                            '<option value=""></option>'+
                            '@foreach($course2 as $data)'+
                            '<option value="{{$data->course_code}}">{{$data->course_code}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group" id="precode">'+
                            '<label for="">Prerequisite ID</label>'+
                            '<input type="text" value="" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<div class="form-group">'+
                            '<label for="">Course Title (only active course)</label>'+
                            '<select class="form-control" name="titlepre" id="titlepre" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">'+
                            '<option value=""></option>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">Effective Term</label>'+
                            '<select class="form-control" name="effpre" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;">'+
                            '<option value=""></option>'+
                            '@foreach($effectiveterm as $data)'+
                            '<option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">End Term</label>'+
                            '<select class="form-control" name="endpre" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;">'+
                            '<option value=""></option>'+
                            '@foreach($endterm as $data)'+
                            '<option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                '</div>'
                ); //add input box
		}

	});
	
	$(wrapper2).on("click",".remove_field2", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); y--;
	});

    
	$(wrapper2).on("change","#condition2", function(e){ //user click on remove text
		var valpre = $(this).val();
        var pre = document.getElementById("pre2");
        if (valpre == "none") {
            pre.style.display = "none";
        }else{
            pre.style.display = "block";
        }
	});

    $(wrapper2).on("change","#idpre", function(e){ //user click on remove text
		var val = $(this).val();
        $("#titlepre").html(
            "@foreach($courseactive as $data)"+
            '<option value="{{$data->long_title}}">{{$data->long_title}}</option>'+
            "@endforeach"
        );

        $("#precode").html(
            '<label for="">Prerequisite ID</label>'+
            '<input type="text" value="'+val+'_P_00'+y+'" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>'
        )
	});

    var max_fields3      = 99; //maximum input boxes allowed
	var wrapper3   		= $("#co"); //Fields wrapper
	var add_button3      = $("#addco"); //Add button ID
	
	var z = 0; //initlal text box count
    var zz = 1;
	$(add_button3).click(function(e){ //on add input button click
		e.preventDefault();
		if(z < max_fields3){ //max input box allowed
			z++; //text box increment
            zz++;
			$(wrapper3).append(
                '<a href="#" class="remove_field" style="margin-left:90%;">remove <i class="fas fa-times"></i></a>'+
                '<div class="row content">'+
                    '<div class="col-md-1">'+
                        '<div class="form-group">'+
                            '<label for="">Sequence</label>'+
                            '<input type="text" value="'+z+'" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">Course ID</label>'+
                            '<select class="form-control" name="idco" id="idco" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">'+
                            '<option value=""></option>'+
                            '@foreach($courseactive as $data)'+
                            '<option value="{{$data->course_code}}">{{$data->course_code}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<div class="form-group">'+
                            '<label for="">Course Title (only active course)</label>'+
                            '<select class="form-control" name="titleco" id="titleco" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">Effective Term</label>'+
                            '<select class="form-control" name="effco" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">'+
                            '<option value=""></option>'+
                            '@foreach($effectiveterm as $data)'+
                            '<option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">End Term</label>'+
                            '<select class="form-control" name="endco" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">'+
                            '<option value=""></option>'+
                            '@foreach($endterm as $data)'+
                            '<option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="row content">'+
                    '<div class="col-md-3">'+
                        '<div class="form-group">'+
                            '<label for="">Condition</label>'+
                            '<select class="form-control" name="condition3" id="condition3" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;">'+
                                '<option value=""></option>'+
                                '<option value="and">And</option>'+
                                '<option value="or">Or</option>'+
                                '<option value="none">None</option>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="row content" id="co2" style="display:none;">'+
                    '<div class="col-md-1">'+
                        '<div class="form-group">'+
                            '<label for="">Sequence</label>'+
                            '<input type="text" value="'+zz+'" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">Course ID</label>'+
                            '<select class="form-control" id="idco2" name="idco2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">'+
                            '<option value=""></option>'+
                            '@foreach($courseactive as $data)'+
                            '<option value="{{$data->course_code}}">{{$data->course_code}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<div class="form-group">'+
                            '<label for="">Course Title (only active course)</label>'+
                            '<select class="form-control" id="titleco2" name="titleco2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">Effective Term</label>'+
                            '<select class="form-control" name="effco2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">'+
                            '<option value=""></option>'+
                            '@foreach($effectiveterm as $data)'+
                            '<option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>'+
                            '@endforeach'+
                            '</select>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">End Term</label>'+
                            '<select class="form-control" name="endco2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">'+
                            '<option value=""></option>'+
                            '@foreach($endterm as $data)'+
                            '<option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>'+
                            '@endforeach'+
                            '</select>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                '</div>'
                ); //add input box
		}
	});
	
	$(wrapper3).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); z--;
	});

    $(wrapper3).on("change","#condition3", function(e){ //user click on remove text
		var valpre = $(this).val();
        var pre = document.getElementById("co2");
        if (valpre == "none") {
            pre.style.display = "none";
        }else{
            pre.style.display = "block";
        }
	});

    
    $(wrapper3).on("change","#idco", function(e){ //user click on remove text
		var val = $(this).val();
        $("#titleco").html(
            "@foreach($courseactive as $data)"+
            '<option value="{{$data->long_title}}">{{$data->long_title}}</option>'+
            "@endforeach"
        );
	});

    $(wrapper3).on("change","#idco2", function(e){ //user click on remove text
		var val = $(this).val();
        $("#titleco2").html(
            "@foreach($courseactive as $data)"+
            '<option value="{{$data->long_title}}">{{$data->long_title}}</option>'+
            "@endforeach"
        );
	});

    
    var max_fields4      = 99; //maximum input boxes allowed
	var wrapper4   		= $("#qui"); //Fields wrapper
	var add_button4      = $("#addqui"); //Add button ID
	
	var equi = 0; //initlal text box count
    var equi2 = 1;
	$(add_button4).click(function(e){ //on add input button click
		e.preventDefault();
		if(equi < max_fields4){ //max input box allowed
			equi++; //text box increment
            equi2++;
			$(wrapper4).append(
                '<a href="#" class="remove_field" style="margin-left:90%;">remove <i class="fas fa-times"></i></a>'+
                '<div class="row content">'+
                    '<div class="col-md-1">'+
                        '<div class="form-group">'+
                            '<label for="">Sequence</label>'+
                            '<input type="text" value="'+equi+'" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">Course ID</label>'+
                            '<select class="form-control" name="idqui" id="idqui" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">'+
                            '<option value=""></option>'+
                            '@foreach($courseactive as $data)'+
                            '<option value="{{$data->course_code}}">{{$data->course_code}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<div class="form-group">'+
                            '<label for="">Course Title (only active course)</label>'+
                            '<select class="form-control" name="titlequi" id="titlequi" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">Effective Term</label>'+
                            '<select class="form-control" name="effqui" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">'+
                            '<option value=""></option>'+
                            '@foreach($effectiveterm as $data)'+
                            '<option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">End Term</label>'+
                            '<select class="form-control" name="endqui" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">'+
                            '<option value=""></option>'+
                            '@foreach($endterm as $data)'+
                            '<option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="row content">'+
                    '<div class="col-md-3">'+
                        '<div class="form-group">'+
                            '<label for="">Condition</label>'+
                            '<select class="form-control" name="condition4" id="condition4" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;">'+
                                '<option value=""></option>'+
                                '<option value="and">And</option>'+
                                '<option value="or">Or</option>'+
                                '<option value="none">None</option>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="row content" id="qui2" style="display:none;">'+
                    '<div class="col-md-1">'+
                        '<div class="form-group">'+
                            '<label for="">Sequence</label>'+
                            '<input type="text" value="'+equi2+'" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">Course ID</label>'+
                            '<select class="form-control" name="idqui2" id="idqui2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">'+
                            '<option value=""></option>'+
                            '@foreach($courseactive as $data)'+
                            '<option value="{{$data->course_code}}">{{$data->course_code}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<div class="form-group">'+
                            '<label for="">Course Title (only active course)</label>'+
                            '<select class="form-control" name="titlequi2" id="titlequi2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">Effective Term</label>'+
                            '<select class="form-control" name="effqui2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">'+
                            '<option value=""></option>'+
                            '@foreach($effectiveterm as $data)'+
                            '<option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">End Term</label>'+
                            '<select class="form-control" name="endqui2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">'+
                            '<option value=""></option>'+
                            '@foreach($endterm as $data)'+
                            '<option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                '</div>'
                ); //add input box
		}

        $('#idqui').html(
            '<label for="">ID</label>'+
            '<input type="text" class="form-control" placeholder="" name="idqui1" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:130px;" required value="{{$course->full_course_code}}_E_00'+equi+'">'
        );
	});
	
	$(wrapper4).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); y--;
	});

    $(wrapper4).on("change","#condition4", function(e){ //user click on remove text
		var valpre = $(this).val();
        var pre = document.getElementById("qui2");
        if (valpre == "none") {
            pre.style.display = "none";
        }else{
            pre.style.display = "block";
        }
	});

    
    $(wrapper4).on("change","#idqui", function(e){ //user click on remove text
		var val = $(this).val();
        $("#titlequi").html(
            "@foreach($courseactive as $data)"+
            '<option value="{{$data->long_title}}">{{$data->long_title}}</option>'+
            "@endforeach"
        );
	});

    $(wrapper4).on("change","#idqui2", function(e){ //user click on remove text
		var val = $(this).val();
        $("#titlequi2").html(
            "@foreach($courseactive as $data)"+
            '<option value="{{$data->long_title}}">{{$data->long_title}}</option>'+
            "@endforeach"
        );
	});

});
</script>