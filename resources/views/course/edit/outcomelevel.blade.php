<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script>
    </script>
<style type="text/css">

.requiredStar{
    color:red;
}

hr {
    border: none;
    height: 1px;
    /* Set the hr color */
    color: #333; /* old IE */
    background-color: #333; /* Modern Browsers */
}
.MultiCheckBox {
    border:1px solid #e2e2e2;
    padding: 5px;
    border-radius:4px;
    cursor:pointer;
}

.MultiCheckBox .k-icon{ 
    font-size: 15px;
    float: right;
    font-weight: bolder;
    margin-top: -7px;
    height: 10px;
    width: 14px;
    color:#787878;
} 

.MultiCheckBoxDetail {
    display:none;
    position:absolute;
    border:1px solid #e2e2e2;
    overflow-y:hidden;
    margin-bottom:50px

}

.MultiCheckBoxDetailBody {
    overflow-y:scroll;
}

    .MultiCheckBoxDetail .cont  {
        clear:both;
        overflow: hidden;
        padding: 2px;
    }

    .MultiCheckBoxDetail .cont:hover  {
        background-color:#cfcfcf;
    }

    .MultiCheckBoxDetailBody > div > div {
        float:left;
    }

.MultiCheckBoxDetail>div>div:nth-child(1) {

}

.MultiCheckBoxDetailHeader {
    overflow:hidden;
    position:relative;
    height: 28px;
    background-color:#3d3d3d;
}

.MultiCheckBoxDetailHeader>input {
    position: absolute;
    top: 4px;
    left: 3px;
}

.MultiCheckBoxDetailHeader>div {
    position: absolute;
    top: 5px;
    left: 24px;
    color:#fff;
}
</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                            <div class="row">
                            <div class="col-md-8">
                            <a href="{{url('/home/course')}}">
                                <h5 style="text-align:center; margin-top:20px; color:#a99451;">Changing Course Number</h5>
                            </a>
                            </div>
                            </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Course Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="{{url('course/level')}}" class="active">Course Main Attributes</a>
                    </li>
                    <li>
                        <a href="{{url('course/hour')}}" class="inactive">Course Hours</a>
                    </li>
                    <li>
                        <a href="{{url('course/requisite/edit')}}" class="active">Course Requisite and Equivalencies</a>
                    </li>
                    <li>
                        <a href="{{url('course/association/level')}}" class="active">Program Assosiation</a>
                    </li>
                    <li>
                        <a href="{{url('course/outcome/level')}}" class="active"  style="border: 4px solid #0b4176;">Course Learning Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('course/assessment')}}" class="inactive">Course Content and Assessments</a>
                    </li>
                    <li>
                        <a href="{{url('course/textbook')}}" class="inactive">Assigned Textbook and Other Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/optional')}}" class="inactive">Optional Reading Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/grading')}}" class="inactive">Grading Schema</a>
                    </li>
                    <li>
                        <a href="{{url('course/policies')}}" class="inactive">Course Policies</a>
                    </li>
                    <li>
                        <a href="{{url('course/special')}}" class="inactive">Course Special Requirements</a>
                    </li>
                    <li>
                        <a href="{{url('course/pedagogy')}}" class="inactive">Pedagogy</a>
                    </li>
                    <li>
                        <a href="#" class="inactive">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="{{ route('course.outcomelevel.update') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
                @endforeach
            </div>
            <div id="page-inner">
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <p style="color:#0b4176; font-size:17px; text-weigth:bold; font-family:times new roman;">Course Learning Outcomes</p>
                    </div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Code</label>
                            <input type="text" class="form-control" name="old" value="{{$coursecode2}}" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>
                        </div>
                    </div>
                </div>
                <br><br>
                <?php $i=1;?>
                @if($countclo == 0)
                    @foreach($clo as $data)
                    <div id="induk">
                    <div class="row content">
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                                <label for="">ID</label>
                                <input type="text" class="form-control" id="clo" name="clo_code[{{$i}}]" value="{{$course->subject_code}}_{{$number}}_CLO_0{{$i}}" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9;">
                            </div>
                        </div>
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                                <label for="">Course Learning Outcome Description</label>
                                <textarea name="text[{{$i}}]" class="form-control" placeholder="" style="border:2px solid #6fa7d9; color:#6fa7d9; width:500px" readonly>{{$data->clo_description}}</textarea>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <?php $i++?>
                    @endforeach
                @else
                    @foreach($clo as $data)
                    <div id="induk">
                    <div class="row content">
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                                <label for="">ID</label>
                                <input type="text" class="form-control" id="clo" name="clo_code[{{$i}}]" value="{{$course->subject_code}}_{{$number}}_CLO_0{{$i}}" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">
                            </div>
                        </div>
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                                <label for="">Course Learning Outcome Description</label>
                                <textarea name="text[{{$i}}]" class="form-control" placeholder="" style="border:2px solid #6fa7d9; color:#6fa7d9; width:500px" readonly>{{$data->clo_description}}</textarea>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <?php $i++?>
                    @endforeach
                @endif
                <br><br><br>
                <div class="row content">
                    <div class="col-md-8" style="display:flex">
                        <div class="form-group">
                            <label for="">Program</label>
                            <select class="form-control" name="program" id="program" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; " readonly>
                                @if($countmapping != "0")
                                <option value="{{$mappingtitle->program_code}}">{{$mappingtitle->program_code}} - {{$mappingtitle->title}}</option>
                                @else
                                <option value=""></option>
                                @foreach($program as $data)
                                <option value="{{$data->program_code}}">{{$data->program_code}} - {{$data->title}}</option>
                                @endforeach
                                @endif
                            </select>
                            
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-12" style="display:flex">
                        <div class="form-group">
                            <label for="">PLO ID Mapping</label>
                                <div style="border:2px solid #6fa7d9; color:#6fa7d9; background-color:#fffbf1; width:1000px;">
                                @if($countmapping != "0")
                                @foreach($mapping as $data)
                                @foreach($mappingplo as $data2)
                                @if($data->plo_code == $data2->plo_code)
                                <div class="row content">
                                <label for="one">
                                    <input type="checkbox" style="margin-left:20px;" id="one" name="plo[]" value="{{$data->plo_code}}" checked readonly/>&nbsp;{{$data->plo_code}} - {{$data2->plo_description}}</label>
                                </div>
                                @endif
                                @endforeach
                                @endforeach
                                @else
                                <label for="one">
                                <input type="checkbox" id="one" name="plo[]"/>No Data Added</label>
                                @endif
                                </div>
                        </div>
                    </div>
                </div>
                </div>
                <br>
                <div id="txt">
                </div>
                <br><br>
                <div class="row content" >
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" style="width:190px; color:white; border:3px solid #D3D3D3;">Save</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn" style="width:190px; background-color:rgb(215, 227, 191); font-weight:bold; color:black; border:3px solid black;" data-toggle="modal" data-target="#myModal2">Submit The Application</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="">
                            <h2>Exit</h2>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p>Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->

     
    </div>
    <form>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
    
</body>
</html>
<script>
var expanded2 = false;

    // window.addEventListener("click", function(event) {
    // // var checkboxes2 = document.getElementById("checkboxes2");
    // if (!expanded2) {
    //         alert('ok');
    //         expanded2 = true;
    //     }

    // if (expanded2) {
    //     alert('no');
    //     expanded2 = false;
    // }
    // // });
    // var body = document.getElementById('txt');
    // var except = document.getElementById('except');
    // body.addEventListener("click", function () {
    //     alert("wrapper");
    // }, false);

    
    // except.addEventListener("click", function (ev) {
    //     alert("except");
    //     ev.stopPropagation();
    // }, false);


    function remove(){
        var row = document.getElementById("induk");
         row.style.display = "none";
    }

    function showCheckboxes2() {
        var checkboxes2 = document.getElementById("checkboxes2");
        if (!expanded2) {
            checkboxes2.style.display = "block";
            expanded2 = true;
        } else {
            checkboxes2.style.display = "none";
            expanded2 = false;
        }
    }

    // function program(x) {
    //     var asd = document.getElementById("clo2_"+x).value;
    //     var checkboxes2 = $("#checkbox")
    //     $(checkboxes).append(
    //         
    //     );   
    // }

$(document).ready(function () {

    var max_fields      = 99; //maximum input boxes allowed
	var wrapper   		= $("#txt"); //Fields wrapper
    var add_button      = $("#addtxt"); //Add button ID

	
	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append(
                '<hr>'+
                '<a href="#" class="remove_field" style="margin-left:90%;">remove <i class="fas fa-times"></i></a>'+
                '<br><br>'+
                '<div class="row content">'+
                    '<div class="col-md-3" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">ID</label>'+
                            '<input type="text" class="form-control" id="clo2_'+x+'" name="clo_code['+x+']" value="{{$course->course_code}}_CL_00'+x+'" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-3" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Course Learning Outcome Description</label>'+
                            '<textarea name="text['+x+']" class="form-control" placeholder="" style="border:2px solid #6fa7d9; color:#6fa7d9; width:500px"></textarea>'+
                        '</div>'+
                    '</div>'+
                '</div>'
            ); //add input box
		}
	});

	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    });

    $(document).ready(function(){ //user click on remove text
        $("#test").CreateMultiCheckBox({ width: '900px', defaultText : 'Select Below', height:'250px' });
        $("#test2").CreateMultiCheckBox({ width: '900px', defaultText : 'Select Below', height:'250px' });
        $("#test3").CreateMultiCheckBox({ width: '900px', defaultText : 'Select Below', height:'250px' });
    });
    
});


    $(document).ready(function () {
            $(document).on("click", ".MultiCheckBox", function () {
                var detail = $(this).next();
                detail.show();
            });

            $(document).on("click", ".MultiCheckBoxDetailHeader input", function (e) {
                e.stopPropagation();
                var hc = $(this).prop("checked");
                $(this).closest(".MultiCheckBoxDetail").find(".MultiCheckBoxDetailBody input").prop("checked", hc);
                $(this).closest(".MultiCheckBoxDetail").next().UpdateSelect();
            });

            $(document).on("click", ".MultiCheckBoxDetailHeader", function (e) {
                var inp = $(this).find("input");
                var chk = inp.prop("checked");
                inp.prop("checked", !chk);
                $(this).closest(".MultiCheckBoxDetail").find(".MultiCheckBoxDetailBody input").prop("checked", !chk);
                $(this).closest(".MultiCheckBoxDetail").next().UpdateSelect();
            });

            $(document).on("click", ".MultiCheckBoxDetail .cont input", function (e) {
                e.stopPropagation();
                $(this).closest(".MultiCheckBoxDetail").next().UpdateSelect();

                var val = ($(".MultiCheckBoxDetailBody input:checked").length == $(".MultiCheckBoxDetailBody input").length)
                $(".MultiCheckBoxDetailHeader input").prop("checked", val);
            });

            $(document).on("click", ".MultiCheckBoxDetail .cont", function (e) {
                var inp = $(this).find("input");
                var chk = inp.prop("checked");
                inp.prop("checked", !chk);

                var multiCheckBoxDetail = $(this).closest(".MultiCheckBoxDetail");
                var multiCheckBoxDetailBody = $(this).closest(".MultiCheckBoxDetailBody");
                multiCheckBoxDetail.next().UpdateSelect();

                var val = ($(".MultiCheckBoxDetailBody input:checked").length == $(".MultiCheckBoxDetailBody input").length)
                $(".MultiCheckBoxDetailHeader input").prop("checked", val);
            });

            $(document).mouseup(function (e) {
                var container = $(".MultiCheckBoxDetail");
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    container.hide();
                }
            });
        });

        var defaultMultiCheckBoxOption = { width: '220px', defaultText: 'Select Below', height: '200px' };

        jQuery.fn.extend({
            CreateMultiCheckBox: function (options) {

                var localOption = {};
                localOption.width = (options != null && options.width != null && options.width != undefined) ? options.width : defaultMultiCheckBoxOption.width;
                localOption.defaultText = (options != null && options.defaultText != null && options.defaultText != undefined) ? options.defaultText : defaultMultiCheckBoxOption.defaultText;
                localOption.height = (options != null && options.height != null && options.height != undefined) ? options.height : defaultMultiCheckBoxOption.height;

                this.hide();
                this.attr("multiple", "multiple");
                var divSel = $("<div class='MultiCheckBox'>" + localOption.defaultText + "<span class='k-icon k-i-arrow-60-down'><svg aria-hidden='true' focusable='false' data-prefix='fas' data-icon='sort-down' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 320 512' class='svg-inline--fa fa-sort-down fa-w-10 fa-2x'><path fill='currentColor' d='M41 288h238c21.4 0 32.1 25.9 17 41L177 448c-9.4 9.4-24.6 9.4-33.9 0L24 329c-15.1-15.1-4.4-41 17-41z' class=''></path></svg></span></div>").insertBefore(this);
                divSel.css({ "width": localOption.width });

                var detail = $("<div class='MultiCheckBoxDetail'><div class='MultiCheckBoxDetailHeader'><input type='checkbox' class='mulinput' value='-1982' /><div>Select All</div></div><div class='MultiCheckBoxDetailBody'></div></div>").insertAfter(divSel);
                detail.css({ "width": parseInt(options.width) + 10, "max-height": localOption.height });
                var multiCheckBoxDetailBody = detail.find(".MultiCheckBoxDetailBody");

                this.find("option").each(function () {
                    var val = $(this).attr("value");

                    if (val == undefined)
                        val = '';

                    multiCheckBoxDetailBody.append("<div class='cont'><div><input type='checkbox' class='mulinput' value='" + val + "' /></div><div>" + $(this).text() + "</div></div>");
                });

                multiCheckBoxDetailBody.css("max-height", (parseInt($(".MultiCheckBoxDetail").css("max-height")) - 28) + "px");
            },
            UpdateSelect: function () {
                var arr = [];

                this.prev().find(".mulinput:checked").each(function () {
                    arr.push($(this).val());
                });

                this.val(arr);
            },
        });
</script>