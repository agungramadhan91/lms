<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}

</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                            <div class="row">
                            <div class="col-md-10">
                            <a href="{{url('/home/course')}}">
                                <h5 style="text-align:center; margin-top:20px; color:#a99451;">Changing Grading Schema</h5>
                            </a>
                            </div>
                            </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Course Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="{{url('course')}}" class="inactive">Course Main Attributes</a>
                    </li>
                    <li>
                        <a href="{{url('course/hour')}}" class="inactive">Course Hours</a>
                    </li>
                    <li>
                        <a href="{{url('course/requisite')}}" class="inactive">Course Requisite and Equivalencies</a>
                    </li>
                    <li>
                        <a href="{{url('course/association')}}" class="inactive">Program Assosiation</a>
                    </li>
                    <li>
                        <a href="{{url('course/outcome')}}" class="inactive">Course Learning Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('course/assessment')}}" class="inactive">Course Content and Assessments</a>
                    </li>
                    <li>
                        <a href="{{url('course/textbook')}}" class="inactive">Assigned Textbook and Other Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/optional')}}" class="inactive">Optional Reading Materials</a>
                    </li>
                    <li>
                    @if($count == 0)
                        <a href="{{url('course/grading')}}" class="active" style="border: 4px solid #0b4176;">Grading Schema</a>
                    @else
                        <a href="{{url('course/grading')}}" class="done" style="border: 4px solid #0b4176;">Grading Schema</a>
                    @endif
                    </li>
                    <li>
                        <a href="{{url('course/policies')}}" class="inactive">Course Policies</a>
                    </li>
                    <li>
                        <a href="{{url('course/special')}}" class="inactive">Course Special Requirements</a>
                    </li>
                    <li>
                        <a href="{{url('course/pedagogy')}}" class="inactive">Pedagogy</a>
                    </li>
                    <li>
                        <a href="#" class="inactive">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="{{ route('course.grading.update') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
            <div class="flash-message">
                
            @foreach (['Error', 'warning', 'Confirmation', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <!-- <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p> -->
                    <div class="modal" id="popup1" style="display:block;">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header {{ $msg }}">
                                <h4 style="font-weight:bold;">{{ $msg }}</h4>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body">
                                <p style="font-size:17px;">{{ Session::get('alert-' . $msg) }}</p>
                            </div>  
                            <!-- Modal footer -->
                            <div class="modal-footer">
                            <!-- <a href="{{url('/')}}">
                                <button type="button" class="btn btn-danger">OK</button>
                            </a> -->
                                <button type="button" id="clickmodal" style="width:190px; color:white; border:3px solid #D3D3D3;" class="btn btn-primary" data-dismiss="modal">Ok</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
            <div id="page-inner">
            @if($countgrading == "0")
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Code</label>
                            <input type="text" class="form-control" placeholder="" value="{{$course->course_code}}" name="course" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                        </div>
                    </div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Grading Schema</label>
                            <select class="form-control" name="grading" id="grading" style="background-color:#fffbf1; border:2px solid red; color:#6fa7d9; ">
                                <option value="">No Information Added</option>
                                @foreach($grading as $data)
                                <option value="{{$data->grading_mode_code}}">{{$data->grading_mode_code}} - {{$data->grading_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row content">
                    <div class="col-md-6">
                    <label for="">View of Schema</label>
                    <br><br>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Percentage</th>
                            <th>Grade</th>
                            <th>Grade Point</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody id="badan">
                        </tbody>
                    </table>
                    </div>
                </div>
            @else
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Code</label>
                            <input type="text" class="form-control" placeholder="" value="{{$course->course_code}}" name="course" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;" readonly>
                        </div>
                    </div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Grading Schema</label>
                            @if($count == 0)
                            <select class="form-control" name="grading" id="grading" style="background-color:#fffbf1; border:2px solid red; color:#6fa7d9; ">
                            @else
                            <select class="form-control" name="grading" id="grading" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            @endif
                                <option value="{{$course->grading_mode_code}}">{{$course->grading_mode_code}}</option>
                                @foreach($grading as $data)
                                <option value="{{$data->grading_mode_code}}">{{$data->grading_mode_code}} - {{$data->grading_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">Effective Term</label>
                            @if($count == 0)
                            <select class="form-control" id="effterm" name="eff_term" style="background-color:#fffbf1; border:2px solid red; color:#6fa7d9; width:120%; width:230px;">
                            @else
                            <select class="form-control" id="effterm" name="eff_term" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;">
                            @endif
                                @foreach($effectiveterm as $data)
                                <option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">End Term</label>
                            @if($count == 0)
                            <select class="form-control" id="endterm" name="end_term" style="background-color:#fffbf1; border:2px solid red; color:#6fa7d9; width:120%; width:230px;">
                            @else
                            <select class="form-control" id="endterm" name="end_term" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;">
                            @endif
                                @foreach($endterm as $data)
                                <option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row content">
                    <div class="col-md-6">
                    <label for="">View of Schema</label>
                    <br><br>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Percentage</th>
                            <th>Grade</th>
                            <th>Grade Point</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody id="badan">
                        @foreach($schema as $data)
                            <tr>
                                <td>{{$data->percentage_from}} - {{$data->percentage_to}}</td>
                                <td>{{$data->grade}}</td>
                                <td>{{$data->grade_points}}</td>
                                <td>{{$data->description}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            @endif
                <br><br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group" id="simpan">
                            <button type="submit" class="btn btn-primary" style="width:190px; color:white; border:3px solid #D3D3D3;">Save</button>
                        </div>
                        </form>
                    </div>
                    <div class="col-md-3">
                        <form action="{{ route('course.grading.submit') }}" method="post">
                        {{ csrf_field() }}
                        <button type="submit" class="btn" style="width:190px; background-color:rgb(215, 227, 191); font-weight:bold; color:black; border:3px solid black;" data-toggle="modal" data-target="#myModal2">Submit The Application</button>
                        </form>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="background-color : rgb(255, 242, 204)">
                            <h4 style="font-weight:bold;">Warning !</h4>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p style="font-size:17px;">Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/home/course')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</body>
</html>
<script>
$(document).ready(function () {
    var pre = document.getElementById("popup1");

    $("#clickmodal").click(function(){
        pre.style.display = "none";
    });
    $("#grading").change(function () {
        var val = $(this).val();

        if (val == "ULET") {
            $("#badan").html(
                "@foreach($schema1 as $data)"+
                    "<tr>"+
                        "<td>{{$data->percentage_from}} - {{$data->percentage_to}}</td>"+
                        "<td>{{$data->grade}}</td>"+
                        "<td>{{$data->grade_points}}</td>"+
                        "<td>{{$data->description}}</td>"+
                    "</tr>"+
                "@endforeach"
            );
        } else if (val == "UPAF") {
            $("#badan").html(
                "@foreach($schema2 as $data)"+
                    "<tr>"+
                        "<td>{{$data->percentage_from}} - {{$data->percentage_to}}</td>"+
                        "<td>{{$data->grade}}</td>"+
                        "<td>{{$data->grade_points}}</td>"+
                        "<td>{{$data->description}}</td>"+
                    "</tr>"+
                "@endforeach"
            );
        } else if (val == "GLET"){
            $("#badan").html(
                "@foreach($schema3 as $data)"+
                    "<tr>"+
                        "<td>{{$data->percentage_from}} - {{$data->percentage_to}}</td>"+
                        "<td>{{$data->grade}}</td>"+
                        "<td>{{$data->grade_points}}</td>"+
                        "<td>{{$data->description}}</td>"+
                    "</tr>"+
                "@endforeach"
            );
        }
    });
});
</script>