<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}

</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                            <div class="row">
                            <div class="col-md-8">
                            <a href="{{url('/home/course')}}">
                                <h5 style="text-align:center; margin-top:20px; color:#a99451;">Changing the assigned textbook/reading</h5>
                            </a>
                            </div>
                            </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Course Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="{{url('course')}}" class="inactive">Course Main Attributes</a>
                    </li>
                    <li>
                        <a href="{{url('course/hour')}}" class="inactive">Course Hours</a>
                    </li>
                    <li>
                        <a href="{{url('course/requisite')}}" class="inactive">Course Requisite and Equivalencies</a>
                    </li>
                    <li>
                        <a href="{{url('course/association')}}" class="inactive">Program Assosiation</a>
                    </li>
                    <li>
                        <a href="{{url('course/outcome')}}" class="inactive">Course Learning Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('course/assessment')}}" class="inactive">Course Content and Assessments</a>
                    </li>
                    <li>
                        <a href="{{url('course/textbook')}}" class="active">Assigned Textbook and Other Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/optional')}}" class="inactive">Optional Reading Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/grading')}}" class="inactive">Grading Schema</a>
                    </li>
                    <li>
                        <a href="{{url('course/policies')}}" class="inactive">Course Policies</a>
                    </li>
                    <li>
                        <a href="{{url('course/special')}}" class="inactive">Course Special Requirements</a>
                    </li>
                    <li>
                        <a href="{{url('course/pedagogy')}}" class="inactive">Pedagogy</a>
                    </li>
                    <li>
                        <a href="#" class="inactive">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="{{ route('course.textbook.update') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
            <div class="flash-message">
            @foreach (['Error', 'warning', 'Confirmation', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <!-- <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p> -->
                    <div class="modal" id="popup1" style="display:block;">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header {{ $msg }}">
                                <h4 style="font-weight:bold;">{{ $msg }}</h4>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body">
                                <p style="font-size:17px;">{{ Session::get('alert-' . $msg) }}</p>
                            </div>  
                            <!-- Modal footer -->
                            <div class="modal-footer">
                            <!-- <a href="{{url('/')}}">
                                <button type="button" class="btn btn-danger">OK</button>
                            </a> -->
                                <button type="button" id="clickmodal" style="width:190px; color:white; border:3px solid #D3D3D3;" class="btn btn-primary" data-dismiss="modal">Ok</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
            <div id="page-inner">
                <p style="color:#0b4176; font-size:17px; text-weigth:bold; font-family:times new roman;">Assigned Textbook and Other Materials</p><div class="row content">
                    <div class="col-md-12" style="display:flex">
                        <button type="button" id="addtxt" class="bttn" style="background-color: #b1cfeb; margin-top:10px;width:240px;">Insert Textbook <i class="fas fa-plus"></i></button>
                    </div>
                </div>
                <br><br>
                    <div class="row content">
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Course Code</label>
                                <input type="text" class="form-control" value="{{$course->course_code}}" name="sequence" value="1" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>
                            </div>
                        </div>
                    </div>
                @if($textbook == 0)
                @else
                @foreach($textbook2 as $data)
                @if($count == 0)
                    <div class="row content">
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Sequence</label>
                                <input type="text" class="form-control" value="{{$data->course_textbook_code}}" name="sequence" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Title</label>
                                <input type="text" class="form-control" value="{{$data->textbook}}" placeholder="No Data Added" name="title" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9;">
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Publisher</label>
                                <input type="text" class="form-control" value="{{$data->publisher}}" placeholder="No Data Added" name="publisher" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9;">
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Author</label>
                                <input type="text" class="form-control" value="{{$data->author}}" placeholder="No Data Added" name="author" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9;">
                            </div>
                        </div>
                        <div class="col-md-1" style="display:flex">
                            <div class="form-group">
                                <label for="">ISBN</label>
                                <input type="text" class="form-control" value="{{$data->isbn}}" placeholder="No Data Added" name="isbn" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9;">
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Publication Type</label>
                                <input type="text" class="form-control" value="{{$data->publication_type}}" placeholder="No Data Added" name="publication" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9;">
                            </div>
                        </div>
                        <div class="col-md-1" style="display:flex">
                            <div class="form-group">
                                <label for="">Year</label>
                                <input type="text" class="form-control" value="{{$data->year}}" placeholder="No Data Added" name="year" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9;">
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="row content">
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Sequence</label>
                                <input type="text" class="form-control" value="{{$data->course_textbook_code}}" name="sequence" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Title</label>
                                <input type="text" class="form-control" value="{{$data->textbook}}" placeholder="No Data Added" name="title" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Publisher</label>
                                <input type="text" class="form-control" value="{{$data->publisher}}" placeholder="No Data Added" name="publisher" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Author</label>
                                <input type="text" class="form-control" value="{{$data->author}}" placeholder="No Data Added" name="author" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">
                            </div>
                        </div>
                        <div class="col-md-1" style="display:flex">
                            <div class="form-group">
                                <label for="">ISBN</label>
                                <input type="text" class="form-control" value="{{$data->isbn}}" placeholder="No Data Added" name="isbn" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Publication Type</label>
                                <input type="text" class="form-control" value="{{$data->publication_type}}" placeholder="No Data Added" name="publication" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">
                            </div>
                        </div>
                        <div class="col-md-1" style="display:flex">
                            <div class="form-group">
                                <label for="">Year</label>
                                <input type="text" class="form-control" value="{{$data->year}}" placeholder="No Data Added" name="year" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="row content" style="margin-left:2px;">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">Effective Term</label>
                            @if($count == 0)
                            <select class="form-control" id="effterm" name="eff_term" style="background-color:#fffbf1; border:2px solid red; color:#6fa7d9; width:120%; width:230px;">
                            @else
                            <select class="form-control" id="effterm" name="eff_term" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;">
                            @endif
                                @foreach($effectiveterm as $data)
                                <option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">    
                            <label for="">End Term</label>
                            @if($count == 0)
                            <select class="form-control" id="endterm" name="end_term" style="background-color:#fffbf1; border:2px solid red; color:#6fa7d9; width:120%; width:230px;">
                            @else
                            <select class="form-control" id="endterm" name="end_term" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; width:120%; width:230px;">
                            @endif
                                @foreach($endterm as $data)
                                <option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
                <div class="row content">
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                                <label for="">Effective Term</label>
                                <select class="form-control" id="effterm" name="eff_term" style="background-color:#fffbf1; border:2px solid red; color:#6fa7d9; width:120%; width:230px;">
                                    @foreach($effectiveterm as $data)
                                    <option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                                <label for="">End Term</label>
                                <select class="form-control" name="end_term" style="background-color:#fffbf1; border:2px solid red; color:#6fa7d9; width:120%; width:230px;">
                                    @foreach($endterm as $data)
                                    <option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                <div id="txt">
                </div>
                <br><br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" style="width:190px; color:white; border:3px solid #D3D3D3;">Save</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                        <form action="{{ route('course.textbook.submit') }}" method="post">
                        {{ csrf_field() }}
                            <button type="submit" class="btn" style="width:190px; background-color:rgb(215, 227, 191); font-weight:bold; color:black; border:3px solid black;" data-toggle="modal" data-target="#myModal2">Submit The Application</button>
                        </form>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="">
                            <h2>Exit</h2>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p>Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/home/course')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <form>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</body>
</html><script>
$(document).ready(function () {
    
    var pre = document.getElementById("popup1");

    $("#clickmodal").click(function(){
        pre.style.display = "none";
    });
    
    var max_fields      = 99; //maximum input boxes allowed
	var wrapper   		= $("#txt"); //Fields wrapper
	var add_button      = $("#addtxt"); //Add button ID
	
	var x = 0; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append(
                '<div>'+
                '<a href="#" class="remove_field" style="margin-left:90%;">remove <i class="fas fa-times"></i></a>'+
                '<div class="row content">'+
                    '<div class="col-md-1" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Sequence</label>'+
                            '<input type="text" class="form-control" name="sequence" value='+x+' style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Title</label>'+
                            '<input type="text" class="form-control" name="title" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Publisher</label>'+
                            '<input type="text" class="form-control" name="publisher" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Author</label>'+
                            '<input type="text" class="form-control" name="author" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-1" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">ISBN</label>'+
                            '<input type="text" class="form-control" name="isbn" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Publication Type</label>'+
                            '<input type="text" class="form-control" name="publication" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-1" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Year</label>'+
                            '<input type="text" class="form-control" name="year" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '</div>'
                ); //add input box
		}
	});
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); x--;
	});
});
</script>
