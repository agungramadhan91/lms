<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}

</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                            <div class="row">
                            <div class="col-md-8">
                            <a href="{{url('/home/course')}}">
                                <h5 style="text-align:center; margin-top:20px; color:#a99451;">Changing course hours</h5>
                            </a>
                            </div>
                            </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Course Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="{{url('course/description')}}"  class="inactive">Course Main Attributes</a>
                    </li>
                    <li>
                    @if($hourdata->sequence == "1")
                        <a href="{{url('course/hour/description')}}"  class="active"  style="border: 4px solid #0b4176;">Course Hours</a>
                    @else
                        <a href="{{url('course/hour/description')}}"  class="done"  style="border: 4px solid #0b4176;">Course Hours</a>
                    @endif
                    </li>
                    <li>
                        <a href="{{url('course/requisite')}}" class="inactive">Course Requisite and Equivalencies</a>
                    </li>
                    <li>
                        <a href="{{url('course/association/description')}}" class="active">Program Assosiation</a>
                    </li>
                    <li>
                        <a href="{{url('course/outcome')}}" class="inactive">Course Learning Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('course/assessment')}}" class="inactive">Course Content and Assessments</a>
                    </li>
                    <li>
                        <a href="{{url('course/textbook')}}"  class="inactive">Assigned Textbook and Other Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/research')}}"  class="inactive">Optional Reading Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/grading')}}" class="inactive">Grading Schema</a>
                    </li>
                    <li>
                        <a href="{{url('course/supportive')}}" class="inactive">Course Policies</a>
                    </li>
                    <li>
                        <a href="{{url('course/cost')}}" class="inactive">Course Special Requirements</a>
                    </li>
                    <li>
                        <a href="{{url('course/pedagogy')}}" class="inactive">Pedagogy</a>
                    </li>
                    <li>
                        <a href="#" class="inactive">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="{{ route('course.hourdescription.store') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
            <div class="flash-message">
            @foreach (['Error', 'warning', 'Confirmation', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <!-- <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p> -->
                    <div class="modal" id="popup1" style="display:block;">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header {{ $msg }}">
                                <h4 style="font-weight:bold;">{{ $msg }}</h4>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body">
                                <p style="font-size:17px;">{{ Session::get('alert-' . $msg) }}</p>
                            </div>  
                            <!-- Modal footer -->
                            <div class="modal-footer">
                            <!-- <a href="{{url('/')}}">
                                <button type="button" class="btn btn-danger">OK</button>
                            </a> -->
                                <button type="button" id="clickmodal" style="width:190px; color:white; border:3px solid #D3D3D3;" class="btn btn-primary" data-dismiss="modal">Ok</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
            <div id="page-inner">
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Code</label>
                            <input type="text" class="form-control" value="{{$course2->course_code}}" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;" required>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Student Credit Hours</label>
                            <input type="number" class="form-control" value="{{$hourdata->credit_hours}}" name="credit" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9; width:230px;" required>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Student Contact Hours</label>
                            <input type="number" class="form-control" placeholder="" value="{{$hourdata->contact_hours}}" name="contact" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Lecture Hours</label>
                            @if($course == "0")
                            @if($course2->course_genre_code == "LAB01")
                            <input type="number" class="form-control" placeholder="" value="" name="lecture" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                            @else
                            <input type="number" class="form-control" placeholder="" value="" name="lecture" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                            @endif
                            @else
                            <input type="number" class="form-control" placeholder="" value="{{$hourdata->lecture_hours}}" name="lecture" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                            @endif
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Lab Hours</label>
                            @if($course == "0")
                            @if($course2->course_genre_code == "LEC01")
                            <input type="number" class="form-control" placeholder="" value="{{$hourdata->lab_hours}}" name="lab" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                            @else
                            <input type="number" class="form-control" placeholder="" value="{{$hourdata->lab_hours}}" name="lab" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                            @endif
                            @else
                            <input type="number" class="form-control" placeholder="" value="{{$hourdata->lab_hours}}" name="lab" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Other Hours</label>
                            <input type="number" class="form-control" placeholder="" name="other" value="{{$hourdata->other_hours}}" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Faculty Workload Hours</label>
                            <input type="number" class="form-control" placeholder="" value="{{$hourdata->faculty_load_hours}}" name="faculty" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9; width:230px;">
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-5" style="display:flex">
                        <div class="form-group">    
                            <label for="">Formula</label>
                            <textarea name="formula" class="form-control" placeholder="" style="border:2px solid #6fa7d9; color:#6fa7d9; width:450px">{{$hourdata->formula}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-5" style="display:flex">
                        <div class="form-group">    
                        <label for="">Additional Text</label>
                        <textarea name="additional" class="form-control" placeholder="" style="border:2px solid #6fa7d9; color:#6fa7d9; width:450px">{{$hourdata->additional_text}}</textarea>
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" name="continue" value="1" class="btn btn-primary" style="width:190px; color:white; border:3px solid #D3D3D3;">Save</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="background-color : rgb(255, 242, 204)">
                            <h4 style="font-weight:bold;">Warning !</h4>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p style="font-size:17px;">Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/')}}">
                            <button type="button" class="btn btn-primary" style="width:190px; color:white; border:3px solid #D3D3D3;">OK</button>
                        </a>
                            <button type="button" class="btn" data-dismiss="modal" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <form>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</body>
</html>
<script>
$(document).ready(function () {
    var pre = document.getElementById("popup1");

    $("#clickmodal").click(function(){
        pre.style.display = "none";
    });
});
</script>