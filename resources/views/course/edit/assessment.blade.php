<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}

</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                            <div class="row">
                            <div class="col-md-8">
                            <a href="{{url('/home/course')}}">
                                <h5 style="text-align:center; margin-top:20px; color:#a99451;">Change Course Assessment</h5>
                            </a>
                            </div>
                            </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Course Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="{{url('course')}}" class="inactive">Course Main Attributes</a>
                    </li>
                    <li>
                        <a href="{{url('course/hour')}}" class="inactive">Course Hours</a>
                    </li>
                    <li>
                        <a href="{{url('course/structure')}}"  class="inactive">Course Requisite and Equivalencies</a>
                    </li>
                    <li>
                        <a href="{{url('course/association')}}" class="inactive">Program Assosiation</a>
                    </li>
                    <li>
                        <a href="{{url('course/outcome')}}" class="inactive">Course Learning Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('course/assessment')}}" class="active">Course Content and Assessments</a>
                    </li>
                    <li>
                        <a href="{{url('course/textbook')}}" class="inactive">Assigned Textbook and Other Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/optional')}}" class="inactive">Optional Reading Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/grading')}}" class="inactive">Grading Schema</a>
                    </li>
                    <li>
                        <a href="{{url('course/policies')}}" class="inactive">Course Policies</a>
                    </li>
                    <li>
                        <a href="{{url('course/cost')}}" class="inactive">Course Special Requirements</a>
                    </li>
                    <li>
                        <a href="{{url('course/community')}}" class="inactive">Pedagogy</a>
                    </li>
                    <li>
                        <a href="#" class="inactive">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="{{ route('course.assessment.update') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
            <div class="flash-message">
            @foreach (['Error', 'warning', 'Confirmation', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <!-- <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p> -->
                    <div class="modal" id="popup1" style="display:block;">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header {{ $msg }}">
                                <h4 style="font-weight:bold;">{{ $msg }}</h4>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body">
                                <p style="font-size:17px;">{{ Session::get('alert-' . $msg) }}</p>
                            </div>  
                            <!-- Modal footer -->
                            <div class="modal-footer">
                            <!-- <a href="{{url('/')}}">
                                <button type="button" class="btn btn-danger">OK</button>
                            </a> -->
                                <button type="button" id="clickmodal" style="width:190px; color:white; border:3px solid #D3D3D3;" class="btn btn-primary" data-dismiss="modal">Ok</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            </div>

            <div id="page-inner">
                <p style="color:#0b4176; font-size:17px; text-weigth:bold; font-family:times new roman;">Course Content</p>
                <br><br>
                @if($countcontent == "0")
                @else
                @endif
                <br>
                <div id="replace">
                </div>
                <br>
                <p style="color:#0b4176; font-size:17px; text-weigth:bold; font-family:times new roman;">Weekly Outline</p>
                <br><br>
                @if($countweekly == "0")
                <div id="pre">
                </div>
                @else
                @endif
                <br>
                <p style="color:#0b4176; font-size:17px; text-weigth:bold; font-family:times new roman;">Assessment Method</p>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <button type="button" id="addco" class="bttn" style="background-color: #b1cfeb; margin-top:10px;width:240px;">Insert Assessment Method <i class="fas fa-plus"></i></button>
                    </div>
                </div>
                <br>
                @if($countassessment == "0")
                    <div class="row content">
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                                <label for="">Course Code</label>
                                <input type="text" class="form-control" placeholder="" value="{{$coursecode3}}" name="course" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;" readonly>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="row content">
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                                <label for="">Course Code</label>
                                <input type="text" class="form-control" placeholder="" value="{{$coursecode3}}" name="course" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:230px;" readonly>
                            </div>
                        </div>
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">    
                                <label for="">Effective Term</label>
                                <select class="form-control" id="a" name="eff_term" style="background-color:#fffbf1; border:2px solid red; color:#6fa7d9; width:120%; width:230px;">
                                    <option value="{{$term2->term_code}}" selected>{{$term2->term_code}} - {{$term2->term_description}}</option>
                                    @foreach($effectiveterm as $data)
                                    <option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">    
                                <label for="">End Term</label>
                                <select class="form-control" name="end_term" style="background-color:#fffbf1; border:2px solid red; color:#6fa7d9; width:120%; width:230px;">
                                <option value="{{$term3->term_code}}" selected>{{$term3->term_code}} - {{$term3->term_description}}</option>
                                    @foreach($endterm as $data)
                                    <option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    @foreach($assessment as $data)
                        <div class="row content">
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label for="">Sequence</label>
                                    <input type="text" value="{{$data->course_assessment_code}}" class="form-control" placeholder="" name="idassessment" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">Assigment Category</label>
                                    <select class="form-control" id="type" name="category" style="border:2px solid red; color:#6fa7d9; width:150px;" required>
                                    <option value="{{$data->category}}">{{$data->category}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Assessment Type</label>
                                    <input type="text" class="form-control" value="{{$data->course_assessment_type}}" placeholder="" name="type" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9;" >
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">Weight</label>
                                    <input type="text" class="form-control" value="{{$data->weight}}" placeholder="" name="weight" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9;" >
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <input type="text" class="form-control" value="{{$data->course_assessment_description}}" placeholder="" name="description" style="border:2px solid red; background-color:#fffbf1; color:#6fa7d9;" >
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">CLO/PLO</label>
                                    <select class="form-control" id="type" name="mapping" style="border:2px solid red; color:#6fa7d9; width:150px;" required>
                                    <option value="{{$data->mapping}}">{{$data->mapping}}</option>
                                    @foreach($plo as $data)
                                        <option value="{{$data->mapping_code}}">{{$data->mapping_code}}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
                <div class="row content">
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                                <label for="">Effective Term</label>
                                <select class="form-control" id="effterm" name="eff_term" style="background-color:#fffbf1; border:2px solid red; color:#6fa7d9; width:120%; width:230px;">
                                    @foreach($effectiveterm as $data)
                                    <option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                                <label for="">End Term</label>
                                <select class="form-control" name="end_term" style="background-color:#fffbf1; border:2px solid red; color:#6fa7d9; width:120%; width:230px;">
                                    @foreach($endterm as $data)
                                    <option value="{{$data->term_code}}">{{$data->term_code}} - {{$data->term_description}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                <div id="co">
                </div>
                <br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" style="width:190px; color:white; border:3px solid #D3D3D3;">Save</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <button type="submit" class="btn" style="width:190px; background-color:rgb(215, 227, 191); font-weight:bold; color:black; border:3px solid black;" data-toggle="modal" data-target="#myModal2">Submit The Application</button>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="background-color : rgb(255, 242, 204)">
                            <h4 style="font-weight:bold;">Warning !</h4>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p style="font-size:17px;">Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/home/course')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <form>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</body>
</html>
<script>
function remove(){
        var row = document.getElementById("row");
         row.style.display = "none";
    }
$(document).ready(function () {

    var pre = document.getElementById("popup1");

    $("#clickmodal").click(function(){
        pre.style.display = "none";
    });

    var max_fields      = 99; //maximum input boxes allowed
	var wrapper   		= $("#replace"); //Fields wrapper
	var add_button      = $("#addrpl"); //Add button ID
	
	var x = 0; //initlal text box count
    var xx = 1;
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
            xx++
			$(wrapper).append(
                '<a href="#" class="remove_field" style="margin-left:90%;">remove <i class="fas fa-times"></i></a>'+
                '<div class="row content">'+
                    '<div class="col-md-1">'+
                        '<div class="form-group">'+
                            '<label for="">Sequence</label>'+
                            '<input type="text" value="'+x+'" class="form-control" placeholder="" name="idcontent1" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<div class="form-group">'+
                            '<label for="">Topic</label>'+
                            '<input type="text" class="form-control" placeholder="" name="topiccontent" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">'+
                        '</div>'+
                    '</div>'+
                '</div>'
                ); //add input box
		}
	});


	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); x--;
	});

    $(wrapper).on("change","#condition", function(e){ //user click on remove text
		var val2 = $(this).val();
        var rep2 = document.getElementById("rep2");
        if (val2 == "none") {
            rep2.style.display = "none";
        }else{
            rep2.style.display = "block";
        }
	});


    var max_fields2      = 99; //maximum input boxes allowed
	var wrapper2   		= $("#pre"); //Fields wrapper
	var add_button2      = $("#addpre"); //Add button ID
	
	var y = 0; //initlal text box count
    var yy = 1;
	$(add_button2).click(function(e){ //on add input button click
		e.preventDefault();
		if(y < max_fields2){ //max input box allowed
			y++; //text box increment
            yy++;
			$(wrapper2).append(
                '<a href="#" class="remove_field2" style="margin-left:90%;">remove <i class="fas fa-times"></i></a>'+
                '<div class="row content">'+
                    '<div class="col-md-1">'+
                        '<div class="form-group">'+
                            '<label for="">Sequence</label>'+
                            '<input type="text" value="'+y+'" class="form-control" placeholder="" name="idweekly" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">Week</label>'+
                            '<input type="text"  class="form-control" placeholder="" name="week" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" >'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">Topic</label>'+
                            '<input type="text" class="form-control" placeholder="" name="topicweekly" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" >'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">Activities</label>'+
                            '<input type="text" class="form-control" placeholder="" name="activities" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" >'+
                        '</div>'+
                    '</div>'+
                '</div>'
                ); //add input box
		}

	});
	
	$(wrapper2).on("click",".remove_field2", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); y--;
	});


    var max_fields3      = 99; //maximum input boxes allowed
	var wrapper3   		= $("#co"); //Fields wrapper
	var add_button3      = $("#addco"); //Add button ID
	
	var z = 0; //initlal text box count
    var zz = 1;
	$(add_button3).click(function(e){ //on add input button click
		e.preventDefault();
		if(z < max_fields3){ //max input box allowed
			z++; //text box increment
            zz++;
			$(wrapper3).append(
                '<a href="#" class="remove_field" style="margin-left:90%;">remove <i class="fas fa-times"></i></a>'+
                '<div class="row content">'+
                    '<div class="col-md-1">'+
                        '<div class="form-group">'+
                            '<label for="">Sequence</label>'+
                            '<input type="text" value="'+z+'" class="form-control" placeholder="" name="idassessment" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">Assessment Category</label>'+
                            '<select class="form-control" id="category" name="category" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;">'+
                            '<option value=""></option>'+
                            '<option value="resource_paper">Resource Paper</option>'+
                            '<option value="discussion">Discussion</option>'+
                            '<option value="test">Test</option>'+
                            '<option value="assigment">Assigment</option>'+
                            '<option value="comment_assessment">Comment Assessment</option>'+
                            '<option value="case_study">Case Study</option>'+
                            '<option value="project">Project</option>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<div class="form-group">'+
                            '<label for="">Assessment Type</label>'+
                            '<select class="form-control" id="type" name="type" style="border:2px solid #6fa7d9; color:#6fa7d9; width:230px;">'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">Weight</label>'+
                            '<input type="text" class="form-control" placeholder="" name="weight" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" >'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">Description</label>'+
                            '<input type="text" class="form-control" placeholder="" name="description" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" >'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label for="">CLO/PLO</label>'+
                            '<select class="form-control" id="type" name="mapping" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>'+
                            '<option value=""></option>'+
                            '@foreach($plo as $data)'+
                                '<option value="{{$data->mapping_code}}">{{$data->mapping_code}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                '</div>'
                ); //add input box
		}

	});
	
	$(wrapper3).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); z--;
	});

    $(wrapper3).on("change","#category", function(e){ //user click on remove text
		var val = $(this).val();
        if (val == "resource_paper") {
            $("#type").html(
                "<option value='conducting_resource'>Conducting Resource</option>"+
                "<option value='analysis_resource'>Analysis Resource</option>"
            );
        } else if (val == "test") {
            $("#type").html(
                "<option value='mid_term'>Mid Term</option>"+
                "<option value='final'>Final</option>"+
                "<option value='quiz'>Quiz</option>"
            );
        } else if (val == "case_study") {
            $("#type").html(
                "<option value='analysis'>Analysis</option>"+
                "<option value='case_study_review'>Case Study Review</option>"
            );
        } else if (val == "project") {
            $("#type").html(
                "<option value='individual'>Individual</option>"+
                "<option value='group'>Group</option>"
            );
        } else if (val == "discussion") {
            $("#type").html(
                "<option value=''></option>"
            );
        } else if (val == "assigment") {
            $("#type").html(
                "<option value=''></option>"
            );
        } else if (val == "comment_assessment") {
            $("#type").html(
                "<option value=''></option>"
            );
        }
	});
});
</script>