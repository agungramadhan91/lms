<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.bootstrap3.min.css')}}" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}

hr {
    border: none;
    height: 1px;
    /* Set the hr color */
    color: #333; /* old IE */
    background-color: #333; /* Modern Browsers */
}
.selectize-control{
    width:400px;
    color:#6fa7d9;
    background-color:#fffbf1;
}

.selectize-input{
    background-color:#fffbf1;
    border:2px solid #6fa7d9;

}
.select-state{
    color:#6fa7d9;
    background-color:#fffbf1;
}
</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                            <div class="row">
                            <div class="col-md-8">
                            <a href="{{url('/home/course')}}">
                                <h5 style="text-align:center; margin-top:20px; color:#a99451;">Change course subject</h5>
                            </a>
                            </div>
                            <div class="col-md-4">                    
                            <form action="{{ route('course.subjectassociation.submit') }}" method="post">
                                {{ csrf_field() }}
                                <button type="submit" class="btn-small" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3; margin-top:15px; margin-left:45px" data-toggle="modal" data-target="#myModal2">Submit The Application</button>
                            </form>
                            </div>
                            </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Course Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="{{url('course/subject')}}" class="done">Course Main Attributes</a>
                    </li>
                    <li>
                        <a href="{{url('#')}}" class="inactive">Course Hours</a>
                    </li>
                    <li>
                        <a href="{{url('course/requisitesubject/edit')}}" class="done">Course Requisite and Equivalencies</a>
                    </li>
                    <li>
                    @if($count == 0)
                        <a href="{{url('course/associationsubject/edit')}}" class="active" style="border: 4px solid #0b4176;">Course Program Assosiation</a>
                    @else
                        <a href="{{url('course/associationsubject/edit')}}" class="done" style="border: 4px solid #0b4176;">Course Program Assosiation</a>
                    @endif
                    </li>
                    <li>
                        <a href="{{url('/course/outcome/level')}}" class="inactive">Course Learning Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('course/assessment')}}" class="inactive">Course Content and Assessments</a>
                    </li>
                    <li>
                        <a href="{{url('course/textbook')}}" class="inactive">Assigned Textbook and Other Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/optional')}}" class="inactive">Optional Reading Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/grading')}}" class="inactive">Grading Schema</a>
                    </li>
                    <li>
                        <a href="{{url('course/policies')}}" class="inactive">Course Policies</a>
                    </li>
                    <li>
                        <a href="{{url('course/special')}}" class="inactive">Course Special Requirements</a>
                    </li>
                    <li>
                        <a href="{{url('course/pedagogy')}}" class="inactive">Pedagogy</a>
                    </li>
                    <li>
                        <a href="#"  class="inactive">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="{{ route('course.subjectassociation.update') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
            <div class="flash-message">
            @foreach (['Error', 'warning', 'Confirmation', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <!-- <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p> -->
                    <div class="modal" id="popup1" style="display:block;">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header {{ $msg }}">
                                <h4 style="font-weight:bold;">{{ $msg }}</h4>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body">
                                <p style="font-size:17px;">{{ Session::get('alert-' . $msg) }}</p>
                            </div>  
                            <!-- Modal footer -->
                            <div class="modal-footer">
                            <!-- <a href="{{url('/')}}">
                                <button type="button" class="btn btn-danger">OK</button>
                            </a> -->
                                <button type="button" id="clickmodal" style="width:190px; color:white; border:3px solid #D3D3D3;" class="btn btn-primary" data-dismiss="modal">Ok</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
                @if($countstudy != 0)
                @if($sum != $status->required_hours)
                <p class="alert alert-danger">The Required Hours must Match with Total Program Hours !. Please Adjust The Required Hours on Program Site<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @else
                <p class="alert alert-success">The Required Hours is Match with Total Program Hours<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
                @endif
            </div>
            <div id="page-inner">
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Old Course Code</label>
                            <input type="text" value="{{$course->course_code}}" class="form-control" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="old" readonly>
                        </div>
                    </div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">New Course Code</label>
                            <input type="text" value="{{$newcode2}}" class="form-control" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="old" readonly>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-6" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Code</label>
                            <textarea class="form-control" style="border:2px solid #6fa7d9; color:#6fa7d9; width:450px;" name="program" readonly>{{$status->program_code}} - {{$status->title}}</textarea>
                        </div>
                    </div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Status</label>
                            <input type="text" value="{{$status->status}}" id="status" class="form-control" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="status" readonly>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row content">
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                            <label for="">Semester</label>
                            <select class="form-control" name="semester" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" readonly>
                            @if($countstudy != 0)
                                <option value="{{$study2->semester_code}}" selected>{{$study2->semester_code}}</option>
                            @else
                                <option value="" selected>No Data Added</option>
                            @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                            <label for="">course</label>
                            <select class="form-control" id="course" name="course" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" readonly>
                                @if($countstudy != 0)
                                    <option value="{{$newcode2}}" selected>{{$newcode2}}</option>
                                @else
                                    <option value="" selected>No Data Added</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Type</label>
                            <select class="form-control" id="type" name="type" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                            @if($countstudy != 0)
                            <option value="{{$study2->course_type}}" selected>{{$study2->course_type}}</option>
                            @else
                            <option value="" selected>No Data Added</option>
                            @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                            <label for="">Course Genre</label>
                            <select class="form-control" id="genre" name="genre" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                            @if($countstudy != 0)
                            <option value="{{$study2->course_genre}}" selected>{{$study2->course_genre}}</option>
                            @else
                            <option value="" selected>No Data Added</option>
                            @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                            <label for="">Credit</label>
                            <select class="form-control" id="credit" name="credit" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                            @if($countstudy != 0)
                            <option value="{{$study2->credit_hour}}" selected>{{$study2->credit_hour}}</option>
                            @else
                            <option value="" selected>No Data Added</option>
                            @endif
                            </select>
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" name="continue" value="1" class="btn btn-primary" style="width:190px; color:white; border:3px solid #D3D3D3;">Save</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="background-color : rgb(255, 242, 204)">
                            <h4 style="font-weight:bold;">Warning !</h4>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p style="font-size:17px;">Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/home/course')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <form>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
    
</body>
</html>
<script>
$("#program").change(function () {
        var val = $(this).val();
        if(val == "CBBAMGT"){
            $("#status").val('Pending');    
        }else{
            $("#status").val('{{$program3->status}}');
        }

    });
$(document).ready(function () {

    var pre = document.getElementById("popup1");

    $("#clickmodal").click(function(){
        pre.style.display = "none";
    });
    $('#program').selectize({
          sortField: 'text'
      });

    var max_fields      = 99; //maximum input boxes allowed
	var wrapper   		= $("#txt"); //Fields wrapper
    var add_button      = $("#addtxt"); //Add button ID

	
	var x = 0; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append(
                '<a href="#" class="remove_field" style="margin-left:90%;">remove <i class="fas fa-times"></i></a>'+
                '<div class="row content">'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Semester</label>'+
                            '<select class="form-control" name="semester['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>'+
                                '<option value=""></option>'+
                                '@foreach($semester as $data)'+
                                '<option value="{{$data->semester_code}}">{{$data->semester_code}} - {{$data->semester_description}}</option>'+
                                '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">course</label>'+
                            '<select class="select-state" id="course'+x+'" name="course['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>'+
                                '<option value=""></option>'+
                                '@foreach($course2 as $data)'+
                                '<option value="{{$data->course_code}}">{{$data->course_code}} - {{$data->long_title}}</option>'+
                                '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Course Type</label>'+
                            '<select class="form-control" id="type'+x+'" name="type['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>'+
                            '<option value=""></option>'+
                            '@foreach($type as $data)'+
                                '<option value="{{$data->course_type_code}}">{{$data->course_type_code}} - {{$data->type_description}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Course Genre</label>'+
                            '<select class="form-control" id="genre'+x+'" name="genre['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Credit</label>'+
                            '<select class="form-control" id="credit'+x+'" name="credit['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                '</div>'
            ); //add input box

            $('#course'+x+'').selectize({
                sortField: 'text'
            });

            $(wrapper).on("change","#course"+x+"", function(e){ //user click on remove text
                var val = $(this).val();
                $("#genre"+x+"").html(
                    '<option value="{{$course->course_genre_code_01}}" selected>{{$course->course_genre_code_01}}</option>'
                );
                
                $("#credit"+x+"").html(
                    '<option value="{{$course->credit_hours}}" selected>{{$course->credit_hours}}</option>'
                );
            });
            
            $(wrapper).on("change","#type"+x+"", function(e){ //user click on remove text
                var val2 = $("#type"+x+"").val();
                // alert(val2);

                if(val2 == "Core"){
                    document.getElementById('credit'+x+'').setAttribute("name","creditcore["+x+"]");
                    var s = $('select[name^="creditcore"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum = s.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalCore").val(sum);
                }else if(val2 == "GEN"){
                    document.getElementById('credit'+x+'').setAttribute("name","creditgen["+x+"]");
                    var y = $('select[name^="creditgen"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum2 = y.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalGened").val(sum2);
                }else if(val2 == "ELECT"){
                    document.getElementById('credit'+x+'').setAttribute("name","creditelect["+x+"]");
                    var y = $('select[name^="creditelect"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum2 = y.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalElect").val(sum2);
                }else if(val2 == "CONC"){
                    document.getElementById('credit'+x+'').setAttribute("name","creditconc["+x+"]");
                    var y = $('select[name^="creditconc"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum2 = y.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalConcentration").val(sum2);
                }

                var a1 = parseInt($('#totalGened').val());
                var a2 = parseInt($('#totalCore').val());
                var a3 = parseInt($('#totalElect').val());
                var a4 = parseInt($('#totalConcentration').val());

                var sum3 = a1 + a2 + a3 + a4;

                $("#totalProgram").val(sum3);
            });
		}
	});

	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    });
});
</script>