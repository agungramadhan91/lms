<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.bootstrap3.min.css')}}" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}

hr {
    border: none;
    height: 1px;
    /* Set the hr color */
    color: #333; /* old IE */
    background-color: #333; /* Modern Browsers */
}
.selectize-control{
    width:400px;
    color:#6fa7d9;
    background-color:#fffbf1;
}

.selectize-input{
    background-color:#fffbf1;
    border:2px solid #6fa7d9;

}
.select-state{
    color:#6fa7d9;
    background-color:#fffbf1;
}
</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                            <div class="row">
                            <div class="col-md-8">
                            <a href="{{url('/home/course')}}">
                                <h5 style="text-align:center; margin-top:20px; color:#a99451;">Change course credit hour</h5>
                            </a>
                            </div>
                            </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Course Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="{{url('course/description')}}" class="inactive">Course Main Attributes</a>
                    </li>
                    <li>
                        <a href="{{url('course/hour/description')}}" class="active">Course Hours</a>
                    </li>
                    <li>
                        <a href="{{url('course/requisite')}}" class="inactive">Course Requisite and Equivalencies</a>
                    </li>
                    <li>
                        <a href="{{url('course/association/description')}}" class="active" style="border: 4px solid #0b4176;">Program Assosiation</a>
                    </li>
                    <li>
                        <a href="{{url('course/outcome')}}" class="inactive">Course Learning Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('course/assessment')}}" class="inactive">Course Content and Assessments</a>
                    </li>
                    <li>
                        <a href="{{url('course/textbook')}}" class="inactive">Assigned Textbook and Other Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/optional')}}" class="inactive">Optional Reading Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/grading')}}" class="inactive">Grading Schema</a>
                    </li>
                    <li>
                        <a href="{{url('course/policies')}}" class="inactive">Course Policies</a>
                    </li>
                    <li>
                        <a href="{{url('course/special')}}" class="inactive">Course Special Requirements</a>
                    </li>
                    <li>
                        <a href="{{url('course/pedagogy')}}" class="inactive">Pedagogy</a>
                    </li>
                    <li>
                        <a href="#"  class="inactive">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
            <div class="flash-message">
            @foreach (['Error', 'warning', 'Success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <!-- <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p> -->
                    <div class="modal" id="popup1" style="display:block;">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header {{ $msg }}">
                                <h4>{{ $msg }}</h4>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body">
                                <p>{{ Session::get('alert-' . $msg) }}</p>
                            </div>  
                            <!-- Modal footer -->
                            <div class="modal-footer">
                            <!-- <a href="{{url('/')}}">
                                <button type="button" class="btn btn-danger">OK</button>
                            </a> -->
                                <button type="button" id="clickmodal" class="btn btn-primary" data-dismiss="modal">Ok</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endif
                @endforeach

                @if($countstudy != "0")
                    @if($sum != $status->required_hours)
                    <p class="alert alert-danger">The Required Hours must Match with Total Program Hours !. Please Adjust The Required Hours on Program Site<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @else
                    <p class="alert alert-success">The Required Hours is Match with Total Program Hours<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif
                @else    
                @endif

            </div>
            <div id="page-inner">
                <div class="row content">
                    <div class="col-md-6" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Study Plan</label>
                            <select class="select-state" id="program" class="form-control" name="program" style="border:2px solid #6fa7d9; color:#6fa7d9;" required>
                                @if($countstudy != "0")
                                <option value="{{$status->program_code}}" selected>{{$status->program_code}} - {{$status->title}}</option>
                                @else
                                <option value="">No Data Added</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Program Status</label>
                            @if($countstudy == "0")
                            <input type="text" id="status" class="form-control" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="status" readonly>
                            @else
                            <input type="text" value="{{$status->status}}" id="status" class="form-control" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="status" readonly>
                            @endif
                        </div>
                    </div>
                </div>
                <br><br>
                @if($countstudy !=0)
                @foreach($study as $data)
                    <div class="row content">
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Semester</label>
                                <select class="form-control" name="semester1" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" readonly>
                                    <option value="{{$data->semester_code}}" selected>{{$data->semester_code}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">course</label>
                                <select class="form-control" id="course" name="course" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" readonly>
                                    @foreach($course3 as $data2)
                                    @if($data2->course_code == $data->course_code)
                                    <option value="{{$data->course_code}}" selected>{{$data->course_code}} - {{$data2->long_title}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Course Type</label>
                                <select class="form-control" id="type" name="type" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" readonly>
                                <option value="{{$data->course_type}}" selected>{{$data->course_type}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Course Genre</label>
                                <select class="form-control" id="genre" name="genre" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                <option value="{{$data->course_genre}}" selected>{{$data->course_genre}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Credit</label>
                                @if($data->course_type == 'GEN')
                                <select class="form-control" id="credit" name="creditgen" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                @elseif($data->course_type == 'CONC')
                                <select class="form-control" id="credit" name="creditconc" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                @elseif($data->course_type == 'ELECT')
                                <select class="form-control" id="credit" name="creditelect" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                @elseif($data->course_type == 'Core')
                                <select class="form-control" id="credit" name="creditcore" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                @else
                                <select class="form-control" id="credit" name="credit" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                @endif
                                <option value="{{$data->credit_hour}}" selected>{{$data->credit_hour}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                @endforeach
                @endif
                <div id="txt">
                </div>
                
                <div class="row content">
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group" id="gened">
                            <label for="">Total Gened</label>
                            @if($countstudy == 0)
                            <input type="text" id="totalGened" value="0" class="form-control" placeholder="Total Gened" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_gened">
                            @else
                            <input type="text" id="totalGened" value="{{$sumgen}}" class="form-control" placeholder="Total Gened" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_gened">
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                        <label for="">Total Core</label>
                        @if($countstudy == 0)
                            <input type="text" id="totalCore" value="0" class="form-control" placeholder="Total Core" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_core">
                            @else
                            <input type="text" id="totalCore" value="{{$sumcore}}" class="form-control" placeholder="Total Core" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_core">
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                        <label for="">Total Concentration 1</label>
                        @if($countstudy == 0)
                            <input type="text" id="totalConcentration" value="0" class="form-control" placeholder="Total Concentration 1" style="border:2px solid #6fa7d9; color:#6fa7d9; width:240px;" name="total_concentration">
                            @else
                            <input type="text" id="totalConcentration" value="{{$sumcon}}" class="form-control" placeholder="Total Concentration 1" style="border:2px solid #6fa7d9; color:#6fa7d9; width:240px;" name="total_concentration">
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                            <label for=""> Total Electives</label>
                            @if($countstudy == 0)
                            <input type="text" id="totalElect" value="0" class="form-control" placeholder="Total Electives" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_elective">
                            @else
                            <input type="text" id="totalElect" value="{{$sumelect}}" class="form-control" placeholder="Total Electives" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_elective">
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2" style="display:flex">
                        <div class="form-group">
                            <label for="">Total Program</label>
                            @if($countstudy == 0)
                            <input type="text" id="totalProgram" class="form-control" placeholder="Total Program" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_program">
                            @else
                            <input type="text" id="totalProgram" value="{{$sum}}" class="form-control" placeholder="Total Program" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_program">
                            @endif
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" name="exit" value="1" class="btn btn-primary" style="width:190px; color:white; border:3px solid #D3D3D3;">Save</button>
                        </div>
                    </div>          
    </form>
                    <div class="col-md-3">
                        <form action="{{ route('course.inactive.submit') }}" method="post">
                        {{ csrf_field() }}
                        <button type="submit" class="btn" style="width:190px; background-color:rgb(215, 227, 191); font-weight:bold; color:black; border:3px solid black;" data-toggle="modal" data-target="#myModal2">Submit The Application</button>
                        </form>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="">
                            <h2>Exit</h2>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p>Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
    
</body>
</html>
<script>
$("#program").change(function () {
        var val = $(this).val();
        if(val == "CBBAMGT"){
            $("#status").val('Pending');    
        }else{
            $("#status").val('{{$program3->status}}');
        }

    });
$(document).ready(function () {

    var pre = document.getElementById("popup1");

    $("#clickmodal").click(function(){
        pre.style.display = "none";
    });

    $('#program').selectize({
          sortField: 'text'
      });

    var max_fields      = 99; //maximum input boxes allowed
	var wrapper   		= $("#txt"); //Fields wrapper
    var add_button      = $("#addtxt"); //Add button ID

	
	var x = 0; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append(
                '<a href="#" class="remove_field" style="margin-left:90%;">remove <i class="fas fa-times"></i></a>'+
                '<div class="row content">'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Semester</label>'+
                            '<select class="form-control" name="semester['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>'+
                                '<option value=""></option>'+
                                '@foreach($semester as $data)'+
                                '<option value="{{$data->semester_code}}">{{$data->semester_code}} - {{$data->semester_description}}</option>'+
                                '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">course</label>'+
                            '<select class="select-state" id="course'+x+'" name="course['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>'+
                                '<option value=""></option>'+
                                '@foreach($course2 as $data)'+
                                '<option value="{{$data->course_code}}">{{$data->course_code}} - {{$data->long_title}}</option>'+
                                '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Course Type</label>'+
                            '<select class="form-control" id="type'+x+'" name="type['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>'+
                            '<option value=""></option>'+
                            '@foreach($type as $data)'+
                                '<option value="{{$data->course_type_code}}">{{$data->course_type_code}} - {{$data->type_description}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Course Genre</label>'+
                            '<select class="form-control" id="genre'+x+'" name="genre['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Credit</label>'+
                            '<select class="form-control" id="credit'+x+'" name="credit['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                '</div>'
            ); //add input box

            $('#course'+x+'').selectize({
                sortField: 'text'
            });

            $(wrapper).on("change","#course"+x+"", function(e){ //user click on remove text
                var val = $(this).val();
                $("#genre"+x+"").html(
                    '<option value="{{$course->course_genre_code_01}}" selected>{{$course->course_genre_code_01}}</option>'
                );
                
                $("#credit"+x+"").html(
                    '<option value="{{$course->credit_hours}}" selected>{{$course->credit_hours}}</option>'
                );
            });
            
            $(wrapper).on("change","#type"+x+"", function(e){ //user click on remove text
                var val2 = $("#type"+x+"").val();
                // alert(val2);

                if(val2 == "Core"){
                    document.getElementById('credit'+x+'').setAttribute("name","creditcore["+x+"]");
                    var s = $('select[name^="creditcore"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum = s.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalCore").val(sum);
                }else if(val2 == "GEN"){
                    document.getElementById('credit'+x+'').setAttribute("name","creditgen["+x+"]");
                    var y = $('select[name^="creditgen"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum2 = y.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalGened").val(sum2);
                }else if(val2 == "ELECT"){
                    document.getElementById('credit'+x+'').setAttribute("name","creditelect["+x+"]");
                    var y = $('select[name^="creditelect"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum2 = y.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalElect").val(sum2);
                }else if(val2 == "CONC"){
                    document.getElementById('credit'+x+'').setAttribute("name","creditconc["+x+"]");
                    var y = $('select[name^="creditconc"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum2 = y.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalConcentration").val(sum2);
                }

                var a1 = parseInt($('#totalGened').val());
                var a2 = parseInt($('#totalCore').val());
                var a3 = parseInt($('#totalElect').val());
                var a4 = parseInt($('#totalConcentration').val());

                var sum3 = a1 + a2 + a3 + a4;

                $("#totalProgram").val(sum3);
            });
		}
	});

	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    });
});
</script>