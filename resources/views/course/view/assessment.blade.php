<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}

</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                            <div class="row">
                            <div class="col-md-8">
                                <h5 style="text-align:center; margin-top:20px; color:#a99451;">View Course</h5>
                            </div>
                            </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Course Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="{{url('course/view')}}" class="done">Course Main Attributes</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/hour')}}" class="done">Course Hours</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/requisite')}}"  class="done">Course Requisite and Equivalencies</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/association')}}" class="done">Program Assosiation</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/outcome')}}" class="done">Course Learning Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/assessment')}}" class="done" style="border: 4px solid #0b4176;">Course Content and Assessments</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/textbook')}}" class="done">Assigned Textbook and Other Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/optional')}}" class="done">Optional Reading Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/grading')}}" class="done">Grading Schema</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/policies')}}" class="done">Course Policies</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/special')}}" class="done">Course Special Requirements</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/pedagogy')}}" class="done">Pedagogy</a>
                    </li>
                    <li>
                        <a href="#">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="{{ route('course.assessment.store') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
                @endforeach
            </div>
            <div id="page-inner">
                <p style="color:#0b4176; font-size:17px; text-weigth:bold; font-family:times new roman;">Course Content</p>
                <div class="row content">
                </div>
                <br><br>
                @if($countcontent == "0")
                <div class="row content">
                    <div class="col-md-5" style="display:flex">
                        <div class="form-group">
                        <textarea name="additional" class="form-control" placeholder="" style="border:2px solid #6fa7d9; color:#6fa7d9; width:450px" readonly>No Information Added</textarea>
                        </div>
                    </div>
                </div>
                @else
                <div id="row">
                <a href="#" class="remove_field" onclick="remove()" style="margin-left:90%;">remove <i class="fas fa-times"></i></a>
                <div class="row content">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="">Sequence</label>
                            <input type="text" value="1" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Course ID</label>
                            <select class="form-control" name="idrep" id="idrep" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$replace->course_code}}">{{$replace->course_code}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                </div>
                @endif
                <br>
                <div id="replace">
                </div>
                <p style="color:#0b4176; font-size:17px; text-weigth:bold; font-family:times new roman;">Weekly Outline</p>
                <div class="row content">
                </div>
                <br><br>
                @if($countweekly == "0")
                <div id="pre">
                </div>
                <div class="row content">
                    <div class="col-md-5" style="display:flex">
                        <div class="form-group">
                        <textarea name="additional" class="form-control" placeholder="" style="border:2px solid #6fa7d9; color:#6fa7d9; width:450px" readonly>No Information Added</textarea>
                        </div>
                    </div>
                </div>
                @else
                <div class="row content">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="">Sequence</label>
                            <input type="text" value="1" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>
                        </div>
                    </div>
                </div>
                @endif
                <br>
                <p style="color:#0b4176; font-size:17px; text-weigth:bold; font-family:times new roman;">Assessment Method</p>
                <div class="row content">
                </div>
                <br><br>
                @if($countassessment == "0")
                <div id="co">
                </div>
                @else
                <div class="row content">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="">Sequence</label>
                            <input type="text" value="1" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Course ID</label>
                            <select class="form-control" name="idco" id="idco" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$corequisite->course_code}}">{{$corequisite->course_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Course Title (only active course)</label>
                            <select class="form-control" name="titleco" id="titleco" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$title3->long_title}}">{{$title3->long_title}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Effective Term</label>
                            <select class="form-control" name="effco" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$corequisite->effective_term_code}}">{{$corequisite->effective_term_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">End Term</label>
                            <select class="form-control" name="endco" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$corequisite->end_term_code}}">{{$corequisite->end_term_code}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Condition</label>
                            <select class="form-control" name="condition3" id="condition3" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;">
                            <option value="{{$corequisite->condition}}">{{$corequisite->condition}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row content" id="co2">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="">Sequence</label>
                            <input type="text" value="2" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Course ID</label>
                            <select class="form-control" id="idco2" name="idco2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$corequisite2->course_code}}">{{$corequisite2->course_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Course Title (only active course)</label>
                            <select class="form-control" id="titleco2" name="titleco2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$title3->long_title}}">{{$title3->long_title}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Effective Term</label>
                            <select class="form-control" name="effco2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$corequisite2->effective_term_code}}">{{$corequisite2->effective_term_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">End Term</label>
                            <select class="form-control" name="endco2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$corequisite2->end_term_code}}">{{$corequisite2->end_term_code}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                @endif
                <br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="">
                            <h2>Exit</h2>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p>Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <form>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</body>
</html>
<script>
</script>