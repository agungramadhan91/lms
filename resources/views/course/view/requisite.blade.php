<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}

</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                            <div class="row">
                            <div class="col-md-8">
                                <h5 style="text-align:center; margin-top:20px; color:#a99451;">View  Course</h5>
                            </div>
                            </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Course Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="{{url('course/view')}}" class="done">Course Main Attributes</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/hour')}}" class="done">Course Hours</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/structure')}}"  class="done" style="border: 4px solid #0b4176;">Course Requisite and Equivalencies</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/association')}}" class="done">Program Assosiation</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/outcome')}}" class="done">Course Learning Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/assessment')}}" class="done">Course Content and Assessments</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/textbook')}}" class="done">Assigned Textbook and Other Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/optional')}}" class="done">Optional Reading Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/grading')}}" class="done">Grading Schema</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/policies')}}" class="done">Course Policies</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/cost')}}" class="done">Course Special Requirements</a>
                    </li>
                    <li>
                        <a href="{{url('course/view/community')}}" class="done">Pedagogy</a>
                    </li>
                    <li>
                        <a href="#">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="{{ route('course.requisite.store') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
                @endforeach
            </div>
            <div id="page-inner">
                <p style="color:#0b4176; font-size:17px; text-weigth:bold; font-family:times new roman;">This Course Replaces</p>
                <br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group" style="margin-top:-20px;" id="id">
                        @if($countreplace == "0")
                        @else
                            <label for="">ID</label>
                            <input type="text" class="form-control" placeholder="" value="{{$replace->course_replace_code}}" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:130px;">
                        @endif
                        </div>
                    </div>
                </div>
                @if($countreplace == "0")
                <div class="row content">
                    <div class="col-md-5" style="display:flex">
                        <div class="form-group">
                        <textarea name="additional" class="form-control" placeholder="" style="border:2px solid #6fa7d9; color:#6fa7d9; width:450px" readonly>No Information Added</textarea>
                        </div>
                    </div>
                </div>
                @else
                <div id="row">
                <div class="row content">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="">Sequence</label>
                            <input type="text" value="1" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Course ID</label>
                            <select class="form-control" name="idrep" id="idrep" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$replace->course_code}}">{{$replace->course_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="">Course Title (only inactive course)</label>
                            <select class="form-control" name="titlerep" id="titlerep" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$title->long_title}}">{{$title->long_title}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Condition</label>
                            <select class="form-control" name="condition" id="condition" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;">
                                <option value="{{$replace->condition}}">{{$replace->condition}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row content" id="rep2">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="">Sequence</label>
                            <input type="text" value="2" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Course ID</label>
                            <select class="form-control" name="idrep2" id="idrep2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$replace2->course_code}}" selected>{{$replace2->course_code}}</option>
                            @foreach($courseinactive as $data)
                            <option value="{{$data->course_code}}">{{$data->course_code}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="">Course Title (only inactive course)</label>
                            <select class="form-control" name="titlerep2" id="titlerep2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$title->long_title}}">{{$title->long_title}}</option>
                            @foreach($courseinactive as $data)
                            <option value="{{$data->course_code}}">{{$data->course_code}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                </div>
                @endif
                <div id="replace">
                </div>
                

                <br>
                <p style="color:#0b4176; font-size:17px; text-weigth:bold; font-family:times new roman;">Course Prerequisites</p>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group" style="margin-top:-20px;" id="idpre">
                        @if($countprerequisite == "0")
                        @else
                            <label for="">ID</label>
                            <input type="text" class="form-control" value="{{$prerequisite->course_prerequisite_code}}" placeholder="" name="idpre1" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:130px;">
                        @endif
                        </div>
                    </div>
                </div>
                <br>
                @if($countprerequisite == "0")
                <div class="row content">
                    <div class="col-md-5" style="display:flex">
                        <div class="form-group">
                        <textarea name="additional" class="form-control" placeholder="" style="border:2px solid #6fa7d9; color:#6fa7d9; width:450px" readonly>No Information Added</textarea>
                        </div>
                    </div>
                </div>
                @else
                <div class="row content">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="">Sequence</label>
                            <input type="text" value="1" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Course ID</label>
                            <select class="form-control" name="idpre" id="idpre" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$prerequisite->course_code}}">{{$prerequisite->course_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Course Title (only active course)</label>
                            <select class="form-control" name="titlepre" id="titlepre" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$title2->long_title}}">{{$title2->long_title}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Effective Term</label>
                            <select class="form-control" name="effpre" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$prerequisite->effective_term_code}}">{{$prerequisite->effective_term_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">End Term</label>
                            <select class="form-control" name="endpre" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$prerequisite->end_term_code}}">{{$prerequisite->end_term_code}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Condition</label>
                            <select class="form-control" name="condition2" id="condition2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;">
                            <option value="{{$prerequisite->condition}}">{{$prerequisite->condition}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row content" id="pre2">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="">Sequence</label>
                            <input type="text" value="2" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Course ID</label>
                            <select class="form-control" name="idpre2" id="idpre2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$prerequisite2->course_code}}">{{$prerequisite2->course_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Course Title (only active course)</label>
                            <select class="form-control" name="titlepre2" id="titlepre2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$title2->long_title}}">{{$title2->long_title}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Effective Term</label>
                            <select class="form-control" name="effpre2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$prerequisite2->effective_term_code}}">{{$prerequisite2->effective_term_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">End Term</label>
                            <select class="form-control" name="endpre2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$prerequisite2->end_term_code}}">{{$prerequisite2->end_term_code}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                @endif
                <br>
                <p style="color:#0b4176; font-size:17px; text-weigth:bold; font-family:times new roman;">Course Co-requisites</p>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group" style="margin-top:-20px;" id="idco">
                        @if($countcorequisite == "0")
                        @else
                            <label for="">ID</label>
                            <input type="text" class="form-control" value="{{$corequisite->course_corequisite_code}}" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:130px;">
                        @endif
                        </div>
                    </div>
                </div>
                <br>
                @if($countcorequisite == "0")
                <div class="row content">
                    <div class="col-md-5" style="display:flex">
                        <div class="form-group">
                        <textarea name="additional" class="form-control" placeholder="" style="border:2px solid #6fa7d9; color:#6fa7d9; width:450px" readonly>No Information Added</textarea>
                        </div>
                    </div>
                </div>
                @else
                <div class="row content">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="">Sequence</label>
                            <input type="text" value="1" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Course ID</label>
                            <select class="form-control" name="idco" id="idco" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$corequisite->course_code}}">{{$corequisite->course_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Course Title (only active course)</label>
                            <select class="form-control" name="titleco" id="titleco" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$title3->long_title}}">{{$title3->long_title}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Effective Term</label>
                            <select class="form-control" name="effco" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$corequisite->effective_term_code}}">{{$corequisite->effective_term_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">End Term</label>
                            <select class="form-control" name="endco" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$corequisite->end_term_code}}">{{$corequisite->end_term_code}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Condition</label>
                            <select class="form-control" name="condition3" id="condition3" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;">
                            <option value="{{$corequisite->condition}}">{{$corequisite->condition}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row content" id="co2">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="">Sequence</label>
                            <input type="text" value="2" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Course ID</label>
                            <select class="form-control" id="idco2" name="idco2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$corequisite2->course_code}}">{{$corequisite2->course_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Course Title (only active course)</label>
                            <select class="form-control" id="titleco2" name="titleco2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$title3->long_title}}">{{$title3->long_title}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Effective Term</label>
                            <select class="form-control" name="effco2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$corequisite2->effective_term_code}}">{{$corequisite2->effective_term_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">End Term</label>
                            <select class="form-control" name="endco2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$corequisite2->end_term_code}}">{{$corequisite2->end_term_code}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                @endif
                <br>
                <p style="color:#0b4176; font-size:17px; text-weigth:bold; font-family:times new roman;">Course Equivalency</p>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group" style="margin-top:-20px;" id="idqui">
                        @if($countequivalency == 0)
                        @else
                            <label for="">ID</label>
                            <input type="text" value="{{$equivalency->course_equivalency_code}}" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9; width:130px;">
                        @endif
                        </div>
                    </div>
                </div>
                <br>
                @if($countequivalency == 0)
                <div class="row content">
                    <div class="col-md-5" style="display:flex">
                        <div class="form-group">
                        <textarea name="additional" class="form-control" placeholder="" style="border:2px solid #6fa7d9; color:#6fa7d9; width:450px" readonly>No Information Added</textarea>
                        </div>
                    </div>
                </div>
                @else
                <div class="row content">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="">Sequence</label>
                            <input type="text" value="1" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Course ID</label>
                            <select class="form-control" name="idqui" id="idqui" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$equivalency->course_code}}">{{$equivalency->course_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Course Title (only active course)</label>
                            <select class="form-control" name="titlequi" id="titlequi" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$title4->long_title}}">{{$title4->long_title}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Effective Term</label>
                            <select class="form-control" name="effqui" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$equivalency->effective_term_code}}">{{$equivalency->effective_term_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">End Term</label>
                            <select class="form-control" name="endqui" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$equivalency->end_term_code}}">{{$equivalency->end_term_code}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Condition</label>
                            <select class="form-control" name="condition4" id="condition4" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9;">
                            <option value="{{$equivalency->condition}}">{{$equivalency->condition}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row content" id="qui2">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="">Sequence</label>
                            <input type="text" value="2" class="form-control" placeholder="" name="id" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Course ID</label>
                            <select class="form-control" name="idqui2" id="idqui2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$equivalency2->course_code}}">{{$equivalency2->course_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Course Title (only active course)</label>
                            <select class="form-control" name="titlequi2" id="titlequi2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$title4->long_title}}">{{$title4->long_title}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Effective Term</label>
                            <select class="form-control" name="effqui2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$equivalency2->effective_term_code}}">{{$equivalency2->effective_term_code}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">End Term</label>
                            <select class="form-control" name="endqui2" style="background-color:#fffbf1; border:2px solid #6fa7d9; color:#6fa7d9; ">
                            <option value="{{$equivalency2->end_term_code}}">{{$equivalency2->end_term_code}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                @endif
                <br><br>
                <div class="row content">
                <div class="col-md-3">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="">
                            <h2>Exit</h2>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p>Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <form>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</body>
</html>
<script>
</script>