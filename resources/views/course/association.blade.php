<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.bootstrap3.min.css')}}" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}

hr {
    border: none;
    height: 1px;
    /* Set the hr color */
    color: #333; /* old IE */
    background-color: #333; /* Modern Browsers */
}
.selectize-control{
    width:400px;
    color:#6fa7d9;
    background-color:#fffbf1;
}

.selectize-input{
    background-color:#fffbf1;
    border:2px solid #6fa7d9;

}
.select-state{
    color:#6fa7d9;
    background-color:#fffbf1;
}
</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                            <div class="row">
                            <div class="col-md-8">
                                <h5 style="text-align:center; margin-top:20px; color:#a99451;">Add New Course</h5>
                            </div>
                            <div class="col-md-4">
                                <button type="submit" class="btn-small" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3; margin-top:15px; margin-left:-30px" data-toggle="modal" data-target="#myModal2">Submit The Application</button>
                            </div>
                            </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Course Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="{{url('course')}}" class="done" style="border-bottom: 3px solid red;">Course Main Attributes</a>
                    </li>
                    <li>
                        <a href="{{url('course/hour')}}" class="done">Course Hours</a>
                    </li>
                    <li>
                        <a href="{{url('course/requisite')}}" class="done">Course Requisite and Equivalencies</a>
                    </li>
                    <li>
                        @if($countstudy == 0)
                        <a href="{{url('course/association')}}" class="active" style="border-bottom: 3px solid red;">Program Assosiation</a>
                        @else
                        <a href="{{url('course/association')}}" class="done" style="border: 4px solid #0b4176; border-bottom: 3px solid red;">Program Assosiation</a>
                        @endif
                    </li>
                    <li>
                        <a href="{{url('course/outcome')}}" class="done" style="border-bottom: 3px solid red;">Course Learning Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('course/assessment')}}" class="done">Course Content and Assessments</a>
                    </li>
                    <li>
                    @if($textbook == "0")
                        <a href="{{url('course/textbook')}}">Assigned Textbook and Other Materials</a>
                    @else
                        <a href="{{url('course/textbook')}}" class="done">Assigned Textbook and Other Materials</a>
                    @endif
                    </li>
                    <li>
                        <a href="{{url('course/optional')}}" class="done">Optional Reading Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/grading')}}" class="done" style="border-bottom: 3px solid red;">Grading Schema</a>
                    </li>
                    <li>
                    @if($countpolicy == "0")
                        <a href="{{url('course/policies')}}">Course Policies</a>
                    @else
                        <a href="{{url('course/policies')}}" class="done">Course Policies</a>
                    @endif
                    </li>
                    <li>
                    @if($countspecial == "0")
                        <a href="{{url('course/special')}}">Course Special Requirements</a>
                    @else
                        <a href="{{url('course/special')}}" class="done">Course Special Requirements</a>
                    @endif
                    </li>
                    <li>
                    @if($countpedagogy == "0")
                        <a href="{{url('course/pedagogy')}}" class="active">Pedagogy</a>
                    @else
                        <a href="{{url('course/pedagogy')}}" class="done">Pedagogy</a>
                    @endif
                    </li>
                    <li>
                        <a href="#">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        
        <form action="{{ route('course.association.store') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
  
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Program 1</a></li>
            <li><a data-toggle="tab" href="#menu1">Program 2</a></li>
        </ul>
            <div class="flash-message">
                
            @foreach(['Error', 'warning', 'Confirmation', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <!-- <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p> -->
                    <div class="modal" id="popup1" style="display:block;">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header {{ $msg }}">
                                <h4 style="font-weight:bold;">{{ $msg }}</h4>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body">
                                <p style="font-size:17px;">{{ Session::get('alert-' . $msg) }}</p>
                            </div>  
                            <!-- Modal footer -->
                            <div class="modal-footer">
                            <!-- <a href="{{url('/')}}">
                                <button type="button" class="btn btn-danger">OK</button>
                            </a> -->
                                <button type="button" id="clickmodal" style="width:190px; color:white; border:3px solid #D3D3D3;" class="btn btn-primary" data-dismiss="modal">Ok</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endif
            @endforeach
            </div>
            
        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <div id="page-inner">
                    <div class="row content">
                        <div class="col-md-6" style="display:flex">
                            <div class="form-group">
                                <label for="">Program Study Plan</label>
                                <select class="select-state" placeholder="Pick a program..." id="program" name="program" style="border:2px solid #6fa7d9; color:#6fa7d9;" required>
                                    <option value=""></option>
                                    @if($countstudy != "0")
                                    <option value="{{$status->program_code}}" selected>{{$status->program_code}} - {{$status->title}}</option>
                                    @foreach($plan as $data)
                                    <option value="{{$data->program_code}}">{{$data->program_code}} - {{$data->title}}</option>
                                    @endforeach
                                    @else
                                    @foreach($plan as $data)
                                    <option value="{{$data->program_code}}">{{$data->program_code}} - {{$data->title}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                                <label for="">Program Status</label>
                                @if($countstudy == "0")
                                <input type="text" id="status" class="form-control" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="status" readonly>
                                @else
                                <input type="text" value="{{$status->status}}" id="status" class="form-control" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="status" readonly>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row content">
                        <div class="col-md-12" style="display:flex">
                            <button type="button" id="addtxt" class="bttn" style="background-color: #b1cfeb; margin-top:10px;width:300px;">Insert Course <i class="fas fa-plus"></i></button>
                        </div>
                    </div>
                    <br><br>
                    @if($countstudy !=0)
                    @foreach($study as $data)
                        <div class="row content">
                            <div class="col-md-2" style="display:flex">
                                <div class="form-group">
                                    <label for="">Semester</label>
                                    <select class="form-control" name="semester1" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>
                                        <option value="{{$data->semester_code}}" selected>{{$data->semester_code}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2" style="display:flex">
                                <div class="form-group">
                                    <label for="">course</label>
                                    <select class="form-control" id="course1" name="course1" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>
                                        <option value="{{$data->course_code}}" selected>{{$data->course_code}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2" style="display:flex">
                                <div class="form-group">
                                    <label for="">Course Type</label>
                                    <select class="form-control" id="type'+x+'" name="type['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>
                                    <option value="{{$data->course_type}}" selected>{{$data->course_type}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2" style="display:flex">
                                <div class="form-group">
                                    <label for="">Course Genre</label>
                                    <select class="form-control" id="genre'+x+'" name="genre['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                    <option value="{{$data->course_genre}}" selected>{{$data->course_genre}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2" style="display:flex">
                                <div class="form-group">
                                    <label for="">Credit</label>
                                    <select class="form-control" id="credit'+x+'" name="credit['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>
                                    <option value="{{$data->credit_hour}}" selected>{{$data->credit_hour}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @endif
                    <div id="txt">
                    </div>
                    
                    <div class="row content">
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group" id="gened">
                                <label for="">Total Gened</label>
                                @if($countstudy == 0)
                                <input type="text" id="totalGened" value="0" class="form-control" placeholder="Total Gened" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_gened">
                                @else
                                <input type="text" id="totalGened" value="{{$sumgen}}" class="form-control" placeholder="Total Gened" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_gened">
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                            <label for="">Total Core</label>
                            @if($countstudy == 0)
                                <input type="text" id="totalCore" value="0" class="form-control" placeholder="Total Core" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_core">
                                @else
                                <input type="text" id="totalCore" value="{{$sumcore}}" class="form-control" placeholder="Total Core" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_core">
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                            <label for="">Total Concentration 1</label>
                            @if($countstudy == 0)
                                <input type="text" id="totalConcentration" value="0" class="form-control" placeholder="Total Concentration 1" style="border:2px solid #6fa7d9; color:#6fa7d9; width:240px;" name="total_concentration">
                                @else
                                <input type="text" id="totalConcentration" value="{{$sumcon}}" class="form-control" placeholder="Total Concentration 1" style="border:2px solid #6fa7d9; color:#6fa7d9; width:240px;" name="total_concentration">
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for=""> Total Electives</label>
                                @if($countstudy == 0)
                                <input type="text" id="totalElect" value="0" class="form-control" placeholder="Total Electives" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_elective">
                                @else
                                <input type="text" id="totalElect" value="{{$sumelect}}" class="form-control" placeholder="Total Electives" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_elective">
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Total Program</label>
                                @if($countstudy == 0)
                                <input type="text" id="totalProgram" class="form-control" placeholder="Total Program" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_program">
                                @else
                                <input type="text" id="totalProgram" value="{{$sum}}" class="form-control" placeholder="Total Program" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_program">
                                @endif
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="row content">
                        <div class="col-md-3">
                            <div class="form-group">
                                <button type="submit" name="exit" value="1" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Save and Exit</button>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <button type="submit" name="continue" value="1" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Save and Continue</button>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                            </div>
                        </div>
                    </div>
                    <!-- /. ROW  -->
                    
                    <div class="modal fade" id="myModal">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header" style="">
                                <h2>Exit</h2>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body">
                                <p>Your data will be lost if you exit</p>
                            </div>  
                            <!-- Modal footer -->
                            <div class="modal-footer">
                            <a href="{{url('home/course')}}">
                                <button type="button" class="btn btn-danger">OK</button>
                            </a>
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                            </div>
                            
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="menu1" class="tab-pane fade">
                <div id="page-inner">
                    <div class="row content">
                        <div class="col-md-6" style="display:flex">
                            <div class="form-group">
                                <label for="">Program Study Plan</label>
                                <select class="select-state" placeholder="Pick a program..." id="program2" name="program" style="border:2px solid #6fa7d9; color:#6fa7d9;" required>
                                    <option value=""></option>
                                    @foreach($plan as $data)
                                    <option value="{{$data->program_code}}">{{$data->program_code}} - {{$data->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                                <label for="">Program Status</label>
                                <input type="text" id="status2" class="form-control" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="status" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row content">
                        <div class="col-md-12" style="display:flex">
                            <button type="button" id="addtxt2" class="bttn" style="background-color: #b1cfeb; margin-top:10px;width:300px;">Insert Course <i class="fas fa-plus"></i></button>
                        </div>
                    </div>
                    <br><br>
                    <div id="txt2">
                    </div>
                    
                    <div class="row content">
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group" id="gened">
                                <label for="">Total Gened</label>
                                <input type="text" id="totalGened2" value="0" class="form-control" placeholder="Total Gened" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_gened">
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                            <label for="">Total Core</label>
                                <input type="text" id="totalCore2" value="0" class="form-control" placeholder="Total Core" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_core">
                            </div>
                        </div>
                        <div class="col-md-3" style="display:flex">
                            <div class="form-group">
                            <label for="">Total Concentration 1</label>
                                <input type="text" id="totalConcentration2" value="0" class="form-control" placeholder="Total Concentration 1" style="border:2px solid #6fa7d9; color:#6fa7d9; width:240px;" name="total_concentration">
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for=""> Total Electives</label>
                                <input type="text" id="totalElect2" value="0" class="form-control" placeholder="Total Electives" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_elective">
                            </div>
                        </div>
                        <div class="col-md-2" style="display:flex">
                            <div class="form-group">
                                <label for="">Total Program</label>
                                <input type="text" id="totalProgram2" class="form-control" placeholder="Total Program" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" name="total_program">
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="row content">
                        <div class="col-md-3">
                            <div class="form-group">
                                <button type="submit" name="exit" value="1" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Save and Exit</button>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <button type="submit" name="continue" value="1" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Save and Continue</button>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                            </div>
                        </div>
                    </div>
                    <!-- /. ROW  -->
                    
                    <div class="modal fade" id="myModal">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header" style="">
                                <h2>Exit</h2>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body">
                                <p>Your data will be lost if you exit</p>
                            </div>  
                            <!-- Modal footer -->
                            <div class="modal-footer">
                            <a href="{{url('home/course')}}">
                                <button type="button" class="btn btn-danger">OK</button>
                            </a>
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                            </div>
                            
                        </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <form>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
    
</body>
</html>
<script>
$("#program").change(function () {
        var val = $(this).val();
        if(val == "CBBAMGT"){
            $("#status").val('Pending');    
        }else{
            $("#status").val('{{$program3->status}}');
        }

    });
    
$("#program2").change(function () {
        var val = $(this).val();
        if(val == "CBBAMGT"){
            $("#status2").val('Pending');    
        }else{
            $("#status2").val('{{$program3->status}}');
        }

    });
$(document).ready(function () {
    
    var pre = document.getElementById("popup1");

    $("#clickmodal").click(function(){
        pre.style.display = "none";
    });

    $('#program').selectize({
          sortField: 'text'
      });

    $('#program2').selectize({
        sortField: 'text'
    });

    var max_fields      = 99; //maximum input boxes allowed
	var wrapper   		= $("#txt"); //Fields wrapper
    var add_button      = $("#addtxt"); //Add button ID

	
	var x = 0; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append(
                '<div class="row content">'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Semester</label>'+
                            '<select class="form-control" name="semester['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>'+
                                '<option value=""></option>'+
                                '@foreach($semester as $data)'+
                                '<option value="{{$data->semester_code}}">{{$data->semester_code}} - {{$data->semester_description}}</option>'+
                                '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">course</label>'+
                            '<select class="select-state" id="course'+x+'" name="course['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>'+
                                '<option value=""></option>'+
                                '@foreach($course2 as $data)'+
                                '<option value="{{$data->course_code}}">{{$data->course_code}} - {{$data->long_title}}</option>'+
                                '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Course Type</label>'+
                            '<select class="form-control" id="type'+x+'" name="type['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>'+
                            '<option value=""></option>'+
                            '@foreach($type as $data)'+
                                '<option value="{{$data->course_type_code}}">{{$data->course_type_code}} - {{$data->type_description}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Course Genre</label>'+
                            '<select class="form-control" id="genre'+x+'" name="genre['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Credit</label>'+
                            '<select class="form-control" id="credit'+x+'" name="credit['+x+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                '</div>'
            ); //add input box

            $('#course'+x+'').selectize({
                sortField: 'text'
            });
            $(wrapper).on("change","#course"+x+"", function(e){ //user click on remove text
                var val = $(this).val();
                $("#genre"+x+"").html(
                    '<option value="{{$course->course_genre_code_01}}" selected>{{$course->course_genre_code_01}}</option>'
                );
                
                $("#credit"+x+"").html(
                    '<option value="{{$course->credit_hours}}" selected>{{$course->credit_hours}}</option>'
                );
            });
            
            $(wrapper).on("change","#type"+x+"", function(e){ //user click on remove text
                var val2 = $("#type"+x+"").val();
                // alert(val2);

                if(val2 == "Core"){
                    document.getElementById('credit'+x+'').setAttribute("name","creditcore["+x+"]");
                    var s = $('select[name^="creditcore"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum = s.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalCore").val(sum);
                }else if(val2 == "GEN"){
                    document.getElementById('credit'+x+'').setAttribute("name","creditgen["+x+"]");
                    var y = $('select[name^="creditgen"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum2 = y.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalGened").val(sum2);
                }else if(val2 == "ELECT"){
                    document.getElementById('credit'+x+'').setAttribute("name","creditelect["+x+"]");
                    var y = $('select[name^="creditelect"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum2 = y.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalElect").val(sum2);
                }else if(val2 == "CONC"){
                    document.getElementById('credit'+x+'').setAttribute("name","creditconc["+x+"]");
                    var y = $('select[name^="creditconc"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum2 = y.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalConcentration").val(sum2);
                }

                var a1 = parseInt($('#totalGened').val());
                var a2 = parseInt($('#totalCore').val());
                var a3 = parseInt($('#totalElect').val());
                var a4 = parseInt($('#totalConcentration').val());

                var sum3 = a1 + a2 + a3 + a4;

                $("#totalProgram").val(sum3);
            });
		}
	});

	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    });

    var max_fields      = 99; //maximum input boxes allowed
	var wrapper2   		= $("#txt2"); //Fields wrapper
    var add_button2      = $("#addtxt2"); //Add button ID

	
	var y = 0; //initlal text box count
	$(add_button2).click(function(e){ //on add input button click
		e.preventDefault();
		if(y < max_fields){ //max input box allowed
			y++; //text box increment
			$(wrapper2).append(
                '<div class="row content">'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Semester</label>'+
                            '<select class="form-control" name="semester2_['+y+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>'+
                                '<option value=""></option>'+
                                '@foreach($semester as $data)'+
                                '<option value="{{$data->semester_code}}">{{$data->semester_code}} - {{$data->semester_description}}</option>'+
                                '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">course</label>'+
                            '<select class="select-state" id="course2_'+y+'" name="course2_['+y+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>'+
                                '<option value=""></option>'+
                                '@foreach($course2 as $data)'+
                                '<option value="{{$data->course_code}}">{{$data->course_code}} - {{$data->long_title}}</option>'+
                                '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Course Type</label>'+
                            '<select class="form-control" id="type2_'+y+'" name="type2_['+y+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required>'+
                            '<option value=""></option>'+
                            '@foreach($type as $data)'+
                                '<option value="{{$data->course_type_code}}">{{$data->course_type_code}} - {{$data->type_description}}</option>'+
                            '@endforeach'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Course Genre</label>'+
                            '<select class="form-control" id="genre2_'+y+'" name="genre2_['+y+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Credit</label>'+
                            '<select class="form-control" id="credit2_'+y+'" name="credit2_['+y+']" style="border:2px solid #6fa7d9; color:#6fa7d9; width:150px;" required readonly>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                '</div>'
            ); //add input box

            $('#course2_'+y+'').selectize({
                sortField: 'text'
            });
            $(wrapper).on("change","#course2_"+y+"", function(e){ //user click on remove text
                var val = $(this).val();
                $("#genre2_"+y+"").html(
                    '<option value="{{$course->course_genre_code_01}}" selected>{{$course->course_genre_code_01}}</option>'
                );
                
                $("#credit2_"+y+"").html(
                    '<option value="{{$course->credit_hours}}" selected>{{$course->credit_hours}}</option>'
                );
            });
            
            $(wrapper).on("change","#type2_"+y+"", function(e){ //user click on remove text
                var val2 = $("#type2_"+y+"").val();
                // alert(val2);

                if(val2 == "Core"){
                    document.getElementById('credit2_'+y+'').setAttribute("name","creditcore2_["+y+"]");
                    var s = $('select[name^="creditcore2_"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum = s.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalCore2").val(sum);
                }else if(val2 == "GEN"){
                    document.getElementById('credit2_'+y+'').setAttribute("name","creditgen2_["+y+"]");
                    var y = $('select[name^="creditgen2_"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum2 = y.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalGened2").val(sum2);
                }else if(val2 == "ELECT"){
                    document.getElementById('credit2_'+y+'').setAttribute("name","creditelect2_["+y+"]");
                    var y = $('select[name^="creditelect2_"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum2 = y.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalElect2").val(sum2);
                }else if(val2 == "CONC"){
                    document.getElementById('credit2_'+y+'').setAttribute("name","creditconc2_["+y+"]");
                    var y = $('select[name^="creditconc2_"] option:selected').map(function() {
                        return this.value
                    }).get()

                    var sum2 = y.reduce((pv, cv) => {
                        return pv + (parseFloat(cv) || 0);
                    }, 0);

                    $("#totalConcentration2").val(sum2);
                }

                var a1 = parseInt($('#totalGened2').val());
                var a2 = parseInt($('#totalCore2').val());
                var a3 = parseInt($('#totalElect2').val());
                var a4 = parseInt($('#totalConcentration2').val());

                var sum3 = a1 + a2 + a3 + a4;

                $("#totalProgram2").val(sum3);
            });
		}
	});

	$(wrapper).on("click",".remove_field2", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    });
});
</script>