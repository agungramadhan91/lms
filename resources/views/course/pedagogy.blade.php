<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}

</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:300px;">
                            <div class="row">
                            <div class="col-md-8">
                                <h5 style="text-align:center; margin-top:20px; color:#a99451;">Add New Course</h5>
                            </div>
                            <div class="col-md-4">
                                <button type="submit" class="btn-small" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3; margin-top:15px; margin-left:-30px" data-toggle="modal" data-target="#myModal2">Submit The Application</button>
                            </div>
                            </div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li style="margin-left:-100px;"><a href="#">Course Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a href="{{url('course')}}" class="done" style="border-bottom: 3px solid red;">Course Main Attributes</a>
                    </li>
                    <li>
                        <a href="{{url('course/hour')}}" class="done" style="border-bottom: 3px solid red;">Course Hours</a>
                    </li>
                    <li>
                        <a href="{{url('course/requisite')}}" class="done">Course Requisite and Equivalencies</a>
                    </li>
                    <li>
                        <a href="{{url('course/assesment')}}" style="border-bottom: 3px solid red;">Program Assosiation</a>
                    </li>
                    <li>
                        <a href="{{url('course/outcome')}}" class="done" style="border-bottom: 3px solid red;">Course Learning Outcomes</a>
                    </li>
                    <li>
                        <a href="{{url('course/fitness')}}">Course Content and Assessments</a>
                    </li>
                    <li>
                    @if($textbook == "0")
                        <a href="{{url('course/textbook')}}">Assigned Textbook and Other Materials</a>
                    @else
                        <a href="{{url('course/textbook')}}" class="done">Assigned Textbook and Other Materials</a>
                    @endif
                    </li>
                    <li>
                        <a href="{{url('course/optional')}}" class="done">Optional Reading Materials</a>
                    </li>
                    <li>
                        <a href="{{url('course/resource')}}" style="border-bottom: 3px solid red;">Grading Schema</a>
                    </li>
                    <li>
                    @if($countpolicy == "0")
                        <a href="{{url('course/policies')}}">Course Policies</a>
                    @else
                        <a href="{{url('course/policies')}}" class="done">Course Policies</a>
                    @endif
                    </li>
                    <li>
                    @if($countspecial == "0")
                        <a href="{{url('course/special')}}">Course Special Requirements</a>
                    @else
                        <a href="{{url('course/special')}}" class="done">Course Special Requirements</a>
                    @endif
                    </li>
                    <li>
                    @if($countpedagogy == "0")
                        <a href="{{url('course/pedagogy')}}" class="active">Pedagogy</a>
                    @else
                        <a href="{{url('course/pedagogy')}}" class="done" style="border: 4px solid #0b4176;">Pedagogy</a>
                    @endif
                    </li>
                    <li>
                        <a href="#">Appendices</a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <form action="{{ route('course.pedagogy.store') }}" method="post">
        {{ csrf_field() }}
        <div id="page-wrapper">
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
                @endforeach
            </div>
            <div id="page-inner">
                <p style="color:#0b4176; font-size:17px; text-weigth:bold; font-family:times new roman;">Pedagogy</p>
                <div class="row content">
                    <div class="col-md-12" style="display:flex">
                        <button type="button" id="addtxt" class="bttn" style="background-color: #b1cfeb; margin-top:10px;width:240px;">Insert Pedagogy <i class="fas fa-plus"></i></button>
                    </div>
                </div>
                <br>
                @if($countpedagogy == "0")
                @else
                <div id="row">
                <a href="#" class="remove_field" onclick="remove()" style="margin-left:90%;">remove <i class="fas fa-times"></i></a>
                <div class="row content">
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Sequence</label>
                            <input type="text" class="form-control" name="sequence" value="{{$pedagogy->pedagogy_code}}" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>
                        </div>
                    </div>
                    <div class="col-md-3" style="display:flex">
                        <div class="form-group">
                            <label for="">Pedagogy Title</label>
                            @if($pedagogy->pedagogy_title == "")
                            <input type="text" class="form-control" placeholder="No Information Added" name="title" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">
                            @else
                            <input type="text" class="form-control" value="{{$pedagogy->pedagogy_title}}" name="title" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-12" style="display:flex">
                        <div class="form-group">
                            <label for="">Pedagogy Text</label>
                            @if($pedagogy->pedagogy_text == "")
                            <textarea name="text" class="form-control" placeholder="No Information Added" style="border:2px solid #6fa7d9; color:#6fa7d9; width:800px"></textarea>
                            @else
                            <textarea name="text" class="form-control" placeholder="" style="border:2px solid #6fa7d9; color:#6fa7d9; width:800px">{{$pedagogy->pedagogy_text}}</textarea>
                            @endif
                        </div>
                    </div>
                </div>
                </div>
                @endif
                <br>
                <div id="txt">
                </div>
                <br><br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Save and Exit</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;">Save and Continue</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="">
                            <h2>Exit</h2>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p>Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <form>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</body>
</html>
<script>
function remove(){
        var row = document.getElementById("row");
         row.style.display = "none";
    }
$(document).ready(function () {
    var max_fields      = 99; //maximum input boxes allowed
	var wrapper   		= $("#txt"); //Fields wrapper
	var add_button      = $("#addtxt"); //Add button ID
	
	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append(
                '<a href="#" class="remove_field" style="margin-left:90%;">remove <i class="fas fa-times"></i></a>'+
                '<div class="row content">'+
                    '<div class="col-md-3" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Sequence</label>'+
                            '<input type="text" class="form-control" name="sequence" value="{{$course->course_code}}_PG_00'+x+'" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;" readonly>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-3" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Pedagogy Title</label>'+
                            '<input type="text" class="form-control" name="title" style="border:2px solid #6fa7d9; background-color:#fffbf1; color:#6fa7d9;">'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="row content">'+
                    '<div class="col-md-12" style="display:flex">'+
                        '<div class="form-group">'+
                            '<label for="">Pedagogy Text</label>'+
                            '<textarea name="text" class="form-control" placeholder="" style="border:2px solid #6fa7d9; color:#6fa7d9; width:800px"></textarea>'+
                        '</div>'+
                    '</div>'+
                '</div>'
                
                ); //add input box
		}
	});
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); x--;
	});
});
</script>