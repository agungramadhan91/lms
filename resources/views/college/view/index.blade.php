<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Eduval</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />
    
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.bootstrap3.min.css')}}" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<style>

.requiredStar{
    color:red;
}


.selectize-control{
    width:400px;
    color:#6fa7d9;
    background-color:#fffbf1;
}

.selectize-input{
    background-color:#fffbf1;
    border:1px solid #6fa7d9;

}

.select-state{
    color:#6fa7d9;
    background-color:#fffbf1;
    border:1px solid #6fa7d9;
}

</style>
<body>
    <div id="wrapper">
        <div class="header">
            <div class="row">
                <div class="col-md-2" style="background-color:white;">
                    <img src="{{ asset('assets/img/eduval-logo.png')}}" width="120px" style="margin-left:40px">
                </div>
                <div class="col-md-10" style=" color:white">
                    <h6 class="h6header">Curriculum and Catalogue Management System (CCMS)</h6>
                </div>
            </div>
        </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                
                <div class="navbar navbar-inverse tetap blue">
                    <div class="adjust-nav">
                        <div class="navbar-header" style="width:192px;">
                            <h5 style="text-align:center; margin-top:20px; color:#a99451">View College</h5>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav center">
                                <li><a href="#">Course Management</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                </div>
            </div>
        <!-- /. NAV TOP  -->
        
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row content">
                    <div class="col-md-12">
                        <table id="campuses-table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Program Code</th>
                                    <th>Program Description</th>
                                    <th>Effective Term Code</th>
                                    <th>End Term Code</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($program as $c)
                                    <tr>
                                        <td>{{ $c->program_code }}</td>
                                        <td>{{ $c->title }}</td>
                                        <td>{{ $c->effective_term_code }}</td>
                                        <td>{{ $c->end_term_code }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <br><br>
                <div class="row content">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn" style="width:190px; background-color:#484648; color:white; border:3px solid #D3D3D3;" data-toggle="modal" data-target="#myModal">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
                <!-- /. ROW  -->
                
                <div class="modal" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header" style="">
                            <h2>Exit</h2>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <p>Your data will be lost if you exit</p>
                        </div>  
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <a href="{{url('/')}}">
                            <button type="button" class="btn btn-danger">OK</button>
                        </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('assets/js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
</body>
</html>
<script>
$(document).ready(function () {

    $('#course').selectize({
      sortField: 'text'
    });

});
</script>