<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCoursess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->string('course_genre_code_03', 10)->nullable();
            $table->foreign('course_genre_code_03')->references('course_genre_code')->on('course_genres')->onDelete('cascade');
            $table->string('course_genre_code_04', 10)->nullable();
            $table->foreign('course_genre_code_04')->references('course_genre_code')->on('course_genres')->onDelete('cascade');
            $table->string('course_genre_code_05', 10)->nullable();
            $table->foreign('course_genre_code_05')->references('course_genre_code')->on('course_genres')->onDelete('cascade');
            $table->string('course_genre_code_06', 10)->nullable();
            $table->foreign('course_genre_code_06')->references('course_genre_code')->on('course_genres')->onDelete('cascade');
            $table->string('course_genre_code_07', 10)->nullable();
            $table->foreign('course_genre_code_07')->references('course_genre_code')->on('course_genres')->onDelete('cascade');
            $table->string('course_genre_code_08', 10)->nullable();
            $table->foreign('course_genre_code_08')->references('course_genre_code')->on('course_genres')->onDelete('cascade');
            $table->string('course_genre_code_09', 10)->nullable();
            $table->foreign('course_genre_code_09')->references('course_genre_code')->on('course_genres')->onDelete('cascade');
            $table->string('course_genre_code_10', 10)->nullable();
            $table->foreign('course_genre_code_10')->references('course_genre_code')->on('course_genres')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
