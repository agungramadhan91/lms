<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToProgramAcademicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campus_program', function(Blueprint $table){
            $table->string('program_code', 10);

            $table->foreign('program_code')
              ->references('program_code')
              ->on('academic_programs')->onDelete('cascade');

            $table->string('campus_code', 7);

            $table->foreign('campus_code')
              ->references('campus_code')
              ->on('campuses')->onDelete('cascade');
        });
        // Schema::table('program_academics', function (Blueprint $table) {
        //     for ($i=1; $i <= 10; $i++) { 
        //         $table->string('language_code_'.$i);

        //         $table->foreign('language_code_'.$i)->references('');
        //     }
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('program_academics', function (Blueprint $table) {
            //
        });
    }
}
