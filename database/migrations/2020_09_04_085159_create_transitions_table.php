<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transitions', function (Blueprint $table) {
            $table->string('transition_id', 10)->primary();
            $table->string('proces_id', 10);
            $table->foreign('proces_id')->references('proces_id')->on('proces')->onDelete('cascade');
            $table->string('current_state_id', 10);
            $table->foreign('current_state_id')->references('state_id')->on('states')->onDelete('cascade');
            $table->string('next_state_id', 10);
            $table->foreign('next_state_id')->references('state_id')->on('states')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transitions');
    }
}
