<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcademicProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academic_programs', function (Blueprint $table) {
            $table->string('program_code', 7)->primary();
            $table->string('campus_code', 7);
            $table->foreign('campus_code')->references('campus_code')->on('campuses')->onDelete('cascade');
            $table->string('degree_type_code', 7);
            $table->foreign('degree_type_code')->references('degree_type_code')->on('degree_types')->onDelete('cascade');
            $table->string('degree_level_code', 7);
            $table->foreign('degree_level_code')->references('degree_level_code')->on('degree_levels')->onDelete('cascade');
            $table->string('college_code', 7);
            $table->foreign('college_code')->references('college_code')->on('colleges')->onDelete('cascade');
            $table->string('title', 100);
            $table->integer('required_hours');
            $table->string('effective_term_code', 7);
            $table->foreign('effective_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->string('end_term_code', 7);
            $table->foreign('end_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->date('system_date');
            $table->integer('sequence')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academic_programs');
    }
}
