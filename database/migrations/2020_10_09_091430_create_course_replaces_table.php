<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseReplacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_replaces', function (Blueprint $table) {
            $table->string('course_replace_code', 15)->primary();
            $table->string('course_code', 7);
            $table->foreign('course_code')->references('course_code')->on('courses')->onDelete('cascade');
            $table->string('effective_term_code', 7);
            $table->foreign('effective_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->string('end_term_code', 7);
            $table->foreign('end_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->date('system_date');
            $table->integer('sequence')->nullable();
            $table->enum('condition',['AND', 'OR']);
            $table->integer('condition_sequence');
            $table->string('instance_code', 10)->nullable();
            $table->foreign('instance_code')->references('instance_id')->on('instances')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_replaces');
    }
}
