<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAssesment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('program_assessments', function (Blueprint $table) {
            $table->dropColumn('college_code');
        });

        Schema::table('program_assessments', function (Blueprint $table) {
            $table->string('program_code', 10)->nullable();
            $table->foreign('program_code')->references('program_code')->on('academic_programs')->onDelete('cascade');
            $table->text('proposed_program_assessment_narrative')->nullable();
            $table->text('planned_direct_assesment')->nullable();
            $table->text('planned_indirect_assesment')->nullable();
            $table->text('other_pertinent_information')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
