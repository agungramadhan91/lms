<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->string('course_code', 7)->primary();
            $table->string('program_code', 7);
            $table->foreign('program_code')->references('program_code')->on('academic_programs')->onDelete('cascade');
            $table->string('college_code', 7);
            $table->foreign('college_code')->references('college_code')->on('colleges')->onDelete('cascade');
            $table->string('grading_mode_code', 7);
            $table->foreign('grading_mode_code')->references('grading_mode_code')->on('grading_modes')->onDelete('cascade');
            $table->string('course_type_code', 7);
            $table->foreign('course_type_code')->references('course_type_code')->on('course_types')->onDelete('cascade');
            $table->string('long_title', 225);
            $table->string('short_title', 10);
            $table->char('grading_mode', 1);
            $table->string('course_prerequisites');
            $table->integer('multiple_columns');
            $table->string('course_content');
            $table->integer('course_weekly_outline');
            $table->string('course_assessment');
            $table->string('course_genre');
            $table->string('course_level');
            $table->string('course_learning_outcomes');
            $table->string('effective_term_code', 7);
            $table->foreign('effective_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->string('end_term_code', 7);
            $table->foreign('end_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->date('system_date');
            $table->integer('sequence')->nullable();
            $table->float('credit_hours', 10, 0);
            $table->float('contact_hours', 10, 0);
            $table->float('lecture_hours', 10, 0);
            $table->float('lab_hours', 10, 0);
            $table->float('other_hours', 10, 0);
            $table->float('faculty_load_hours', 10, 0);
            $table->string('this_course_replace', 7);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
