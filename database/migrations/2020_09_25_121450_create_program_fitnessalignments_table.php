<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramFitnessalignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_fitnessalignments', function (Blueprint $table) {
            $table->string('pfs_code',15)->primary();
            $table->text('college_and_program_mission')->nullable();
            $table->text('college_and_program_organization')->nullable();
            $table->text('college_and_program_governance')->nullable();
            $table->text('college_and_program_quality')->nullable();
            $table->text('program_rational')->nullable();
            $table->text('program_alignment_with_university')->nullable();
            $table->text('program_alignment_with_quality')->nullable();
            $table->text('program_accreditating')->nullable();
            $table->text('external_reviewes')->nullable();
            $table->text('market_analysis')->nullable();
            $table->text('benchmarking')->nullable();
            $table->text('feasibility_study')->nullable();
            $table->text('impact_on_other_program')->nullable();
            $table->text('other_pertinant_information')->nullable();
            $table->string('program_code', 10)->nullable();
            $table->foreign('program_code')->references('program_code')->on('academic_programs')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_fitnessalignments');
    }
}
