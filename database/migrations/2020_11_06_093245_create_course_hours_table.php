<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_hours', function (Blueprint $table) {
            $table->string('course_code', 20);
            $table->foreign('course_code')->references('course_code')->on('courses')->onDelete('cascade');
            $table->float('credit_hours', 10, 0);
            $table->float('contact_hours', 10, 0);
            $table->float('lecture_hours', 10, 0);
            $table->float('lab_hours', 10, 0);
            $table->float('other_hours', 10, 0);
            $table->float('faculty_load_hours', 10, 0);
            $table->string('effective_term_code', 7);
            $table->foreign('effective_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->string('end_term_code', 7);
            $table->foreign('end_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->enum('status',['Draft','Active', 'Inactive', 'Pending', 'Approve', 'Hold']);
            $table->date('system_date');
            $table->integer('sequence')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_hours');
    }
}
