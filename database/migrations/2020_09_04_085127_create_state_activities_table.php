<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('state_activities', function (Blueprint $table) {
            $table->string('state_id', 10);
            $table->foreign('state_id')->references('state_id')->on('states')->onDelete('cascade');
            $table->string('activity_id', 10);
            $table->foreign('activity_id')->references('activity_id')->on('activities')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('state_activities');
    }
}
