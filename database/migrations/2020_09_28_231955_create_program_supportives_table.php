<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramSupportivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_supportives', function (Blueprint $table) {
            $table->string('par_code', 20)->primary();
            $table->text('safety_issue')->nullable();
            $table->text('it_requirement')->nullable();
            $table->text('other_concern')->nullable();
            $table->text('other_pertinent_information')->nullable();
            $table->string('program_code', 10)->nullable();
            $table->foreign('program_code')->references('program_code')->on('academic_programs')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_supportives');
    }
}
