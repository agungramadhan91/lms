<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->string('course_type_code_02', 10)->nullable();
            $table->foreign('course_type_code_02')->references('course_type_code')->on('course_types')->onDelete('cascade');
            $table->string('course_type_code_03', 10)->nullable();
            $table->foreign('course_type_code_03')->references('course_type_code')->on('course_types')->onDelete('cascade');
            $table->string('course_type_code_04', 10)->nullable();
            $table->foreign('course_type_code_04')->references('course_type_code')->on('course_types')->onDelete('cascade');
            $table->string('course_type_code_05', 10)->nullable();
            $table->foreign('course_type_code_05')->references('course_type_code')->on('course_types')->onDelete('cascade');
            $table->string('course_type_code_06', 10)->nullable();
            $table->foreign('course_type_code_06')->references('course_type_code')->on('course_types')->onDelete('cascade');
            $table->string('course_type_code_07', 10)->nullable();
            $table->foreign('course_type_code_07')->references('course_type_code')->on('course_types')->onDelete('cascade');
            $table->string('course_type_code_08', 10)->nullable();
            $table->foreign('course_type_code_08')->references('course_type_code')->on('course_types')->onDelete('cascade');
            $table->string('course_type_code_09', 10)->nullable();
            $table->foreign('course_type_code_09')->references('course_type_code')->on('course_types')->onDelete('cascade');
            $table->string('course_type_code_10', 10)->nullable();
            $table->foreign('course_type_code_10')->references('course_type_code')->on('course_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
