<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_assessments', function (Blueprint $table) {
            $table->string('course_assessment_code', 7)->primary();
            $table->string('course_code', 7);
            $table->foreign('course_code')->references('course_code')->on('courses')->onDelete('cascade');
            $table->string('course_assessment_type');
            $table->text('course_assessment_description');
            $table->string('effective_term_code', 7);
            $table->foreign('effective_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->string('end_term_code', 7);
            $table->foreign('end_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->date('system_date');
            $table->integer('sequence')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_assessments');
    }
}
