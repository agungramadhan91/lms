<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkflowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflows', function (Blueprint $table) {
            $table->string('workflow_code', 10)->primary();
            $table->integer('sequence')->nullable();
            // $table->string('workflow_approval_type_code', 7);
            // $table->foreign('workflow_approval_type_code')->references('workflow_approval_type_code')->on('workflow_approval_types')->onDelete('cascade');
            $table->string('role_code', 7);
            $table->foreign('role_code')->references('role_code')->on('roles')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workflows');
    }
}
