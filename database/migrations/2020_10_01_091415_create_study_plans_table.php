<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudyPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('study_plans', function (Blueprint $table) {
            $table->string('program_code', 7);
            $table->foreign('program_code')->references('program_code')->on('academic_programs')->onDelete('cascade');
            $table->string('semester_code', 7);
            $table->foreign('semester_code')->references('semester_code')->on('semesters')->onDelete('cascade');
            $table->string('course_code', 7);
            $table->foreign('course_code')->references('course_code')->on('courses')->onDelete('cascade');
            $table->integer('sequence')->default('1');
            $table->string('course_genre');
            $table->string('credit_hour');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('study_plans');
    }
}
