<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransitionActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transition_actions', function (Blueprint $table) {
            $table->string('transition_id', 10);
            $table->foreign('transition_id')->references('transition_id')->on('transitions')->onDelete('cascade');
            $table->string('activity_id', 10);
            $table->foreign('activity_id')->references('activity_id')->on('activities')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transition_actions');
    }
}
