<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCourseTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->string('semester_01', 10)->nullable();
            $table->foreign('semester_program_01')->references('semester_code')->on('semesters')->onDelete('cascade');
            $table->string('semester_program_02', 10)->nullable();
            $table->foreign('semester_program_02')->references('semester_code')->on('semesters')->onDelete('cascade');
            $table->string('semester_program_03', 10)->nullable();
            $table->foreign('semester_program_03')->references('semester_code')->on('semesters')->onDelete('cascade');
            $table->string('semester_program_04', 10)->nullable();
            $table->foreign('semester_program_04')->references('semester_code')->on('semesters')->onDelete('cascade');
            $table->string('semester_program_05', 10)->nullable();
            $table->foreign('semester_program_05')->references('semester_code')->on('semesters')->onDelete('cascade');
            $table->string('semester_program_06', 10)->nullable();
            $table->foreign('semester_program_06')->references('semester_code')->on('semesters')->onDelete('cascade');
            $table->string('semester_program_07', 10)->nullable();
            $table->foreign('semester_program_07')->references('semester_code')->on('semesters')->onDelete('cascade');
            $table->string('semester_program_08', 10)->nullable();
            $table->foreign('semester_program_08')->references('semester_code')->on('semesters')->onDelete('cascade');
            $table->string('semester_program_09', 10)->nullable();
            $table->foreign('semester_program_09')->references('semester_code')->on('semesters')->onDelete('cascade');
            $table->string('semester_program_10', 10)->nullable();
            $table->foreign('semester_program_10')->references('semester_code')->on('semesters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
