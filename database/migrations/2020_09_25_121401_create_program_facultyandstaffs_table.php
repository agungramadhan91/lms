<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramFacultyandstaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_facultyandstaffs', function (Blueprint $table) {
            $table->string('pfs_ode', 15)->primary();
            $table->text('faculty_student_ratio')->nullable();
            $table->text('faculty_recruitment_plan')->nullable();
            $table->text('staff_recruitment_plan')->nullable();
            $table->text('faculty_and_course_credntialing')->nullable();
            $table->text('other_pertinent_information')->nullable();
            $table->string('program_code', 10)->nullable();
            $table->foreign('program_code')->references('program_code')->on('academic_programs')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_facultyandstaffs');
    }
}
