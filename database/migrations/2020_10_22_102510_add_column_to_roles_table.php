<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->integer('sequence')->default(1);
            $table->integer('is_shown')->default(TRUE)->after('is_active');
        });
        
        Schema::table('workflows', function (Blueprint $table) {
            $table->string('workflow_code_description')->after('workflow_code');
            $table->string('role_description')->after('workflow_code_description');
            $table->integer('is_shown')->default(TRUE)->after('sequence');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            //
        });
    }
}
