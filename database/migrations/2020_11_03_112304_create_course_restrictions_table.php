<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseRestrictionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_restrictions', function (Blueprint $table) {
            $table->string('course_restrictions_code', 20)->primary();
            $table->string('course_code', 20);
            $table->foreign('course_code')->references('course_code')->on('courses')->onDelete('cascade');
            $table->string('college_code', 20);
            $table->foreign('college_code')->references('college_code')->on('colleges')->onDelete('cascade');
            $table->string('campus_code', 20);
            $table->foreign('campus_code')->references('campus_code')->on('campuses')->onDelete('cascade');
            $table->string('program_code', 20);
            $table->foreign('program_code')->references('program_code')->on('academic_programs')->onDelete('cascade');
            $table->string('effective_term_code', 7);
            $table->foreign('effective_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->string('end_term_code', 7);
            $table->foreign('end_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->enum('status',['Draft','Active', 'Inactive', 'Pending', 'Approve', 'Hold']);
            $table->date('system_date');
            $table->integer('sequence')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_restrictions');
    }
}
