<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumToProgramLearningOutcomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('program_learning_outcomes', function (Blueprint $table) {
            $table->integer('is_shown')->default(TRUE)->after('sequence');
            $table->enum('status', [
                'Draft','Active','Inactive','Pending','Approve','Hold'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('program_learning_outcomes', function (Blueprint $table) {
            //
        });
    }
}
