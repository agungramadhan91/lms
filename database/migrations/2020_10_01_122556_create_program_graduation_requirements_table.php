<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramGraduationRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_graduation_requirements', function (Blueprint $table) {
            $table->string('graduation_code',20)->primary();
            $table->text('description');
            $table->string('effective_term_code', 10);
            $table->foreign('effective_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->string('end_term_code', 10);
            $table->foreign('end_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->integer('sequence')->default('1');
            $table->enum('status',['Draft','Active', 'Inactive', 'Pending', 'Approval', 'Hold']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_graduation_requirements');
    }
}
