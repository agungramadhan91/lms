<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramAdmissionRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_admission_requirements', function (Blueprint $table) {
            $table->string('par_code', 7)->primary();
            $table->string('program_code', 7);
            $table->foreign('program_code')->references('program_code')->on('academic_programs')->onDelete('cascade');
            $table->text('par_description');
            $table->string('effective_term_code', 7);
            $table->foreign('effective_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->string('end_term_code', 7);
            $table->foreign('end_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->date('system_date');
            $table->integer('sequence')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_admission_requirements');
    }
}
