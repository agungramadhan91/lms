<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnOnUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table)
        {
            $table->string('firstname')->nullable()->after('name');
            $table->string('lastname')->nullable()->after('firstname');
            $table->string('college', 10)->nullable()->after('lastname');
            $table->string('department', 10)->nullable()->after('college');
            // $table->dropColumn('role_code');

            $table->foreign('college')->references('college_code')->on('colleges')->onDelete('cascade');
            $table->foreign('department')->references('department_code')->on('departments')->onDelete('cascade');
            // $table->foreign('role')->references('role_code')->on('roles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
