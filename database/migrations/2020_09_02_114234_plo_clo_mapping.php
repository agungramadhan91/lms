<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PloCloMapping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plo_clo_mappings', function (Blueprint $table) {
            $table->string('mapping_code', 7)->primary();
            $table->string('course_code', 7);
            $table->foreign('course_code')->references('course_code')->on('courses')->onDelete('cascade');
            $table->string('clo_code', 15);
            $table->foreign('clo_code')->references('clo_code')->on('course_learning_outcomes')->onDelete('cascade');
            $table->string('program_code', 7);
            $table->foreign('program_code')->references('program_code')->on('academic_programs')->onDelete('cascade');
            $table->string('plo_code', 7);
            $table->foreign('plo_code')->references('plo_code')->on('program_learning_outcomes')->onDelete('cascade');
            $table->string('effective_term_code', 7);
            $table->foreign('effective_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->string('end_term_code', 7);
            $table->foreign('end_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->date('system_date');
            $table->integer('sequence')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
