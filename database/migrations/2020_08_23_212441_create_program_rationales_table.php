<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramRationalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_rationales', function (Blueprint $table) {
            $table->string('pr_code', 7)->primary();
            $table->string('college_code', 7);
            $table->foreign('college_code')->references('college_code')->on('colleges')->onDelete('cascade');
            $table->string('title', 100);
            $table->string('effective_term_code', 7);
            $table->foreign('effective_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->string('end_term_code', 7);
            $table->foreign('end_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->date('system_date');
            $table->integer('sequence')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_rationales');
    }
}
