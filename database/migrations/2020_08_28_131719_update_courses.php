<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->string('program_code_02', 10)->nullable();
            $table->foreign('program_code_02')->references('program_code')->on('academic_programs')->onDelete('cascade');
            $table->string('program_code_03', 10)->nullable();
            $table->foreign('program_code_03')->references('program_code')->on('academic_programs')->onDelete('cascade');
            $table->string('program_code_04', 10)->nullable();
            $table->foreign('program_code_04')->references('program_code')->on('academic_programs')->onDelete('cascade');
            $table->string('program_code_05', 10)->nullable();
            $table->foreign('program_code_05')->references('program_code')->on('academic_programs')->onDelete('cascade');
            $table->string('program_code_06', 10)->nullable();
            $table->foreign('program_code_06')->references('program_code')->on('academic_programs')->onDelete('cascade');
            $table->string('program_code_07', 10)->nullable();
            $table->foreign('program_code_07')->references('program_code')->on('academic_programs')->onDelete('cascade');
            $table->string('program_code_08', 10)->nullable();
            $table->foreign('program_code_08')->references('program_code')->on('academic_programs')->onDelete('cascade');
            $table->string('program_code_09', 10)->nullable();
            $table->foreign('program_code_09')->references('program_code')->on('academic_programs')->onDelete('cascade');
            $table->string('program_code_10', 10)->nullable();
            $table->foreign('program_code_10')->references('program_code')->on('academic_programs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
