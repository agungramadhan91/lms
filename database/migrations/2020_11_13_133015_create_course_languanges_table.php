<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseLanguangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_languanges', function (Blueprint $table) {
            $table->string('course_code', 20);
            $table->foreign('course_code')->references('course_code')->on('courses')->onDelete('cascade');
            $table->string('instructional_languages_code',20)->primary();
            $table->foreign('instructional_languages_code')->references('instructional_languages_code')->on('instructional_languages')->onDelete('cascade');
            $table->string('effective_term_code', 10);
            $table->foreign('effective_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->string('end_term_code', 10);
            $table->foreign('end_term_code')->references('term_code')->on('terms')->onDelete('cascade');
            $table->integer('sequence')->default('1');
            $table->enum('status',['Draft','Active', 'Inactive', 'Pending', 'Approval', 'Hold']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_languanges');
    }
}
