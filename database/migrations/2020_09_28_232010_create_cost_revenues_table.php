<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCostRevenuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cost_revenues', function (Blueprint $table) {
            $table->string('pcr_code', 20)->primary();
            $table->text('expect_cost')->nullable();
            $table->text('expect_revenue')->nullable();
            $table->text('other_pertinent_information')->nullable();
            $table->string('program_code', 10)->nullable();
            $table->foreign('program_code')->references('program_code')->on('academic_programs')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cost_revenues');
    }
}
