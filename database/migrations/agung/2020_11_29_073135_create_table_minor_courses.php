<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMinorCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('minor_courses', function (Blueprint $table) {
            $table->string('minor_code', 15);

            $table->foreign('minor_code')
              ->references('minor_code')
              ->on('minors')->onDelete('cascade');

            $table->string('course_code', 15);

            $table->foreign('course_code')
              ->references('course_code')
              ->on('courses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_minor_courses');
    }
}
