<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCoursesss extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->enum('status',['Active', 'Inactive', 'Pending', 'Approval', 'Hold']);
            $table->string('policy')->nullable();
            $table->string('honor')->nullable();
            $table->string('attendance')->nullable();
            $table->string('disability')->nullable();
            $table->string('pedagon')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
