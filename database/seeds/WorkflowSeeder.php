<?php

use App\workflow;
use Illuminate\Database\Seeder;

class WorkflowSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        workflow::create(["workflow_code"=>"CMNS01", "workflow_code_description"=>"Changing the course level (100/200, 300/400, 500/600) - -  will impact programs", "sequence"=>1]);
        workflow::create(["workflow_code"=>"CMNS02", "workflow_code_description"=>"Changing the course title (no content or focus change)  - -  will impact programs", "sequence"=>1]);
        workflow::create(["workflow_code"=>"CMNS03", "workflow_code_description"=>"Changing course assessment methods", "sequence"=>1]);
        workflow::create(["workflow_code"=>"CMNS04", "workflow_code_description"=>"Changing course instructional delivery mode", "sequence"=>1]);
        workflow::create(["workflow_code"=>"CMNS05", "workflow_code_description"=>"Changing grading mode", "sequence"=>1]);
        workflow::create(["workflow_code"=>"CMNS06", "workflow_code_description"=>"Change in course type (Lab, Lecture, Internship, Project, Independent Study, Advising)", "sequence"=>1]);
        workflow::create(["workflow_code"=>"CMNS07", "workflow_code_description"=>"Changing the assigned textbook/reading", "sequence"=>1]);
        workflow::create(["workflow_code"=>"CMNS09", "workflow_code_description"=>"Change in registration restrictions", "sequence"=>1]);
        workflow::create(["workflow_code"=>"CMNS10", "workflow_code_description"=>"Changing a course prerequisite  - -  will impact programs", "sequence"=>1]);
        workflow::create(["workflow_code"=>"CMNS11", "workflow_code_description"=>"Change a course co-requisite  - -  will impact programs", "sequence"=>1]);
        workflow::create(["workflow_code"=>"CMSB01", "workflow_code_description"=>"Creating a new course - -  will impact programs", "sequence"=>1]);
        workflow::create(["workflow_code"=>"CMSB02", "workflow_code_description"=>"Discontinuing an existing course - -  will impact programs", "sequence"=>1]);
        workflow::create(["workflow_code"=>"CMSB03", "workflow_code_description"=>"Making a major change in a course description", "sequence"=>1]);
        workflow::create(["workflow_code"=>"CMSB04", "workflow_code_description"=>"Making a major change in a learning outcome", "sequence"=>1]);
        workflow::create(["workflow_code"=>"CMSB05", "workflow_code_description"=>"Changing course credit hours - -  will impact programs", "sequence"=>1]);
        workflow::create(["workflow_code"=>"CMSB06", "workflow_code_description"=>"Changing a course pre/co requisite if one of the courses is offered by another College - -  will impact programs", "sequence"=>1]);
        workflow::create(["workflow_code"=>"PRNS01", "workflow_code_description"=>"Moving course(s) between compulsory and elective status without changing the number of credit hours", "sequence"=>1]);
        workflow::create(["workflow_code"=>"PRNS02", "workflow_code_description"=>"Replacement of an existing course with another", "sequence"=>1]);
        workflow::create(["workflow_code"=>"PRNS03", "workflow_code_description"=>"Adding an existing course to the electives list", "sequence"=>1]);
        workflow::create(["workflow_code"=>"PRNS04", "workflow_code_description"=>"Minor changes to program learning outcomes", "sequence"=>1]);
        workflow::create(["workflow_code"=>"PRSB01", "workflow_code_description"=>"Establishing a new degree program (Done)", "sequence"=>1]);
        workflow::create(["workflow_code"=>"PRSB02", "workflow_code_description"=>"Discontinuing an existing degree program", "sequence"=>1]);
        workflow::create(["workflow_code"=>"PRSB03", "workflow_code_description"=>"Changing details of a degree program", "sequence"=>1]);
        workflow::create(["workflow_code"=>"PRSB04", "workflow_code_description"=>"Changing the title of an academic degree", "sequence"=>1]);
        workflow::create(["workflow_code"=>"PRSB05", "workflow_code_description"=>"Major changes to program learning outcomes", "sequence"=>1]);
        workflow::create(["workflow_code"=>"PRSB06", "workflow_code_description"=>"Change to a program's total number of credit hours or the relative distribution of credit hours between compulsory and elective courses;", "sequence"=>1]);
        workflow::create(["workflow_code"=>"PRSB07", "workflow_code_description"=>"Adding a new concentration", "sequence"=>1]);
        workflow::create(["workflow_code"=>"PRSB08", "workflow_code_description"=>"Changing concentration hours or course structure", "sequence"=>1]);
        workflow::create(["workflow_code"=>"PRSB09", "workflow_code_description"=>"Discontinuing an existing concentration", "sequence"=>1]);
        workflow::create(["workflow_code"=>"PRSB10", "workflow_code_description"=>"Change to the primary language of instruction in a degree program", "sequence"=>1]);
        workflow::create(["workflow_code"=>"PRSB11", "workflow_code_description"=>"Changing program instructional delivery mode", "sequence"=>1]);
        workflow::create(["workflow_code"=>"PRSB12", "workflow_code_description"=>"Change or addition of program offering location", "sequence"=>1]);
        workflow::create(["workflow_code"=>"PRSB13", "workflow_code_description"=>"Replacement of more than three courses", "sequence"=>1]);

    }
}
