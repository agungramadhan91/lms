<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CampusProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $programs   = DB::table('academic_programs')
                    ->select('program_code')
                    ->groupBy('program_code')
                    ->get();

        $campuses   = DB::table('campuses')
                    ->select('campus_code')
                    ->groupBy('campus_code')
                    ->get();

        foreach ($programs as $data) {
            $programs2 = DB::table('academic_programs')
                    ->where('program_code', $data->program_code)
                    ->first();

            foreach ($campuses as $data2) {
                if ($programs2->campus_code == $data2->campus_code) {
                    DB::table('campus_program')->insert([
                        'program_code'  => $data->program_code,
                        'campus_code'   => $data2->campus_code
                    ]);
                }
            }
        }
    }
}
