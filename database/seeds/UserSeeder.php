<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(['id'=>'Admin','name'=>'admin2','firstname'=>'System','lastname'=>'Admin', 'college'=>'ALL','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'Faculty_CB','name'=>'Faculty CB','firstname'=>'Faculty','lastname'=>'CB', 'college'=>'CB','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'FACULTY_TI','name'=>'FACULTY TI','firstname'=>'Faculty','lastname'=>'TI', 'college'=>'TI','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'FACULTY_CA','name'=>'FACULTY CA','firstname'=>'Faculty','lastname'=>'CA', 'college'=>'CA','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'FACULTY_CM','name'=>'FACULTY CM','firstname'=>'Faculty','lastname'=>'CM', 'college'=>'CM','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'FACULTY_UC','name'=>'FACULTY UC','firstname'=>'Faculty','lastname'=>'UC', 'college'=>'UC','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'FACULTY_HS','name'=>'FACULTY HS','firstname'=>'Faculty','lastname'=>'HS', 'college'=>'HS','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'FACULTY_NH','name'=>'FACULTY NH','firstname'=>'Faculty','lastname'=>'NH', 'college'=>'NH','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'FACULTY_ED','name'=>'FACULTY ED','firstname'=>'Faculty','lastname'=>'ED', 'college'=>'ED','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'PROGCORD_CB','name'=>'PROGCORD CB','firstname'=>'PROGRAM COORDINATOR','lastname'=>'CB', 'college'=>'CB','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'PROGCORD_TI','name'=>'PROGCORD TI','firstname'=>'PROGRAM COORDINATOR','lastname'=>'TI', 'college'=>'TI','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'PROGCORD_CA','name'=>'PROGCORD CA','firstname'=>'PROGRAM COORDINATOR','lastname'=>'CA', 'college'=>'CA','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'PROGCORD_CM','name'=>'PROGCORD CM','firstname'=>'PROGRAM COORDINATOR','lastname'=>'CM', 'college'=>'CM','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'PROGCORD_UC','name'=>'PROGCORD UC','firstname'=>'PROGRAM COORDINATOR','lastname'=>'UC', 'college'=>'UC','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'PROGCORD_HS','name'=>'PROGCORD HS','firstname'=>'PROGRAM COORDINATOR','lastname'=>'HS', 'college'=>'HS','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'PROGCORD_NH','name'=>'PROGCORD NH','firstname'=>'PROGRAM COORDINATOR','lastname'=>'NH', 'college'=>'NH','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'PROGCORD_ED','name'=>'PROGCORD ED','firstname'=>'PROGRAM COORDINATOR','lastname'=>'ED', 'college'=>'ED','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'DPTCHAIR_CB','name'=>'DPTCHAIR CB','firstname'=>'DEPARTMENT CHAIR','lastname'=>'CB', 'college'=>'CB','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'DPTCHAIR_TI','name'=>'DPTCHAIR TI','firstname'=>'DEPARTMENT CHAIR','lastname'=>'TI', 'college'=>'TI','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'DPTCHAIR_CA','name'=>'DPTCHAIR CA','firstname'=>'DEPARTMENT CHAIR','lastname'=>'CA', 'college'=>'CA','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'DPTCHAIR_CM','name'=>'DPTCHAIR CM','firstname'=>'DEPARTMENT CHAIR','lastname'=>'CM', 'college'=>'CM','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'DPTCHAIR_UC','name'=>'DPTCHAIR UC','firstname'=>'DEPARTMENT CHAIR','lastname'=>'UC', 'college'=>'UC','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'DPTCHAIR_HS','name'=>'DPTCHAIR HS','firstname'=>'DEPARTMENT CHAIR','lastname'=>'HS', 'college'=>'HS','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'DPTCHAIR_NH','name'=>'DPTCHAIR NH','firstname'=>'DEPARTMENT CHAIR','lastname'=>'NH', 'college'=>'NH','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'DPTCHAIR_ED','name'=>'DPTCHAIR ED','firstname'=>'DEPARTMENT CHAIR','lastname'=>'ED', 'college'=>'ED','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'CCURCOMT_CB','name'=>'CCURCOMT CB','firstname'=>'COLLEGE CURRICULUM COMMITTEE','lastname'=>'CB', 'college'=>'CB','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'CCURCOMT_TI','name'=>'CCURCOMT TI','firstname'=>'COLLEGE CURRICULUM COMMITTEE','lastname'=>'TI', 'college'=>'TI','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'CCURCOMT_CA','name'=>'CCURCOMT CA','firstname'=>'COLLEGE CURRICULUM COMMITTEE','lastname'=>'CA', 'college'=>'CA','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'CCURCOMT_CM','name'=>'CCURCOMT CM','firstname'=>'COLLEGE CURRICULUM COMMITTEE','lastname'=>'CM', 'college'=>'CM','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'CCURCOMT_UC','name'=>'CCURCOMT UC','firstname'=>'COLLEGE CURRICULUM COMMITTEE','lastname'=>'UC', 'college'=>'UC','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'CCURCOMT_HS','name'=>'CCURCOMT HS','firstname'=>'COLLEGE CURRICULUM COMMITTEE','lastname'=>'HS', 'college'=>'HS','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'CCURCOMT_NH','name'=>'CCURCOMT NH','firstname'=>'COLLEGE CURRICULUM COMMITTEE','lastname'=>'NH', 'college'=>'NH','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'CCURCOMT_ED','name'=>'CCURCOMT ED','firstname'=>'COLLEGE CURRICULUM COMMITTEE','lastname'=>'ED', 'college'=>'ED','department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'DEAN_CB','name'=>'DEAN CB','firstname'=>'DEAN','lastname'=>'CB', 'college'=>NUll,'department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'DEAN_TI','name'=>'DEAN TI','firstname'=>'DEAN','lastname'=>'TI', 'college'=>NUll,'department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'DEAN_CA','name'=>'DEAN CA','firstname'=>'DEAN','lastname'=>'CA', 'college'=>NUll,'department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'DEAN_CM','name'=>'DEAN CM','firstname'=>'DEAN','lastname'=>'CM', 'college'=>NUll,'department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'DEAN_UC','name'=>'DEAN UC','firstname'=>'DEAN','lastname'=>'UC', 'college'=>NUll,'department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'DEAN_HS','name'=>'DEAN HS','firstname'=>'DEAN','lastname'=>'HS', 'college'=>NUll,'department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'DEAN_NH','name'=>'DEAN NH','firstname'=>'DEAN','lastname'=>'NH', 'college'=>NUll,'department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'DEAN_ED','name'=>'DEAN ED','firstname'=>'DEAN','lastname'=>'ED', 'college'=>NUll,'department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'UCURCOMIT	','name'=>'UCURCOMIT','firstname'=>'UNIVERSITY CURRICULUM','lastname'=>'COMMITTEE', 'college'=>NULL,'department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'OFFIPROV	','name'=>'OFFIPROV','firstname'=>'OFFICE OF','lastname'=>'PROVOST', 'college'=>NULL,'department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'ACADCOUNCIL','name'=>'ACADCOUNCIL','firstname'=>'ACADEMIC','lastname'=>'COUNCIL', 'college'=>NULL,'department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'PROVCOUNCIL','name'=>'PROVCOUNCIL','firstname'=>'PROV','lastname'=>'COUNCIL', 'college'=>NULL,'department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
        User::create(['id'=>'UNIVCOUNCIL','name'=>'UNIVCOUNCIL','firstname'=>'UNIVERSITY','lastname'=>'COUNCIL', 'college'=>NULL,'department'=>NULL,'is_active'=>TRUE,'password'=>bcrypt('eduval2020')]);
                                

    }
}
