<?php

use App\role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        role::create(['role_code'=>'FACULTY', 'role_level_code'=>'C', 'role_description'=>'Faculty Member', 'is_approver'=>3, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>1, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:18:42', 'updated_at'=>'2020-10-23 04:45:30']);
        role::create(['role_code'=>'CRSCORD', 'role_level_code'=>'C', 'role_description'=>'Course Coordinator', 'is_approver'=>3, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>1, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:27:41', 'updated_at'=>'2020-10-25 14:23:41']);
        role::create(['role_code'=>'PROCORD', 'role_level_code'=>'C', 'role_description'=>'Program Coordinator', 'is_approver'=>3, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>1, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:47:39', 'updated_at'=>'2020-10-23 04:47:39']);
        role::create(['role_code'=>'DEPTCHR', 'role_level_code'=>'C', 'role_description'=>'Department Chair', 'is_approver'=>1, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>1, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:48:55', 'updated_at'=>'2020-10-23 04:48:55']);
        role::create(['role_code'=>'CCURCOM', 'role_level_code'=>'C', 'role_description'=>'College Curriculum Committe', 'is_approver'=> 1, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>3, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:49:44', 'updated_at'=>'2020-10-25 14:08:36']);
        role::create(['role_code'=>'COLDEAN', 'role_level_code'=>'C', 'role_description'=>'College Dean', 'is_approver'=>1, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>1, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:50:07', 'updated_at'=>'2020-10-23 04:50:07']);
        role::create(['role_code'=>'OFFPROV', 'role_level_code'=>'U', 'role_description'=>'Office of Provost', 'is_approver'=>1, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>1, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:51:12', 'updated_at'=>'2020-10-23 04:51:12']);
        role::create(['role_code'=>'UCURCOM', 'role_level_code'=>'U', 'role_description'=>'University Curriculum Committe','is_approver'=> 1, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>1, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:51:51', 'updated_at'=>'2020-10-23 04:51:51']);
        role::create(['role_code'=>'ACACNSL', 'role_level_code'=>'U', 'role_description'=>'Academic Council', 'is_approver'=>1, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>1, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:52:38', 'updated_at'=>'2020-10-23 04:52:38']);
        role::create(['role_code'=>'PROVCNCL', 'role_level_code'=>'U', 'role_description'=>'Provost Council', 'is_approver'=>2, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>1, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:53:10', 'updated_at'=>'2020-10-23 04:53:10']);
        role::create(['role_code'=>'UNICNCL', 'role_level_code'=>'U', 'role_description'=>'University Council', 'is_approver'=>0, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>1, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:53:40', 'updated_at'=>'2020-10-23 19:00:28']);
        role::create(['role_code'=>'VICEPRES', 'role_level_code'=>'U', 'role_description'=>'Vice President', 'is_approver'=>0, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>1, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:53:40', 'updated_at'=>'2020-10-23 19:00:28']);
        role::create(['role_code'=>'PRSDENT', 'role_level_code'=>'U', 'role_description'=>'President', 'is_approver'=>0, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>1, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:53:40', 'updated_at'=>'2020-10-23 19:00:28']);
        role::create(['role_code'=>'EXTACC1', 'role_level_code'=>'U', 'role_description'=>'External Accreditation Body - 1', 'is_approver'=>0, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>2, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:53:40', 'updated_at'=>'2020-10-23 19:00:28']);
        role::create(['role_code'=>'EXTACC2', 'role_level_code'=>'U', 'role_description'=>'External Accreditation Body - 2', 'is_approver'=>0, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>2, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:53:40', 'updated_at'=>'2020-10-23 19:00:28']);
        role::create(['role_code'=>'IMPTEM1', 'role_level_code'=>'U', 'role_description'=>'Implementation Team (SIS)', 'is_approver'=>0, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>2, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:53:40', 'updated_at'=>'2020-10-23 19:00:28']);
        role::create(['role_code'=>'IMPTEM2', 'role_level_code'=>'U', 'role_description'=>'Implementation Team (LMS)', 'is_approver'=>0, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>2, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:53:40', 'updated_at'=>'2020-10-23 19:00:28']);
        role::create(['role_code'=>'IMPTEM3', 'role_level_code'=>'U', 'role_description'=>'Implementation Team (ADV)', 'is_approver'=>0, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>2, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:53:40', 'updated_at'=>'2020-10-23 19:00:28']);
        role::create(['role_code'=>'IMPTEM4', 'role_level_code'=>'U', 'role_description'=>'Implementation Team - Other 1', 'is_approver'=>0, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>2, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:53:40', 'updated_at'=>'2020-10-23 19:00:28']);
        role::create(['role_code'=>'IMPTEM5', 'role_level_code'=>'U', 'role_description'=>'Implementation Team - Other 2', 'is_approver'=>0, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>2, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:53:40', 'updated_at'=>'2020-10-23 19:00:28']);
        // role::create(['role_code'=>'IMPTEM1', 'role_level_code'=>'I', 'role_description'=>'Implementation Team 1', 'is_approver'=>1, 'is_active'=>'true', 'is_shown'=>1, 'sequence'=>1, 'department_code'=>'empty', 'created_at'=>'2020-10-23 04:54:53', 'updated_at'=>'2020-10-23 04:54:53']);
    }
}
