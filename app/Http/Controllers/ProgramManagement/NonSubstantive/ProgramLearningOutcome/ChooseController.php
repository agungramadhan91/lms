<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\ProgramLearningOutcome;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ChooseController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        Session::forget('program_code');
        Session::forget('plo_code');
        Session::forget('plo_description');
        Session::forget('plo_term_ef');
        Session::forget('plo_term_en');

        $plos = DB::table('program_learning_outcomes')
                ->select('program_code')
                ->where('status','Active')
                ->groupBy('program_code');
                // ->get();

        $programs   = DB::table('academic_programs as ap')
                    ->select('ap.program_code','ap.title')
                    ->where('ap.status','Active')
                    ->joinSub($plos, 'plo', function($join){
                        $join->on('ap.program_code','=','plo.program_code');
                    })
                    ->get();
                    
        return view('program.non-substantive.program_learning_outcomes.choose', compact('programs')); 
    }
}
