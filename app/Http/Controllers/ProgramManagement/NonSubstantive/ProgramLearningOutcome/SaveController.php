<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\ProgramLearningOutcome;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SaveController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function outcome(Request $request)
    {
        $program_code   = Session::get('program_code');
        $code           = $request->code;
        $descriptions   = $request->description;
        $ef_tc          = $request->effective_term;
        $en_tc          = $request->end_term;
        $collect_code   = array();
        $collect_desc   = array();
        $collect_term   = array();
        $collect_out    = array();

        if($en_tc == $ef_tc){
            return redirect()->back()->with('error','End Term must be greater than Effective Term');
        }

        // return dd($code, $collect_out, $collect_desc);
        for ($c=0; $c < count($code); $c++) { 
            if($code[$c] != null)
            {
                array_push($collect_code, $code[$c]);
            }
        }
        
        for ($i=0; $i < count($descriptions); $i++) 
        { 
            $exist_outcomes  = DB::table('program_learning_outcomes')
                            ->where('plo_code', $collect_code[$i])
                            ->where('status','Active')
                            ->orWhere('status','Approve')
                            ->first();

            $sub = substr($exist_outcomes->plo_description, -1);
            // array_push($collect_out, $exist_outcomes->plo_description);
            // array_push($collect_desc, $descriptions[$i]);
            
            if($sub == ' ')
            {
                if($exist_outcomes->plo_description != $descriptions[$i].' ')
                {
                    array_push($collect_desc, $descriptions[$i]);
                }
            }else{

                if($exist_outcomes->plo_description != $descriptions[$i])
                {
                    array_push($collect_desc, $descriptions[$i]);
                }
            }
        }
        // return dd($collect_code,$collect_desc,$collect_out,$en_tc);
        
        if (count($collect_desc) < 1) 
        {
            return redirect()->back()->with('error','Please update the description first!');
        }

        Session::put('program_code', $program_code);
        Session::put('plo_code', $collect_code);
        Session::put('plo_description', $collect_desc);
        Session::put('plo_term_ef', $ef_tc);
        Session::put('plo_term_en', $en_tc);
        
        return  redirect()
                // ->route('program.non-substantive.program_learning_outcome.main')
                ->back()
                ->with('success','Saved Successfully');
    }

    public function main(Request $request)
    {
        $ef_tc = $request->effective_term;
        $en_tc = $request->end_term;

        if($ef_tc == '' || $en_tc == ''){
            return redirect()->back()->with('error','Please update the terms first!');
        }

        if($ef_tc == $en_tc){
            return back()->with('error','Effective term can not be same with end term');
        }

        $concat_term = $ef_tc.'-'.$en_tc;

        Session::put('plo_term_ef', $ef_tc);
        Session::put('plo_term_en', $en_tc);
        // return dd($concat_term);
        return  redirect()
                ->route('program.non-substantive.program_learning_outcome.main')
                ->with('success','Saved Successfully');
    }
}
