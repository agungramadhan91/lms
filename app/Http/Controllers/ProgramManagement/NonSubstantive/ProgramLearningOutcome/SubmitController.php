<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\ProgramLearningOutcome;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SubmitController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // $program_code   = Session::get('program_code');
        // $code           = Session::get('code');
        // $descriptions   = Session::get('changed_description');
        $click_btn      = $request->click_btn;
        $ef_tc          = $request->effective_term;
        $en_tc          = $request->end_term;
        $collect_erro   = array();
        $collect_code   = array();
        $collect_desc   = array();
        $collect_term   = array();

        $program_code   = Session::get('program_code');
        $plo_code       = Session::get('plo_code');
        $plo_description= Session::get('plo_description');
        $plo_term_ef    = Session::get('plo_term_ef');
        $plo_term_en    = Session::get('plo_term_en');
        // return dd($plo_code, $plo_description, $plo_term_ef, $plo_term_en);
        // if ($descriptions == NULL) {
        //     return redirect()->route('home')->with('error','No Terms Updated!');
        // }
                // dd($plo_term_ef." - ".$plo_term_en);
        

        for ($i=0; $i < count($plo_description); $i++) 
        { 
            $exist_outcomes = DB::table('program_learning_outcomes')
                            ->where('plo_code', $plo_code[$i])
                            ->where('status','Active')
                            ->first();
            
            if($exist_outcomes->plo_description != $plo_description[$i])
            {
                // array_push($collect_code, $plo_code[$i]);
                // array_push($collect_desc, $plo_description[$i]);
                // array_push($collect_term, [$plo_term_ef, $plo_term_en]);
                DB::table('program_learning_outcomes')->insert([
                    'plo_code'          => $plo_code[$i],
                    'program_code'      => $program_code,
                    'plo_description'   => $plo_description[$i],
                    'sequence'          => $exist_outcomes->sequence + 1,
                    'status'            => 'Active',
                    'effective_term_code' => $plo_term_ef,
                    'end_term_code'     => $plo_term_en,
                    'system_date'       => $exist_outcomes->system_date
                ]);

                $update_outcomes = DB::table('program_learning_outcomes')
                                ->where('plo_code', $plo_code[$i])
                                ->where('sequence', $exist_outcomes->sequence)
                                ->update([
                                    'status'        => 'Inactive',
                                    'end_term_code' => $plo_term_ef,
                                ]);
                                // ->where('effective_term_code', $ef_tc)
            }
        }
        // return dd($collect_code, $collect_desc, $collect_term);

        return  redirect()
                ->route('home')
                ->with('success','Application submitted succesfully!');
        // if(count($descriptions) > 0){
        //     if ($request->submit[0] == 'main') {
        //     }else{
        //         return redirect()->route('home')->with('success','Updated Successfully');
        //     }
        // }else{
        //     if ($request->submit[0] == 'main') {
        //         return redirect()->back()->with('error','Please update the description first!');
        //     } else {
        //         return redirect()->back()->with('error','Please Complete All Required Fields !!');
        //     }
        // }
        // return redirect()->route('home')->with('success','The Terms updated succesfully');
    }
}
