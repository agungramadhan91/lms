<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\ProgramLearningOutcome;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class OutcomeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if(Session::has('plo_description')){
            $program_code   = Session::get('program_code');
            $plo_code       = Session::get('plo_code');
            $plo_description= Session::get('plo_description');
            $plo_term_ef    = Session::get('plo_term_ef');
            $plo_term_en    = Session::get('plo_term_en');
        }else{
            $program_code   = $request->program_code;
            $plo_code       = "";
            $plo_description= "";
            $plo_term_ef    = "";
            $plo_term_en    = "";
        }
        
        $outcomes   = DB::table('program_learning_outcomes')
                    ->where('program_code', $program_code)
                    ->where('status','Active')
                    ->orWhere('status', 'Approve')
                    ->get();
        $sgl_outcome = $outcomes->first();
        $terms      = DB::table('terms')
                    ->where('calender_year','>', Carbon::now()->format('Y'))
                    // ->where('calender_year','<', '2099')
                    ->get();
        // dd($terms);
        if($plo_description != ''){
            foreach ($outcomes as $oc) {
                for ($i = 0; $i < count($plo_description); $i++){
                    if($plo_code[$i] == $oc->plo_code){
                        $oc->changed = $plo_description[$i];
                    }else{
                        $oc->changed = '';
                    }
                }
            }
        }
        // return dd($plo_code, $plo_description);

        Session::put('program_code', $program_code);

        return view('program.non-substantive.program_learning_outcomes.outcome', compact(
            'outcomes','program_code','plo_code','plo_description','plo_term_ef','plo_term_en','terms'
        ));
    }
}
