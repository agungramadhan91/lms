<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\ProgramLearningOutcome;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class MainController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if(Session::has('plo_description') || Session::has('plo_terms')){
            $program_code   = Session::get('program_code');
            $plo_code       = Session::get('plo_code');
            $plo_description= Session::get('plo_description');
            $plo_term_ef    = Session::get('plo_term_ef');
            $plo_term_en    = Session::get('plo_term_en');
        }else{
            $program_code   = $request->program_code;
            $plo_code       = "";
            $plo_description= "";
            $plo_term_ef    = "";
            $plo_term_en    = "";
        }
        // return dd($plo_term_ef);
        if ($plo_description == '') {
            return redirect()->home()->with('error','Sorry for inconvineint');
        }

        $user           = Auth::user();
        $db_programs    = DB::table('academic_programs')->where('program_code', $program_code);
        $count_programs = $db_programs->count();
        $program        = $db_programs->first();
        
        $db_majors      = DB::table('majors')->where('program_code', $program->program_code);
        $count_majors   = $db_majors->count();
        
        $campuses       = DB::table('campuses')->get();
        $colleges       = DB::table('colleges')->get();
        $dtypes         = DB::table('degree_types')->get();
        $dlevels        = DB::table('degree_levels')->get();
        $departments    = DB::table('departments')->where('college_code',$program->college_code)->get();
        // $plos           = DB::table('program_learning_outcomes as plo')
        //                 ->select('plo.*')
        //                 ->leftJoin('program_learning_outcomes as plo2', function($join){
        //                     $join->on('plo.plo_code','=','plo2.plo_code')
        //                     ->whereRaw(DB::raw('plo.sequence > plo2.sequence'));
        //                 })
        //                 ->where('plo.program_code',$program->program_code)
        //                 ->where('plo.is_shown',TRUE)
        //                 ->first();
        $terms          = DB::table('terms')
                        ->where('academic_year', '>', Carbon::now()->format('Y'))
                        ->get();
// return dd($plos->first());
        if(Session::has('changed_description')){
            $count_desc = count(Session::get('changed_description'));
        }else{
            $count_desc = NULL;
        }

        // Session::forget('program_code', $code);

        return view('program.non-substantive.program_learning_outcomes.main', compact(
            'campuses','colleges','dtypes','dlevels','departments','terms','program',
            'count_majors','count_desc','program_code','plo_code','plo_description','plo_term_ef','plo_term_en'
            // 'college2','level2','type2','effterm','endterm','major',
            // 'major_name','outcome','assesment','faculty','fitness','student'
        ));
    }
}
