<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\ReplaceCourse;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SaveController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $code           = $request->program_code;
        $semester_prog  = $request->semester_program;
        $course_code    = $request->course_code;
        $old_cc         = $request->old_course_code;
        $course_type    = $request->course_type;
        $course_genre   = $request->course_genre;
        $ef_tc          = $request->effective_term;
        $en_tc          = $request->end_term;
        // dd($request);
        if($course_code == null){
            return redirect()->back()->with('error', 'No selected course code');
        }
        
        for ($i=0; $i < count($code); $i++) { 
            if(!isset($course_code[$i])){
                return redirect()->back()->with('error','Please Complete All Required Fields!');
            }

            for ($j=0; $j < count($code); $j++) { 
                if($i != $j){
                    if ($course_code[$j] == $course_code[$i]) {
                        return redirect()->back()->with('error', $code[$j].' & '.$code[$i].', Cannot use the same course code!');
                    }
                }
            }
        }

        Session::put('program_code', $code);
        Session::put('semester_prog', $semester_prog);
        Session::put('course_code', $course_code);
        Session::put('old_cc', $old_cc);
        Session::put('course_type', $course_type);
        Session::put('course_genre', $course_genre);
        Session::put('ef_tc', $ef_tc);
        Session::put('en_tc', $en_tc);

        return redirect()->back()->with('success','Saved successfully, please submit the application!');
    }
}
