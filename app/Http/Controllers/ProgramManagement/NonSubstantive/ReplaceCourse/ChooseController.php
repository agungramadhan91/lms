<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\ReplaceCourse;

use App\Helper\CourseHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ChooseController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        Session::forget('program_code');
        Session::forget('changes');
        Session::forget('ef_tc');
        Session::forget('en_tc');
        Session::forget('semester_prog');
        Session::forget('course_code');
        Session::forget('old_cc');
        Session::forget('course_type');
        Session::forget('course_genre');

        $courses = CourseHelper::unique();

        return view('program.non-substantive.replace_course.choose', compact('courses')); 
    }
}
