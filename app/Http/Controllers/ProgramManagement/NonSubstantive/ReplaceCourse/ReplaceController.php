<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\ReplaceCourse;

use App\Helper\TermHelper;
use App\Helper\CourseHelper;
use Illuminate\Http\Request;
use App\Helper\ProgramHelper;
use App\Helper\SemesterHelper;
use App\Helper\StudyPlanHelper;
use App\Helper\CourseTypeHelper;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ReplaceController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if(Session::has('program_code'))
        {
            $codes      = Session::get('program_code');
            $changes    = Session::get('course_code');
            $course     = Session::get('old_cc');
            $ef_tc      = Session::get('ef_tc');
            $en_tc      = Session::get('en_tc');
            $saved_gened= Session::get('saved_gened');
            $saved_elec = Session::get('saved_elec');
            $saved_conc = Session::get('saved_conc');
            $saved_core = Session::get('saved_core');
            $saved_int  = Session::get('saved_int');
        }else{
            $codes      = $request->program_code;
            $course     = $request->course_code;
            $changes    = null;
            $ef_tc      = null;
            $en_tc      = null;
            $saved_gened= 0;
            $saved_elec = 0;
            $saved_conc = 0;
            $saved_core = 0;
            $saved_int  = 0;
        }
// dd($course);
        if ($codes == null) {
            return redirect()->back()->with('error','Please choose any Program Code');
        }

        $terms          = TermHelper::unique();
        $program        = ProgramHelper::first($codes);
        $programs       = ProgramHelper::uniqueByCodes($codes);
        $semesters      = SemesterHelper::unique();
        $study_plans    = StudyPlanHelper::getByProgram($codes);
        $course_types   = CourseTypeHelper::unique();
        $courses        = CourseHelper::unique($codes);
        // $other_courses  = CourseHelper::uniqueByOtherProgram($codes);

        $order = 0;
        foreach ($programs as $data) {
            $data->order            = $order + 1;
            $data->request_course   = $course[$order];
            $data->changes          = $changes[$order];
            $data->ef_tc            = $ef_tc[$order];
            $data->en_tc            = $en_tc[$order];

            $data->course_list  = CourseHelper::uniqueByProgram($data->program_code);
            $data->course_other = CourseHelper::uniqueByOtherProgram($data->program_code);
            $data->total_gened  = CourseHelper::getCreditByProgram($data->program_code, 'GEN');
            $data->total_core   = CourseHelper::getCreditByProgram($data->program_code, 'Core');
            $data->total_conc   = CourseHelper::getCreditByProgram($data->program_code, 'CONC');
            $data->total_elec   = CourseHelper::getCreditByProgram($data->program_code, 'ELECT');
            $data->total_int    = CourseHelper::getCreditByProgram($data->program_code, 'INT');
            $data->grand_total  =   $data->total_gened +
                                    $data->total_core +
                                    $data->total_conc +
                                    $data->total_int +
                                    $data->total_elec;

            $order++;
        }
        // return dd(Session::get('saved_conc'));
        // return dd($programs);
        return view('program.non-substantive.replace_course.course',compact(
            'study_plans','course','courses','course_types','terms','coursesByPrograms','semesters',
            'programs','codes','changes','ef_tc','en_tc',
            'saved_gened','saved_elec','saved_conc','saved_core','saved_int'
        ));
    }
}
