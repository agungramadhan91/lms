<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\ReplaceCourse;

use App\course;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Session\Session;

class SubmitController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $program_code   = $request->session()->get('program_code');
        $ef_tc          = $request->session()->get('ef_tc');
        $en_tc          = $request->session()->get('en_tc');
        $semester_prog  = $request->session()->get('semester_prog');
        $course_code    = $request->session()->get('course_code');
        $old_cc         = $request->session()->get('old_cc');
        $course_type    = $request->session()->get('course_type');
        $course_genre   = $request->session()->get('course_genre');
        
        if ($course_code != null) {
            // return dd($course_type);
            for ($i=0; $i < count($course_code); $i++) 
            { 
                $code   = $program_code[$i];
                $db2    = DB::table('courses')
                        ->where('course_code', $course_code[$i]);
                $seq2   = $db2->max('sequence');
                $first  = $db2->where('sequence', $seq2)->first();

                $new = new course();
                $new->course_code               = $course_code[$i];
                $new->college_code              = $first->college_code;
                $new->grading_mode_code         = $first->grading_mode_code;
                $new->long_title                = $first->long_title;
                $new->short_title               = $first->short_title;
                $new->description               = $first->description;
                $new->course_prerequisites      = $first->course_prerequisites;
                $new->course_content            = $first->course_content;
                $new->course_weekly_outline     = $first->course_weekly_outline;
                $new->course_assessment         = $first->course_assessment;
                $new->course_learning_outcomes  = $first->course_learning_outcomes;
                $new->effective_term_code       = $ef_tc;
                $new->end_term_code             = $en_tc;
                $new->sequence                  = $first->sequence + 1;
                $new->credit_hours              = $first->credit_hours;
                $new->contact_hours             = $first->contact_hours;
                $new->lecture_hours             = $first->lecture_hours;
                $new->lab_hours                 = $first->lab_hours;
                $new->other_hours               = $first->other_hours;
                $new->faculty_load_hours        = $first->faculty_load_hours;
                $new->this_course_replace       = $first->this_course_replace;
                $new->subject_code              = $first->subject_code;

                for ($a = 1; $a < 10; $a++){
                    if ($first->{'program_code_0'.$a} == NULL){
                        $new->{'semester_program_0'.$a} =  $semester_prog[$i];
                        $new->{'program_code_0'.$a} =  $code;
                        $new->{'course_type_code_0'.$a} = $course_type[$i];
                        $new->{'course_genre_code_0'.$a} = $course_genre[$i];
                        break;
                    }else{
                        $new->{'semester_program_0'.$a} = $first->{'semester_program_0'.$a};
                        $new->{'program_code_0'.$a} =  $first->{'program_code_0'.$a};
                        $new->{'course_type_code_0'.$a} = $first->{'course_type_code_0'.$a};
                        $new->{'course_genre_code_0'.$a} = $first->{'course_genre_code_0'.$a};
                    }
                }

                $new->status                    = 'Active';
                $new->policy                    = $first->policy;
                $new->honor                     = $first->honor;
                $new->attendance                = $first->attendance;
                $new->disability                = $first->disability;
                $new->pedagogy                  = $first->pedagogy;
                $new->course_level_code         = $first->course_level_code;
                $new->role_level_code           = $first->role_level_code;
                $new->full_course_code          = $first->full_course_code;
                $new->created_by                = $first->created_by;
                $new->formula                   = $first->formula;
                $new->additional_text           = $first->additional_text;
                $new->created_at                = Carbon::now();
                $new->updated_at                = Carbon::now();
                $new->save();
                
                $course_update = $db2->update([
                    'status' => 'Inactive',
                    'end_term_code' => $ef_tc
                ]);

                //==========================================================================

                // $db = DB::table('study_plans')->where('course_code', $course_code[$i]);
                // $seq = $db->max('sequence');
                // $get = $db->where('sequence', $seq)->first();
                
                // if ($get != null) {
                //     $sp_insert = DB::table('study_plans')->insert([
                //         'program_code' => $get->program_code,
                //         'semester_code' => $get->semester_code,
                //         'course_code' => $course_code[$i],
                //         'sequence' => $get->sequence + 1,
                //         'course_genre' => $get->course_genre,
                //         'course_type' => $course_type[$i],
                //         'credit_hour' => $get->credit_hour,
                //         'status' => 'Active',
                //         'effective_term_code' => $ef_tc,
                //         'end_term_code' => $en_tc,
                //     ]);
    
                //     $sp_update = $db->update([
                //         'status' => 'Inactive',
                //         'end_term_code' => $ef_tc
                //     ]);
                // }
            }

            $db3    = DB::table('courses')
                    ->where('course_code', $old_cc[0]);
            $seq3   = $db3->max('sequence');
            $old    = $db3->where('sequence', $seq3)->first();

            $rmv = new course();
            $rmv->course_code               = $old_cc[0];
            $rmv->college_code              = $old->college_code;
            $rmv->grading_mode_code         = $old->grading_mode_code;
            $rmv->long_title                = $old->long_title;
            $rmv->short_title               = $old->short_title;
            $rmv->description               = $old->description;
            $rmv->course_prerequisites      = $old->course_prerequisites;
            $rmv->course_content            = $old->course_content;
            $rmv->course_weekly_outline     = $old->course_weekly_outline;
            $rmv->course_assessment         = $old->course_assessment;
            $rmv->course_learning_outcomes  = $old->course_learning_outcomes;
            $rmv->effective_term_code       = $ef_tc;
            $rmv->end_term_code             = $en_tc;
            $rmv->sequence                  = $old->sequence + 1;
            $rmv->credit_hours              = $old->credit_hours;
            $rmv->contact_hours             = $old->contact_hours;
            $rmv->lecture_hours             = $old->lecture_hours;
            $rmv->lab_hours                 = $old->lab_hours;
            $rmv->other_hours               = $old->other_hours;
            $rmv->faculty_load_hours        = $old->faculty_load_hours;
            $rmv->this_course_replace       = $old->this_course_replace;
            $rmv->subject_code              = $old->subject_code;

            for ($j=0; $j < count($program_code); $j++){
                $old_code   = $program_code[$j];

                for ($b = 1; $b < 10; $b++){
                    if ($old->{'program_code_0'.$b} == $old_code){
                        $rmv->{'semester_program_0'.$b} = NULL;
                        $rmv->{'program_code_0'.$b} =  NULL;
                        $rmv->{'course_type_code_0'.$b} = NULL;
                        $rmv->{'course_genre_code_0'.$b} = NULL;
                        break;
                    }
                    // else{
                    //     $rmv->{'semester_program_0'.$b} = $old->{'semester_program_0'.$b};
                    //     $rmv->{'program_code_0'.$b} =  $old->{'program_code_0'.$b};
                    //     $rmv->{'course_type_code_0'.$b} = $old->{'course_type_code_0'.$b};
                    //     $rmv->{'course_genre_code_0'.$b} = $old->{'course_genre_code_0'.$b};
                    // }
                }
            }

            $rmv->status                    = 'Active';
            $rmv->policy                    = $old->policy;
            $rmv->honor                     = $old->honor;
            $rmv->attendance                = $old->attendance;
            $rmv->disability                = $old->disability;
            $rmv->pedagogy                  = $old->pedagogy;
            $rmv->course_level_code         = $old->course_level_code;
            $rmv->role_level_code           = $old->role_level_code;
            $rmv->full_course_code          = $old->full_course_code;
            $rmv->created_by                = $old->created_by;
            $rmv->formula                   = $old->formula;
            $rmv->additional_text           = $old->additional_text;
            $rmv->created_at                = Carbon::now();
            $rmv->updated_at                = Carbon::now();
            $rmv->save();

            $course_update3 = $db3->update([
                'status' => 'Inactive',
                'end_term_code' => $ef_tc
            ]);
        }

        $request->session()->forget('program_code');
        $request->session()->forget('changes');
        $request->session()->forget('ef_tc');
        $request->session()->forget('en_tc');
        $request->session()->forget('semester_prog');
        $request->session()->forget('course_code');
        $request->session()->forget('old_cc');
        $request->session()->forget('course_type');
        $request->session()->forget('course_genre');

        return redirect()->route('home')->with('success','Application submited succesfully');
    }
}
