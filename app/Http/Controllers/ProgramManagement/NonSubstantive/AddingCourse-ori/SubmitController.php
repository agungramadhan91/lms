<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\AddingCourse;

use App\course;
use Carbon\Carbon;
use App\academic_program;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SubmitController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $program_code   = Session::get('program_code');
        $semester_prog  = Session::get('semester_prog');
        $course_code    = Session::get('course_code');
        $course_type    = Session::get('course_type');
        $electives      = Session::get('electives');
        $course_genre   = Session::get('course_genre');
        $ef_tc          = Session::get('ef_tc');
        $en_tc          = Session::get('en_tc');
        
        if ($course_type != null) 
        {
            $db2    = DB::table('courses')->where('course_code', $course_code);
            $seq2   = $db2->max('sequence');
            $first  = $db2->where('sequence', $seq2)->first();
            $exs_p  = array();

            for ($y=1; $y <= 10; $y++) { 
                if ($y < 10) {
                    $pc = $first->{'program_code_0'.$y};
                } else {
                    $pc = $first->{'program_code_10'};
                }
                
                if ($pc != NULL) {
                    array_push($exs_p, $pc);
                }
            }

            $new = new course();
            $new->course_code               = $course_code;
            $new->college_code              = $first->college_code;
            $new->grading_mode_code         = $first->grading_mode_code;
            $new->long_title                = $first->long_title;

            if($electives != NULL){
                for ($z=0; $z < count($program_code); $z++) { 
                    $exp = explode(",", $electives[$z]);
                    
                    for ($x=0; $x < count($exp); $x++) { 
                        if($exp[$x] == 'University'){
                            $new->elect_by_university = TRUE;
                        }elseif($exp[$x] == 'College'){
                            $new->elect_by_college = TRUE;
                        }else{
                            $new->elect_by_program = TRUE;
                        }
                    }
                }
            }
            
            $new->short_title               = $first->short_title;
            $new->description               = $first->description;
            $new->course_prerequisites      = $first->course_prerequisites;
            $new->course_content            = $first->course_content;
            $new->course_weekly_outline     = $first->course_weekly_outline;
            $new->course_assessment         = $first->course_assessment;
            $new->course_learning_outcomes  = $first->course_learning_outcomes;
            $new->effective_term_code       = $ef_tc;
            $new->end_term_code             = $en_tc;
            $new->sequence                  = $first->sequence + 1;
            $new->credit_hours              = $first->credit_hours;
            $new->contact_hours             = $first->contact_hours;
            $new->lecture_hours             = $first->lecture_hours;
            $new->lab_hours                 = $first->lab_hours;
            $new->other_hours               = $first->other_hours;
            $new->faculty_load_hours        = $first->faculty_load_hours;
            $new->this_course_replace       = $first->this_course_replace;
            $new->subject_code              = $first->subject_code;

            for ($i=0; $i < count($exs_p); $i++) 
            { 
                $a      = $i + 1;
                if(isset($program_code[$i]) == TRUE){
                    $code   = $program_code[$i];

                    if ($first->{'program_code_0'.$a} == $code){
                        $new->{'program_code_0'.$a} =  $code;
                        $new->{'course_type_code_0'.$a} = $course_type[$i];
                        $new->{'course_genre_code_0'.$a} = $course_genre[$i];
                        $new->{'semester_program_0'.$a} = $semester_prog[$i];
                    }
                }else{
                    $new->{'program_code_0'.$a} = $first->{'program_code_0'.$a};
                    $new->{'course_type_code_0'.$a} = $first->{'course_type_code_0'.$a};
                    $new->{'course_genre_code_0'.$a} = $first->{'course_genre_code_0'.$a};
                    $new->{'semester_program_0'.$a} = $first->{'semester_program_0'.$a};
                }
            }

            $new->status                    = $first->status;
            $new->policy                    = $first->policy;
            $new->honor                     = $first->honor;
            $new->attendance                = $first->attendance;
            $new->disability                = $first->disability;
            $new->pedagogy                  = $first->pedagogy;
            $new->course_level_code         = $first->course_level_code;
            $new->role_level_code           = $first->role_level_code;
            $new->full_course_code          = $first->full_course_code;
            $new->created_by                = $first->created_by;
            $new->formula                   = $first->formula;
            $new->additional_text           = $first->additional_text;
            $new->created_at                = Carbon::now();
            $new->updated_at                = Carbon::now();
            $new->save();
            
            $course_update = $db2->update([
                'status' => 'Inactive',
                'end_term_code' => $ef_tc
            ]);

            //==========================================================================

            // $db = DB::table('study_plans')->where('course_code', $course_code[$i]);
            // $seq = $db->max('sequence');
            // $get = $db->where('sequence', $seq)->first();
            
            // if ($get != null) {
            //     $sp_insert = DB::table('study_plans')->insert([
            //         'program_code' => $get->program_code,
            //         'semester_code' => $get->semester_code,
            //         'course_code' => $course_code[$i],
            //         'sequence' => $get->sequence + 1,
            //         'course_genre' => $get->course_genre,
            //         'course_type' => $course_type[$i],
            //         'credit_hour' => $get->credit_hour,
            //         'status' => 'Active',
            //         'effective_term_code' => $ef_tc,
            //         'end_term_code' => $en_tc,
            //     ]);

            //     $sp_update = $db->update([
            //         'status' => 'Inactive',
            //         'end_term_code' => $ef_tc
            //     ]);
            // }
        }

        Session::forget('saved_gened');
        Session::forget('saved_elec');
        Session::forget('saved_conc');
        Session::forget('saved_core');
        Session::forget('saved_int');

        return redirect()->route('home')->with('success','Application submited succesfully');
    }
}
