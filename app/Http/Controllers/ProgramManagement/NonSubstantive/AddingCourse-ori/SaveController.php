<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\AddingCourse;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SaveController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $saved_gened= Session::forget('saved_gened');
        $saved_elec = Session::forget('saved_elec');
        $saved_conc = Session::forget('saved_conc');
        $saved_core = Session::forget('saved_core');
        $saved_int  = Session::forget('saved_int');

        $code           = $request->program_code;
        $semester_prog  = $request->semester_program;
        $course_code    = $request->course_code;
        $course_type    = $request->course_type;
        $old_ct         = $request->old_course_type;
        $course_genre   = $request->course_genre;
        $credit         = $request->credit;
        $ef_tc          = $request->effective_term;
        $en_tc          = $request->end_term;
        
        if($course_type == null){
            return redirect()->back()->with('error', 'No selected course type');
        }
        // dd($request);
        for ($i=0; $i < count($code); $i++) { 
            if(!isset($course_type[$i])){
                return redirect()->back()->with('error','Please Complete All Required Fields!');
            }elseif($request->old_course_type[$i] == $course_type[$i]){
                return redirect()->back()->with('error','Course type must be different!');
            }

            // if ($request->elective[$i] == NULL) {
            //     return redirect()->back()->with('error','Please choose ELECT course type!');
            // }
        }
        // dd($request);

        $total_gened    = array();
        $total_conc     = array();
        $total_elec     = array();
        $total_core     = array();
        $total_int      = array();

        for ($i=0; $i < count($code); $i++) { 
            $ct     = $request->{'ct-'.$code[$i]};
            $gened  = array();
            $conc   = array();
            $elec   = array();
            $core   = array();
            $int    = array();
            
            for ($j=0; $j < count($ct); $j++) { 
                if ($ct[$j] == $old_ct[$i]) {
                    if ($course_type[$i] == 'GEN') {
                        array_push($gened, $credit[$i]);
                    }elseif ($course_type[$i] == 'CONC') {
                        array_push($conc, $credit[$i]);
                    }elseif ($course_type[$i] == 'ELECT') {
                        array_push($elec, $credit[$i]);
                    }elseif ($course_type[$i] == 'Core') {
                        array_push($core, $credit[$i]);
                    }else{
                        array_push($int, $credit[$i]);
                    }
                }else{

                    if ($ct[$j] == 'GEN') {
                        array_push($gened, $credit[$j]);
                    }elseif ($ct[$j] == 'CONC') {
                        array_push($conc, $credit[$j]);
                    }elseif ($ct[$j] == 'ELECT') {
                        array_push($elec, $credit[$j]);
                    }elseif ($ct[$j] == 'Core') {
                        array_push($core, $credit[$j]);
                    }else{
                        array_push($int, $credit[$j]);
                    }
                }
            }

            if ($old_ct[$i] == 'GEN') {
                array_pop($gened);
            }elseif ($old_ct[$i] == 'CONC') {
                array_pop($conc);
            }elseif ($old_ct[$i] == 'ELECT') {
                array_pop($elec);
            }elseif ($old_ct[$i] == 'Core') {
                array_pop($core);
            }else{
                array_pop($int);
            }

            array_push($total_gened, [
                $code[$i] => array_sum($gened)
            ]);
            array_push($total_elec, [
                $code[$i] => array_sum($elec)
            ]);
            array_push($total_conc, [
                $code[$i] => array_sum($conc)
            ]);
            array_push($total_core, [
                $code[$i] => array_sum($core)
            ]);
            array_push($total_int, [
                $code[$i] => array_sum($int)
            ]);  
        }

        // return dd($total_gened);
        Session::put('program_code', $code);
        Session::put('semester_prog', $semester_prog);
        Session::put('course_code', $course_code);
        Session::put('course_type', $course_type);
        Session::put('electives', $request->electives);
        Session::put('course_genre', $course_genre);
        Session::put('ef_tc', $ef_tc);
        Session::put('en_tc', $en_tc);

        Session::put('saved_gened', $total_gened);
        Session::put('saved_elec', $total_elec);
        Session::put('saved_conc', $total_conc);
        Session::put('saved_core', $total_core);
        Session::put('saved_int', $total_int);

        return redirect()->back()->with('success','Saved successfully, please submit the application!');
    }
}
