<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\Third;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class MainController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if(Session::has('program_code')){
            $code = Session::get('program_code');
        }else{
            $code = $request->program_code;
        }

        $user           = Auth::user();
        $db_programs    = DB::table('academic_programs')->where('program_code', $code);
        $count_programs = $db_programs->count();
        $program        = $db_programs->first();
        
        $db_majors      = DB::table('majors')->where('program_code', $program->program_code);
        $count_majors   = $db_majors->count();
        
        $campuses       = DB::table('campuses')->where('is_shown', TRUE)->get();
        $colleges       = DB::table('colleges')->where('is_shown', TRUE)->get();
        $dtypes         = DB::table('degree_types')->where('is_shown', TRUE)->get();
        $dlevels        = DB::table('degree_levels')->where('is_shown', TRUE)->get();
        $departments    = DB::table('departments')->where('college_code',$program->college_code)->get();
        $terms          = DB::table('terms')->where('is_shown', TRUE)
                        ->where('academic_year', '>', Carbon::now()->format('Y'))
                        ->get();
        $count_desc     = count(Session::get('changed_description'));
        // return dd();
        Session::put('program_code', $code);

        return view('program.non-substantive.program_learning_outcomes.main', compact(
            'campuses','colleges','dtypes','dlevels','departments','terms','program',
            'count_majors','count_desc'
            // 'college2','level2','type2','effterm','endterm','major',
            // 'major_name','outcome','assesment','faculty','fitness','student'
        ));
    }
}
