<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\Third;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $program_code   = $request->program_code;
        $code           = $request->code;
        $descriptions   = $request->description;
        $collect_code   = array();
        $collect_desc   = array();

        for ($i=0; $i < count($descriptions); $i++) 
        { 
            $exist_outcomes  = DB::table('program_learning_outcomes')->where('plo_code', $code[$i])->first();
            
            if($exist_outcomes->plo_description != $descriptions[$i])
            {
                array_push($collect_code, $code[$i]);
                array_push($collect_desc, $descriptions[$i]);
            }
        }
        
        Session::put('program_code', $program_code);
        Session::put('code', $collect_code);
        Session::put('changed_description', $collect_desc);
        
        return redirect()->route('program.non-substantive.program_learning_outcome.main');
        // return dd(Session::get('code'));
                // ->with([
                //     'program_code' => $program_code
                //     ]);
    }
}
