<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\Third;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ThirdController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if(Session::has('program_code')){
            $code = Session::get('program_code');
        }else{
            $code = $request->program_code;
        }

        $db_studies     = DB::table('study_plans')->where('program_code', $code);
        $studies        = $db_studies->get();
        $count_studies  = $db_studies->count();

        $deliveries     = DB::table('delivery_modes')->get();
        $languages      = DB::table('instructional_languages')->get();
        $semesters      = DB::table('semesters')->get();
        $course_types   = DB::table('courses')->get();

        Session::put('program_code', $code);        

        return view('program.non-substantive.third.structure', compact(
            'code','deliveries','languages','studies','course_types','semesters',
            'count_studies'
        ));

        // $user = Auth::user();
        // $program = Academic_Program::first();
        // // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        // $student = Student::where('program_code','=',$program->program_code)->first();
        // $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        // $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        // $countresearch = Research::where('program_code','=',$program->program_code)->count();
        // $countresource = Resource::where('program_code','=',$program->program_code)->count();
        // $countsupportive = Program_Supportive::where('program_code','=',$program->program_code)->count();
        // $countcost = Cost_Revenue::where('program_code','=',$program->program_code)->count();
        // $countcommunity = Community_Engagement::where('program_code','=',$program->program_code)->count();
        // $countpublic = Public_Diclosure::where('program_code','=',$program->program_code)->count();
        // $countstudy = Study_Plan::where('program_code','=',$program->program_code)->count();
        // $delivery = Delivery_Mode::get();
        // $language = Instructional_Language::get();
        // $course = Course::all();
        // // dd($countstudy);
        // $genre = Course_Genre::get();
        // $semester = Semester::get();
        // $type = Course_Type::get();
        // $study = Study_Plan::where('program_code','=',$program->program_code)->get();
        // $sum = Study_Plan::where('created_by','=', $user['id'])->sum('credit_hour');
        // $sumcon = Study_Plan::where([
        //     ['created_by','=', $user['id']],
        //     ['course_type','=','CONC']
        // ])->sum('credit_hour');
        // $sumgen = Study_Plan::where([
        //     ['created_by','=', $user['id']],
        //     ['course_type','=','GEN']
        // ])->sum('credit_hour');
        // $sumcore = Study_Plan::where([
        //     ['created_by','=', $user['id']],
        //     ['course_type','=','Core']
        // ])->sum('credit_hour');
        // $sumelect = Study_Plan::where([
        //     ['created_by','=', $user['id']],
        //     ['course_type','=','Elect']
        // ])->sum('credit_hour');
        // // dd($course2);
        // return view('program.view.structure', compact('program', 'faculty', 'student','fitness','countresearch','countresource','countsupportive','countcost','countcommunity','countpublic','countstudy','delivery','language','course','genre','semester','type','study','sum','sumelect','sumcore','sumcon','sumgen'));
    }
}
