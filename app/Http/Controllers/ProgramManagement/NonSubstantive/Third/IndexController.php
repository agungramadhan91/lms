<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\Third;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if(Session::has('program_code')){
            $code = Session::get('program_code');
        }else{
            $code = $request->program_code;
        }

        $outcomes   = DB::table('program_learning_outcomes')
                    ->where('program_code', $code)
                    ->where('status','Active')
                    ->where('is_shown',TRUE)
                    ->orWhere('status', 'Approve')
                    ->get();

        Session::put('program_code', $code);        

        return view('program.non-substantive.program_learning_outcomes.index', compact(
            'code','outcomes'
        ));
    }
}
