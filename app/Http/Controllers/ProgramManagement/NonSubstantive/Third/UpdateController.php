<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\Third;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class UpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $program_code   = Session::get('program_code');
        $code           = Session::get('code');
        $descriptions   = Session::get('changed_description');
        $ef_tc          = $request->effective_term;
        $en_tc          = $request->end_term;

        if ($descriptions == NULL) {
            return redirect()->route('home')->with('error','No Terms Updated!');
        }

        for ($i=0; $i < count($descriptions); $i++) 
        { 
            $exist_outcomes  = DB::table('program_learning_outcomes')->where('plo_code', $code[$i])->first();
            
            if($exist_outcomes->plo_description != $descriptions[$i])
            {
                $update_outcomes = DB::table('program_learning_outcomes')
                                ->where('plo_code', $code[$i])
                                ->update([
                                    'sequence'        => $exist_outcomes->sequence + 1,
                                    'is_shown'        => TRUE,
                                    'plo_description' => $descriptions[$i],
                                    'effective_term_code'   => $ef_tc,
                                    'end_term_code'         => $en_tc
                                ]);

                DB::table('program_learning_outcomes')->insert([
                    'plo_code'          => $exist_outcomes->plo_code.'+'.$exist_outcomes->sequence,
                    'program_code'      => $exist_outcomes->program_code,
                    'plo_description'   => $exist_outcomes->plo_description,
                    'sequence'          => $exist_outcomes->sequence,
                    'is_shown'          => FALSE,
                    'status'            => 'Active',
                    'effective_term_code' => $exist_outcomes->effective_term_code,
                    'end_term_code'     => $exist_outcomes->end_term_code,
                    'system_date'       => $exist_outcomes->system_date
                ]);
            }
        }

        Session::put('program_code', $program_code);

        return redirect()->route('home')->with('success','The Terms updated succesfully');
    }
}
