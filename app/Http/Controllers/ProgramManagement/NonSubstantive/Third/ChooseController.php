<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\Third;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ChooseController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $programs   = DB::table('academic_programs')
                    ->where('status','Active')
                    ->orWhere('status','Approve')
                    ->get();

        return view('program.non-substantive.third.choose', compact('programs')); 
    }
}
