<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\AddingCourse;

use App\Helper\CourseHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ListController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $university_courses = CourseHelper::unique();
        $college_courses    = '';
        $program_courses    = '';
        // $colleges           = DB::table('colleges')->get();

        return view('program.non-substantive.adding_course.list', compact(
            'university_courses','college_courses','program_courses'
        )); 
    }

    public function byCollege(Request $request)
    {
        return ;
    }

    public function byProgram(Request $request)
    {
        return ;
    }
}
