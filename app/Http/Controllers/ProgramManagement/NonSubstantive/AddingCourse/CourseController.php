<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\AddingCourse;

use Carbon\Carbon;
use App\Helper\GetProgram;
use App\Helper\TermHelper;
use App\Helper\CourseHelper;
use Illuminate\Http\Request;
use App\Helper\ProgramHelper;
use App\Helper\SemesterHelper;
use App\Helper\StudyPlanHelper;
use App\Helper\CourseTypeHelper;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CourseController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {        
        if(Session::has('course_code'))
        {
            $course     = Session::get('course_code');
            $ef_tc      = Session::get('ef_tc');
            $en_tc      = Session::get('en_tc');
            $changes    = Session::get('electives');
            $elect_by_program   = Session::get('elect_by_program');
            $elect_by_college   = Session::get('elect_by_college');
            $elect_by_university= Session::get('elect_by_university');
        }else{
            $course     = $request->course_code;
            $ef_tc      = null;
            $en_tc      = null;
            $changes    = null;
            $elect_by_program   = null;
            $elect_by_college   = null;
            $elect_by_university= null;
        }
        
        $terms          = TermHelper::unique();
        $semesters      = SemesterHelper::unique();
        $data           = CourseHelper::first($course);
        $courses        = CourseHelper::uniqueByProgram($data->program_code_01);
        $course_types   = CourseTypeHelper::unique();
        
        return view('program.non-substantive.adding_course.course',compact(
            'data','courses','terms','semesters','course_types',
            'programs','codes','changes','ef_tc','en_tc',
            'elect_by_program','elect_by_college','elect_by_university'
        ));
    }
}
