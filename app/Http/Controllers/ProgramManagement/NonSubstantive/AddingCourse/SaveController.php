<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\AddingCourse;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SaveController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $code           = $request->program_code;
        $course_code    = $request->course_code;
        $course_type    = $request->course_type;
        $old_ct         = $request->old_course_type;
        $ef_tc          = $request->effective_term;
        $en_tc          = $request->end_term;
        $elect_by_program   = $request->elect_by_program;
        $elect_by_college   = $request->elect_by_college;
        $elect_by_university= $request->elect_by_university;
        
        if($course_type == null){
            return redirect()->back()->with('error', 'No selected course type');
        }

        // $exp = explode(',',$electives);
        // for ($i=0; $i < count($exp); $i++) { 
        //     if($exp[$i] == 'Program'){
        //     }elseif ($exp[$i] == 'College') {
        //     }else{
        //     }
        // }
        Session::put('elect_by_program', $elect_by_program); 
        Session::put('elect_by_college', $elect_by_college); 
        Session::put('elect_by_university', $elect_by_university); 
        
        // dd($request);
        Session::put('course_code', $course_code);
        Session::put('electives', $electives);
        Session::put('ef_tc', $ef_tc);
        Session::put('en_tc', $en_tc);

        return redirect()->back()->with('success','Saved successfully, please submit the application!');
    }
}
