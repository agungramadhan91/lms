<?php

namespace App\Http\Controllers\ProgramManagement\NonSubstantive\AddingCourse;

use App\Helper\CourseHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ChooseController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        Session::forget('program_code');
        Session::forget('changes');
        Session::forget('semester_prog');
        Session::forget('course_code');
        Session::forget('course_type');
        Session::forget('ef_tc');
        Session::forget('en_tc');
        // $sp = DB::table('study_plans')
        //         ->select('program_code')
        //         ->where('status','Active')
        //         ->groupBy('program_code');
        //         // return dd($sp);

        // $programs   = DB::table('academic_programs as ap')
        //             ->select('ap.program_code','ap.title')
        //             // ->where('ap.status','Active')
        //             ->joinSub($sp, 'plo', function($join){
        //                 $join->on('ap.program_code','=','plo.program_code');
        //             })
        //             ->get();
        // $programs = DB::table('academic_programs as ap')
        //             ->join('courses as sp','sp.program_code','=','ap.program_code')
        //             ->select('ap.*')
        //             ->where('ap.status','Active')
        //             // ->groupBy('ap.program_code')
        //             ->get();

        $courses = CourseHelper::unique();

        return view('program.non-substantive.adding_course.choose', compact('courses')); 
    }
}
