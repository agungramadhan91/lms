<?php

namespace App\Http\Controllers\ProgramManagement\Substantive\ProgramLearningOutcome;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class OutcomeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if(Session::has('plo_description')){
            $program_code   = Session::get('program_code');
            $plo_code       = Session::get('plo_code');
            $plo_description= Session::get('plo_description');
            $plo_term_ef    = Session::get('plo_term_ef');
            $plo_term_en    = Session::get('plo_term_en');
        }else{
            $program_code   = $request->program_code;
            $plo_code       = "";
            $plo_description= "";
            $plo_term_ef    = "";
            $plo_term_en    = "";
        }

        $outcomes   = array();
        $plo_codes  = array();
        $sequences  = array();
        $real_code  = "";
        $count_code = "";
        
        $terms      = DB::table('terms')
                    ->where('calender_year','>', Carbon::now()->format('Y'))
                    ->get();

        $group_plo  = DB::table('program_learning_outcomes')
                    ->select('plo_code')
                    ->where('program_code', $program_code)
                    ->groupBy('plo_code')
                    ->get();
        
        foreach ($group_plo as $data) {
            $db_plo     = DB::table('program_learning_outcomes');
            $sequence   = $db_plo
                        ->where('plo_code', $data->plo_code)
                        ->max('sequence');
            $plo        = $db_plo
                        ->where('plo_code', $data->plo_code)
                        ->where('sequence', $sequence)
                        ->first();
            
            $rc  = substr($data->plo_code, 0, -2);
            $q   = substr($data->plo_code, -2);
            $real_code = $rc;
            $count_code = $q;

            $data->program_code = $plo->program_code;
            $data->plo_code     = $plo->plo_code;
            $data->plo_description = $plo->plo_description;
            $data->sequence     = $plo->sequence;
            $data->status       = $plo->status;
        }

        Session::put('program_code', $program_code);

        return view('program.substantive.program_learning_outcomes.outcome', compact(
            'outcomes','program_code','plo_code','plo_description','plo_term_ef','plo_term_en','terms',
            'real_code','count_code','group_plo'
        ));
    }
}
