<?php

namespace App\Http\Controllers\ProgramManagement\Substantive\ProgramLearningOutcome;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SaveController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function outcome(Request $request)
    {
        $program_code   = Session::get('program_code');
        $code           = $request->code;
        $descriptions   = $request->description;
        $adcode         = $request->add_code;
        $addescription  = $request->add_description;
        $ef_tc          = $request->effective_term;
        $en_tc          = $request->end_term;
        $collect_code   = array();
        $collect_desc   = array();
        $collect_term   = array();
        $collect_out    = array();

        if($en_tc == $ef_tc){
            return redirect()->back()->with('error','End Term must be greater than Effective Term');
        }
        
        if(isset($code)){
            for ($c=0; $c < count($code); $c++) { 
                if($code[$c] != null){
                    array_push($collect_code, $code[$c]);
                }
            }
        }
        // return dd($collect_code);
        if(isset($descriptions)){
            for ($i=0; $i < count($descriptions); $i++) 
            { 
                $exist_outcomes  = DB::table('program_learning_outcomes')
                                ->where('plo_code', $collect_code[$i])
                                ->where('status','Active')
                                ->orWhere('status','Approve')
                                ->first();
    
                $sub = substr($exist_outcomes->plo_description, -1);
                
                if($sub == ' ')
                {
                    if($exist_outcomes->plo_description != $descriptions[$i].' ')
                    {
                        array_push($collect_desc, $descriptions[$i]);
                    }
                }else{
    
                    if($exist_outcomes->plo_description != $descriptions[$i])
                    {
                        array_push($collect_desc, $descriptions[$i]);
                    }
                }
            }
        }

        if(isset($addescription))
        {
            for ($j=0; $j < count($addescription); $j++) 
            {
                array_push($collect_code, $adcode[$j]); 
                array_push($collect_desc, $addescription[$j]);
            }
        }
        // return dd($collect_code,$collect_desc,$collect_out,$en_tc);
        // return dd($collect_code,$collect_desc);
        if (count($collect_desc) < 1) 
        {
            return redirect()->back()->with('error','Please complete all required fields before you save!');
        }

        Session::put('program_code', $program_code);
        Session::put('plo_code', $collect_code);
        Session::put('plo_description', $collect_desc);
        Session::put('plo_term_ef', $ef_tc);
        Session::put('plo_term_en', $en_tc);
        
        return  redirect()
                // ->route('program.non-substantive.program_learning_outcome.main')
                ->back()
                ->with('success','Saved Successfully, please submit the application');
    }

    public function inactivate(Request $request, $code)
    {
        $code   = $request->code;
        $status = $request->status;
        $en_tc  = $request->end_term;
        
        $sequence = DB::table('program_learning_outcomes')
                ->where('plo_code', $code)
                ->max('sequence');
        $db_plo     = DB::table('program_learning_outcomes')->where('plo_code', $code);
        $old_plo    = $db_plo->where('sequence', $sequence);
        $old_plo2    = $old_plo->first();     // return dd($old_plo);
        $new_plo    = DB::table('program_learning_outcomes')->insert([
            'plo_code'  => $old_plo2->plo_code,
            'program_code'  => $old_plo2->program_code,
            'plo_description'  => $old_plo2->plo_description,
            'effective_term_code' => $old_plo2->effective_term_code,
            'end_term_code' => $en_tc,
            'sequence' => $old_plo2->sequence + 1,
            'is_shown' => TRUE,
            'system_date' => Carbon::now(),
            'status'        => $status
        ]);   
        
        $old_plo->update([
            'status'        => 'Inactive',
            'end_term_code' => $old_plo2->effective_term_code,
        ]);
        
        return  redirect()
                ->back()
                ->with('success','Inactivate Successfully');
    }
}
