<?php

namespace App\Http\Controllers\ProgramManagement\Substantive\ProgramLearningOutcome;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SubmitController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $click_btn      = $request->click_btn;
        $ef_tc          = $request->effective_term;
        $en_tc          = $request->end_term;
        $collect_erro   = array();
        $collect_code   = array();
        $collect_desc   = array();
        $collect_term   = array();

        $program_code   = Session::get('program_code');
        $plo_code       = Session::get('plo_code');
        $plo_description= Session::get('plo_description');
        $plo_term_ef    = Session::get('plo_term_ef');
        $plo_term_en    = Session::get('plo_term_en');
        // return dd(count($plo_code));
        for ($i=0; $i < count($plo_description); $i++) 
        { 
            $exist_outcomes = DB::table('program_learning_outcomes')
                            ->where('plo_code', $plo_code[$i])
                            ->where('status','Active')
                            ->first();
            
            if(isset($exist_outcomes)){
                if($exist_outcomes->plo_description != $plo_description[$i])
                {
                    DB::table('program_learning_outcomes')->insert([
                        'plo_code'          => $plo_code[$i],
                        'program_code'      => $program_code,
                        'plo_description'   => $plo_description[$i],
                        'sequence'          => $exist_outcomes->sequence + 1,
                        'status'            => 'Active',
                        'effective_term_code' => $plo_term_ef,
                        'end_term_code'     => $plo_term_en,
                        'system_date'       => $exist_outcomes->system_date
                    ]);
    
                    $update_outcomes = DB::table('program_learning_outcomes')
                                    ->where('plo_code', $plo_code[$i])
                                    ->where('sequence', $exist_outcomes->sequence)
                                    ->update([
                                        'status'        => 'Inactive',
                                        'end_term_code' => $plo_term_ef,
                                    ]);
                }
            }else{
                DB::table('program_learning_outcomes')->insert([
                    'plo_code'          => $plo_code[$i],
                    'program_code'      => $program_code,
                    'plo_description'   => $plo_description[$i],
                    'status'            => 'Active',
                    'effective_term_code' => $plo_term_ef,
                    'end_term_code'     => $plo_term_en,
                    'system_date'       => Carbon::now()
                ]);
            }
        }

        return  redirect()
                ->route('home')
                ->with('success','Application submitted succesfully');
    }
}
