<?php

namespace App\Http\Controllers\ProgramManagement\Substantive\DeliveryMode;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SubmitController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $code       = Session::get('program_code');
        $delivery   = Session::get('delivery');
        $ef_tc      = Session::get('ef_tc');
        $en_tc      = Session::get('en_tc');
        // return dd($delivery);
        $db_programs    = DB::table('academic_programs')->where('program_code', $code);
        $seq_program    = $db_programs->max('sequence');
        $get_program    = $db_programs->where('sequence', $seq_program);
        $old_program    = $get_program->first();

        $new_prgm = DB::table('academic_programs')->insert([
            'program_code'          => $code,
            'title'                 => $old_program->title,
            'required_hours'        => $old_program->required_hours,
            'college_code'          => $old_program->college_code,
            // 'delivery_code'           => $delivery,
            'degree_level_code'     => $old_program->degree_level_code,
            'degree_type_code'      => $old_program->degree_type_code,
            'sequence'              => $old_program->sequence + 1,
            'effective_term_code'   => $ef_tc,
            'end_term_code'         => $en_tc,
            'status'                => 'Active',
            'system_date'           => Carbon::now(),
        ]);

        for ($i=0; $i < count($delivery); $i++) { 
            DB::table('program_delivery_mode')->insert([
                'program_code'  => $code,
                'delivery_code'   => $delivery[$i],
                'sequence'      => $old_program->sequence + 1,
            ]);

            Session::forget($delivery[$i]);
        }
        
        $get_program->update([
            'end_term_code' => $ef_tc,
            'status'        => 'Inactive'
        ]);

        Session::put('program_code', $code);
        // Session::forget('delivery');

        return redirect()->route('home')->with('success','Application submitted succesfully');
    }
}
