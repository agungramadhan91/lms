<?php

namespace App\Http\Controllers\ProgramManagement\Substantive\DeliveryMode;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Session\Session;

class SaveController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if($request->session()->has('program_code')){
            $program_code   = $request->session()->get('program_code');
        }else{
            $program_code   = $request->program_code;
        }
        // return dd($request);
        $delivery = $request->delivery;
        $ef_tc  = $request->effective_term;
        $en_tc  = $request->end_term;

        if ($delivery == null) {
            return redirect()->back()->with('error', 'Please fill the delivery modes');
        }
        
        if($en_tc <= $ef_tc){
            return redirect()->back()->with('error', 'End Term must be greater than Effective Term');
        }

        $request->session()->put('program_code', $program_code);
        $request->session()->put('delivery', $delivery);
        $request->session()->put('ef_tc', $ef_tc);
        $request->session()->put('en_tc', $en_tc);
        
        // return dd(Session::get('campus'));
        
        return redirect()->back()->with('success', 'Saved Succesfully, please submit the application');
    }
}
