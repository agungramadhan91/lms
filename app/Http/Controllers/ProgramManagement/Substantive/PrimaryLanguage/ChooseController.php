<?php

namespace App\Http\Controllers\ProgramManagement\Substantive\PrimaryLanguage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ChooseController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        Session::forget('program_code');
        Session::forget('language');
        Session::forget('ef_tc');
        Session::forget('en_tc');
        // Session::flush();
        // return dd(Session::get('language'));

        $programs   = DB::table('academic_programs')
                    ->where('status','Active')
                    ->select('program_code')
                    ->groupBy('program_code')
                    ->get();

                    foreach ($programs as $data) {
                        $db     = DB::table('academic_programs')->where('program_code', $data->program_code);
                        $seq    = $db->max('sequence');
                        $get    = $db->where('sequence', $seq)->first();

                        $data->campus_code = $get->campus_code;
                        $data->degree_type_code = $get->degree_type_code;
                        $data->degree_level_code = $get->degree_level_code;
                        $data->college_code = $get->college_code;
                        $data->title = $get->title;
                        $data->required_hours = $get->required_hours;
                        $data->effective_term_code = $get->effective_term_code;
                        $data->end_term_code = $get->end_term_code;
                        $data->status = $get->status;
                    }

        return view('program.substantive.primary_language.choose', compact('programs')); 
    }
}
