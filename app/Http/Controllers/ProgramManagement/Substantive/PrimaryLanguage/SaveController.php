<?php

namespace App\Http\Controllers\ProgramManagement\Substantive\PrimaryLanguage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SaveController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if(Session::has('program_code')){
            $program_code   = Session::get('program_code');
        }else{
            $program_code   = $request->program_code;
        }
        // return dd($request);
        $language = $request->language;
        $ef_tc  = $request->effective_term;
        $en_tc  = $request->end_term;

        if ($language == null) {
            return redirect()->back()->with('error', 'Please fill the language!');
        }
        
        if($en_tc <= $ef_tc){
            return redirect()->back()->with('error', 'End Term must be greater than Effective Term!');
        }

        Session::put('program_code', $program_code);
        Session::put('language', $language);
        Session::put('ef_tc', $ef_tc);
        Session::put('en_tc', $en_tc);
        
        // return dd(Session::get('campus'));
        
        return redirect()->back()->with('success', 'Saved Succesfully, please submit the application!');
    }
}
