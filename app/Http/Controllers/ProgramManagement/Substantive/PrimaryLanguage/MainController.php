<?php

namespace App\Http\Controllers\ProgramManagement\Substantive\PrimaryLanguage;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class MainController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if(Session::has('program_code')){
            $code   = Session::get('program_code');
            $language = Session::get('language');
            $ef_tc  = Session::get('ef_tc');
            $en_tc  = Session::get('en_tc');

            // return redirect()->back()->with('error','All required fields has been saved, please submit!');
        }else{
            $code   = $request->program_code;
            $language = null;
            $ef_tc  = "";
            $en_tc  = "";
        }
        // return dd($language);
        
        $db_programs    = DB::table('academic_programs')->where('program_code', $code);
        $seq_program    = $db_programs->max('sequence');
        $program        = $db_programs->where('sequence', $seq_program)->first();

        if($language == null || $language == ""){
            $db_program = DB::table('program_language')
                            ->where('program_code', $code);
            $seq_program= $db_program->max('sequence');
            $program_language = $db_program->where('sequence', $seq_program)->get();
        }
        // return dd($program_language);
        $db_majors      = DB::table('majors')->where('program_code', $code);
        $count_majors   = $db_majors->count();
        $deliveries       = DB::table('delivery_modes')->get();
        $languages       = DB::table('instructional_languages')->get();
        $campuses       = DB::table('campuses')->get();
        $colleges       = DB::table('colleges')->get();
        $dtypes         = DB::table('degree_types')->get();
        $dlevels        = DB::table('degree_levels')->get();
        $departments    = DB::table('departments')->where('college_code',$program->college_code)->get();
        $terms          = DB::table('terms')
                        ->where('academic_year', '>', Carbon::now()->format('Y'))
                        ->get();
        $countstudy     = DB::table('study_plans')->where('program_code','=',$program->program_code)->count();
        $semester       = DB::table('semesters')->get();
        $course         = DB::table('courses')->get();
        $sumcon = DB::table('study_plans')->where([
            ['course_type','=','CONC']
        ])->sum('credit_hour');
        $sumgen = DB::table('study_plans')->where([
            ['course_type','=','GEN']
        ])->sum('credit_hour');
        $sumcore = DB::table('study_plans')->where([
            ['course_type','=','Core']
        ])->sum('credit_hour');
        $sumelect = DB::table('study_plans')->where([
            ['course_type','=','Elect']
        ])->sum('credit_hour');
        // dd($course2);

        Session::put('program_code', $code);

        return view('program.substantive.primary_language.structure', compact(
            'program', 'faculty', 'student','fitness','countresearch','countresource',
            'countsupportive','countcost','countcommunity','countpublic','countstudy',
            'deliveries','languages','course','genre','semester','type','study','course2',
            'sum','sumelect','sumcore','sumcon','sumgen','ef_tc','en_tc','terms','language',
            'program_language'
        ));
        
    }
}
