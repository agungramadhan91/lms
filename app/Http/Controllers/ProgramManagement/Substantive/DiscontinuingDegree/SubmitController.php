<?php

namespace App\Http\Controllers\ProgramManagement\Substantive\DiscontinuingDegree;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SubmitController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $code   = Session::get('program_code');
        $ef_tc  = Session::get('ef_tc');
        $inactive  = Session::get('inactivate');

        if($inactive == null){
            return redirect()->back()->with('error', 'Please fill the required field!');
        }

        $db_programs    = DB::table('academic_programs')->where('program_code', $code);
        $seq_program    = $db_programs->max('sequence');
        $get_program    = $db_programs->where('sequence', $seq_program);
        $old_program    = $get_program->first();

        DB::table('academic_programs')->insert([
            'program_code'          => $code,
            'title'                 => $old_program->title,
            'required_hours'        => $old_program->required_hours,
            'college_code'          => $old_program->college_code,
            'campus_code'           => $old_program->campus_code,
            'degree_level_code'     => $old_program->degree_level_code,
            'degree_type_code'      => $old_program->degree_type_code,
            'sequence'              => $old_program->sequence + 1,
            'effective_term_code'   => $ef_tc,
            'end_term_code'         => '999999',
            'status'                => 'Inactive',
            'system_date'           => Carbon::now(),
        ]);
        
        $get_program->update([
            'end_term_code' => $ef_tc,
            'status'        => 'Inactive'
        ]);

        Session::put('program_code', $code);
        Session::forget('inactivate');

        return redirect()->route('home')->with('success','Application submitted succesfully');
    }
}
