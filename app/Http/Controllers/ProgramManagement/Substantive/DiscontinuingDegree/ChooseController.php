<?php

namespace App\Http\Controllers\ProgramManagement\Substantive\DiscontinuingDegree;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ChooseController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        Session::forget('program_code');
        Session::forget('inactivate');
        Session::forget('ef_tc');
        Session::forget('en_tc');
        
        $programs   = DB::table('academic_programs')
                    ->select('program_code')
                    ->where('status','Active')
                    ->groupBy('program_code')
                    ->get();

                    foreach ($programs as $data) {
                        $db     = DB::table('academic_programs')->where('program_code', $data->program_code);
                        $seq    = $db->max('sequence');
                        $get    = $db->where('sequence', $seq)->first();

                        $data->campus_code = $get->campus_code;
                        $data->degree_type_code = $get->degree_type_code;
                        $data->degree_level_code = $get->degree_level_code;
                        $data->college_code = $get->college_code;
                        $data->title = $get->title;
                        $data->required_hours = $get->required_hours;
                        $data->effective_term_code = $get->effective_term_code;
                        $data->end_term_code = $get->end_term_code;
                        $data->status = $get->status;
                    }

        // $programs   = DB::table('academic_programs')
        //             ->where('status','Active')
        //             ->orWhere('status','Approve')
        //             ->get();

        return view('program.substantive.discontinuing_degree.choose', compact('programs')); 
    }
}
