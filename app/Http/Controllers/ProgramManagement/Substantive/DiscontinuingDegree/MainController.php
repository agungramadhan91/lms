<?php

namespace App\Http\Controllers\ProgramManagement\Substantive\DiscontinuingDegree;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class MainController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if(Session::has('program_code')){
            $code   = Session::get('program_code');
            $inactivate  = Session::get('inactivate');
            $ef_tc  = Session::get('ef_tc');
            $en_tc  = Session::get('en_tc');

            // return redirect()->back()->with('error','All required fields has been saved, please submit!');
        }else{
            $code   = $request->program_code;
            $inactivate = null;
            $ef_tc  = "";
            $en_tc  = "";
        }
        // return dd($code);
        // $user           = Auth::user();
        $db_programs    = DB::table('academic_programs')->where('program_code', $code);
        $seq_program    = $db_programs->max('sequence');
        $program        = $db_programs->where('sequence', $seq_program)->first();
        
        $db_majors      = DB::table('majors')->where('program_code', $code);
        $count_majors   = $db_majors->count();
        
        $campuses       = DB::table('campuses')->get();
        $colleges       = DB::table('colleges')->get();
        $dtypes         = DB::table('degree_types')->get();
        $dlevels        = DB::table('degree_levels')->get();
        $departments    = DB::table('departments')->where('college_code',$program->college_code)->get();
        $terms          = DB::table('terms')
                        // ->where('academic_year', '>', Carbon::now()->format('Y'))
                        ->get();
        // $count_desc     = count(Session::get('changed_description'));
        // return dd();
        Session::put('program_code', $code);

        return view('program.substantive.discontinuing_degree.main', compact(
            'campuses','colleges','dtypes','dlevels','departments','terms','program',
            'count_majors','count_desc',
            'activation','ef_tc','en_tc','inactivate'
            // 'college2','level2','type2','effterm','endterm','major',
            // 'major_name','outcome','assesment','faculty','fitness','student'
        ));
    }
}
