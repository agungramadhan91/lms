<?php

namespace App\Http\Controllers\ProgramManagement\Substantive\TitleAcademicDegree;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SubmitController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $code   = Session::get('program_code');
        $title  = Session::get('title');
        $ef_tc  = Session::get('ef_tc');
        $en_tc  = Session::get('en_tc');

        $db_programs    = DB::table('academic_programs')->where('program_code', $code);
        $seq_program    = $db_programs->max('sequence');
        $get_program    = $db_programs->where('sequence', $seq_program);
        $old_program    = $get_program->first();

        DB::table('academic_programs')->insert([
            'program_code'          => $code,
            'title'                 => $title,
            'required_hours'        => $old_program->required_hours,
            'college_code'          => $old_program->college_code,
            'campus_code'           => $old_program->campus_code,
            'degree_level_code'     => $old_program->degree_level_code,
            'degree_type_code'      => $old_program->degree_type_code,
            'sequence'              => $old_program->sequence + 1,
            'effective_term_code'   => $ef_tc,
            'end_term_code'         => $en_tc,
            'status'                => 'Active',
            'system_date'           => Carbon::now(),
        ]);
        
        $get_program->update([
            'end_term_code' => $ef_tc,
            'status'        => 'Inactive'
        ]);

        Session::put('program_code', $code);

        return redirect()->route('home')->with('success','Application submitted succesfully');
    }
}
