<?php

namespace App\Http\Controllers\ProgramManagement\Substantive\TitleAcademicDegree;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SaveController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if(Session::has('program_code')){
            $program_code   = Session::get('program_code');
        }else{
            $program_code   = $request->program_code;
        }

        $title  = $request->title;
        $ef_tc  = $request->effective_term;
        $en_tc  = $request->end_term;

        $db = DB::table('academic_programs')->where('status','Active')->first();

        if($db->title == $title){
            return redirect()->back()->with('error', 'No changes title were made!');
            
            if($en_tc <= $ef_tc){
                return redirect()->back()->with('error', 'End Term must be greater than Effective Term');
            }
        }

        Session::put('program_code', $program_code);
        Session::put('title', $title);
        Session::put('ef_tc', $ef_tc);
        Session::put('en_tc', $en_tc);
        Session::put('changes', 'true');

        
        // return dd(Session::get('en_tc'));
        
        return redirect()->back()->with('success', 'Saved Succesfully, please submit the application!');
    }
}
