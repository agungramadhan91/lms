<?php

namespace App\Http\Controllers\Management\RoleUser;

use App\role;
use App\User;
use App\workflow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'code'      => 'required|unique:roles,role_code',
            'level'     => 'required',
            'name'      => 'required',
            'status'    => 'required',
        ]);

        $code   = $request->code;
        $name   = $request->name;
        $level  = $request->level;

        if ($request->status == 'true') {
            $status = TRUE;
        } elseif ($request->status == 'false') {
            $status = FALSE;
        } elseif($request->status == '2'){
            $status = 2;
        } elseif($request->status == '3'){
            $status = 3;
        } else {
            $status = 4;
        }

        $department = $request->department;
        // return dd($department);

        $role   = role::create([
            'role_code'         => $code,
            'role_level_code'   => $level,
            'role_description'  => $name,
            'is_approver'       => $status,
            'is_active'         => 'true',
            'department_code'   => $department == '' ? 'empty':$department, 

        ]);

        return redirect()->back()->with('success','Success create the role');
    }

    public function store_user(Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required',
            'lastname'  => 'required',
            'college'   => 'required|exists:colleges,college_code',
            'id'        => 'required|unique:users',
            'email'     => 'required|unique:users',
            'password'  => 'required'
        ]);

        $firstname      = $request->firstname;
        $lastname       = $request->lastname;
        $college        = $request->college;
        $department     = $request->department;
        $role           = $request->role;
        $id             = $request->id;
        $name           = $firstname.' '.$lastname;
        $email          = $request->email;
        $password       = $request->password;
        $role_code      = $request->role_code;

        // return dd($role_code);
        // if(count($role_code) > 0){
        //     for ($i=0; $i < count($role_code); $i++) { 
        //         if($role_code[$i] == $role_code[$i--]){
        //             // return redirect()->back()->with('user_error','Duplicate role, please choose different role!');
        //         }
        //     }
        // }


        $user = new User();
        $user->id           = $id;
        $user->name         = $name;
        $user->firstname    = $firstname;
        $user->lastname     = $lastname;
        $user->college      = $college;
        $user->department   = $department;
        // $user->role         = $role;
        // $user->role_code    = $role;
        $user->email        = $email;
        $user->password     = bcrypt($password);
        $user->save();

        for ($i=0; $i < count($role_code); $i++) { 
            $user->roles()->attach($role_code[$i]);
        }
        
        return redirect()->back()->with('success','Success create the user');
    }

    public function store_workflow(Request $request)
    {
        $this->validate($request, [
            'workflow_code' => 'required|unique:workflows',
        ]);

        $code           = $request->workflow_code;
        $description    = $request->description;
        $role_code      = $request->role_code;
        $role_description   = $request->role_description;
        
        $flow       = new workflow();
        $flow->workflow_code = $code;
        $flow->workflow_code_description = $description;
        $flow->sequence = 1;
        // $flow->save();

        // $rd = DB::table('workflows')->where('workflow_code',$code)->first();
        $no = 1;
        for($i = 0; $i < count($role_code); $i++)
        {
            $desc = DB::table('roles')->where('role_code', $role_code[$i])->first();

            if($role_code[$i] > 0 && $role_code[$i] == $role_code[$i--]){
                return redirect()->back()->with('warning','Please choose different role than before');
            }

            $flow->{"role_code_".$no} = $role_code[$i];
            $flow->{"role_description_".$no} = $desc->role_description;

            $no++;
        }

        $flow->save();

        return redirect()->back()->with('success','Success create the workflow');
    }
}
