<?php

namespace App\Http\Controllers\Management\RoleUser;

use App\role;
use App\User;
use App\workflow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class UpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $code)
    {
        $this->validate($request, [
            // 'code'      => 'required',
            // 'level'     => 'required',
            // 'name'      => 'required',
            // 'status'    => 'required',
        ]);

        // $code   = $request->code;
        $level  = $request->level;
        $name   = $request->name;
        $status = $request->status;
        $activation = $request->activation;

        if ($request->status == 'true') {
            $status = TRUE;
        } elseif ($request->status == 'false') {
            $status = FALSE;
        } elseif($request->status == '3'){
            $status = 2;
        } elseif($request->status == '4'){
            $status = 3;
        } else {
            $status = 4;
        }
        
        $department = $request->department == NULL ? 'empty':$request->department;
        
        $sequence   = role::where('role_code', $code)->max('sequence');
        $old_role   = role::where('role_code', $code)->where('sequence', $sequence)->first();
        $old_role->update([
            'is_shown'          => FALSE,
        ]);

        $new_role   = DB::table('roles')->insert([
            'role_code'         => $code,
            'role_level_code'   => $level,
            'role_description'  => $name,
            'is_approver'       => $status,
            'is_active'         => $activation,
            'sequence'          => $old_role->sequence + 1,
            'is_shown'          => TRUE,
            'department_code'   => $department
        ]);

        return redirect()->back()->with('success','Success edit the '.$name.' role');
    }

    public function update_user(Request $request, $user_id)
    {
        // return dd($request);
        $firstname      = $request->firstname;
        $lastname       = $request->lastname;
        $college        = $request->college;
        $department     = $request->department;
        $role           = $request->role;
        // $id             = $request->id;
        $name           = $firstname.' '.$lastname;
        $email          = $request->email;
        $password       = $request->password;
        $role_code      = $request->role_code;
        $activation     = $request->activation;


        $user = User::where('id', $user_id)->first();
        // $user->id           = $id;
        $user->name         = $name;
        $user->firstname    = $firstname;
        $user->lastname     = $lastname;
        $user->college      = $college;
        $user->department   = $department;
        $user->is_active    = $activation == 'true' ? TRUE:FALSE;
        // $user->role         = $role;
        // $user->role_code    = $role;
        $user->email        = $email;
        $user->password     = bcrypt($password);
        $user->save();

        $user->roles()->detach();
        for ($i=0; $i < count($role_code); $i++) { 
            $user->roles()->attach($role_code[$i]);
        }

        return redirect()->back()->with('success','Success update '. $user->firstname .' user');
    }

    public function update_workflow(Request $request, $code)
    {
        // $this->validate($request, [
        //     'role_code.*' => 'distinct',
        // ]);
        // return dd($request);

        $code           = $request->code;
        $description    = $request->description;
        $role_code      = $request->role_code;

        $sequence   = workflow::where('workflow_code', $code)->max('sequence');
        $old_flow   = workflow::where('workflow_code', $code)
                    ->where('sequence', $sequence)
                    ->first();
        // 
        $flow = new workflow();
        // $flow->workflow_code = $code;
        // $flow->workflow_code_description = $description;
        // $flow->sequence = (int)$old_flow->sequence + 1;
        // $flow->is_shown = TRUE;
        
        // for($i = 1; $i < 25; $i++)
        // {
        //     if ($old_flow->{"role_code_".$i} !== NULL) {
        //         $flow->{"role_code_".$i} = $old_flow->{"role_code_".$i};
        //         $flow->{"role_description_".$i} = $old_flow->{"role_description_".$i};
        //     }
        // }

        // $flow->save();
        
        //=================================================================
        
        $ind_role   = array();
        $ind_desc   = array();
        $no         = 1;

        for($z = 0; $z < count($role_code); $z++)
        {
            if($role_code[$z] != NULL)
            {
                $desc = DB::table('roles')->where('role_code', $role_code[$z])->first();

                array_push($ind_role, $role_code[$z]);
                array_push($ind_desc, $desc->role_description);
            }
        }

        $flow->workflow_code = $code;
        $flow->workflow_code_description = $description;
        $flow->sequence = (int)$old_flow->sequence + 1;

        for ($j=0; $j < count($ind_role); $j++) 
        { 
            $flow->{"role_code_".$no} = $ind_role[$j];
            $flow->{"role_description_".$no} = $ind_desc[$j];

            $no++;
        }
        
        for ($j=count($ind_role); $j < 25; $j++) 
        { 
            $j_no = $j + 1;
            $flow->{"role_code_".$j_no} = NULL;
            $flow->{"role_description_".$j_no} = NULL;
        }

        $flow->save();
        
        return redirect()->back()->with('success','Success edit the '.$code.' Workflow');
    }

    // public function activate(Request $request, $code){
    //     $role   = role::where('role_code', $code)->update([
    //         'is_active'         => $request->activate,
    //         // 'department_code'   => $department
    //     ]);

    //     return redirect()->back();
    // }
}

// if($ind_role[$j] > 0 && $ind_role[$j] == $ind_role[$j--]){
            //     return redirect()->back()->with('warning','Please choose different role than before');
            // }