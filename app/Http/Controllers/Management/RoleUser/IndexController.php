<?php

namespace App\Http\Controllers\Management\RoleUser;

use App\role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $users          = User::all();
        $levels         = DB::table('role_levels')->get();
        $departments    = DB::table('departments')->get();
        $colleges       = DB::table('colleges')->get();
        $roles          = DB::table('roles')
                        ->select('role_code')
                        ->groupBy('role_code')
                        ->get();

        $workflows      = DB::table('workflows')
                        ->select('workflow_code')
                        ->groupBy('workflow_code')
                        ->get();

        foreach ($roles as $role) {
            $db_roles   = DB::table('roles')->where('role_code', $role->role_code);
            $roles_seq  = $db_roles->max('sequence');
            $sgl_role   = $db_roles->where('sequence', $roles_seq)->first();
            $level      = DB::table('role_levels')
                        ->where('role_level_code', $sgl_role->role_level_code)
                        ->first();

            $role->code     = $sgl_role->role_code;
            $role->name     = $sgl_role->role_description;
            $role->level    = $level->role_description;
            $role->is_active = $sgl_role->is_active;
            $role->role_level_code = $sgl_role->role_level_code;
            $role->is_approver = $sgl_role->is_approver;
            $role->department_code = $sgl_role->department_code;
            $role->role_description = $sgl_role->role_description;

            if($sgl_role->is_approver == 1){
                $role->status   = 'Approver';
            }elseif($sgl_role->is_approver == 0){
                $role->status   = 'Reviewer';
            }elseif($sgl_role->is_approver == 2){
                $role->status   = 'Approver and Reviewer';
            }elseif($sgl_role->is_approver == 3){
                $role->status   = 'Initiator';
            }else{
                $role->status   = 'Execution';
            }

            if($sgl_role->department_code == 'empty'){
                $role->department = "-";
            }else{
                $department = DB::table('departments')->where('department_code', $sgl_role->department_code)->first();
                $role->department = $sgl_role->department_code;
            }
        }

        foreach($workflows as $flow){
            $db_wf  = DB::table('workflows')->where('workflow_code', $flow->workflow_code);
            $wf_seq = $db_wf->max('sequence');
            $wf     = $db_wf->where('sequence', $wf_seq)->first();

            $flow->workflow_code_description = $wf->workflow_code_description;
            $flow->sequence = $wf->sequence;

            for ($i=1; $i <= 25; $i++) { 
                $flow->{"role_code_". $i} = $wf->{"role_code_". $i};
                $flow->{"role_description_". $i} = $wf->{"role_description_". $i};
            }
        }

        return view('admin.management.role_user.index', compact(
            'roles','levels','departments','colleges','users','workflows'
        ));
    }
}
