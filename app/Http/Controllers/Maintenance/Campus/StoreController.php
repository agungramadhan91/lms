<?php

namespace App\Http\Controllers\Maintenance\Campus;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|unique:campuses,campus_code',
            'effective_tc' => 'required',
            'end_tc' => 'required|gt:effective_tc'
        ]);

        $exp_code = explode(" ", $request->code);

        $code           = implode("_", $exp_code);
        $description    = $request->description;
        $city           = $request->city;
        $effective_tc   = $request->effective_tc;
        $end_tc         = $request->end_tc;

        if($end_tc <= $effective_tc){
            return redirect()->back()->with('error','End Term must be greater than Effective Term!');
        }

        $college = DB::table('campuses')->insert([
            'campus_code'           => $code,
            'campus_description'    => $description,
            'campus_city'           => $city,
            'effective_term_code'   => $effective_tc,
            'end_term_code'         => $end_tc,
        ]);

        return redirect()->back()->with('success','Success input new Campus');
    }
}
