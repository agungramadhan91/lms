<?php

namespace App\Http\Controllers\Maintenance\Campus;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $campuses   = DB::table('campuses')
                    ->select('campus_code')
                    ->groupBy('campus_code')
                    ->get();

                    foreach ($campuses as $data) {
                        $db     = DB::table('campuses')->where('campus_code', $data->campus_code);
                        $seq    = $db->max('sequence');
                        $get    = $db->where('sequence', $seq)->first();

                        $data->campus_description = $get->campus_description;
                        $data->is_active = $get->is_active;
                        $data->campus_city = $get->campus_city;
                        $data->effective_term_code = $get->effective_term_code;
                        $data->end_term_code = $get->end_term_code;
                        $data->is_active = $get->is_active;
                    }

        $terms      = DB::table('terms')
                    ->where('calender_year','>',Carbon::now()->format('Y'))
                    ->select('term_code')
                    ->groupBy('term_code')
                    ->get();

                    foreach ($terms as $data) {
                        $db     = DB::table('terms')->where('term_code', $data->term_code);
                        $seq    = $db->max('sequence');
                        $get    = $db->where('sequence', $seq)->first();

                        $data->term_code = $get->term_code;
                        $data->term_description = $get->term_description;
                    }
        
        return view('admin.maintenance.campus.index', compact('campuses','terms'));
    }
}
