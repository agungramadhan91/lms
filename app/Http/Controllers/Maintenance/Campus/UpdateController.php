<?php

namespace App\Http\Controllers\Maintenance\Campus;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class UpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $code)
    {
        $this->validate($request, [
            'code' => 'required|exists:campuses,campus_code',
            'effective_tc' => 'required',
            'end_tc' => 'required|gt:effective_tc'
        ]);

        $code           = $request->code;
        $description    = $request->description;
        $city           = $request->city;
        $effective_tc   = $request->effective_tc;
        $end_tc         = $request->end_tc;
        $activation     = $request->activation;

        if($end_tc <= $effective_tc){
            return redirect()->back()->with('error','End Term must be greater than Effective Term!');
        }

        // $update_db  = DB::table('campuses')->where('campus_code', $code);
        // $old_db     = $update_db->first();

        $update_db = DB::table('campuses')->where('campus_code', $code);
        $seq_db = $update_db->max('sequence');
        $old_db = $update_db->where('sequence', $seq_db)->first();

        $campus = DB::table('campuses')->insert([
            'campus_code'           => $code,
            'campus_description'    => $description,
            'campus_city'           => $city,
            'is_active'             => $activation,
            'sequence'              => $old_db->sequence + 1,
            'effective_term_code'   => $effective_tc,
            'end_term_code'         => $end_tc,
        ]);

        $update_db->update([
            'is_active'             => 0,
            'end_term_code'         => $effective_tc,
        ]);

        return redirect()
                ->back()
                ->with('success','Success edit '.$old_db->campus_code.' Campus');
    }
}
