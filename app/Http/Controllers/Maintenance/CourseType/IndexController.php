<?php

namespace App\Http\Controllers\Maintenance\CourseType;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $dg_types   = DB::table('course_types')->where('is_shown', TRUE)->get();
        $dg_levels  = DB::table('course_levels')->where('is_shown', TRUE)->get();
        $terms      = DB::table('terms')
                    ->where('is_active', TRUE)
                    ->where('is_shown', TRUE)
                    ->where('calender_year','>',Carbon::now()->format('Y'))
                    ->get();
        
        return view('admin.maintenance.course_types.index', compact('dg_types','dg_levels','terms'));
    }
}
