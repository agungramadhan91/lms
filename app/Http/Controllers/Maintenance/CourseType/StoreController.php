<?php

namespace App\Http\Controllers\Maintenance\CourseType;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|unique:course_types,course_type_code'
        ]);

        $exp_code = explode(" ", $request->code);

        $code           = implode("_", $exp_code);
        $description    = $request->description;
        $effective_tc   = $request->effective_tc;
        $end_tc         = $request->end_tc;
        // $level          = $request->level;

        $dl = DB::table('course_types')->insert([
            'course_type_code'      => $code,
            'type_description'      => $description,
            'effective_term_code'   => $effective_tc,
            'end_term_code'         => $end_tc,
            // 'course_level_code'     => $level,
            // 'system_date'           => Carbon::now(),
        ]);

        return redirect()->back()->with('success','Success input new Course Type');
    }
}
