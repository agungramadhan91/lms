<?php

namespace App\Http\Controllers\Maintenance\Department;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|unique:departments,department_code'
        ]);

        $exp_code = explode(" ", $request->code);

        $code           = implode("_", $exp_code);
        $title          = $request->title;
        $college_code   = $request->college_code;
        $effective_tc   = $request->effective_tc;
        $end_tc         = $request->end_tc;
        // $level          = $request->level;

        $dl = DB::table('departments')->insert([
            'department_code'       => $code,
            'title'                 => $title,
            'college_code'          => $college_code,
            'effective_term_code'   => $effective_tc,
            'end_term_code'         => $end_tc,
            'system_date'           => Carbon::now(),
            // 'course_level_code'     => $level,
        ]);

        return redirect()->back()->with('success','Success input new Department');
    }
}
