<?php

namespace App\Http\Controllers\Maintenance\CourseLevel;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class UpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|exists:course_levels,course_level_code'
        ]);

        $code           = $request->code;
        $title          = $request->title;
        $effective_tc   = $request->effective_tc;
        $end_tc         = $request->end_tc;

        $update_db = DB::table('course_levels')->where('course_level_code', $code);
        $seq_db = $update_db->max('sequence');
        $old_db = $update_db->where('sequence', $seq_db)->first();

        $campus = DB::table('course_levels')->insert([
            'course_level_code'    => $code,
            'title'                 => $title,
            'is_shown'              => TRUE,
            'is_active'             => $request->activation,
            'sequence'              => $old_db->sequence + 1,
            'effective_term_code'   => $effective_tc,
            'end_term_code'         => $end_tc,
            'system_date'           => Carbon::now(),
        ]);

        $update_dg->update([
            'is_active'             => 0,
            'end_term_code'         => $effective_tc,
        ]);

        return redirect()
                ->back()
                ->with('success','Success edit '.$old_db->course_level_code.' Course Level');
    }
}
