<?php

namespace App\Http\Controllers\Maintenance\College;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class UpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $code)
    {
        $this->validate($request, [
            'code' => 'required|exists:colleges,college_code'
        ]);

        $code = $request->code;
        $title = $request->title;
        $campus_code = $request->campus_code;
        $effective_tc = $request->effective_tc;
        $end_tc = $request->end_tc;
        $activation = $request->activation;

        if($end_tc <= $effective_tc){
            return redirect()->back()->with('error','End Term must be greater than Effective Term!');
        }

        $update_college = DB::table('colleges')->where('college_code', $code);
        $seq_college = $update_college->max('sequence');
        $old_college = $update_college->where('sequence', $seq_college)->first();

        $college = DB::table('colleges')->insert([
            'college_code'          => $code,
            'title'                 => $title,
            'campus_code'           => $campus_code,
            'sequence'              => $old_college->sequence + 1,
            'is_active'             => $activation,
            'effective_term_code'   => $effective_tc,
            'end_term_code'         => $end_tc,
            'system_date'           => Carbon::now()
        ]);

        $update_college->update([
            'is_active'             => 0,
            'end_term_code'         => $effective_tc,
        ]);

        return redirect()
                ->back()
                ->with('success','Success edit '.$old_college->college_code.' College');
    }
}
