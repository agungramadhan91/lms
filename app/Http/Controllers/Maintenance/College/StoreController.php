<?php

namespace App\Http\Controllers\Maintenance\College;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|unique:colleges,college_code'
        ]);

        $exp_code = explode(" ", $request->code);

        $code           = implode("_", $exp_code);
        $title          = $request->title;
        $campus_code    = $request->campus_code;
        $effective_tc   = $request->effective_tc;
        $end_tc         = $request->end_tc;

        $college = DB::table('colleges')->insert([
            'college_code'          => $code,
            'title'                 => $title,
            'campus_code'           => $campus_code,
            'effective_term_code'   => $effective_tc,
            'end_term_code'         => $end_tc,
            'system_date'           => Carbon::now(),
        ]);

        return redirect()->back()->with('success','Success input new College');
    }
}
