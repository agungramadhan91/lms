<?php

namespace App\Http\Controllers\Maintenance\Subject;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|unique:subjects,subject_code'
        ]);

        $exp_code = explode(" ", $request->code);

        $code           = implode("_", $exp_code);
        $description    = $request->description;
        $college        = $request->college_code;
        $department     = $request->department_code;
        $effective_tc   = $request->effective_tc;
        $end_tc         = $request->end_tc;
        // $level          = $request->level;

        $dl = DB::table('subjects')->insert([
            'subject_code'          => $code,
            'subject_description'   => $description,
            'college_code'          => $college,
            'department_code'        => $department,
            'effective_term_code'   => $effective_tc,
            'end_term_code'         => $end_tc,
            // 'course_level_code'     => $level,
            // 'system_date'           => Carbon::now(),
        ]);

        return redirect()->back()->with('success','Success input new Subject Code');
    }
}
