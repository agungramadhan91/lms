<?php

namespace App\Http\Controllers\Maintenance\Subject;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $subjects       = DB::table('subjects')->where('is_shown', TRUE)->get();
        $colleges       = DB::table('colleges')->where('is_shown', TRUE)->get();
        $departments    = DB::table('departments')->where('is_shown', TRUE)->get();
        $terms          = DB::table('terms')
                        ->where('is_active', TRUE)
                        ->where('is_shown', TRUE)
                        ->where('calender_year','>',Carbon::now()->format('Y'))
                        ->get();
        
        return view('admin.maintenance.subjects.index', compact('subjects','colleges','departments','terms'));
    }
}
