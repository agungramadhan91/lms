<?php

namespace App\Http\Controllers\Maintenance\Subject;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class UpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|exists:subjects,subject_code'
        ]);

        $code           = $request->code;
        $description    = $request->description;
        $college        = $request->college_code;
        $department     = $request->department_code;
        $effective_tc   = $request->effective_tc;
        $end_tc         = $request->end_tc;
        
        $update_db = DB::table('subjects')->where('subject_code', $code);
        $seq_db = $update_db->max('sequence');
        $old_db = $update_db->where('sequence', $seq_db)->first();

        $campus = DB::table('subjects')->insert([
            'subject_description'   => $description,
            'college_code'          => $college,
            'department_code'       => $department,
            'sequence'              => $old_db->sequence+1,
            'is_active'             => $request->activation,
            'effective_term_code'   => $effective_tc,
            'end_term_code'         => $end_tc,
            'system_date'           => Carbon::now(),
        ]);

        $update_db->update([
            'is_active'             => 0,
            'end_term_code'         => $effective_tc,
        ]);

        return redirect()
                ->back()
                ->with('success','Success edit '.$code.' Subject');
    }
}
