<?php

namespace App\Http\Controllers\Maintenance\Programs;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class UpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $code)
    {
        $this->validate($request, [
            'code' => 'required|exists:academic_programs,program_code'
        ]);

        $code           = $request->code;
        $title          = $request->title;
        $hours          = $request->hours;
        $campus_code    = $request->campus_code;
        $college_code   = $request->college_code;
        $dl_code        = $request->dl_code;
        $dt_code        = $request->dt_code;
        $effective_tc   = $request->effective_tc;
        $end_tc         = $request->end_tc;

        $update_program = ProgramHelper::firstData($code);
        $old_program = $update_program->first();
        // return dd($code);

        $program = DB::table('academic_programs')->insert([
            'program_code'          => $code,
            'title'                 => $title,
            'required_hours'        => $hours,
            'campus_code'           => $campus_code,
            'college_code'          => $college_code,
            'degree_level_code'     => $dl_code,
            'degree_type_code'      => $dt_code,
            'effective_term_code'   => $effective_tc,
            'end_term_code'         => $end_tc,
            'sequence'              => $old_db->sequence+1,
            'is_active'             => $request->activation,
            'system_date'           => Carbon::now(),
        ]);

        $update_program->update([
            'is_active'             => 0,
            'end_term_code'         => $effective_tc,
        ]);

        return redirect()
                ->back()
                ->with('success','Success edit '.$code.' Program');
    }
}
