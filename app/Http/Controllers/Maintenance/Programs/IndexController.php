<?php

namespace App\Http\Controllers\Maintenance\Programs;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // $programs   = DB::table('academic_programs')->where('is_shown', TRUE)->get();
        $programs   = DB::table('academic_programs')
                    ->select('program_code')
                    ->groupBy('program_code')
                    ->get();

                    foreach ($programs as $data) {
                        $db     = DB::table('academic_programs')->where('program_code', $data->program_code);
                        $seq    = $db->max('sequence');
                        $get    = $db->where('sequence', $seq)->first();

                        $data->campus_code = $get->campus_code;
                        $data->degree_type_code = $get->degree_type_code;
                        $data->degree_level_code = $get->degree_level_code;
                        $data->college_code = $get->college_code;
                        $data->title = $get->title;
                        $data->required_hours = $get->required_hours;
                        $data->effective_term_code = $get->effective_term_code;
                        $data->end_term_code = $get->end_term_code;
                        $data->status = $get->status;
                    }
        $campuses   = DB::table('campuses')->where('is_shown', TRUE)->get();
        $colleges   = DB::table('colleges')->where('is_shown', TRUE)->get();
        $dg_levels  = DB::table('degree_levels')->where('is_shown', TRUE)->get();
        $dg_types   = DB::table('degree_types')->where('is_shown', TRUE)->get();
        $terms      = DB::table('terms')
                    ->where('calender_year','>',Carbon::now()->format('Y'))
                    ->select('term_code')
                    ->groupBy('term_code')
                    ->get();

                    foreach ($terms as $data) {
                        $db     = DB::table('terms')->where('term_code', $data->term_code);
                        $seq    = $db->max('sequence');
                        $get    = $db->where('sequence', $seq)->first();

                        $data->term_code = $get->term_code;
                        $data->term_description = $get->term_description;
                    }
        
        return view('admin.maintenance.programs.index', compact(
            'programs','campuses','colleges','dg_levels','dg_types','terms'
        ));
    }

    public function degreeType(Request $request, $code)
    {
        $dt = DB::table("degree_types")
            ->where("degree_level_code", $code)
            ->where('is_shown', TRUE)
            ->pluck("title","degree_type_code")->all();
        return json_encode($dt);
    }
}
