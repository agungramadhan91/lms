<?php

namespace App\Http\Controllers\Maintenance\GradingMode;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|unique:grading_modes,grading_mode_code'
        ]);

        $exp_code = explode(" ", $request->code);

        $code           = implode("_", $exp_code);
        $description    = $request->description;
        $effective_tc   = $request->effective_tc;
        $end_tc         = $request->end_tc;

        $gm = DB::table('grading_modes')->insert([
            'grading_mode_code'     => $code,
            'grading_description'   => $description,
            'effective_term_code'   => $effective_tc,
            'end_term_code'         => $end_tc,
        ]);

        return redirect()->back()->with('success','Success input new Grading Modes');
    }
}
