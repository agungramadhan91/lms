<?php

namespace App\Http\Controllers\Maintenance\Terms;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|unique:terms,term_code'
        ]);

        $exp_code = explode(" ", $request->code);

        $code           = implode("_", $exp_code);
        $description    = $request->description;
        $academic       = $request->academic_year;
        $calendar       = $request->calendar_year;
        $term_start     = $request->term_start;
        $term_end       = $request->term_end;

        $dl = DB::table('terms')->insert([
            'term_code'         => $code,
            'term_description'  => $description,
            'academic_year'     => $academic,
            'calender_year'     => $calendar,
            'term_start_date'   => $term_start,
            'term_end_date'     => $term_end,
        ]);

        return redirect()->back()->with('success','Success input a New Term');
    }
}
