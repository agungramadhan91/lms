<?php

namespace App\Http\Controllers\Maintenance\Terms;

use App\term;
use Carbon\Carbon;
use App\Helper\TermHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // $terms      = DB::table('terms')
        //             ->where('is_shown', TRUE)
        //             ->where('calender_year','>',Carbon::now()->format('Y'))
        //             ->get();
        $terms      = TermHelper::unique();

        return view('admin.maintenance.terms.index', compact('terms'));
    }
}
