<?php

namespace App\Http\Controllers\Maintenance\Terms;

use Carbon\Carbon;
use App\Helper\TermHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class UpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|exists:terms,term_code'
        ]);

        $code           = $request->code;
        $description    = $request->description;
        $academic       = $request->academic_year;
        $calendar       = $request->calendar_year;
        $term_start     = $request->term_start;
        $term_end       = $request->term_end;
        $status         = $request->activation;

        // $update_dg  = DB::table('terms')->where('term_code', $code)->where('is_shown',TRUE);
        // return dd($update_dg);
        $update_dg = TermHelper::firstData($code);
        $old_dg     = $update_dg->first();

        $campus = DB::table('terms')->insert([
            'term_code'         => $code,
            'term_description'  => $description,
            'academic_year'     => $academic,
            'calender_year'     => $calendar,
            'term_start_date'   => $term_start,
            'term_end_date'     => $term_end,
            'is_shown'          => TRUE,
            'sequence'          => $old_dg->sequence+1,
            'is_active'         => $status,
        ]);

        $update_dg->update([
            'is_active'         => FALSE,
            'term_end_date'     => $term_start,
        ]);

        return redirect()
                ->back()
                ->with('success','Success edit '.$code.' Term');
    }
}
