<?php

namespace App\Http\Controllers\Maintenance\GradingSchema;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|unique:grading_schema_details,grading_schema_details_code'
        ]);

        $gm_code        = $request->gm_code;
        $effective_tc   = $request->effective_tc;
        $end_tc         = $request->end_tc;

        $code               = $request->code;
        $percentage_from    = $request->percentage_from;
        $percentage_to      = $request->percentage_to;
        $grade              = $request->grade;
        $grade_point        = $request->grade_point;
        $description        = $request->description;

        // return dd(count($code));
        for ($i=0; $i < count($code); $i++) { 
            $gm = DB::table('grading_schema_details')->insert([
                'grading_mode_code' => $gm_code,
                'effective_term_code'   => $effective_tc,
                'end_term_code'         => $end_tc,

                'grading_schema_details_code'   => $code[$i],
                'percentage_from'   => $percentage_from[$i],
                'percentage_to'     => $percentage_to[$i],
                'grade'             => $grade[$i],
                'grade_points'      => $grade_point[$i],
                'description'       => $description[$i],
            ]);
        }

        return redirect()->back()->with('success','Success input new Grading Schema Details');
    }
}
