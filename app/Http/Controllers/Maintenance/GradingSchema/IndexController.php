<?php

namespace App\Http\Controllers\Maintenance\GradingSchema;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $g_schemas  = DB::table('grading_schema_details')->where('is_shown', TRUE)->get();
        $g_modes    = DB::table('grading_modes')->where('is_shown', TRUE)->get();
        $terms      = DB::table('terms')
                    ->where('is_active', TRUE)
                    ->where('is_shown', TRUE)
                    ->where('calender_year','>',Carbon::now()->format('Y'))
                    ->get();
        
        return view('admin.maintenance.grading_schemas.index', compact('g_schemas','g_modes','terms'));
    }
}
