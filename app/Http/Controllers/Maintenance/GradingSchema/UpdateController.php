<?php

namespace App\Http\Controllers\Maintenance\GradingSchema;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class UpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|exists:grading_schema_details,grading_schema_details_code'
        ]);

        $code               = $request->code;
        $gm_code            = $request->gm_code;
        $percentage_from    = $request->percentage_from;
        $percentage_to      = $request->percentage_to;
        $grade              = $request->grade;
        $grade_point        = $request->grade_point;
        $description        = $request->description;
        $effective_tc       = $request->effective_tc;
        $end_tc             = $request->end_tc;

        $update_db = DB::table('grading_schema_details')->where('grading_schema_details_code', $code);
        $seq_db = $update_db->max('sequence');
        $old_db = $update_db->where('sequence', $seq_db)->first();

        $campus = DB::table('grading_schema_details')->insert([
            'grading_schema_details_code' => $code,
            'grading_mode_code' => $gm_code,
            'percentage_from'   => $percentage_from,
            'percentage_to'     => $percentage_to,
            'grade'             => $grade,
            'grade_points'      => $grade_point,
            'description'       => $description,
            'sequence'              => $old_db->sequence+1,
            'is_active'             => $request->activation,
            'effective_term_code'   => $effective_tc,
            'end_term_code'         => $end_tc,
            'system_date'           => Carbon::now(),
        ]);

        $update_db->update([
            'is_active'             => 0,
            'end_term_code'         => $effective_tc,
        ]);

        return redirect()
                ->back()
                ->with('success','Success edit '.$old_db->grading_mode_code.' Grading Schema Details');
    }
}
