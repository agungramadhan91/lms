<?php

namespace App\Http\Controllers\Maintenance\DegreeLevel;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $dg_levels  = DB::table('degree_levels')->get();
        $terms      = DB::table('terms')
                    ->where('calender_year','>',Carbon::now()->format('Y'))
                    ->select('term_code')
                    ->groupBy('term_code')
                    ->get();

                    foreach ($terms as $data) {
                        $db     = DB::table('terms')->where('term_code', $data->term_code);
                        $seq    = $db->max('sequence');
                        $get    = $db->where('sequence', $seq)->first();

                        $data->term_code = $get->term_code;
                        $data->term_description = $get->term_description;
                    }
        
        return view('admin.maintenance.degree_levels.index', compact('dg_levels','terms'));
    }
}
