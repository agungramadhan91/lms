<?php

namespace App\Http\Controllers;

use App\Course;
use Carbon\Carbon;
use App\Helper\TermHelper;
use App\Helper\MinorHelper;
use App\Helper\CampusHelper;
use App\Helper\CourseHelper;
use Illuminate\Http\Request;
use App\Helper\CollegeHelper;
use App\Helper\ProgramHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // Session::forget('program_code');
        // Session::forget('plo_code');
        // Session::forget('plo_description');
        
        return view('home');
    }

    public function courseIndex()
    {
        $course = Course::get();
        return view('course_main_page',compact('course'));
    }


    public function minorIndex()
    {
        $minors     = MinorHelper::unique();
        $campuses   = CampusHelper::unique();
        $courses    = CourseHelper::unique();
        $colleges   = CollegeHelper::unique();
        $terms      = TermHelper::unique();

        foreach ($minors as $data) {
            $data->courses = DB::table('minor_courses')
                            ->where('minor_code', $data->minor_code)
                            ->where('sequence', $data->sequence)
                            ->pluck('course_code');
        }
        
        return view('minor', compact(
            'minors','campuses','colleges','courses','terms'
        ));
    }

    public function minorStore(Request $request)
    {
        $college    = $request->college_code;
        $minor      = $request->minor_code;
        $title      = $request->description;
        $campus     = $request->campus_code;
        $courses    = $request->course_codes;
        $ef_tc      = $request->effective_tc;
        $en_tc      = $request->end_tc;
        // return dd($request);
        if($courses == null || count($courses) < 1){
            return redirect()->back()->with('error','Please select the course(s)');
        }

        $db_minor = DB::table('minors')->insert([
            'minor_code'            => $minor,
            'college_code'          => $college,
            'campus_code'           => $campus,
            'title'                 => $title,
            'sequence'              => 1,
            'effective_term_code'   => $ef_tc,
            'end_term_code'         => $en_tc,
            'system_date'           => Carbon::now(),
        ]);

        for ($i=0; $i < count($courses); $i++) { 
            DB::table('minor_courses')->insert([
                'minor_code'    => $minor,
                'course_code'   => $courses[$i]
            ]);
        }

        return redirect()->back()->with('success','Saved Succesfully');
    }

    public function minorUpdate(Request $request, $code)
    {
        $title      = $request->description;
        $campus     = $request->campus_code;
        $courses    = $request->course_codes;
        $ef_tc      = $request->effective_tc;
        $en_tc      = $request->end_tc;
        // return dd($request);
        if($courses == null || count($courses) < 1){
            return redirect()->back()->with('error','Please select the course(s)');
        }

        $db_minor = DB::table('minors')->where('minor_code', $code);
        $db_seq = $db_minor->max('sequence');
        $db_first = $db_minor->where('sequence', $db_seq)->first();

        $minor = DB::table('minors')->insert([
            'minor_code'            => $code,
            'college_code'          => $db_first->college_code,
            'campus_code'           => $campus,
            'title'                 => $title,
            'sequence'              => $db_first->sequence + 1,
            'effective_term_code'   => $ef_tc,
            'end_term_code'         => $en_tc,
            'system_date'           => Carbon::now(),
        ]);

        for ($i=0; $i < count($courses); $i++) { 
            DB::table('minor_courses')->insert([
                'minor_code'    => $code,
                'course_code'   => $courses[$i],
                'sequence'      => $db_first->sequence + 1,
            ]);
        }

        $db_minor->update([
            'end_term_code' => $ef_tc,
        ]);

        return redirect()->back()->with('success','Saved Succesfully');
        // return dd($request);
    }

    public function getProgram(Request $request, $code)
    {
        $db         = DB::table('courses')
                    ->where('course_code', $code);
        $seq        = $db->max('sequence');
        $first      = $db->where('sequence', $seq)->first();
        $program    = array();

        for ($i=1; $i < 10; $i++) { 
            if($first->{'program_code_0'.$i} != ''){
                $db1 = ProgramHelper::firstData($first->{'program_code_0'.$i})->pluck('title','program_code'); 

                array_push($program, $db1);
            }
        }

        return response()->json($program);
    }

    public function getCourseByCollege(Request $request, $code)
    {
        // $db = CourseHelper::uniqueByCollege($code);
        $db = DB::table('courses')
                // ->select('course_code')
                ->where('college_code', $code)
                ->where('status','Active')
                ->pluck('short_title','course_code');

        return response()->json($db);
    }
    
    public function getCourseByProgram(Request $request, $code)
    {
        $db = CourseHelper::uniqueByProgram($code);

        return response()->json($db);
    }
}
