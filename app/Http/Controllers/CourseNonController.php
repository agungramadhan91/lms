<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon;
use App\Academic_Program;
use App\Course_Subject;
use App\Department;
use App\Campus;
use App\College;
use App\Course;
use App\Course_Restrictions;
use App\Course_Prerequisite;
use App\Course_Corequisite;
use App\Course_Equivalency;
use App\Course_Replace;
use App\Course_Textbook;
use App\Course_Additional_Material;
use App\Course_Genre;
use App\Course_Level;
use App\Course_Type;
use App\Course_Policy;
use App\Course_Special_Requirement;
use App\Course_Learning_Outcomes;
use App\Course_Content;
use App\Course_Weekly_Outline;
use App\Course_Assessment;
use App\Program_Learning_Outcomes;
use App\Pedagogy;
use App\Term;
use App\Grading_Mode;
use App\Grading_Schema_Detail;
use App\Plo_Clo_Mapping;
use App\Semester;
use App\Study_Plan;
use Auth;
use DB;
use Session;
use App\Delivery_Mode;
use App\Instructional_Language;

class CourseNonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function chooselevel()
    {
        $course = Course::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('course.edit.chooselevel',compact('course'));
    }

    public function index(Request $request)
    {
        // dd($request->course);
            $subject = Course_Subject::get();
            $subject2 = Course_Subject::first();    
            $college = College::get();
            $genre = Course_Genre::get();
            $type = Course_Type::get();
            $level = Course_Level::get();
            $department = Department::get();
            $user = Auth::user();
            $request->session()->push('course',$request->course);
            $coursecode = session('course');
            $coursecode2 = array_slice($coursecode, -1)[0];
            for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
            $coursecode3 = substr($coursecode[$i], 0, 6);
            $newcode = session('course2');
            // dd($newcode);
            $course = Course::where('course_code', '=', $coursecode3)->count();
            $course2 = Course::where('course_code', '=', $coursecode3)->max('sequence');
            // dd($request->course);
            if($newcode == ""){
                $sequence2 = Course::where('course_Code', '=', $coursecode3)->max('sequence');
                $coursedata = Course::where([
                    ['course_code', '=', $coursecode3],
                    ['sequence','=', $sequence2],
                    ])->first();
                $count = Course::where([
                    ['course_code', '=', $coursecode3],
                    ['status','=','Draft'],
                    ['sequence','=',$sequence2],
                ])->count();
            }else{
                $newcode2 = array_slice($newcode, -1)[0];
                $check = Course::where([
                    ['course_code', '=', $newcode2],
                    ])->count();
                // dd($sequence2);
                if($check != 0){
                    $sequence2 = Course::where('course_Code', '=', $newcode2)->max('sequence');
                    $coursedata = Course::where([
                        ['course_code', '=', $newcode2],
                        ])->first();
                    $count = Course::where([
                        ['course_code', '=', $newcode2],
                        ['status','=','Draft'],
                        ['sequence','=',$sequence2],
                    ])->count();
                }else{
                    $sequence2 = Course::where('course_Code', '=', $coursecode3)->max('sequence');
                    $coursedata = Course::where([
                        ['course_code', '=', $coursecode3],
                        ['sequence','=', $sequence2],
                        ])->first();
                    $count = Course::where([
                        ['course_code', '=', $coursecode3],
                        ['status','=','Draft'],
                        ['sequence','=',$sequence2],
                    ])->count();
                }
            }
            // dd($count);
            $number = preg_replace("/[^0-9]{1,4}/", '', $coursedata->course_code);
            $carbon = date('Y-m-d');
            $endterm = Term::where([
                ['term_code','!=', $coursedata->effective_term_code],
                ['term_start_date', '>=', $carbon],
                ])->get();
            $effectiveterm = Term::where([
                ['term_code','!=', $coursedata->effective_term_code],
                ['term_start_date', '>=', $carbon],
                ['term_code', '!=', '999999'],
                ])->get();
            $term2 = Term::where('term_code','=', $coursedata->effective_term_code)->first();
            $term3 = Term::where('term_code','=', $coursedata->end_term_code)->first();
            $college2 = College::where('college_code','=',$coursedata->college_code)->first();
            $genre2 = Course_Genre::where('course_genre_code','=',$coursedata->course_genre_code_01)->first();
            $level2 = Course_Level::where('course_level_code','=',$coursedata->course_level_code)->first();
            $subject3 = Course_Subject::where('subject_code','=',$coursedata->subject_code)->first();
            $hour = Course::where('formula','!=','')->count();
            // dd($term2);
            return view('course.edit.level', compact('subject','subject2','college','genre','level','department','endterm','effectiveterm','type','course','coursedata','hour','term2','term3','college2','genre2','level2','subject3','number','count'));
    }

    
    public function store(Request $request)
    {
        $user = Auth::user();
        $code = $request->course.$request->no;
        $request->session()->push('course2',$code);
        $newcode = session('course2');
        $newcode2 = array_slice($newcode, -1)[0];
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        // dd($coursecode2);
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        // dd($coursecode2);
        $course = Course::where('course_code', '=', $coursecode3)->first();
        $sequence2 = Course::where('course_code', '=', $coursecode3)->max('sequence');
        // dd($coursecodeold2);
        $check = Course::where('course_code','=',$code)->count();
        $sequence = $course->sequence + 1;
        // dd($code);
        if($request->eff_term == $course->effective_term_code){
            return back()->with('alert-Error','No Update Added');
        }else if($check != 0){
            return back()->with('alert-Error','The Course Code Already Exist !');
        }else if($course->course_level_code == "UG" && $request->no > 499){
            return back()->with('alert-Error','Course Number Must be Less Than 500');
        }else if($course->course_level_code == "G" && $request->no < 500){
            return back()->with('alert-Error','Course Number Must be Above Than 500');
        }else if($request->eff_term == $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else if($request->eff_term > $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else{
            DB::table('courses')->where([
                ['course_code', $coursecode3],
                ['sequence', $sequence2]
                ])->update([
                    'end_term_code' => $request->eff_term,
                    'status' => 'Inactive',
                    // dd($user->id);
            ]);

            $data = New Course();
            $data->course_code = $code;
            $data->description = $request->description;
            $data->program_code_01 = $course->program_code_01;
            $data->college_code = $request->college;
            $data->grading_mode_code = $course->grading_mode_code;
            $data->course_type_code = $course->course_type_code;
            $data->long_title = $request->long_title;
            $data->short_title = $request->short_title;
            $data->course_prerequisites = $course->course_prerequisites;
            $data->course_content = $course->course_content;
            $data->course_weekly_outline = $course->course_weekly_outline;
            $data->course_assessment = $course->course_assessment;
            $data->course_learning_outcomes = $course->course_learning_outcomes;
            $data->effective_term_code = $request->eff_term;
            $data->end_term_code = $request->end_term;
            $data->credit_hours = $course->credit_hours;
            $data->contact_hours = $course->contact_hours;
            $data->lecture_hours = $course->lecture_hours;
            $data->lab_hours = $course->lab_hours;
            $data->other_hours = $course->other_hours;
            $data->faculty_load_hours = $course->faculty_load_hours;
            $data->this_course_replace = $course->this_course_replace;
            $data->subject_code = $request->course;
            $data->course_genre_code_01 = $request->genre;
            $data->policy = $course->policy;
            $data->honor = $course->honor;
            $data->attendance = $course->attendance;
            $data->disability = $course->disability;
            $data->pedagogy = $course->pedagogy;
            $data->course_level_code = $request->level;
            $data->role_level_code = $course->role_level_code;
            $data->sequence = $sequence;
            $data->status = $course->status;
            $data->created_by = $user->id;
            $data->status = "Draft";

            $data->save();
            
            return redirect()->route('course.requisite.edit')->with('alert-Confirmation','Data Successfully Saved');
        }
        
    }
    
    public function levelsubmit(Request $request)
    {
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $newcode = session('course2');
        $newcode2 = array_slice($newcode, -1)[0];
        $sequence2 = Course::where('course_Code', '=', $newcode2)->max('sequence');
        $course = Course::where([
            ['course_code', '=', $newcode2],
            ['sequence','=',$sequence2]
            ])->first();
        // dd($request->eff_term);
        if($course->status != 'Draft'){
            return back()->with('alert-Error','Please Complete All Required Fields !');
        }else{
            DB::table('courses')->where([
                ['course_code', $newcode2],
                ['sequence','=',$sequence2],
                ])->update([
                'status' => 'Active',
                // dd($user->id);
            ]);
            session()->forget('course2');
            return redirect()->route('home.course')->with('alert-Confirmation','Application Submitted');
        }
    }

    public function requisite()
    {
        $carbon = date('Y-m-d');
        $user = Auth::user();
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $newcode = session('course2');
        $newcode2 = array_slice($newcode, -1)[0];
        $coursecode3 = substr($coursecode[$i], 0, 6);
        // dd($coursecode2);
        $course = Course::where('course_code', '=', $newcode2)->first();
        // dd($course);
            $sequence2 = Course::where('course_Code', '=', $coursecode3)->max('sequence');
        $count = Course_Replace::where([
            ['course_code', '=', $newcode2],
            ['status','=','Draft'],
        ])->count();
        // dd($newcode2);
        $term2 = Term::where('term_code','=', $course->effective_term_code)->first();
        $term3 = Term::where('term_code','=', $course->end_term_code)->first();
        $coursetitle = Course::pluck('long_title','course_code')->first();
        $courseinactive = Course::where('status', '=', 'Inactive')->get();
        $courseactive = Course::get();
        $countreplace = Course_Replace::where('created_by', '=', $user->id)->count();
        $countprerequisite = Course_Prerequisite::where('created_by', '=', $user->id)->count();
        $countcorequisite = Course_Corequisite::where('created_by', '=', $user->id)->count();
        $countequivalency = Course_Equivalency::where('created_by', '=', $user->id)->count();
        $replace = Course_Replace::where([
            ['created_by', '=', $user->id],
            ['sequence','=','1'],
            ])->first(); 
        $replace2 = Course_Replace::where([
            ['created_by', '=', $user->id],
            ['sequence','=','2'],
            ])->first();
        if($countreplace != "0"){
        $title = Course::where('course_code','=',$replace->course_code)->first();
        }else{}
        $prerequisite = Course_Prerequisite::where([
            ['created_by', '=', $user->id],
            ['sequence','=','1'],
            ])->first(); 
        $prerequisite2 = Course_Prerequisite::where([
            ['created_by', '=', $user->id],
            ['sequence','=','2'],
            ])->first();
        // dd($prerequisite2);
        if($countprerequisite != "0"){
        $title2 = Course::where('course_code','=',$prerequisite->course_code)->first();
        }else{}
        $corequisite = Course_Corequisite::where([
            ['created_by', '=', $user->id],
            ['sequence','=','1'],
            ])->first(); 
        $corequisite2 = Course_Corequisite::where([
            ['created_by', '=', $user->id],
            ['sequence','=','2'],
            ])->first();
        if($countcorequisite != "0"){
        $title3 = Course::where('course_code','=',$corequisite->course_code)->first();
        }else{}
        $equivalency = Course_Equivalency::where([
            ['created_by', '=', $user->id],
            ['sequence','=','1'],
            ])->first(); 
        $equivalency2 = Course_Equivalency::where([
            ['created_by', '=', $user->id],
            ['sequence','=','2'],
            ])->first();
        if($countequivalency != "0"){
        $title4 = Course::where('course_code','=',$equivalency->course_code)->first();
        }else{}
        $effectiveterm = Term::where([
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        $endterm = Term::where('term_start_date', '>=', $carbon)->get();
        $textbook = Course_Textbook::where('course_code','=',$course->course_code)->count();
        $additional = Course_Additional_Material::where('course_code','=',$course->course_code)->count();
        $hourdata = Course::where('created_by', '=', $user['id'])->first();
        // dd($coursetitle);
        return view('course.edit.requisite',compact('course','additional','textbook','courseinactive','courseactive','effectiveterm','endterm','coursetitle','countreplace','countprerequisite','countcorequisite','countequivalency','replace','replace2','title','prerequisite','prerequisite2','title2','corequisite','corequisite2','title3','equivalency','equivalency2','title4','courseold','count','newcode2','coursecode3','term2','term3'));
    }

    public function requisitestore(Request $request)
    {
            $user = Auth::user();
            $course = Course::where('created_by', '=', $user->id)->first();
            $term = Course::where('course_code','=',$request->idrep)->first();
            $newcode = session('course2');
            $newcode2 = array_slice($newcode, -1)[0];
            if($request->eff_term == $request->end_term){
                return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
            }else if($request->eff_term > $request->end_term){
                return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
            }else{
                $rep = new Course_Replace();
                // dd($term);
                $rep->course_replace_code = $newcode2."_R_001";
                $rep->course_code = $request->idrep;
                $rep->effective_term_code = $term->effective_term_code;
                $rep->end_term_code = $term->end_term_code;
                $rep->condition_sequence = '2';
                $rep->sequence='1';
                $rep->condition = $request->condition;
                $rep->created_by = $user->id;

                $rep->save();

                return redirect()->route('course.association.level')->with('alert-Confirmation','Data Successfully Saved');
            }

    }

    public function association()
    {
        $carbon = date('Y-m-d');
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $newcode = session('course2');
        $newcode2 = array_slice($newcode, -1)[0];
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $user = Auth::user();
        $program = Academic_Program::get();
        $course = Course::where('course_code', '=', $coursecode3)->first();
        $term2 = Term::where('term_code','=', $course->effective_term_code)->first();
        $term3 = Term::where('term_code','=', $course->end_term_code)->first();
        // dd($course);
        $count = Study_Plan::where([
            ['course_code', '=', $newcode2],
            ['status','=','Draft'],
        ])->count();
        $course3 = Course::get();
        $program3 = Academic_Program::first();
        $course2 = Course::orderBy('course_code', 'asc')->get();
        $textbook = Course_Textbook::where('course_code','=',$course->course_code)->count();
        $hourdata = Course::where('course_code', '=', $coursecode3)->first();
        $countpolicy = Course_Policy::where('course_code','=',$course->course_code)->count();
        $countspecial = Course_Special_Requirement::where('course_code','=',$course->course_code)->count();
        $countpedagogy = Pedagogy::where('course_code','=',$course->course_code)->count();
        $countoutcome = Course_Learning_Outcomes::where('course_code','=',$course->course_code)->count();
        $countmapping = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->count();
        $semester = Semester::get();
        $type = Course_Type::get();
        $plan = Academic_Program::get();
        $countstudy = Study_Plan::where('course_code','=', $course->course_code)->count();
        // dd($status);
        $endterm = Term::where([
            ['term_code','!=', $course->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ])->get();
        $effectiveterm = Term::where([
            ['term_code','!=', $course->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        $sequence = Study_Plan::where([
            ['course_code','=', $coursecode3],
            ])->max('sequence');
        $status = Academic_Program::where('program_code','=', $course->program_code_01)->first();
        $term2 = Term::where('term_code','=', $course->effective_term_code)->first();
        $term3 = Term::where('term_code','=', $course->end_term_code)->first();
            // dd($course);
        if($countstudy != 0){
        $study = Study_Plan::where('course_code','=', $coursecode3)->first();
        $study2 = Study_Plan::where([
            ['course_code','=', $newcode2],
            ])->first();
            // dd($study2);
        }else{}
        // dd($plo2);
        return view('course.edit.associationlevel',compact('course','textbook','hourdata','countpolicy','countspecial','countpedagogy','countoutcome','clo','program','plo','countmapping','mapping','mappingtitle','mappingplo','program2','plo2','semester','course2','plan','type','program3','study','study2','countstudy','status','course3','term2','term3','effectiveterm','endterm','newcourse','countstudy','newcode2','count','term2','term3'));
    }

    public function associationstore(Request $request)
    {
        $user = Auth::user();
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        // dd($coursecode2);
        $course = Course::where('course_code', '=', $coursecode3)->first();
        $sequence2 = Course::where('course_code', '=', $coursecode3)->max('sequence');
        // dd($coursecodeold2);
        $sequence = $course->sequence + 1;
        // dd($coursecode3);
        DB::table('study_plans')->where([
            ['course_code',$coursecode3],
            ['sequence', $sequence2]
            ])->update([
            'end_term_code' => $request->eff_term,
            'status' => "Inactive",
        ]);
        $study = new Study_Plan();
        // dd($term);
        $study->program_code = $request->program;
        $study->semester_code = $request->semester;
        $study->course_code = $request->course;
        $study->course_genre = $request->genre;
        $study->course_type = $request->type;
        $study->credit_hour = $request->credit;
        $study->sequence = '2';
        $study->effective_term_code = $request->eff_term;
        $study->end_term_code = $request->end_term;
        $study->created_by = $user->id;

        $study->save();

        // dd($outcome);
        return back()->with('alert-Confirmation','Data Successfully Saved, You Can Submit The Application');
    }

    public function associationsubmit(Request $request)
    {
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $newcode = session('course2');
        $newcode2 = array_slice($newcode, -1)[0];
        $sequence2 = Study_Plan::where('course_code', '=', $newcode2)->max('sequence');
        $sequence3 = Course::where('course_Code', '=', $newcode2)->max('sequence');
        $sequence4 = Course_Replace::where('course_Code', '=', $newcode2)->max('sequence');
        $course = Study_Plan::where([
            ['course_code', '=', $newcode2],
            ['sequence','=',$sequence2]
            ])->first();
        if($course->status != 'Draft'){
            return back()->with('alert-Error','Please Complete All Required Fields !');
        }else{
            DB::table('study_plans')->where([
                ['course_code', $newcode2],
                ['sequence','=',$sequence2],
                ])->update([
                'status' => 'Active',
                // dd($user->id);
            ]);
            DB::table('courses')->where([
                ['course_code', $newcode2],
                ['sequence','=',$sequence2],
                ])->update([
                'status' => 'Active',
                // dd($user->id);
            ]);
            DB::table('course_replace')->where([
                ['course_code', $newcode2],
                ['sequence','=',$sequence2],
                ])->update([
                'status' => 'Active',
                // dd($user->id);
            ]);
            session()->forget('course2');
            
            return redirect()->route('home.course')->with('alert-Confirmation','Application Submitted');
        }
    }

    public function outcome()
    {
        $user = Auth::user();
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        // dd($coursecode2);
        $program = Academic_Program::get();
        $course = Course::where('course_code', '=', $coursecode2)->first();
        $plo = Program_Learning_Outcomes::where('program_code','=','CB_BSACC')->get();
        $plo2 = Program_Learning_Outcomes::where('program_code','=','CB_BSMEN')->get();
        $textbook = Course_Textbook::where('course_code','=',$course->full_course_code)->count();
        $hourdata = Course::where('created_by', '=', $user['id'])->first();
        $countpolicy = Course_Policy::where('course_code','=',$course->course_code)->count();
        $countspecial = Course_Special_Requirement::where('course_code','=',$course->course_code)->count();
        $countpedagogy = Pedagogy::where('course_code','=',$course->course_code)->count();
        $countoutcome = Course_Learning_Outcomes::where([
            ['course_code','=',$course->course_code],
            ['status','=','Active']
            ])->count();
        $sum = Course_Learning_Outcomes::where([
            ['course_code','=',$course->course_code]
            ])->count();
        $countclo = Course_Learning_Outcomes::where('course_code','=', $coursecode2)->count();
        $countmapping = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->count();
        // dd($course->course_code);
        $clo = Course_Learning_Outcomes::where([
            ['course_code','=',$course->course_code]
            // ['status','=','Active']
            ])->get();
        $number = preg_replace("/[^0-9]{1,4}/", '', $coursecode2);
        if($countmapping != 0){
            $mapping2 = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->first();

            $mappingtitle = Academic_Program::where('program_code','=', $mapping2->program_code)->first();
            $mapping = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->get();
            $mappingplo = Program_Learning_Outcomes::where('program_code','=',$mapping2->program_code)->get();
            $data = Course_Learning_Outcomes::where('course_code','=',$course->course_code)->first();
            $program2 = Academic_Program::where('program_code','!=',$mapping2->program_code)->get();
        }else{}
            $carbon = date('Y-m-d');
            $term2 = Term::where('term_code','=', $course->effective_term_code)->first();
            $term3 = Term::where('term_code','=', $course->end_term_code)->first();
            $endterm = Term::where([
                ['term_code','!=', $course->effective_term_code],
                ['term_start_date', '>=', $carbon],
                ])->get();
            $effectiveterm = Term::where([
                ['term_code','!=', $course->effective_term_code],
                ['term_start_date', '>=', $carbon],
                ['term_code', '!=', '999999'],
                ])->get();
        // dd($plo2);
        return view('course.edit.outcomelevel',compact('course','textbook','hourdata','countpolicy','countspecial','countpedagogy','countoutcome','clo','program','plo','countmapping','mapping','mappingtitle','mappingplo','program2','plo2','endterm','effectiveterm','term2','term3','sum','coursecode2','coursenew2','number','countclo'));
    }

    public function outcomestore(Request $request)
    {
        $user = Auth::user();
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        // dd($coursecode2);
        $coursenew = session('course2');
        $coursenew2 = array_slice($coursenew, -1)[0];
        $course = Course::where('course_code', '=', $coursenew2)->first();
        $mapping = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->first();
        // if($request->text[2] != null){
            // dd(count($request->clo_code));\
            DB::table('course_learning_outcomes')->where('course_code', $coursecode2)->update([
                'end_term_code' => $course->effective_term_code,
                'status' => 'Inactive',
                // dd($user->id);
            ]);

        $outcome = [];
        for ($i = 1; $i <= count($request->clo_code); $i++) {
            $outcome[] = [
                'clo_code' => $request->clo_code[$i],
                'clo_description' => $request->text[$i],
                'course_code' => $course->course_code,
                'effective_term_code' => $course->effective_term_code,
                'end_term_code' => $course->end_term_code,
                'sequence' => '2',
                'status' => 'Active'
            ];
        }
            // dd($outcome);
        DB::table('course_learning_outcomes')->insert($outcome);
            
            return back()->with('alert-success','Data Successfully Saved');
            // echo("yes");
    }

    public function choosesubject()
    {
        $course = Course::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('course.edit.choosesubject',compact('course'));
    }

    public function subject(Request $request)
    {
        // dd($request->course);
            $subject = Course_Subject::get();
            $subject2 = Course_Subject::first();    
            $college = College::get();
            $genre = Course_Genre::get();
            $type = Course_Type::get();
            $level = Course_Level::get();
            $department = Department::get();
            $user = Auth::user();
            $request->session()->push('course',$request->course);
            $coursecode = session('course');
            $coursecode2 = array_slice($coursecode, -1)[0];
            for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
            $coursecode3 = substr($coursecode[$i], 0, 6);
            $newsubject = session('subject');
            // dd($newsubject);
            $course = Course::where('course_code', '=', $coursecode3)->count();
            $course2 = Course::where('course_code', '=', $coursecode3)->max('sequence');
            // dd($request->course);
            if($newsubject == ""){
                $sequence2 = Course::where('course_Code', '=', $coursecode3)->max('sequence');
                $coursedata = Course::where([
                    ['course_code', '=', $coursecode3],
                    ['sequence','=', $sequence2],
                    ])->first();
                $count = Course::where([
                    ['course_code', '=', $coursecode3],
                    ['status','=','Draft'],
                    ['sequence','=',$sequence2],
                ])->count();
                $countrep = Course_Replace::where([
                    ['course_code', '=', $coursecode3],
                    ['status','=','Draft'],
                ])->count();
            }else{
                $newsubject2 = array_slice($newsubject, -1)[0];
                $check = Course::where([
                    ['course_code', '=', $newsubject2],
                    ])->count();
                // dd($sequence2);
                if($check != 0){
                    $sequence2 = Course::where('course_Code', '=', $newsubject2)->max('sequence');
                    $coursedata = Course::where([
                        ['course_code', '=', $newsubject2],
                        ])->first();
                    $count = Course::where([
                        ['course_code', '=', $newsubject2],
                        ['status','=','Draft'],
                        ['sequence','=',$sequence2],
                    ])->count();
                    $countrep = Course_Replace::where([
                        ['course_code', '=', $newsubject2],
                        ['status','=','Draft'],
                    ])->count();
                }else{
                    $sequence2 = Course::where('course_Code', '=', $coursecode3)->max('sequence');
                    $coursedata = Course::where([
                        ['course_code', '=', $coursecode3],
                        ['sequence','=', $sequence2],
                        ])->first();
                    $count = Course::where([
                        ['course_code', '=', $coursecode3],
                        ['status','=','Draft'],
                        ['sequence','=',$sequence2],
                    ])->count();
                    $countrep = Course_Replace::where([
                        ['course_code', '=', $coursecode3],
                        ['status','=','Draft'],
                    ])->count();
                }
            }
            // dd($count);
            $number = preg_replace("/[^0-9]{1,4}/", '', $coursedata->course_code);
            $carbon = date('Y-m-d');
            $endterm = Term::where([
                ['term_code','!=', $coursedata->effective_term_code],
                ['term_start_date', '>=', $carbon],
                ])->get();
            $effectiveterm = Term::where([
                ['term_code','!=', $coursedata->effective_term_code],
                ['term_start_date', '>=', $carbon],
                ['term_code', '!=', '999999'],
                ])->get();
            $term2 = Term::where('term_code','=', $coursedata->effective_term_code)->first();
            $term3 = Term::where('term_code','=', $coursedata->end_term_code)->first();
            $college2 = College::where('college_code','=',$coursedata->college_code)->first();
            $genre2 = Course_Genre::where('course_genre_code','=',$coursedata->course_genre_code_01)->first();
            $level2 = Course_Level::where('course_level_code','=',$coursedata->course_level_code)->first();
            $subject3 = Course_Subject::where('subject_code','=',$coursedata->subject_code)->first();
            $hour = Course::where('formula','!=','')->count();
            // dd($term2);
            return view('course.edit.subject', compact('subject','subject2','college','genre','level','department','endterm','effectiveterm','type','course','coursedata','hour','term2','term3','college2','genre2','level2','subject3','number','count','countrep'));
    }

    public function subjectstore(Request $request)
    {
        $user = Auth::user();
        $code = $request->course.$request->no;
        $request->session()->push('subject',$code);
        $newsubject = session('subject');
        $newsubject2 = array_slice($newsubject, -1)[0];
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        // dd($coursecode2);
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        // dd($coursecode2);
        $course = Course::where('course_code', '=', $coursecode3)->first();
        $check2 = Course::where('course_code', '=', $coursecode3)->get();
        // dd($coursecodeold2);
        $check = Course::where('course_code','=',$code)->count();
        $sequence2 = Course::where('course_code', '=', $coursecode3)->max('sequence');
        $sequence = $course->sequence + 1;
        // dd($code);
        // dd($sequence);
        if($request->eff_term == $course->effective_term_code){
            return back()->with('alert-Error','Please Select New Effective Term');
        }else if($request->eff_term == $check2[0]->effective_term_code){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else if($request->eff_term == $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else if($check != 0){
            return back()->with('alert-Error','The Course Code Already Exist');
        }else if($request->eff_term > $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else{

            $data = New Course();
            $data->course_code = $code;
            $data->description = $request->description;
            $data->program_code_01 = $course->program_code_01;
            $data->college_code = $request->college;
            $data->grading_mode_code = $course->grading_mode_code;
            $data->course_type_code = $course->course_type_code;
            $data->long_title = $request->long_title;
            $data->short_title = $request->short_title;
            $data->course_prerequisites = $course->course_prerequisites;
            $data->course_content = $course->course_content;
            $data->course_weekly_outline = $course->course_weekly_outline;
            $data->course_assessment = $course->course_assessment;
            $data->course_learning_outcomes = $course->course_learning_outcomes;
            $data->effective_term_code = $request->eff_term;
            $data->end_term_code = $request->end_term;
            $data->credit_hours = $course->credit_hours;
            $data->contact_hours = $course->contact_hours;
            $data->lecture_hours = $course->lecture_hours;
            $data->lab_hours = $course->lab_hours;
            $data->other_hours = $course->other_hours;
            $data->faculty_load_hours = $course->faculty_load_hours;
            $data->this_course_replace = $course->this_course_replace;
            $data->subject_code = $request->course;
            $data->course_genre_code_01 = $request->genre;
            $data->policy = $course->policy;
            $data->honor = $course->honor;
            $data->attendance = $course->attendance;
            $data->disability = $course->disability;
            $data->pedagogy = $course->pedagogy;
            $data->course_level_code = $request->level;
            $data->role_level_code = $course->role_level_code;
            $data->sequence = $sequence;
            $data->status = $course->status;
            $data->created_by = $user->id;
            $data->status = "Draft";

            $data->save();
            
            return redirect()->route('course.requisitesubject.edit')->with('alert-Confirmation','Data Successfully Saved');
        }
        
    }
    
    public function subjectsubmit(Request $request)
    {
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $newsubject = session('subject');
        $newsubject2 = array_slice($newsubject, -1)[0];
        $sequence2 = Course::where('course_Code', '=', $newsubject2)->max('sequence');
        $course = Course::where([
            ['course_code', '=', $newsubject2],
            ['sequence','=',$sequence2]
            ])->first();
        // dd($request->eff_term);
        if($course->status != 'Draft'){
            return back()->with('alert-Error','Please Complete All Required Fields !');
        }else{
            DB::table('courses')->where([
                ['course_code', $newsubject2],
                ['sequence','=',$sequence2],
                ])->update([
                'status' => 'Pending',
                // dd($user->id);
            ]);
            session()->forget('subject');
            return redirect()->route('home.course')->with('alert-Confirmation','Application Submitted');
        }
    }

    public function subjectrequisite()
    {
        $carbon = date('Y-m-d');
        $user = Auth::user();
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $newsubject = session('subject');
        $newsubject2 = array_slice($newsubject, -1)[0];
        $coursecode3 = substr($coursecode[$i], 0, 6);
        // dd($coursecode2);
        $course = Course::where('course_code', '=', $newsubject2)->first();
        // dd($course);
            $sequence2 = Course::where('course_Code', '=', $coursecode3)->max('sequence');
        $count = Course_Replace::where([
            ['course_code', '=', $newsubject2],
            ['status','=','Draft'],
        ])->count();
        // dd($newcode2);
        $term2 = Term::where('term_code','=', $course->effective_term_code)->first();
        $term3 = Term::where('term_code','=', $course->end_term_code)->first();
        $coursetitle = Course::pluck('long_title','course_code')->first();
        $courseinactive = Course::where('status', '=', 'Inactive')->get();
        $courseactive = Course::get();
        $countreplace = Course_Replace::where('created_by', '=', $user->id)->count();
        $countprerequisite = Course_Prerequisite::where('created_by', '=', $user->id)->count();
        $countcorequisite = Course_Corequisite::where('created_by', '=', $user->id)->count();
        $countequivalency = Course_Equivalency::where('created_by', '=', $user->id)->count();
        $replace = Course_Replace::where([
            ['created_by', '=', $user->id],
            ['sequence','=','1'],
            ])->first(); 
        $replace2 = Course_Replace::where([
            ['created_by', '=', $user->id],
            ['sequence','=','2'],
            ])->first();
        if($countreplace != "0"){
        $title = Course::where('course_code','=',$replace->course_code)->first();
        }else{}
        $prerequisite = Course_Prerequisite::where([
            ['created_by', '=', $user->id],
            ['sequence','=','1'],
            ])->first(); 
        $prerequisite2 = Course_Prerequisite::where([
            ['created_by', '=', $user->id],
            ['sequence','=','2'],
            ])->first();
        // dd($prerequisite2);
        if($countprerequisite != "0"){
        $title2 = Course::where('course_code','=',$prerequisite->course_code)->first();
        }else{}
        $corequisite = Course_Corequisite::where([
            ['created_by', '=', $user->id],
            ['sequence','=','1'],
            ])->first(); 
        $corequisite2 = Course_Corequisite::where([
            ['created_by', '=', $user->id],
            ['sequence','=','2'],
            ])->first();
        if($countcorequisite != "0"){
        $title3 = Course::where('course_code','=',$corequisite->course_code)->first();
        }else{}
        $equivalency = Course_Equivalency::where([
            ['created_by', '=', $user->id],
            ['sequence','=','1'],
            ])->first(); 
        $equivalency2 = Course_Equivalency::where([
            ['created_by', '=', $user->id],
            ['sequence','=','2'],
            ])->first();
        if($countequivalency != "0"){
        $title4 = Course::where('course_code','=',$equivalency->course_code)->first();
        }else{}
        $effectiveterm = Term::where([
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        $endterm = Term::where('term_start_date', '>=', $carbon)->get();
        $textbook = Course_Textbook::where('course_code','=',$course->course_code)->count();
        $additional = Course_Additional_Material::where('course_code','=',$course->course_code)->count();
        $hourdata = Course::where('created_by', '=', $user['id'])->first();
        // dd($coursetitle);
        return view('course.edit.replacesubject',compact('course','additional','textbook','courseinactive','courseactive','effectiveterm','endterm','coursetitle','countreplace','countprerequisite','countcorequisite','countequivalency','replace','replace2','title','prerequisite','prerequisite2','title2','corequisite','corequisite2','title3','equivalency','equivalency2','title4','courseold','count','newsubject2','coursecode3','term2','term3'));
    }

    public function subjectrequisitestore(Request $request)
    {
            $user = Auth::user();
            $course = Course::where('created_by', '=', $user->id)->first();
            $term = Course::where('course_code','=',$request->idrep)->first();
            $rep = new Course_Replace();
            // dd($term);
            $rep->course_replace_code = $request->idrep1;
            $rep->course_code = $request->idrep;
            $rep->effective_term_code = $term->effective_term_code;
            $rep->end_term_code = '999999';
            $rep->condition_sequence = '2';
            $rep->sequence='1';
            $rep->condition = $request->condition;
            $rep->created_by = $user->id;

            $rep->save();

            return redirect()->route('course.subjectassociation.level')->with('alert-Confirmation','Data Successfully Saved');

    }

    public function subjectassociation()
    {
        $carbon = date('Y-m-d');
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $newcode = session('course2');
        $newcode2 = array_slice($newcode, -1)[0];
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $user = Auth::user();
        $program = Academic_Program::get();
        $course = Course::where('course_code', '=', $coursecode3)->first();
        $term2 = Term::where('term_code','=', $course->effective_term_code)->first();
        $term3 = Term::where('term_code','=', $course->end_term_code)->first();
        // dd($course);
        $count = Study_Plan::where([
            ['course_code', '=', $newcode2],
            ['status','=','Draft'],
        ])->count();
        $course3 = Course::get();
        $program3 = Academic_Program::first();
        $course2 = Course::orderBy('course_code', 'asc')->get();
        $textbook = Course_Textbook::where('course_code','=',$course->course_code)->count();
        $hourdata = Course::where('course_code', '=', $coursecode3)->first();
        $countpolicy = Course_Policy::where('course_code','=',$course->course_code)->count();
        $countspecial = Course_Special_Requirement::where('course_code','=',$course->course_code)->count();
        $countpedagogy = Pedagogy::where('course_code','=',$course->course_code)->count();
        $countoutcome = Course_Learning_Outcomes::where('course_code','=',$course->course_code)->count();
        $countmapping = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->count();
        $semester = Semester::get();
        $type = Course_Type::get();
        $plan = Academic_Program::get();
        $countstudy = Study_Plan::where('course_code','=', $course->course_code)->count();
        // dd($status);
        $endterm = Term::where([
            ['term_code','!=', $course->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ])->get();
        $effectiveterm = Term::where([
            ['term_code','!=', $course->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        $sequence = Study_Plan::where([
            ['course_code','=', $coursecode3],
            ])->max('sequence');
        $status = Academic_Program::where('program_code','=', $course->program_code_01)->first();
        $term2 = Term::where('term_code','=', $course->effective_term_code)->first();
        $term3 = Term::where('term_code','=', $course->end_term_code)->first();
            // dd($course);
        if($countstudy != 0){
        $study = Study_Plan::where('course_code','=', $coursecode3)->first();
        $study2 = Study_Plan::where([
            ['course_code','=', $newcode2],
            ])->first();
            // dd($study2);
        }else{}
        return view('course.edit.associationsubject',compact('course','textbook','hourdata','countpolicy','countspecial','countpedagogy','countoutcome','clo','program','plo','countmapping','mapping','mappingtitle','mappingplo','program2','plo2','semester','course2','plan','type','program3','study','study2','countstudy','status','course3','term2','term3','effectiveterm','endterm','newcourse','countstudy','newcode2','count','term2','term3'));
    }

    public function subjectassociationstore(Request $request)
    {
        $user = Auth::user();
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        // dd($coursecode2);
        $course = Course::where('course_code', '=', $coursecode3)->first();
        // dd($coursecodeold2);
        $sequence = $course->sequence + 1;
        // dd($coursecode3);
        if($request->eff_term == $course->effective_term_code){
            return back()->with('alert-Error','No Update Added');
        }else{
        $study = new Study_Plan();
        // dd($term);
        $study->program_code = $request->program;
        $study->semester_code = $request->semester;
        $study->course_code = $request->course;
        $study->course_genre = $request->genre;
        $study->course_type = $request->type;
        $study->credit_hour = $request->credit;
        $study->sequence = '2';
        $study->effective_term_code = $request->eff_term;
        $study->end_term_code = $request->end_term;
        $study->created_by = $user->id;

        $study->save();

        // dd($outcome);
        return back()->with('alert-Confirmation','Data Successfully Saved, You Can Submit The Application');
        }
    }

    public function subjectassociationsubmit(Request $request)
    {
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $newcode = session('course2');
        $newcode2 = array_slice($newcode, -1)[0];
        $sequence2 = Study_Plan::where('course_code', '=', $newcode2)->max('sequence');
        $course = Study_Plan::where([
            ['course_code', '=', $newcode2],
            ['sequence','=',$sequence2]
            ])->first();
        if($course->status != 'Draft'){
            return back()->with('alert-Error','Please Complete All Required Fields !');
        }else{
            DB::table('study_plans')->where([
                ['course_code', $newcode2],
                ['sequence','=',$sequence2],
                ])->update([
                'status' => 'Pending',
                // dd($user->id);
            ]);
            DB::table('courses')->where([
                ['course_code', $newsubject2],
                ['sequence','=',$sequence2],
                ])->update([
                'status' => 'Pending',
                // dd($user->id);
            ]);
            session()->forget('subject');
            
            return redirect()->route('home.course')->with('alert-Confirmation','Application Submitted');
        }
    }

    public function choosetitle()
    {
        $course = Course::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('course.edit.choosetitle',compact('course'));
    }

    public function title(Request $request)
    {
            $subject = Course_Subject::get();
            $subject2 = Course_Subject::first();    
            $college = College::get();
            $genre = Course_Genre::get();
            $type = Course_Type::get();
            $level = Course_Level::get();
            $department = Department::get();
            $carbon = date('Y-m-d');
            $user = Auth::user();
            $request->session()->push('course',$request->course);
            $coursecode = session('course');
            $coursecode2 = array_slice($coursecode, -1)[0];
            for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
            $coursecode3 = substr($coursecode[$i], 0, 6);
            $course = Course::where('course_code', '=', $coursecode3)->count();
            $course2 = Course::where('course_code', '=', $coursecode3)->max('sequence');
            // dd($request->course);
            $sequence2 = Course::where('course_Code', '=', $coursecode3)->max('sequence');
            $coursedata = Course::where([
                ['course_code', '=', $coursecode3],
                ['sequence','=', $sequence2],
                ])->first();
            $count = Course::where([
                ['course_code', '=', $coursecode3],
                ['status','=','Draft'],
                ['sequence','=',$sequence2],
            ])->count();
            $number = preg_replace("/[^0-9]{1,4}/", '', $coursedata->course_code);
            $endterm = Term::where([
                ['term_code','!=', $coursedata->effective_term_code],
                ['term_start_date', '>=', $carbon],
                ])->get();
            $effectiveterm = Term::where([
                ['term_code','!=', $coursedata->effective_term_code],
                ['term_start_date', '>=', $carbon],
                ['term_code', '!=', '999999'],
                ])->get();
            $college2 = College::where('college_code','=',$coursedata->college_code)->first();
            $genre2 = Course_Genre::where('course_genre_code','=',$coursedata->course_genre_code_01)->first();
            $level2 = Course_Level::where('course_level_code','=',$coursedata->course_level_code)->first();
            $subject3 = Course_Subject::where('subject_code','=',$coursedata->subject_code)->first();
            $hour = Course::where('formula','!=','')->count();
            $term2 = Term::where('term_code','=', $coursedata->effective_term_code)->first();
            $term3 = Term::where('term_code','=', $coursedata->end_term_code)->first();
            // dd($term2);
            return view('course.edit.title', compact('subject','subject2','college','genre','level','department','endterm','effectiveterm','type','course','coursedata','hour','term2','term3','college2','genre2','level2','subject3','number','count'));
    }

    public function titlestore(Request $request)
    {
        $user = Auth::user();
        $course2 = Course::get();
        $code = $request->course.$request->no;
        $request->session()->push('course2',$code);
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        // dd($coursecode2);
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $course = Course::where('course_code', '=', $coursecode3)->first();
        // dd($coursecodeold2);
        $sequence2 = Course::where('course_Code', '=', $coursecode3)->max('sequence');
        $sequence = $sequence2 + 1;
        if($request->eff_term == $course->effective_term_code){
            return back()->with('alert-Error','No Update Added');
        }else if($request->eff_term == $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else if($request->eff_term > $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else{
            DB::table('courses')->where([
                ['course_code', $coursecode3],
                ['sequence', $sequence2]
                ])->update([
                'end_term_code' => $request->eff_term,
                'status' => 'Inactive',
            ]);

            $data = New Course();
            $data->course_code = $code;
            $data->description = $request->description;
            $data->program_code_01 = $course->program_code_01;
            $data->college_code = $request->college;
            $data->grading_mode_code = $course->grading_mode_code;
            $data->course_type_code = $course->course_type_code;
            $data->long_title = $request->long_title;
            $data->short_title = $request->short_title;
            $data->course_prerequisites = $course->course_prerequisites;
            $data->course_content = $course->course_content;
            $data->course_weekly_outline = $course->course_weekly_outline;
            $data->course_assessment = $course->course_assessment;
            $data->course_learning_outcomes = $course->course_learning_outcomes;
            $data->effective_term_code = $request->eff_term;
            $data->end_term_code = $request->end_term;
            $data->credit_hours = $course->credit_hours;
            $data->contact_hours = $course->contact_hours;
            $data->lecture_hours = $course->lecture_hours;
            $data->lab_hours = $course->lab_hours;
            $data->other_hours = $course->other_hours;
            $data->faculty_load_hours = $course->faculty_load_hours;
            $data->this_course_replace = $course->this_course_replace;
            $data->subject_code = $request->course;
            $data->course_genre_code_01 = $request->genre;
            $data->policy = $course->policy;
            $data->honor = $course->honor;
            $data->attendance = $course->attendance;
            $data->disability = $course->disability;
            $data->pedagogy = $course->pedagogy;
            $data->course_level_code = $request->level;
            $data->role_level_code = $course->role_level_code;
            $data->sequence = $sequence;
            $data->created_by = $user->id;
            $data->status = 'Draft';

            $data->save();
            return back()->with('alert-Confirmation','Data Successfully Saved, You Can Submit The Application');
        }
    }

    public function titlesubmit(Request $request)
    {
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $sequence2 = Course::where('course_Code', '=', $coursecode3)->max('sequence');
        $course = Course::where([
            ['course_code', '=', $coursecode3],
            ['sequence','=',$sequence2]
            ])->first();
        // dd($request->eff_term);
        if($course->status != 'Draft'){
            return back()->with('alert-Error','Please Complete All Required Fields !');
        }else{
            DB::table('courses')->where([
                ['course_code', $coursecode3],
                ['sequence','=',$sequence2],
                ])->update([
                'status' => 'Active',
                // dd($user->id);
            ]);
            
            return redirect()->route('home.course')->with('alert-Confirmation','Application Submitted');
        }
    }
    
    public function chooseassessment()
    {
        $course = Course::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('course.edit.chooseassessment',compact('course'));
    }

    public function assessment(Request $request)
    {
        $carbon = date('Y-m-d');
        $user = Auth::user();
        $request->session()->push('course',$request->course);
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        // dd($coursecode3);
        $course = Course::where('course_code', '=', $coursecode3)->first();
        $carbon = date('Y-m-d');
        $endterm = Term::where([
            ['term_code','!=', $course->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ])->get();
        $effectiveterm = Term::where([
            ['term_code','!=', $course->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        $term2 = Term::where('term_code','=', $course->effective_term_code)->first();
        $term3 = Term::where('term_code','=', $course->end_term_code)->first();
        
        // dd($course);
        $coursetitle = Course::pluck('long_title','course_code')->first();
        $courseinactive = Course::where('status', '=', 'Inactive')->get();
        $courseactive = Course::get();
        $countreplace = Course_Replace::where('created_by', '=', $user->id)->count();
        $countprerequisite = Course_Prerequisite::where('created_by', '=', $user->id)->count();
        $countcorequisite = Course_Corequisite::where('created_by', '=', $user->id)->count();
        $countequivalency = Course_Equivalency::where('created_by', '=', $user->id)->count();
        $countcontent = Course_Content::where('course_code','=', $course->course_code)->count();
        $countweekly = Course_Weekly_Outline::where('course_code','=', $course->course_code)->count();
        $countassessment = Course_Assessment::where('course_code','=', $course->course_code)->count();
        $assessment = Course_Assessment::where('course_code','=', $course->course_code)->get();
        // dd($assessment);
        $plo = PLO_CLO_Mapping::get();
        // dd($coursetitle);
        return view('course.edit.assessment',compact('course','additional','textbook','courseinactive','courseactive','effectiveterm','endterm','coursetitle','countreplace','countprerequisite','countcorequisite','countequivalency','replace','replace2','title','prerequisite','prerequisite2','title2','corequisite','corequisite2','title3','equivalency','equivalency2','title4','countcontent','countweekly','countassessment','plo','assessment','endterm','effterm','term2','term3','coursecode2','coursecode3'));
    }

    public function assessmentstore(Request $request)
    {
            $coursecode = session('course');
            $coursecode2 = array_slice($coursecode, -1)[0];        
            for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
            $coursecode3 = substr($coursecode[$i], 0, 6);
            $course = Course::where('course_code', '=', $coursecode3)->first();
            if($request->weight > 1000){
                return back()->with('alert-Error','Weight Total Must be 1000');
            }else if($request->eff_term == $course->effective_term_code){
                return back()->with('alert-Error','No Update Added');
            }else if($request->eff_term == $request->end_term){
                return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
            }else if($request->eff_term > $request->end_term){
                return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
            }else{
                $user = Auth::user();
                $code = $course->course_code.'AM'.$request->idassessment;
                $assessment = Course_Assessment::where('course_code','=', $course->course_code)->count();
                if($assessment == 0){
                    $co = new Course_Assessment();
                    $co->course_assessment_code = $course->course_code.'AM'.$request->idassessment;
                    $co->course_code = $course->course_code;
                    $co->course_assessment_type = $request->type;
                    $co->course_assessment_description = $request->description;
                    $co->weight = $request->weight;
                    $co->mapping = $request->mapping;
                    $co->category = $request->category;
                    $co->effective_term_code = $request->effective_term_code;
                    $co->end_term_code = $course->end_term_code;
                    $co->sequence = "2";

                    $co->save();
                    return back()->with('alert-success','Data Successfully Saved, You Can Submit The Application');
                }else{
                    $co = new Course_Assessment();
                    $co->course_assessment_code = $request->idassessment;
                    $co->course_code = $course->course_code;
                    $co->course_assessment_type = $request->type;
                    $co->course_assessment_description = $request->description;
                    $co->weight = $request->weight;
                    $co->mapping = $request->mapping;
                    $co->category = $request->category;
                    $co->effective_term_code = $course->effective_term_code;
                    $co->end_term_code = $course->end_term_code;
                    $co->sequence = "2";

                    $co->save();
                    return back()->with('alert-success','Data Successfully Saved, You Can Submit The Application');
                }
            }
    }

    public function choosedelivery()
    {
        $program = Academic_Program::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('course.edit.choosedelivery',compact('program'));
    }

    public function delivery(Request $request)
    {
        $user = Auth::user();
        $request->session()->push('course',$request->course);
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $program = Academic_Program::where('program_code', '=', $coursecode3)->first();
        
        $delivery = Delivery_Mode::get();
        $language = Instructional_Language::get();
        
        return view('course.edit.delivery', compact('delivery','language'));
        
    }

    public function choosegrading()
    {
        $course = Course::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('course.edit.choosegrading',compact('course'));
    }

    public function grading(Request $request)
    {
        $user = Auth::user();
        $request->session()->push('course',$request->course);
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        // dd($request->course);
        $sequence2 = Course::where('course_Code', '=', $coursecode3)->max('sequence');
        $course = Course::where([
            ['course_code', '=', $coursecode3],
            ['sequence','=', $sequence2],
            ])->first();
        $count = Course::where([
            ['course_code', '=', $coursecode3],
            ['status','=','Draft'],
            ['sequence','=',$sequence2],
        ])->count();
        // dd($count);
        $grading = Grading_Mode::where('grading_mode_code','!=',$course->grading_mode_code)->get();  
        $countgrading = Course::where([
            ['course_code', '=', $course->course_code],
            ['grading_mode_code','!=','']
            ])->count();
        $grading2 = Course::where([
            ['course_code', '=', $course->course_code],
            ['grading_mode_code','!=','']
            ])->first();
        $carbon = date('Y-m-d');
        $endterm = Term::where([
            ['term_code','!=', $course->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ])->get();
        $effectiveterm = Term::where([
            ['term_code','!=', $course->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        $term2 = Term::where('term_code','=', $course->effective_term_code)->first();
        $term3 = Term::where('term_code','=', $course->end_term_code)->first();
        $schema = Grading_Schema_Detail::where('grading_mode_code','=',$grading2->grading_mode_code)->get();
        $schema1 = Grading_Schema_Detail::where('grading_mode_code','=','ULET')->get();
        $schema2 = Grading_Schema_Detail::where('grading_mode_code','=','UPAF')->get();
        $schema3 = Grading_Schema_Detail::where('grading_mode_code','=','GLET')->get();
        return view('course.edit.grading',compact('grading','schema1','schema2','schema3','countgrading','grading2','schema','coursecode2','endterm','effectiveterm','term2','term3','course','count'));
    }

    public function gradingstore(Request $request){
        
        $user = Auth::user();
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $course = Course::where('course_code', '=', $coursecode3)->first();
        $sequence2 = Course::where('course_Code', '=', $coursecode3)->max('sequence');
        $sequence = $sequence2 + 1;
        // dd($request->effterm);
        if($request->eff_term == $course->effective_term_code){
            return back()->with('alert-Error','No Update Added');
        }else if($request->eff_term == $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else if($request->eff_term > $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else{
            DB::table('courses')->where([
                ['course_code', $coursecode3],
                ['sequence', $sequence2]
                ])->update([
                'end_term_code' => $request->eff_term,
                'status' => 'Inactive',
                // dd($user->id);
            ]);

            $data = New Course();
            $data->course_code = $course->course_code;
            $data->description = $course->description;
            $data->program_code_01 = $course->program_code_01;
            $data->college_code = $course->college_code;
            $data->grading_mode_code = $request->grading;
            $data->course_type_code = $course->course_type_code;
            $data->long_title = $course->long_title;
            $data->short_title = $course->short_title;
            $data->course_prerequisites = $course->course_prerequisites;
            $data->course_content = $course->course_content;
            $data->course_weekly_outline = $course->course_weekly_outline;
            $data->course_assessment = $course->course_assessment;
            $data->course_learning_outcomes = $course->course_learning_outcomes;
            $data->effective_term_code = $request->eff_term;
            $data->end_term_code = $request->end_term;
            $data->credit_hours = $course->credit_hours;
            $data->contact_hours = $course->contact_hours;
            $data->lecture_hours = $course->lecture_hours;
            $data->lab_hours = $course->lab_hours;
            $data->other_hours = $course->other_hours;
            $data->faculty_load_hours = $course->faculty_load_hours;
            $data->this_course_replace = $course->this_course_replace;
            $data->subject_code = $course->subject_code;
            $data->course_genre_code_01 = $course->course_genre_code_01;
            $data->policy = $course->policy;
            $data->honor = $course->honor;
            $data->attendance = $course->attendance;
            $data->disability = $course->disability;
            $data->pedagogy = $course->pedagogy;
            $data->course_level_code = $course->course_level_code;
            $data->role_level_code = $course->role_level_code;
            $data->sequence = $sequence;
            $data->created_by = $user->id;

            $data->save();
            return back()->with('alert-Confirmation','Data Successfully Saved, You Can Submit The Application');
        }
    }

    public function gradingsubmit(Request $request)
    {
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $sequence2 = Course::where('course_code', '=', $coursecode3)->max('sequence');
        $course = Course::where([
            ['course_code', '=', $coursecode3],
            ['sequence','=',$sequence2]
            ])->first();
        if($course->status != 'Draft'){
            return back()->with('alert-Error','Please Complete All Required Fields !');
        }else{

            DB::table('courses')->where([
                ['course_code', $coursecode3],
                ['sequence','=',$sequence2]
                ])->update([
                'status' => 'Active',
                // dd($user->id);
            ]);
            
            return redirect()->route('home.course')->with('alert-Confirmation','Application Submitted');
        }
    }

    public function choosetype()
    {
        $course = Course::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('course.edit.choosetype',compact('course'));
    }

    public function type(Request $request)
    {
            $subject = Course_Subject::get();
            $subject2 = Course_Subject::first();    
            $college = College::get();
            $genre = Course_Genre::get();
            $type = Course_Type::get();
            $level = Course_Level::get();
            $department = Department::get();
            $user = Auth::user();
            $request->session()->push('course',$request->course);
            $coursecode = session('course');
            $coursecode2 = array_slice($coursecode, -1)[0];
            for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
            $coursecode3 = substr($coursecode[$i], 0, 6);    
            $sequence2 = Course::where('course_Code', '=', $coursecode3)->max('sequence');
            $count = Course::where([
                ['course_code', '=', $coursecode3],
                ['status','=','Draft'],
                ['sequence','=',$sequence2],
            ])->count();
            $course = Course::where('course_code', '=', $coursecode3)->count();
            $course2 = Course::where('course_code', '=', $coursecode3)->max('sequence');
            // dd($course2);    
            $coursedata = Course::where([
                ['course_code', '=', $coursecode3],
                ['sequence','=',$course2]
                ])->first();
            $number = preg_replace("/[^0-9]{1,4}/", '', $coursedata->course_code);
            $carbon = date('Y-m-d');
            $endterm = Term::where([
                ['term_code','!=', $coursedata->effective_term_code],
                ['term_start_date', '>=', $carbon],
                ])->get();
            $effectiveterm = Term::where([
                ['term_code','!=', $coursedata->effective_term_code],
                ['term_start_date', '>=', $carbon],
                ['term_code', '!=', '999999'],
                ])->get();
            $term2 = Term::where('term_code','=', $coursedata->effective_term_code)->first();
            $term3 = Term::where('term_code','=', $coursedata->end_term_code)->first();
            $college2 = College::where('college_code','=',$coursedata->college_code)->first();
            $genre2 = Course_Genre::where('course_genre_code','=',$coursedata->course_genre_code_01)->first();
            $level2 = Course_Level::where('course_level_code','=',$coursedata->course_level_code)->first();
            $subject3 = Course_Subject::where('subject_code','=',$coursedata->subject_code)->first();
            $hour = Course::where('formula','!=','')->count();
            // dd($term2);
            return view('course.edit.type', compact('subject','subject2','college','genre','level','department','endterm','effectiveterm','type','course','coursedata','hour','term2','term3','college2','genre2','level2','subject3','number','count'));
    }

    public function typestore(Request $request)
    {
        $user = Auth::user();
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $course = Course::where('course_code', '=', $coursecode3)->first();
        $sequence2 = Course::where('course_Code', '=', $coursecode3)->max('sequence');
        // $check = Course::where([
        //     ['course_code', $coursecode3],
        //     ['sequence', $sequence],
        //     ])->count();
        // dd($check);
        $sequence = $sequence2 + 1;
        // dd($request->effterm);
        if($request->eff_term == $course->effective_term_code){
            return back()->with('alert-Error','No Update Added');
        }else if($request->eff_term == $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else if($request->eff_term > $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else{
        // dd($code);
        DB::table('courses')->where([
            ['course_code', $coursecode3],
            ['sequence', $sequence2],
            ])->update([
                'end_term_code' => $request->eff_term,
                'status' => 'Inactive',
                // dd($user->id);
        ]);
        $data = New Course();
        $data->course_code = $course->course_code;
        $data->description = $request->description;
        $data->program_code_01 = $course->program_code_01;
        $data->college_code = $request->college;
        $data->grading_mode_code = $course->grading_mode_code;
        $data->course_type_code = $course->course_type_code;
        $data->long_title = $request->long_title;
        $data->short_title = $request->short_title;
        $data->course_prerequisites = $course->course_prerequisites;
        $data->course_content = $course->course_content;
        $data->course_weekly_outline = $course->course_weekly_outline;
        $data->course_assessment = $course->course_assessment;
        $data->course_learning_outcomes = $course->course_learning_outcomes;
        $data->effective_term_code = $request->eff_term;
        $data->end_term_code = $request->end_term;
        $data->credit_hours = $course->credit_hours;
        $data->contact_hours = $course->contact_hours;
        $data->lecture_hours = $course->lecture_hours;
        $data->lab_hours = $course->lab_hours;
        $data->other_hours = $course->other_hours;
        $data->faculty_load_hours = $course->faculty_load_hours;
        $data->this_course_replace = $course->this_course_replace;
        $data->subject_code = $request->course;
        $data->course_genre_code_01 = $request->genre;
        $data->policy = $course->policy;
        $data->honor = $course->honor;
        $data->attendance = $course->attendance;
        $data->disability = $course->disability;
        $data->pedagogy = $course->pedagogy;
        $data->course_level_code = $request->level;
        $data->role_level_code = $course->role_level_code;
        $data->sequence = $sequence;
        $data->status = $course->status;
        $data->created_by = $user->id;
        $data->status = 'Draft';

        $data->save();
        
        return back()->with('alert-Confirmation','Save Successfully, You Can Submit The Application');
        }
    }
    
    public function typesubmit(Request $request)
    {
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $sequence2 = Course::where('course_code', '=', $coursecode3)->max('sequence');
        $course = Course::where([
            ['course_code', '=', $coursecode3],
            ['sequence','=',$sequence2]
            ])->first();
        if($course->status != 'Draft'){
            return back()->with('alert-Error','Please Complete All Required Fields !');
        }else{

            DB::table('courses')->where([
                ['course_code', $coursecode3],
                ['sequence','=',$sequence2]
                ])->update([
                'status' => 'Active',
                // dd($user->id);
            ]);
            
            return redirect()->route('home.course')->with('alert-Confirmation','Application Submitted');
        }
    }

    public function choosetextbook()
    {
        $course = Course::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('course.edit.choosetextbook',compact('course'));
    }

    public function textbook(Request $request)
    {
        $user = Auth::user();
        $request->session()->push('course',$request->course);
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $course = Course::where('course_code', '=', $coursecode3)->first();
        $textbook = Course_Textbook::where('course_code','=',$course->course_code)->count();
        $textbook2 = Course_Textbook::where('course_code','=',$course->course_code)->get();
        // dd($textbook);
        $hourdata = Course::where('created_by', '=', $user['id'])->first();
        $carbon = date('Y-m-d');
        $sequence = Course_Textbook::where('course_code','=',$course->course_code)->max('sequence');
        $count = Course_Textbook::where([
            ['course_code', '=', $coursecode3],
            ['status','=','Draft'],
            ['sequence','=',$sequence],
        ])->count();
        $endterm = Term::where([
            ['term_start_date', '>=', $carbon],
            ])->get();
        $effectiveterm = Term::where([
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        $term2 = Term::where('term_code','=', $course->effective_term_code)->first();
        $term3 = Term::where('term_code','=', $course->end_term_code)->first();
        // dd($textbook);
        return view('course.edit.textbook',compact('course','textbook','hourdata','textbook2','term2','term3','effectiveterm','endterm','count'));
    }

    public function textbookstore(Request $request)
    {
        $user = Auth::user();
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $course = Course::where('course_code', '=', $coursecode3)->first();
        $sequnce = Course_Textbook::where('course_code','=',$course->course_code)->first();
        $sequence = Course_Textbook::where('course_code','=',$course->course_code)->max('sequence');
        $sequence2 = $sequnce['sequence'] + 1;

        
        if($request->eff_term == $course->effective_term_code){
            return back()->with('alert-Error','No Update Added');
        }else if($request->eff_term == $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else if($request->eff_term > $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else{
            DB::table('course_textbooks')->where([
                ['course_code', $coursecode3],
                ['sequence', $sequence]
                ])->update([
                'end_term_code' => $request->effective_term_code,
                'status' => 'Inactive',
                // dd($user->id);
            ]);

            $data = New Course_Textbook();
            $data->course_textbook_code = $course->course_code.'_TB_'.$sequence2;
            $data->textbook = $request->title;
            $data->publisher = $request->publisher;
            $data->author = $request->author;
            $data->isbn = $request->isbn;
            $data->publication_type = $request->publication;
            $data->year = $request->year;
            $data->course_code = $course->course_code;
            $data->effective_term_code = $request->eff_term;
            $data->end_term_code = $request->end_term;

            $data->save();
            
            return back()->with('alert-Confirmation','Data Successfully Saved, You Can Submit The Application');
        }
    }

    public function textbooksubmit(Request $request)
    {
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $sequence2 = Course_Textbook::where('course_code', '=', $coursecode3)->max('sequence');
        $course = Course_Textbook::where([
            ['course_code', '=', $coursecode3],
            ['sequence','=',$sequence2]
            ])->first();
        if($course->status != 'Draft'){
            return back()->with('alert-Error','Please Complete All Required Fields !');
        }else{

            DB::table('course_textbooks')->where([
                ['course_code', $coursecode3],
                ['sequence','=',$sequence2]
                ])->update([
                'status' => 'Active',
                // dd($user->id);
            ]);
            
            return redirect()->route('home.course')->with('alert-Confirmation','Application Submitted');
        }
    }

    public function chooseequivalency()
    {
        $course = Course::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('course.edit.chooseequivalency',compact('course'));
    }

    
    public function equivalency(Request $request)
    {
        $carbon = date('Y-m-d');
        $user = Auth::user();
        $request->session()->push('course',$request->course);
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        // dd($coursecode2);
        $course = Course::where('course_code', '=', $coursecode3)->first();
        // dd($course);
        $coursetitle = Course::pluck('long_title','course_code')->first();
        $course2 = Course::where([
            ['course_code', '!=', $coursecode3],
            ['status','=','Active'],
            ])->orderBy('course_code', 'ASC')->get();
        // dd($course2);
        $courseactive = Course::where('status','=','Active')->get();
        $countequivalency = Course_Equivalency::where('course_code', '=', $coursecode3)->count();
        $sequence2 = Course_Equivalency::where('course_code', '=', $coursecode3)->max('sequence');
        $equivalency = Course_Equivalency::where([
            ['course_code', '=', $coursecode3],
            ['sequence','=', $sequence2]
            ])->get();
            
        $equicode = substr($equivalency[0]->course_equivalency_code, 0, 6);
        // dd($equicode);
        $count = Course_Equivalency::where([
            ['course_code', '=', $coursecode3],
            ['status','=','Active'],
            ])->count();
            // dd($count);
        $title2 = Course::where('course_code','=',$equicode)->first();
        // dd($courseactive);
        $effectiveterm = Term::where([
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        $endterm = Term::where('term_start_date', '>=', $carbon)->get();
        return view('course.edit.equivalency',compact('course','courseinactive','courseactive','countequivalency','equivalency','title2','effectiveterm','endterm','course2','count','endterm2','equicode'));
    }

    public function equivalencystore(Request $request)
    {
            $user = Auth::user();
            $course = Course::where('created_by', '=', $user->id)->first();
            $term = Course::where('course_code','=',$request->idrep)->first();
            $coursecode = session('course');
            $coursecode2 = array_slice($coursecode, -1)[0];
            for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
            $coursecode3 = substr($coursecode[$i], 0, 6);
            $sequence2 = Course_Equivalency::where('course_code', '=', $coursecode3)->max('sequence');
            $sequence = $sequence2 + 1;
            $equivalency = Course_Equivalency::where([
                ['course_code', '=', $coursecode3],
                ['sequence', '=', $sequence2]
                ])->first();
            if($request->eff_term == $equivalency->effective_term_code){
                return back()->with('alert-Error','No Update Added');
            }else if($request->eff_term == $request->end_term){
                return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
            }else if($request->eff_term > $request->end_term){
                return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
            }else{
                DB::table('course_equivalencies')->where([
                    ['course_code', $coursecode3],
                    ['sequence', $sequence2]
                    ])->update([
                    'end_term_code' => $request->eff_term,
                    'status' => 'Inactive',
                    // dd($user->id);
                ]);

                $pre = new Course_Equivalency();
                // dd($term);
                $pre->course_equivalency_code = $request->id;
                $pre->course_code = $coursecode3;
                $pre->effective_term_code = $request->eff_term;
                $pre->end_term_code = $request->end_term;
                $pre->condition_sequence = '2';
                $pre->sequence= $sequence;
                $pre->created_by = $user->id;
                $pre->status = 'Draft';

                $pre->save();

                return back()->with('alert-Confirmation','Save Successfully, You Can Submit The Application');
            }
    }

    public function equivalencysubmit(Request $request)
    {
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $sequence2 = Course_Equivalency::where('course_code', '=', $coursecode3)->max('sequence');
        $course = Course_Equivalency::where([
            ['course_code', '=', $coursecode3],
            ['sequence','=',$sequence2]
            ])->first();
        if($course->status != 'Draft'){
            return back()->with('alert-Error','Please Complete All Required Fields !');
        }else{

            DB::table('course_equivalencies')->where([
                ['course_code', $coursecode3],
                ['sequence','=',$sequence2]
                ])->update([
                'status' => 'Pending',
                // dd($user->id);
            ]);
            
            return redirect()->route('home.course')->with('alert-Confirmation','Application Submitted');
        }
    }

    public function chooseprerequisite()
    {
        $course = Course::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('course.edit.chooseprerequisite',compact('course'));
    }

    
    public function prerequisite(Request $request)
    {
        $carbon = date('Y-m-d');
        $user = Auth::user();
        $request->session()->push('course',$request->course);
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        // dd($coursecode2);
        $course = Course::where('course_code', '=', $coursecode3)->first();
        // dd($course);
        $coursetitle = Course::pluck('long_title','course_code')->first();
        $course2 = Course::where([
            ['course_code', '!=', $coursecode3],
            ['status','=','Active'],
            ])->orderBy('course_code', 'ASC')->get();
        // dd($course2);
        $courseactive = Course::get();
        $countprerequisite = Course_Prerequisite::where('course_code', '=', $coursecode3)->count();
        $prerequisite = Course_Prerequisite::where([
            ['course_code', '=', $coursecode3],
            ['status','=','Active'],
            ])->get();
        $count = Course_Prerequisite::where([
            ['course_code', '=', $coursecode3],
            ['status','=','Active'],
            ])->count();
            // dd($count);
        $title2 = Course::where('course_code','=',$coursecode3)->first();
        // dd($coursetitle);
        $effectiveterm = Term::where([
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        $endterm = Term::where('term_start_date', '>=', $carbon)->get();
        return view('course.edit.prerequisite',compact('course','courseinactive','courseactive','countprerequisite','prerequisite','title2','effectiveterm','endterm','course2','count','endterm2'));
    }

    
    public function prerequisitestore(Request $request)
    {
            $user = Auth::user();
            $course = Course::where('created_by', '=', $user->id)->first();
            $term = Course::where('course_code','=',$request->idrep)->first();
            $coursecode = session('course');
            $coursecode2 = array_slice($coursecode, -1)[0];
            $pre = new Course_Prerequisite();
            // dd($term);
            $pre->course_prerequisite_code = $request->id;
            $pre->course_code = $coursecode2;
            $pre->effective_term_code = $request->effpre;
            $pre->end_term_code = $request->endpre;
            $pre->condition_sequence = '2';
            $pre->sequence='1';
            $pre->created_by = $user->id;
            $pre->status = 'Active';

            $pre->save();

            return redirect()->route('home.course')->with('alert-success','Save Successfully, You Can Submit The Application');
    }
    

    public function choosecorequisite()
    {
        $course = Course::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('course.edit.choosecorequisite',compact('course'));
    }

    
    public function corequisite(Request $request)
    {
        $carbon = date('Y-m-d');
        $user = Auth::user();
        $request->session()->push('course',$request->course);
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        // dd($coursecode2);
        $course = Course::where('course_code', '=', $coursecode3)->first();
        // dd($course);
        $coursetitle = Course::pluck('long_title','course_code')->first();
        $course2 = Course::where([
            ['course_code', '!=', $coursecode3],
            ['status','=','Active'],
            ])->orderBy('course_code', 'ASC')->get();
        // dd($course2);
        $courseactive = Course::get();
        $countcorequisite = Course_Corequisite::where('course_code', '=', $coursecode3)->count();
        $corequisite = Course_Corequisite::where([
            ['course_code', '=', $coursecode3],
            ['status','=','Active'],
            ])->get();
        $count = Course_Corequisite::where([
            ['course_code', '=', $coursecode3],
            ['status','=','Active'],
            ])->count();
        $countprerequisite = Course_Prerequisite::where('course_code', '=', $coursecode3)->count();
        $prerequisite = Course_Prerequisite::where([
            ['course_code', '=', $coursecode3],
            ['status','=','Active'],
            ])->get();
            // dd($count);
        $title2 = Course::where('course_code','=',$coursecode3)->first();
        // dd($coursetitle);
        $effectiveterm = Term::where([
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        $endterm = Term::where('term_start_date', '>=', $carbon)->get();
        return view('course.edit.corequisite',compact('course','courseinactive','courseactive','countcorequisite','corequisite','title2','effectiveterm','endterm','course2','count','countprerequisite','prerequisite'));
    }

    
    public function corequisitestore(Request $request)
    {
            $user = Auth::user();
            $course = Course::where('created_by', '=', $user->id)->first();
            $term = Course::where('course_code','=',$request->idrep)->first();
            $code = $request->id;
            $corequisite = Course_Corequisite::where('course_corequisite_code','=', $code)->count();
            $coursecode = session('course');
            $coursecode2 = array_slice($coursecode, -1)[0];
            if($corequisite == 0){
                $co = new Course_Corequisite();
                // dd($term);
                $co->course_corequisite_code = $request->id;
                $co->course_code = $coursecode2;
                $co->effective_term_code = $request->effco;
                $co->end_term_code = $request->endco;
                $co->condition_sequence = '2';
                $co->sequence='1';
                $co->created_by = $user->id;
                $co->status = 'Active';
            }else{
                DB::table('course_corequisites')->where('course_corequisite_code', $code)->update([
                    'end_term_code' => $request->endco,
                    'status' => 'Inactive',
                    // dd($user->id);
                ]);

                $co = new Course_Corequisite();
                // dd($term);
                $co->course_corequisite_code = $request->id;
                $co->course_code = $coursecode2;
                $co->effective_term_code = $request->effco;
                $co->end_term_code = $request->endco;
                $co->condition_sequence = '2';
                $co->sequence='2';
                $co->created_by = $user->id;
                $co->status = 'Inactive';
            }

            $co->save();

            return back()->with('alert-success','Save Successfully, You Can Submit The Application');
    }



    public function chooserestrictions()
    {
        $course = Course::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('course.edit.chooserestrictions',compact('course'));
    }
    
    public function restriction(Request $request)
    {
        $user = Auth::user();
        $request->session()->push('course',$request->course);
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $restriction = Course_Restrictions::where('course_code', '=', $coursecode3)->get();
        $college = College::get();
        $campus = Campus::get();
        $program = Academic_Program::get();
        $carbon = date('Y-m-d');
        $endterm = Term::where('term_start_date', '>=', $carbon)->get();
        $effectiveterm = Term::where([
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        // dd($textbook);
        return view('course.edit.restrictions',compact('course','college','campus','program','endterm','effectiveterm','restriction','coursecode2'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
