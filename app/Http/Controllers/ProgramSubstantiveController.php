<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Campus;
use Carbon;
use App\College;
use App\Degree_Type;
use App\Degree_Level;
use App\Department;
use App\Term;
use App\Major;
use App\Concentration;
use App\Academic_Program;
use App\Program_Learning_Outcomes;
use App\Program_Objective;
use App\Program_Assessment;
use App\Program_Facultyandstaff;
use App\Program_Fitnessalignment;
use App\Student;
use App\Research;
use App\Resource;
use App\Program_Supportive;
use App\Cost_Revenue;
use App\Community_Engagement;
use App\Public_Diclosure;
use App\Study_Plan;
use App\Delivery_Mode;
use App\Instructional_Language;
use App\Course;
use App\Course_Genre;
use App\Semester;
use App\Course_Type;
use App\PLO_CLO_Mapping;
use App\Course_Learning_Outcomes;
use Auth;
use DB;

class ProgramSubstantiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function chooseconcentration()
    {
        $program = Academic_Program::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('program.substantive.add_concentration.choose',compact('program'));
    }

    public function concentration(Request $request)
    {
        // dd($request->id);
        $request->session()->push('program',$request->program);
        $programcode = session('program');
        $programcode2 = array_slice($programcode, -1)[0];
        for ($i = count($programcode) - 1; $i > 0 && $programcode[$i] == null; $i--);
        $programcode3 = substr($programcode[$i], 0, 8);
        $user = Auth::user();
        $program = Academic_Program::where('program_code', '=', $programcode3)->first();
        $countprogram = Academic_Program::where('program_code', '=', $programcode3)->count();
        $countmajor = Major::where('program_code','=', $program->program_code)->count();
        $countconcentration = Concentration::where('program_code','=', $program->program_code)->count();
        $count = Concentration::where([
            ['program_code','=', $program->program_code],
            ['status','=', 'Draft']
            ])->count();
        if($countmajor != 0){
        $major = Major::where('program_code','=', $program->program_code)->first();
        $major_name = Major::where('program_code','=', $program->program_code)->first();
        $assesment = Program_Assessment::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        }else{
        }
        // dd($outcome);
        $campus = Campus::get();
        $college = College::get();
        $type = Degree_Type::get();
        $level = Degree_Level::get();
        $department = Department::where('college_code','=',$program->college_code)->first();
        $term = Term::get();
        $concentration = Concentration::where([
            ['program_code','=',$program->program_code],
            ['status','!=','Inactive']
            ])->get();
        $outcome = Program_Learning_Outcomes::where('program_code','=',$program->program_code)->get();
        $campus2 = campus::where('campus_code', '=', $program->campus_code)->first();
        $college2 = College::where('college_code', '=', $program->college_code)->first();
        $type2 = Degree_Type::where('degree_type_code','=', $program->degree_type_code)->first();
        $level2 = Degree_Level::where('degree_level_code','=', $program->degree_level_code)->first();
        // $department2 = Department::where('department_code','');
        $carbon = date('Y-m-d');
        $term2 = Term::where('term_code','=', $program->effective_term_code)->first();
        $term3 = Term::where('term_code','=', $program->end_term_code)->first();
        $endterm = Term::where([
            ['term_code','!=', $program->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ])->get();
        $effterm = Term::where([
            ['term_code','!=', $program->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        return view('program.substantive.add_concentration.concentration', compact('campus', 'college', 'type','level','department','term','program','campus2','college2','level2','type2','effterm','endterm','major','major_name','outcome','assesment','faculty','fitness','student','countmajor','term2','term3','countconcentration','concentration','count','programcode3')); 
    }

    
    public function concentrationstore(Request $request)
    {
        $user = Auth::user();
        $programcode = session('program');
        $programcode2 = array_slice($programcode, -1)[0];
        // dd($programcode2);
        for ($i = count($programcode) - 1; $i > 0 && $programcode[$i] == null; $i--);
        $programcode3 = substr($programcode[$i], 0, 8);
        $program = Academic_Program::where('program_code', '=', $programcode3)->first();
        // dd($programcodeold2);
        $sequence2 = Academic_Program::where('program_Code', '=', $programcode3)->max('sequence');
        $sequence = $sequence2 + 1;
        // dd($request->end_term);
        if($request->eff_term == $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else if($request->eff_term > $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else{
            $conc = count($request->concentration);
            // dd($conc);
            if($conc != "0"){
                for ($i = 1; $i <= count($request->concentration); $i++) {
                    $mapping[] = [
                        'concentration_code' => $request->concentration[$i],
                        'program_code' => $program->program_code,
                        'title' => $request->concentration_title[$i],
                        'effective_term_code' => $request->eff_term,
                        'end_term_code' => $request->end_term,
                        'college_code' => $program->college_code,
                        'status' => 'Draft',
                        'concentration_hour' => $request->concentration_hour[$i]
                    ];
                }
                
                DB::table('concentrations')->insert($mapping);
                return back()->with('alert-Confirmation','Success Input Data');
            }
        }
    }

    public function concentrationsubmit(Request $request)
    {
        $programcode = session('program');
        $programcode2 = array_slice($programcode, -1)[0];
        for ($i = count($programcode) - 1; $i > 0 && $programcode[$i] == null; $i--);
        $programcode3 = substr($programcode[$i], 0, 8);
        $sequence2 = Concentration::where('program_code', '=', $programcode3)->max('sequence');
        $program = Concentration::where([
            ['program_code', '=', $programcode3],
            ['sequence','=',$sequence2]
            ])->first();
        // dd($sequence2);
        if($program->status != 'Draft'){
            return back()->with('alert-Error','Please Complete All Required Fields !');
        }else{

            DB::table('concentrations')->where([
                ['program_code', $programcode3],
                ['sequence','=',$sequence2]
                ])->update([
                'status' => 'Pending',
                // dd($user->id);
            ]);
            session()->forget('program');
            return redirect()->route('home')->with('alert-Confirmation','Application Submitted');
        }
    }

    public function choosediscontinuing()
    {
        $program = Academic_Program::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('program.substantive.discontinuing_concentration.choose',compact('program'));
    }

    public function discontinuing(Request $request)
    {
        // dd($request->id);
        $request->session()->push('program',$request->program);
        $programcode = session('program');
        $programcode2 = array_slice($programcode, -1)[0];
        for ($i = count($programcode) - 1; $i > 0 && $programcode[$i] == null; $i--);
        $programcode3 = substr($programcode[$i], 0, 8);
        $user = Auth::user();
        $program = Academic_Program::where('program_code', '=', $programcode3)->first();
        $countprogram = Academic_Program::where('program_code', '=', $programcode3)->count();
        $countmajor = Major::where('program_code','=', $program->program_code)->count();
        $countconcentration = Concentration::where('program_code','=', $program->program_code)->count();
        $count = Concentration::where([
            ['program_code','=', $program->program_code],
            ['status','=', 'Draft']
            ])->count();
        if($countmajor != 0){
        $major = Major::where('program_code','=', $program->program_code)->first();
        $major_name = Major::where('program_code','=', $program->program_code)->first();
        $assesment = Program_Assessment::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        }else{
        }
        // dd($outcome);
        $campus = Campus::get();
        $college = College::get();
        $type = Degree_Type::get();
        $level = Degree_Level::get();
        $department = Department::where('college_code','=',$program->college_code)->first();
        $term = Term::get();
        $concentration = Concentration::where([
            ['program_code','=',$program->program_code],
            ['status','!=','Inactive']
            ])->get();
        $outcome = Program_Learning_Outcomes::where('program_code','=',$program->program_code)->get();
        $campus2 = campus::where('campus_code', '=', $program->campus_code)->first();
        $college2 = College::where('college_code', '=', $program->college_code)->first();
        $type2 = Degree_Type::where('degree_type_code','=', $program->degree_type_code)->first();
        $level2 = Degree_Level::where('degree_level_code','=', $program->degree_level_code)->first();
        // $department2 = Department::where('department_code','');
        $carbon = date('Y-m-d');
        $term2 = Term::where('term_code','=', $program->effective_term_code)->first();
        $term3 = Term::where('term_code','=', $program->end_term_code)->first();
        $endterm = Term::where([
            ['term_code','!=', $program->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ])->get();
        $effterm = Term::where([
            ['term_code','!=', $program->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        return view('program.substantive.discontinuing_concentration.concentration', compact('campus', 'college', 'type','level','department','term','program','campus2','college2','level2','type2','effterm','endterm','major','major_name','outcome','assesment','faculty','fitness','student','countmajor','term2','term3','countconcentration','concentration','count')); 
    }

    public function discontinuingstore(Request $request)
    {
        $user = Auth::user();
        $programcode = session('program');
        $programcode2 = array_slice($programcode, -1)[0];
        // dd($programcode2);
        for ($i = count($programcode) - 1; $i > 0 && $programcode[$i] == null; $i--);
        $programcode3 = substr($programcode[$i], 0, 8);
        $program = Academic_Program::where('program_code', '=', $programcode3)->first();
        // dd($programcodeold2);
        $sequence2 = Academic_Program::where('program_Code', '=', $programcode3)->max('sequence');
        $sequence = $sequence2 + 1;
        // dd($request->end_term);
            $conc = count($request->enableconcentration);
            // dd($conc);
            if($conc != "0"){
                for ($i = 1; $i <= count($request->enableconcentration); $i++) {
                    $conc = Concentration::where('concentration_code', '=', $request->enableconcentration[$i])->first();
                    $concseq = Concentration::where('concentration_code', '=', $request->enableconcentration[$i])->max('sequence');
                    $concseq2 = $concseq + 1; 
                    $term1 = $request->enableeffterm[$i];
                    $term2 = $request->enableendterm[$i];
                    $count = Concentration::where([
                        ['concentration_code', '=', $request->enableconcentration[$i]],
                        ['effective_term_code','=', $term1]
                        ])->count();
                    // dd($count);
                    if($count != 0){
                        return back()->with('alert-Error','No Update Added');
                    }else if($term1 == $term2){
                        return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
                    }else if($term1 > $term2){
                        return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
                    }else{
                        DB::table('concentrations')->where([
                            ['concentration_code', $request->enableconcentration[$i]],
                            ['sequence', $concseq]
                            ])->update([
                            'end_term_code' => $request->enableeffterm[$i],
                            'status' => 'Inactive'
                        ]);

                        $mapping[] = [
                            'concentration_code' => $request->enableconcentration[$i],
                            'program_code' => $program->program_code,
                            'title' => $request->enabletitle[$i],
                            'effective_term_code' => $request->enableeffterm[$i],
                            'end_term_code' => $request->enableendterm[$i],
                            'college_code' => $program->college_code,
                            'status' => 'Draft',
                            'sequence' => $concseq2
                        ];
                    }
                }
                
                DB::table('concentrations')->insert($mapping);
                return back()->with('alert-Confirmation','Success Input Data');
            }
    }

    public function discontinuingsubmit(Request $request)
    {
        $programcode = session('program');
        $programcode2 = array_slice($programcode, -1)[0];
        for ($i = count($programcode) - 1; $i > 0 && $programcode[$i] == null; $i--);
        $programcode3 = substr($programcode[$i], 0, 8);
        $sequence2 = Concentration::where('program_code', '=', $programcode3)->max('sequence');
        $program = Concentration::where([
            ['program_code', '=', $programcode3],
            ['sequence','=',$sequence2]
            ])->first();
        // dd($sequence2);
        if($program->status != 'Draft'){
            return back()->with('alert-Error','Please Complete All Required Fields !');
        }else{

            DB::table('concentrations')->where([
                ['program_code', $programcode3],
                ['status','=','Draft']
                ])->update([
                'status' => 'Pending',
                // dd($user->id);
            ]);
            
            return redirect()->route('home')->with('alert-Confirmation','Application Submitted');
        }
    }

    public function choosechanging()
    {
        $program = Academic_Program::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('program.substantive.changing_concentration.choose',compact('program'));
    }

    public function changing(Request $request)
    {
        $request->session()->push('program',$request->program);
        $programcode = session('program');
        $programcode2 = array_slice($programcode, -1)[0];
        for ($i = count($programcode) - 1; $i > 0 && $programcode[$i] == null; $i--);
        $programcode3 = substr($programcode[$i], 0, 8);
        $carbon = date('Y-m-d');
        $user = Auth::user();
        $program = Academic_Program::where('program_code', '=', $programcode3)->first();
        $course = Course::where('program_code_01', '=', $programcode3)->first();
        $course3 = Course::get();
        $program3 = Academic_Program::first();
        $course2 = Course::orderBy('course_code', 'asc')->get();
        $hourdata = Course::where('program_code_01', '=', $programcode3)->first();
        $countstudy = Study_Plan::where('program_code','=', $programcode3)->count();
        $sequence2 = Study_Plan::where('program_code', '=', $programcode3)->max('sequence');
        $count = Course::where([
            ['program_code_01','=', $programcode3],
            ['status','=','Draft']
            ])->count();
        // dd($count);
        $semester = Semester::get();
        $type = Course_Type::get();
        $plan = Academic_Program::get();
        $term2 = Term::where('term_code','=', $program->effective_term_code)->first();
        $term3 = Term::where('term_code','=', $program->end_term_code)->first();
        $endterm = Term::where([
            ['term_code','!=', $program->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ])->get();
        $effectiveterm = Term::where([
            ['term_code','!=', $program->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        if($countstudy != 0){
        $plans = Study_Plan::where('program_code','=', $programcode3)->first();
        $study = Course::where([
            ['program_code_01','=', $programcode3],
            ['status','!=','Inactive']
            ])->get();
        $study2 = Study_Plan::where('program_code','=', $programcode3)->first();
        $sum = Course::where([
            ['program_code_01','=', $study2->program_code],
            ['status','!=','Inactive']
            ])->sum('credit_hours');
        $sumcon = Course::where([
            ['program_code_01','=', $study2->program_code],
            ['course_type_code_01','=','CONC'],
            ['status','!=','Inactive']
        ])->sum('credit_hours');
        $concentration = Concentration::where([
            ['program_code','=', $study2->program_code],
            ['status','!=','Inactive']
        ])->sum('concentration_hour');
        $sumgen = Course::where([
            ['program_code_01','=', $study2->program_code],
            ['course_type_code_01','=','GEN'],
            ['status','!=','Inactive']
        ])->sum('credit_hours');
        $sumcore = Course::where([
            ['program_code_01','=', $study2->program_code],
            ['course_type_code_01','=','Core'],
            ['status','!=','Inactive']
        ])->sum('credit_hours');
        $sumelect = Course::where([
            ['program_code_01','=', $study2->program_code],
            ['course_type_code_01','=','Elect'],
            ['status','!=','Inactive']
        ])->sum('credit_hours');
        $status = Academic_Program::where('program_code','=', $study2->program_code)->first();
        }
        // dd($plo2);
        return view('program.substantive.changing_concentration.concentration',compact('course','textbook','hourdata','countpolicy','countspecial','countpedagogy','countoutcome','clo','program','plo','countmapping','mapping','mappingtitle','mappingplo','program2','plo2','semester','course2','plan','type','program3','study','study2','countstudy','status','sum','sumcon','sumelect','sumcore','sumgen','course3','term2','term3','effectiveterm','endterm','program','count','concentration'));
    }

    public function changingstore(Request $request)
    {
        // dd($request->total_concentration);
        $user = Auth::user();
        $programcode = session('program');
        $programcode2 = array_slice($programcode, -1)[0];
        for ($i = count($programcode) - 1; $i > 0 && $programcode[$i] == null; $i--);
        $programcode3 = substr($programcode[$i], 0, 8);
        $sequence2 = Concentration::where('program_code', '=', $programcode3)->max('sequence');
        $seq = $sequence2 + 1;
        $concentrationcount  = Concentration::where([
            ['program_code', '=', $programcode3],
            ['sequence','=',$sequence2],
            ['concentration_hour','=',$request->conhour]
            ])->count(); 
        $concentration  = Concentration::where([
            ['program_code', '=', $programcode3],
            ['sequence','=',$sequence2]
            ])->first(); 
        if($request->enablecourse == ''){
            $count = 0;
        }else if($request->eff_term == $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else if($request->eff_term > $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else{
            $count = count($request->enablecredit);
        }
        // dd(count($request->enablecreditcore));
        for ($i = 1; $i <= count($request->enablecredit); $i++) {
            $course = Course::where('course_code', '=', $request->enablecourse[$i])->first();
            $seq = Course::where('course_code', '=', $request->enablecourse[$i])->max('sequence');
            $seq2 = $seq + 1; 
            $count = Course::where([
                ['course_code', '=', $request->enablecourse[$i]],
                ['effective_term_code','=', $request->eff_term]
                ])->count();
            
            if($count != 0){
                return back()->with('alert-Error','No Update Added');
            }else{
                DB::table('courses')->where([
                    ['course_code', $request->enablecourse[$i]],
                    ['sequence', $seq]
                    ])->update([
                    'end_term_code' => $request->eff_term,
                    'status' => 'Inactive'
                ]);

                $semester[] = [
                    'course_code' => $request->enablecourse[$i],
                    'description' => $course->description,
                    'program_code_01' => $request->program,
                    'college_code' => $course->college_code,
                    'grading_mode_code' => $course->grading_mode_code,
                    'course_type_code_01' => $request->enabletype[$i],
                    'long_title' => $course->long_title,
                    'short_title' => $course->short_title,
                    'course_prerequisites' => $course->course_prerequisites,
                    'course_content' => $course->course_content,
                    'course_weekly_outline' => $course->course_weekly_outline,
                    'course_assessment' => $course->course_assessment,
                    'course_learning_outcomes' => $course->course_learning_outcomes,
                    'effective_term_code' => $request->eff_term,
                    'end_term_code' => $request->end_term,
                    'credit_hours' => $request->enablecredit[$i],
                    'contact_hours' => $course->contact_hours,
                    'lecture_hours' => $course->lecture_hours,
                    'lab_hours' => $course->lab_hours,
                    'other_hours' => $course->other_hours,
                    'faculty_load_hours' => $course->faculty_load_hours,
                    'this_course_replace' => $course->this_course_replace,
                    'subject_code' => $course->subject_code,
                    'course_genre_code_01' => $request->enablegenre[$i],
                    'semester_program_01' => $request->enablesemester[$i],
                    'policy' => $course->policy,
                    'honor' => $course->honor,
                    'attendance' => $course->attendance,
                    'disability' => $course->disability,
                    'pedagogy' => $course->pedagogy,
                    'course_level_code' => $course->course_level_code,
                    'role_level_code' => $course->role_level_code,
                    'sequence' => $seq2,
                    'created_by' => $user->id,
                    'status' => 'Draft'
                ];
            }    
        }
        // dd($outcome);
        DB::table('courses')->insert($semester);
        
        return back()->with('alert-Confirmation','Saved Successfully');
    }

    public function changingsubmit(Request $request)
    {
        $programcode = session('program');
        $programcode2 = array_slice($programcode, -1)[0];
        for ($i = count($programcode) - 1; $i > 0 && $programcode[$i] == null; $i--);
        $programcode3 = substr($programcode[$i], 0, 8);
        $sequence2 = Course::where('program_code_01', '=', $programcode3)->max('sequence');
        $program = Course::where([
            ['program_code_01', '=', $programcode3],
            ['sequence','=',$sequence2]
            ])->first();
        // dd($sequence2);
        if($program->status != 'Draft'){
            return back()->with('alert-Error','Please Complete All Required Fields !');
        }else{

            DB::table('courses')->where([
                ['program_code_01', $programcode3],
                ['status','=','Draft']
                ])->update([
                'status' => 'Pending',
                // dd($user->id);
            ]);
            
            return redirect()->route('home')->with('alert-Confirmation','Application Submitted');
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
