<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campus;
use App\College;
use App\Degree_Type;
use App\Degree_Level;
use App\Department;
use App\Term;
use App\Major;
use App\Academic_Program;
use App\Program_Learning_Outcomes;
use App\Program_Objective;
use App\Program_Assessment;
use App\Program_Facultyandstaff;
use App\Program_Fitnessalignment;
use App\Student;
use App\Research;
use App\Resource;
use App\Program_Supportive;
use App\Cost_Revenue;
use App\Community_Engagement;
use App\Public_Diclosure;
use App\Study_Plan;
use App\Delivery_Mode;
use App\Instructional_Language;
use App\Course;
use App\Course_Genre;
use App\Semester;
use App\Course_Type;
use App\PLO_CLO_Mapping;
use App\Course_Learning_Outcomes;
use Auth;
use DB;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // dd($program);
        $major = Major::where('program_code','=', $program->program_code)->first();
        $major_name = Major::where('program_code','=', $program->program_code)->first();
        $outcome = Program_Learning_Outcomes::where('program_code','=',$program->program_code)->get();
        $assesment = Program_Assessment::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        // dd($major);
        $campus = Campus::get();
        $college = College::get();
        $type = Degree_Type::get();
        $level = Degree_Level::get();
        $department = Department::get();
        $term = Term::get();
        $campus2 = campus::where('campus_code', '=', $program->campus_code)->first();
        $college2 = College::where('college_code', '=', $program->college_code)->first();
        $type2 = Degree_Type::where('degree_type_code','=', $program->degree_type_code)->first();
        $level2 = Degree_Level::where('degree_level_code','=', $program->degree_level_code)->first();
        // $department2 = Department::where('department_code','');
        $effterm = Term::where('term_code','=', $program->effective_term_code)->first();
        $endterm = Term::where('term_code','=', $program->end_term_code)->first();
        $countprogram = Academic_Program::where('created_by', '=', $user->id)->count();
        $countmajor = Major::where('program_code','=', $program->program_code)->count();
        // dd($countmajor);
        return view('program.index', compact('campus', 'college', 'type','level','department','term','program','campus2','college2','level2','type2','effterm','endterm','major','major_name','outcome','assesment','faculty','fitness','student','countprogram','countmajor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        
        $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $outcome = Program_Learning_Outcomes::where('program_code','=',$program->program_code)->get();
        $assesment = Program_Assessment::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        $clo = PLO_CLO_Mapping::where('program_code','=',$program->program_code)->first();
        $course = Course::where('course_code','=',$clo->course_code)->first();
        $mapping = PLO_CLO_Mapping::where('program_code','=',$program->program_code)->get();
        $mapping2 = Course_Learning_Outcomes::where('clo_code','=',$clo->clo_code)->get();
        $countmapping = PLO_CLO_Mapping::where('program_code','=',$program->program_code)->count();
        $sequence2 = $sequnce['sequence'] + 1;

        return view('program.outcome', compact('program', 'sequence2','outcome','assesment','faculty','fitness','student','clo','mapping','course','mapping2','countmapping'));
    }

    public function outcomes(Request $request)
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // dd(plo_code[1])
        if($request->plo_description[1] == null){
            $request->session()->flash('alert-danger', 'Fill at Least 1 Program Learning Outcomes!!');
            return back();    
        }else{
            $outcome = [];
            for ($i = 1; $i <= count($request->plo_code); $i++) {
                $outcome[] = [
                    'plo_code' => $request->plo_code[$i],
                    'plo_description' => $request->plo_description[$i],
                    'program_code' => $program->program_code,
                    'effective_term_code' => $program->effective_term_code,
                    'end_term_code' => $program->end_term_code
                ];
            }
            // dd($outcome);
            DB::table('program_learning_outcomes')->insert($outcome);
            return redirect()->route('home')->with('alert-success','Success Input Data');
        }
        if($request->po_description[1] == null){

        }else{
            $objective = [];
            for ($i = 1; $i < count($request->po_code); $i++) {
                $objective[] = [
                    'po_code' => $request->po_code[$i],
                    'plo_code' => $request->plo_code[$i],
                    'po_description' => $request->po_description[$i],
                    'program_code' => $program->program_code,
                    'effective_term_code' => $program->effective_term_code,
                    'end_term_code' => $program->end_term_code
                ];
            }
            // dd($objective);
            DB::table('program_objectives')->insert($objective);
            return redirect()->route('home')->with('alert-success','Success Input Data');
        }
        
    }

    public function assesmentview()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $assesment = Program_Assessment::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        // dd($assesment);
        return view('program.assesment', compact('program', 'assesment', 'faculty','fitness','student'));
    }

    public function assesment(Request $request)
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user['id'])->first();
        $count = Program_Assessment::all()->count();
        // dd($count);
        $i = $count + 1;
        $data = New Program_Assessment();
        $data->pa_code = "programCode_pa".$i;
        $data->proposed_program_assessment_narrative = $request->propose;
        $data->planned_direct_assesment = $request->planned_direct;
        $data->planned_indirect_assesment = $request->planned_indirect;
        $data->other_pertinent_information = $request->other_pertinent;
        $data->effective_term_code = $program->effective_term_code;
        $data->end_term_code = $program->end_term_code;
        $data->program_code = $program->program_code;
        $data->save();

        return redirect()->route('program.faculty')->with('alert-success','Success Input Data');
    }

    public function facultyview()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $assesment = Program_Assessment::where('program_code','=',$program->program_code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        // dd($faculty);
        return view('program.faculty', compact('program', 'faculty', 'fitness','student', 'assesment'));
    }

    public function faculty(Request $request)
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user['id'])->first();
        $count = Program_Facultyandstaff::all()->count();
        // dd($count);
        $i = $count + 1;
        $data = New Program_Facultyandstaff();
        $data->pfs_code = "programcode_pfs".$i;
        $data->faculty_student_ratio = $request->faculty_student;
        $data->faculty_recruitment_plan = $request->faculty_recruitment;
        $data->staff_recruitment_plan = $request->staff_recruitment;
        $data->faculty_and_course_credntialing = $request->faculty_course;
        $data->other_pertinent_information = $request->other_pertinent;
        $data->program_code = $program->program_code;
        $data->save();

        return redirect()->route('program.fitness')->with('alert-success','Success Input Data');
    }

    
    public function fitnessview()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $assesment = Program_Assessment::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        // dd($assesment);
        return view('program.fitness', compact('program', 'assesment', 'fitness','student','faculty'));
    }

    public function fitness(Request $request)
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user['id'])->first();
        $count = Program_Fitnessalignment::all()->count();
        // dd($count);
        $i = $count + 1;
        $data = New Program_Fitnessalignment();
        $data->pfa_code = "programcode_pfa".$i;
        $data->college_and_program_mission = $request->college_program;
        $data->college_and_program_organization = $request->college_program_organization;
        $data->college_and_program_governance = $request->college_program_governance;
        $data->college_and_program_quality = $request->college_program_quality;
        $data->program_rational = $request->program_rational;
        $data->program_alignment_with_university = $request->program_alignment;
        $data->program_alignment_with_quality = $request->program_alignment_quality;
        $data->program_accreditating = $request->program_accreditating;
        $data->external_reviewes = $request->external_reviewer;
        $data->market_analysis = $request->market_analysis;
        $data->benchmarking = $request->benchmarking;
        $data->feasibility_study = $request->feasibility_study;
        $data->impact_on_other_program = $request->impact;
        $data->other_pertinant_information = $request->other_pertinent;
        $data->program_code = $program->program_code;
        $data->save();

        return redirect()->route('program.student')->with('alert-success','Success Input Data');
    }

    public function studentview()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        // dd($faculty);
        return view('program.student', compact('program', 'faculty', 'student','fitness'));
    }

    public function student(Request $request)
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user['id'])->first();
        $count = Student::all()->count();
        // dd($count);
        $i = $count + 1;
        $data = New Student();
        $data->psd_code = "programcode_psd".$i;
        $data->evidence_of_student = $request->evidence_student;
        $data->evidence_of_pottential = $request->evidence_potential;
        $data->student_profile = $request->student_profile;
        $data->other_pertinent_information = $request->other_pertinent;
        $data->program_code = $program->program_code;
        $data->save();

        return redirect()->route('program.research')->with('alert-success','Success Input Data');
    }

    public function researchview()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $countresearch = Research::where('program_code','=',$program->program_code)->count();
        $countresource = Resource::where('program_code','=',$program->program_code)->count();
        $countsupportive = Program_Supportive::where('program_code','=',$program->program_code)->count();
        $countcost = Cost_Revenue::where('program_code','=',$program->program_code)->count();
        $countcommunity = Community_Engagement::where('program_code','=',$program->program_code)->count();
        $countpublic = Public_Diclosure::where('program_code','=',$program->program_code)->count();
        // dd($faculty);
        return view('program.research', compact('program', 'faculty', 'student','fitness','countresearch','countresource','countsupportive','countcost','countcommunity','countpublic'));
    }

    public function research(Request $request)
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user['id'])->first();
        $count = Research::all()->count();
        // dd($count);
        $i = $count + 1;
        $data = New Research();
        $data->prp_code = "programcode_prp".$i;
        $data->program_research = $request->program_research;
        $data->program_scholary = $request->program_scholary;
        $data->other_pertinent_information = $request->other_pertinent;
        $data->program_code = $program->program_code;
        $data->save();

        return redirect()->route('program.resource')->with('alert-success','Success Input Data');
    }

    public function resourceview()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $countresearch = Research::where('program_code','=',$program->program_code)->count();
        $countresource = Resource::where('program_code','=',$program->program_code)->count();
        $countsupportive = Program_Supportive::where('program_code','=',$program->program_code)->count();
        $countcost = Cost_Revenue::where('program_code','=',$program->program_code)->count();
        $countcommunity = Community_Engagement::where('program_code','=',$program->program_code)->count();
        $countpublic = Public_Diclosure::where('program_code','=',$program->program_code)->count();
        // dd($faculty);
        return view('program.resource', compact('program', 'faculty', 'student','fitness','countresearch','countresource','countsupportive','countcost','countcommunity','countpublic'));
    }

    public function resource(Request $request)
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user['id'])->first();
        $count = Resource::all()->count();
        // dd($count);
        $i = $count + 1;
        $data = New Resource();
        $data->pr_code = "programcode_pr".$i;
        $data->learning_resource = $request->learning_resource;
        $data->physical_resource = $request->physical_resource;
        $data->fiscal_resource = $request->fiscal_resource;
        $data->other_pertinent_information = $request->other_pertinent;
        $data->program_code = $program->program_code;
        $data->save();

        return redirect()->route('program.supportive')->with('alert-success','Success Input Data');
    }

    public function supportiveview()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $countresearch = Research::where('program_code','=',$program->program_code)->count();
        $countresource = Resource::where('program_code','=',$program->program_code)->count();
        $countsupportive = Program_Supportive::where('program_code','=',$program->program_code)->count();
        $countcost = Cost_Revenue::where('program_code','=',$program->program_code)->count();
        $countcommunity = Community_Engagement::where('program_code','=',$program->program_code)->count();
        $countpublic = Public_Diclosure::where('program_code','=',$program->program_code)->count();
        // dd($faculty);
        return view('program.supportive', compact('program', 'faculty', 'student','fitness','countresearch','countresource','countsupportive','countcost','countcommunity','countpublic'));
    }

    public function supportive(Request $request)
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user['id'])->first();
        $count = Program_Supportive::all()->count();
        // dd($count);
        $i = $count + 1;
        $data = New Program_Supportive();
        $data->par_code = "programcode_par".$i;
        $data->safety_issue = $request->safety_issue;
        $data->it_requirement = $request->it_requirement;
        $data->other_concern = $request->other_concern;
        $data->other_pertinent_information = $request->other_pertinent;
        $data->program_code = $program->program_code;
        $data->save();

        return redirect()->route('program.cost')->with('alert-success','Success Input Data');
    }

    public function costview()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $countresearch = Research::where('program_code','=',$program->program_code)->count();
        $countresource = Resource::where('program_code','=',$program->program_code)->count();
        $countsupportive = Program_Supportive::where('program_code','=',$program->program_code)->count();
        $countcost = Cost_Revenue::where('program_code','=',$program->program_code)->count();
        $countcommunity = Community_Engagement::where('program_code','=',$program->program_code)->count();
        $countpublic = Public_Diclosure::where('program_code','=',$program->program_code)->count();
        // dd($faculty);
        return view('program.cost', compact('program', 'faculty', 'student','fitness','countresearch','countresource','countsupportive','countcost','countcommunity','countpublic'));
    }

    public function cost(Request $request)
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user['id'])->first();
        $count = Cost_Revenue::all()->count();
        // dd($count);
        $i = $count + 1;
        $data = New Cost_Revenue();
        $data->pcr_code = "programcode_pcr".$i;
        $data->expect_cost = $request->expect_cost;
        $data->expect_revenue = $request->expect_revenue;
        $data->other_pertinent_information = $request->other_pertinent;
        $data->program_code = $program->program_code;
        $data->save();

        return redirect()->route('program.community')->with('alert-success','Success Input Data');
    }

    public function communityview()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $countresearch = Research::where('program_code','=',$program->program_code)->count();
        $countresource = Resource::where('program_code','=',$program->program_code)->count();
        $countsupportive = Program_Supportive::where('program_code','=',$program->program_code)->count();
        $countcost = Cost_Revenue::where('program_code','=',$program->program_code)->count();
        $countcommunity = Community_Engagement::where('program_code','=',$program->program_code)->count();
        $countpublic = Public_Diclosure::where('program_code','=',$program->program_code)->count();
        // dd($faculty);
        return view('program.community', compact('program', 'faculty', 'student','fitness','countresearch','countresource','countsupportive','countcost','countcommunity','countpublic'));
    }
    
    public function community(Request $request)
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user['id'])->first();
        $count = Community_Engagement::all()->count();
        // dd($count);
        $i = $count + 1;
        $data = New Community_Engagement();
        $data->pce_code = "programcode_pce".$i;
        $data->impact_program = $request->impact_program;
        $data->expect_impact = $request->expect_impact;
        $data->other_pertinent_information = $request->other_pertinent;
        $data->program_code = $program->program_code;
        $data->save();

        return redirect()->route('program.public')->with('alert-success','Success Input Data');
    }

    public function publicview()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $countresearch = Research::where('program_code','=',$program->program_code)->count();
        $countresource = Resource::where('program_code','=',$program->program_code)->count();
        $countsupportive = Program_Supportive::where('program_code','=',$program->program_code)->count();
        $countcost = Cost_Revenue::where('program_code','=',$program->program_code)->count();
        $countcommunity = Community_Engagement::where('program_code','=',$program->program_code)->count();
        $countpublic = Public_Diclosure::where('program_code','=',$program->program_code)->count();
        // dd($faculty);
        return view('program.public', compact('program', 'faculty', 'student','fitness','countresearch','countresource','countsupportive','countcost','countcommunity','countpublic'));
    }

    public function public(Request $request)
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user['id'])->first();
        $count = Public_Diclosure::all()->count();
        // dd($count);
        $i = $count + 1;
        $data = New Public_Diclosure();
        $data->pdi_code = "programcode_pdi".$i;
        $data->public_diclosure = $request->public_diclosure;
        $data->integrity = $request->integrity;
        $data->other_pertinent_information = $request->other_pertinent;
        $data->program_code = $program->program_code;
        $data->save();

        return redirect()->route('program.public')->with('alert-success','Success Input Data');
    }

    public function appendicesview()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $countresearch = Research::where('program_code','=',$program->program_code)->count();
        $countresource = Resource::where('program_code','=',$program->program_code)->count();
        $countsupportive = Program_Supportive::where('program_code','=',$program->program_code)->count();
        $countcost = Cost_Revenue::where('program_code','=',$program->program_code)->count();
        $countcommunity = Community_Engagement::where('program_code','=',$program->program_code)->count();
        $countpublic = Public_Diclosure::where('program_code','=',$program->program_code)->count();
        // dd($faculty);
        return view('program.appendices', compact('program', 'faculty', 'student','fitness','countresearch','countresource','countsupportive','countcost','countcommunity','countpublic'));
    }

    public function structureview()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $countresearch = Research::where('program_code','=',$program->program_code)->count();
        $countresource = Resource::where('program_code','=',$program->program_code)->count();
        $countsupportive = Program_Supportive::where('program_code','=',$program->program_code)->count();
        $countcost = Cost_Revenue::where('program_code','=',$program->program_code)->count();
        $countcommunity = Community_Engagement::where('program_code','=',$program->program_code)->count();
        $countpublic = Public_Diclosure::where('program_code','=',$program->program_code)->count();
        $countstudy = Study_Plan::where('program_code','=',$program->program_code)->count();
        $delivery = Delivery_Mode::get();
        $language = Instructional_Language::get();
        $course = Course::all();
        // dd($countstudy);
        $genre = Course_Genre::get();
        $semester = Semester::get();
        $type = Course_Type::get();
        $study = Study_Plan::where('program_code','=',$program->program_code)->get();
        $sum = Study_Plan::where('created_by','=', $user['id'])->sum('credit_hour');
        $sumcon = Study_Plan::where([
            ['created_by','=', $user['id']],
            ['course_type','=','CONC']
        ])->sum('credit_hour');
        $sumgen = Study_Plan::where([
            ['created_by','=', $user['id']],
            ['course_type','=','GEN']
        ])->sum('credit_hour');
        $sumcore = Study_Plan::where([
            ['created_by','=', $user['id']],
            ['course_type','=','Core']
        ])->sum('credit_hour');
        $sumelect = Study_Plan::where([
            ['created_by','=', $user['id']],
            ['course_type','=','Elect']
        ])->sum('credit_hour');
        // dd($course2);

        return view('program.structure', compact('program', 'faculty', 'student','fitness','countresearch','countresource','countsupportive','countcost','countcommunity','countpublic','countstudy','delivery','language','course','genre','semester','type','study','course2','sum','sumelect','sumcore','sumcon','sumgen'));
        
    }

    public function structure(Request $request)
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user['id'])->first();
        $count = Study_Plan::all()->count();
        // dd($count);
        if($program->required_hours != $request->program_hour){
            DB::table('academic_programs')->where('program_code',$program->program_code)->update([
                'required_hours' => $request->program_hour,
                // dd($user->id);
            ]);
        }else{
        $i = $count + 1;
        $data = New Study_plan();
        $data->program_code = $program->program_code;
        $data->semester_code = $request->semester;
        $data->course_code = $request->course;
        $data->course_genre = $request->genre;
        $data->credit_hour = $request->credit;
        $data->save();
        }

        return redirect()->route('program.assesment')->with('alert-success','Success Input Data');
    }

    public function all()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user['id'])->first();
        DB::table('academic_programs')->where('program_code',$program->program_code)->update([
            'status' => "Pending",
            // dd($user->id);
        ]);
        // DB::table('program_learning_outcomes')->where('program_code',$program->program_code)->update([
        //     'status' => "Pending",
        //     // dd($user->id);
        // ]);
        // DB::table('study_plans')->where('program_code',$program->program_code)->update([
        //     'status' => "Pending",
        //     // dd($user->id);
        // ]);
        DB::table('program_assessments')->where('program_code',$program->program_code)->update([
            'status' => "Pending",
            // dd($user->id);
        ]);
        DB::table('program_assessments')->where('program_code',$program->program_code)->update([
            'status' => "Pending",
            // dd($user->id);
        ]);
        DB::table('program_facultyandstaffs')->where('program_code',$program->program_code)->update([
            'status' => "Pending",
            // dd($user->id);
        ]);
        DB::table('program_fitnessalignments')->where('program_code',$program->program_code)->update([
            'status' => "Pending",
            // dd($user->id);
        ]);
        DB::table('students')->where('program_code',$program->program_code)->update([
            'status' => "Pending",
            // dd($user->id);
        ]);
        DB::table('research')->where('program_code',$program->program_code)->update([
            'status' => "Pending",
            // dd($user->id);
        ]);
        DB::table('program_supportives')->where('program_code',$program->program_code)->update([
            'status' => "Pending",
            // dd($user->id);
        ]);
        DB::table('cost_revenues')->where('program_code',$program->program_code)->update([
            'status' => "Pending",
            // dd($user->id);
        ]);
        DB::table('community_engagements')->where('program_code',$program->program_code)->update([
            'status' => "Pending",
            // dd($user->id);
        ]);
        DB::table('public_diclosures')->where('program_code',$program->program_code)->update([
            'status' => "Pending",
            // dd($user->id);
        ]);

        return back()->with('alert-success','Success Input Data');
    }

    // public function structure(Request $request)
    // {
    //     return redirect()->route('program.structure')->with('alert-success','Success Input Data');
    // }
    
    /**
     * Store a newly created resource in storage.
     *Program_Supportive
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $program = Academic_program::where('program_code','=',$request->code)->count();
        // dd($program);
        if($program = 0){
            $data = New Academic_Program();
            $data->program_code = $request->code;
            $data->campus_code = $request->campus;
            $data->degree_type_code = $request->degree_type;
            $data->degree_level_code = $request->degree_level;
            $data->college_code = $request->college;
            $data->title = $request->title;
            $data->status = "Draft";
            $data->effective_term_code = $request->effective_term;
            $data->end_term_code = $request->end_term;
            $data->created_by = $user->id;
            // dd($user->id);
            $data->save();
        }else{
            $sequnce = Academic_program::where('program_code','=',$request->code)->first();
            $sequence2 = $sequnce['sequence'] + 1;
            // dd($sequence2);
            DB::table('academic_programs')->where('program_code',$request->code)->update([
            'program_code' => $request->code,
            'campus_code' => $request->campus,
            'degree_type_code' => $request->degree_type,
            'degree_level_code' => $request->degree_level,
            'college_code' => $request->college,
            'title' => $request->title,
            'status' => "Draft",
            'effective_term_code' => $request->effective_term,
            'end_term_code' => $request->end_term,
            'created_by' => $user->id,
            'sequence' => $sequence2,
            // dd($user->id);
            ]);
        }

        
        $major = Major::where('major_code','=',$request->major)->count();
        // dd($program);
        if($major = 0){
            $data1 = New Major();
            $data1->major_code = $request->major;
            $data1->college_code = $request->college;
            $data1->title = $request->major_title;
            $data1->effective_term_code = $request->effective_term;
            $data1->end_term_code = $request->end_term;
            $data1->program_code = $request->code;
            $data1->save();
        }else{
            $sequnce = Major::where('major_code','=',$request->major)->first();
            $sequence2 = $sequnce['sequence'] + 1;
            DB::table('majors')->where('major_code',$request->major)->update([
            'major_code' => $request->major,
            'college_code' => $request->college,
            'title' => $request->major_title,
            'effective_term_code' => $request->effective_term,
            'end_term_code' => $request->end_term,
            'program_code' => $request->code,
            'sequence' => $sequence2,
            // dd($request->major_title)
            ]);
        }

        return redirect()->route('home')->with('alert-success','Success Input Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
