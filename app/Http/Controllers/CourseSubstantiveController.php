<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon;
use App\Academic_Program;
use App\Course_Subject;
use App\Department;
use App\College;
use App\Course;
use App\Course_Prerequisite;
use App\Course_Corequisite;
use App\Course_Equivalency;
use App\Course_Replace;
use App\Course_Textbook;
use App\Course_Additional_Material;
use App\Course_Genre;
use App\Course_Level;
use App\Course_Type;
use App\Course_Policy;
use App\Course_Special_Requirement;
use App\Course_Learning_Outcomes;
use App\Course_Content;
use App\Course_Weekly_Outline;
use App\Course_Assessment;
use App\Program_Learning_Outcomes;
use App\Pedagogy;
use App\Term;
use App\Grading_Mode;
use App\Grading_Schema_Detail;
use App\Plo_Clo_Mapping;
use App\Semester;
use App\Study_Plan;
use Auth;
use DB;


class CourseSubstantiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function choosedescription()
    {
        $course = Course::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('course.edit.choosedescription',compact('course'));
    }

    public function index(Request $request)
    {
        $request->session()->push('course',$request->course);
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $subject = Course_Subject::get();
        $subject2 = Course_Subject::first();
        $college = College::get();
        $genre = Course_Genre::get();
        $type = Course_Type::get();
        $level = Course_Level::get();
        $department = Department::get();
        $carbon = date('Y-m-d');
        $user = Auth::user();
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $course = Course::where('course_code', '=', $coursecode3)->count();
        $sequence = Course::where('course_Code', '=', $coursecode3)->max('sequence');
        $count = Course::where([
            ['course_code', '=', $coursecode3],
            ['status','=','Draft'],
            ['sequence','=',$sequence],
        ])->count();
        // dd($count);
        // dd($course2);
        $coursedata = Course::where([
            ['course_code', '=', $coursecode3],
            ['sequence','=',$sequence]
            ])->first();
        // dd($coursecode[$i]);
        $number = preg_replace("/[^0-9]{1,4}/", '', $coursecode3);
        $endterm = Term::where([
            ['term_code','!=', $coursedata->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ])->get();
        $effectiveterm = Term::where([
            ['term_code','!=', $coursedata->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        $college2 = College::where('college_code','=',$coursedata->college_code)->first();
        $genre2 = Course_Genre::where('course_genre_code','=',$coursedata->course_genre_code_01)->first();
        $level2 = Course_Level::where('course_level_code','=',$coursedata->course_level_code)->first();
        $subject3 = Course_Subject::where('subject_code','=',$coursedata->subject_code)->first();
        $hour = Course::where('formula','!=','')->count();
        $term2 = Term::where('term_code','=', $coursedata->effective_term_code)->first();
        $term3 = Term::where('term_code','=', $coursedata->end_term_code)->first();
        // dd($term2);
        return view('course.edit.course', compact('subject','subject2','college','genre','level','department','endterm','effectiveterm','type','course','coursedata','hour','term2','term3','college2','genre2','level2','subject3','number','count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // echo('tst');
        $user = Auth::user();
        $course2 = Course::get();
        $code = $request->course.$request->no;
        $request->session()->push('course2',$code);
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        $coursecodeold = session('course2');
        $coursecodeold2 = array_slice($coursecodeold, -1)[0];
        // dd($coursecode2);
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $course = Course::where('course_code', '=', $coursecode3)->first();
        // dd($coursecodeold2);
        $sequence2 = Course::where('course_Code', '=', $coursecode3)->max('sequence');
        $sequence = $sequence2 + 1;
        if($request->eff_term == $course->effective_term_code){
            return back()->with('alert-Error','No Update Added');
        }else{
            DB::table('courses')->where('course_code', $coursecode3)->update([
                'end_term_code' => $request->eff_term,
                'status' => 'Inactive',
                // dd($user->id);
            ]);
            $data = New Course();
            $data->course_code = $code;
            $data->description = $request->description;
            $data->program_code_01 = $course->program_code_01;
            $data->college_code = $request->college;
            $data->grading_mode_code = $course->grading_mode_code;
            $data->course_type_code_01 = $course->course_type_code;
            $data->long_title = $request->long_title;
            $data->short_title = $request->short_title;
            $data->course_prerequisites = $course->course_prerequisites;
            $data->course_content = $course->course_content;
            $data->course_weekly_outline = $course->course_weekly_outline;
            $data->course_assessment = $course->course_assessment;
            $data->course_learning_outcomes = $course->course_learning_outcomes;
            $data->effective_term_code = $request->eff_term;
            $data->end_term_code = $request->end_term;
            $data->credit_hours = $course->credit_hours;
            $data->contact_hours = $course->contact_hours;
            $data->lecture_hours = $course->lecture_hours;
            $data->lab_hours = $course->lab_hours;
            $data->other_hours = $course->other_hours;
            $data->faculty_load_hours = $course->faculty_load_hours;
            $data->this_course_replace = $course->this_course_replace;
            $data->subject_code = $request->course;
            $data->course_genre_code_01 = $request->genre;
            $data->policy = $course->policy;
            $data->honor = $course->honor;
            $data->attendance = $course->attendance;
            $data->disability = $course->disability;
            $data->pedagogy = $course->pedagogy;
            $data->course_level_code = $request->level;
            $data->role_level_code = $course->role_level_code;
            $data->sequence = $sequence;
            $data->created_by = $user->id;
            $data->status = 'Draft';

            $data->save();
        
            return redirect()->route('course.description')->with('alert-Confirmation','Saved Successfully');
        }
    }

    public function descriptionsubmit(Request $request)
    {
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $sequence2 = Course::where('course_Code', '=', $coursecode3)->max('sequence');
        $course = Course::where([
            ['course_code', '=', $coursecode3],
            ['sequence','=',$sequence2]
            ])->first();
        if($course->status != 'Draft'){
            return back()->with('alert-Error','Please Complete All Required Fields !');
        }else{
            DB::table('courses')->where([
                ['course_code', $coursecode3],
                ['status','=','Draft'],
                ])->update([
                'status' => 'Active',
                // dd($user->id);
            ]);
            
            return redirect()->route('home.course')->with('alert-Confirmation','Application Submitted');
        }
    }

    public function chooseinactive()
    {
        $course = Course::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('course.edit.chooseinactive',compact('course'));
    }

    public function inactive(Request $request)
    {
        $request->session()->push('course',$request->course);
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        // dd($coursecode[$i]);
        $subject = Course_Subject::get();
        $subject2 = Course_Subject::first();
        $college = College::get();
        $genre = Course_Genre::get();
        $type = Course_Type::get();
        $level = Course_Level::get();
        $department = Department::get();
        $carbon = date('Y-m-d');
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user['id'])->count();
        $sequence = Course::where('course_Code', '=', $coursecode3)->max('sequence');
        // dd($sequence);
        $count = Course::where([
            ['course_code', '=', $coursecode3],
            ['status','=','Draft'],
            ['sequence','=',$sequence],
        ])->count();
        // dd($coursecode3);
        // dd($count);
        // dd($course2);
        $coursedata = Course::where([
            ['course_code', '=', $coursecode3],
            ['sequence','=',$sequence]
            ])->first();
        // dd($coursecode3);
        $number = preg_replace("/[^0-9]{1,4}/", '', $coursecode3);
        $endterm = Term::where([
            ['term_code','>', $coursedata->effective_term_code],
            ['term_code', '!=', '999999'],
            ])->get();
        $effectiveterm = Term::where([
            ['term_code','!=', $coursedata->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        $college2 = College::where('college_code','=',$coursedata->college_code)->first();
        $genre2 = Course_Genre::where('course_genre_code','=',$coursedata->course_genre_code_01)->first();
        $level2 = Course_Level::where('course_level_code','=',$coursedata->course_level_code)->first();
        $subject3 = Course_Subject::where('subject_code','=',$coursedata->subject_code)->first();
        $hour = Course::where('formula','!=','')->count();
        $term2 = Term::where('term_code','=', $coursedata->effective_term_code)->first();
        $term3 = Term::where('term_code','=', $coursedata->end_term_code)->first();
        // dd($term2);
        return view('course.edit.inactive', compact('subject','subject2','college','genre','level','department','endterm','effectiveterm','type','course','coursedata','hour','term2','term3','college2','genre2','level2','subject3','number','count'));
    }

    public function inactivestore(Request $request)
    {
        // echo('tst');
        $user = Auth::user();
        $course2 = Course::get();
        $code = $request->course.$request->no;
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        // dd($coursecode2);
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $course = Course::where('course_code', '=', $coursecode3)->first();
        // dd($coursecodeold2);
        $sequence2 = Course::where('course_Code', '=', $coursecode3)->max('sequence');
        $sequence = $sequence2 + 1;
        if($request->eff_term == $course->effective_term_code){
            return back()->with('alert-Error','Please Update The End Term First !');
        }else if($request->eff_term == $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else if($request->eff_term > $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else{
            DB::table('courses')->where('course_code', $coursecode3)->update([
                'end_term_code' => $request->eff_term,
            ]);

            $data = New Course();
            $data->course_code = $code;
            $data->description = $request->description;
            $data->program_code_01 = $course->program_code_01;
            $data->college_code = $request->college;
            $data->grading_mode_code = $course->grading_mode_code;
            $data->course_type_code = $course->course_type_code;
            $data->long_title = $request->long_title;
            $data->short_title = $request->short_title;
            $data->course_prerequisites = $course->course_prerequisites;
            $data->course_content = $course->course_content;
            $data->course_weekly_outline = $course->course_weekly_outline;
            $data->course_assessment = $course->course_assessment;
            $data->course_learning_outcomes = $course->course_learning_outcomes;
            $data->effective_term_code = $request->eff_term;
            $data->end_term_code = $request->end_term;
            $data->credit_hours = $course->credit_hours;
            $data->contact_hours = $course->contact_hours;
            $data->lecture_hours = $course->lecture_hours;
            $data->lab_hours = $course->lab_hours;
            $data->other_hours = $course->other_hours;
            $data->faculty_load_hours = $course->faculty_load_hours;
            $data->this_course_replace = $course->this_course_replace;
            $data->subject_code = $request->course;
            $data->course_genre_code_01 = $request->genre;
            $data->policy = $course->policy;
            $data->honor = $course->honor;
            $data->attendance = $course->attendance;
            $data->disability = $course->disability;
            $data->pedagogy = $course->pedagogy;
            $data->course_level_code = $request->level;
            $data->role_level_code = $course->role_level_code;
            $data->sequence = $sequence;
            $data->created_by = $user->id;

            $data->save();
            return back()->with('alert-Confirmation','Saved Successfully, You Can Now Submit The Application');
        }
    }

    public function inactivesubmit(Request $request)
    {
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $sequence2 = Course::where('course_code', '=', $coursecode3)->max('sequence');
        $course = Course::where([
            ['course_code', '=', $coursecode3],
            ['sequence','=',$sequence2]
            ])->first();
        // dd($course);
        if($course->status != 'Draft'){
            return back()->with('alert-Error','Please Complete All Required Fields !');
        }else{

            DB::table('courses')->where([
                ['course_code', $coursecode3],
                ['sequence','=',$sequence2]
                ])->update([
                'status' => 'Pending',
                // dd($user->id);
            ]);
            
            return redirect()->route('home.course')->with('alert-Confirmation','Application Submitted');
        }
    }

    
    public function choosereplacement()
    {
        $program = Academic_Program::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('course.edit.choosereplacement',compact('program'));
    }

    public function replacement(Request $request)
    {
        $request->session()->push('program',$request->program);
        $coursecode = session('program');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 8);
        $carbon = date('Y-m-d');
        $user = Auth::user();
        $program = Academic_Program::where('program_code', '=', $coursecode3)->first();
        $course = Course::where('program_code_01', '=', $coursecode3)->first();
        $course3 = Course::get();
        $program3 = Academic_Program::first();
        $course2 = Course::orderBy('course_code', 'asc')->get();
        $hourdata = Course::where('program_code_01', '=', $coursecode3)->first();
        $countstudy = Study_Plan::where('program_code','=', $coursecode3)->count();
        $sequence2 = Study_Plan::where('program_code', '=', $coursecode3)->max('sequence');
        $count = Study_Plan::where([
            ['program_code','=', $coursecode3],
            ['status','=','Draft']
            ])->count();
        $semester = Semester::get();
        $type = Course_Type::get();
        $plan = Academic_Program::get();
        $term2 = Term::where('term_code','=', $program->effective_term_code)->first();
        $term3 = Term::where('term_code','=', $program->end_term_code)->first();
        $endterm = Term::where([
            ['term_code','!=', $program->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ])->get();
        $effectiveterm = Term::where([
            ['term_code','!=', $program->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        if($countstudy != 0){
        $plans = Study_Plan::where('program_code','=', $coursecode3)->first();
        $study = Study_Plan::where([
            ['program_code','=', $coursecode3],
            ['status','=','Active']
            ])->get();
        $study2 = Study_Plan::where('program_code','=', $coursecode3)->first();
        $sum = Study_Plan::where('program_code','=', $study2->program_code)->sum('credit_hour');
        $sumcon = Study_Plan::where([
            ['program_code','=', $study2->program_code],
            ['course_type','=','CONC']
        ])->sum('credit_hour');
        $sumgen = Study_Plan::where([
            ['program_code','=', $study2->program_code],
            ['course_type','=','GEN']
        ])->sum('credit_hour');
        $sumcore = Study_Plan::where([
            ['program_code','=', $study2->program_code],
            ['course_type','=','Core']
        ])->sum('credit_hour');
        $sumelect = Study_Plan::where([
            ['program_code','=', $study2->program_code],
            ['course_type','=','Elect']
        ])->sum('credit_hour');
        $status = Academic_Program::where('program_code','=', $study2->program_code)->first();
        }
        // dd($plo2);
        return view('course.edit.replacement',compact('course','textbook','hourdata','countpolicy','countspecial','countpedagogy','countoutcome','clo','program','plo','countmapping','mapping','mappingtitle','mappingplo','program2','plo2','semester','course2','plan','type','program3','study','study2','countstudy','status','sum','sumcon','sumelect','sumcore','sumgen','course3','term2','term3','effectiveterm','endterm','program','count'));
    }

    public function replacementstore(Request $request)
    {
        $study = Study_Plan::first();
        $user = Auth::user();
        $semester = [];
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 8);
        $course = Study_Plan::where('program_code', '=', $coursecode3)->first();
        // dd($coursecodeold2);
        // dd($coursecode3);
        $sequence2 = Study_Plan::where('program_code', '=', $coursecode3)->max('sequence');
        $sequence = $sequence2 + 1;
        if($request->enablecourse == ''){
            $count = 0;
        }else if($request->eff_term == $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else if($request->eff_term > $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else{
            $count = count($request->enablecourse);
        }
        // dd(count($request->enablecreditcore));
        $y = 2;
        if($count != 3){
            return back()->with('alert-Error','Please Change 3 Records');
        }else{
            for ($i = 1; $i <= count($request->enablecourse); $i++) {

                    if($request->enabletype[$i] == "Core"){
                    $semester[] = [
                        'program_code' => $request->program,
                        'semester_code' => $request->enablesemester[$i],
                        'course_code' => $request->enablecourse[$i],
                        'course_genre' => $request->enablegenre[$i],
                        'course_type' => $request->enabletype[$i],
                        'credit_hour' => $request->enablecreditcore[$i],
                        'effective_term_code' => $request->eff_term,
                        'end_term_code' => $request->end_term,
                        'created_by' => $user->id,
                        'sequence' => $sequence
                    ];
                    }elseif($request->enabletype[$i] == "GEN"){
                        $semester[] = [
                            'program_code' => $request->program,
                            'semester_code' => $request->enablesemester[$i],
                            'course_code' => $request->enablecourse[$i],
                            'course_genre' => $request->enablegenre[$i],
                            'course_type' => $request->enabletype[$i],
                            'credit_hour' => $request->enablecreditgen[$i],
                            'effective_term_code' => $request->eff_term,
                            'end_term_code' => $request->end_term,
                            'created_by' => $user->id,
                            'sequence' => $sequence
                        ];                        
                    }elseif($request->enabletype[$i] == "ELECT"){
                        $semester[] = [
                            'program_code' => $request->program,
                            'semester_code' => $request->enablesemester[$i],
                            'course_code' => $request->enablecourse[$i],
                            'course_genre' => $request->enablegenre[$i],
                            'course_type' => $request->enabletype[$i],
                            'credit_hour' => $request->enablecreditelect[$i],
                            'effective_term_code' => $request->eff_term,
                            'end_term_code' => $request->end_term,
                            'created_by' => $user->id,
                            'sequence' => $sequence
                        ];                        
                    }elseif($request->enabletype[$i] == "CONC"){
                        $semester[] = [
                            'program_code' => $request->program,
                            'semester_code' => $request->enablesemester[$i],
                            'course_code' => $request->enablecourse[$i],
                            'course_genre' => $request->enablegenre[$i],
                            'course_type' => $request->enabletype[$i],
                            'credit_hour' => $request->enablecreditconc[$i],
                            'effective_term_code' => $request->eff_term,
                            'end_term_code' => $request->end_term,
                            'created_by' => $user->id,
                            'sequence' => $sequence
                        ];                        
                    }
            }
            // dd($outcome);
            DB::table('study_plans')->insert($semester);
            
            return back()->with('alert-Confirmation','Saved Successfully');
        }
    }

    public function replacementsubmit(Request $request)
    {
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 8);
        $sequence2 = Study_Plan::where('program_code', '=', $coursecode3)->max('sequence');
        $course = Study_Plan::where([
            ['program_code', '=', $coursecode3],
            ['sequence','=',$sequence2]
            ])->first();
        // dd($request->eff_term);
        if($course->status != 'Draft'){
           return back()->with('alert-Error','Please Complete All Required Fields !');
        }else{
            DB::table('study_plans')->where([
                ['program_code', $coursecode3],
                ['sequence','=',$sequence2]
                ])->update([
                'status' => 'Pending',
                // dd($user->id);
            ]);
            
            return redirect()->route('home.course')->with('alert-Confirmation','Application Submitted');
        }
    }

    public function chooseoutcome()
    {
        $course = Course::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('course.edit.chooseoutcome',compact('course'));
    }

    public function outcome(Request $request)
    {
        $user = Auth::user();
        $program = Academic_Program::get();
        $request->session()->push('course',$request->course);
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $plo = Program_Learning_Outcomes::where('program_code','=','CB_BSACC')->get();
        $plo2 = Program_Learning_Outcomes::where('program_code','=','CB_BSMEN')->get();
        $course = Course::where('course_code', '=', $coursecode3)->first();
        $textbook = Course_Textbook::where('course_code','=',$coursecode3)->count();
        $hourdata = Course::where('course_code', '=', $coursecode3)->first();
        $countpolicy = Course_Policy::where('course_code','=',$coursecode3)->count();
        $countspecial = Course_Special_Requirement::where('course_code','=',$course->course_code)->count();
        $countpedagogy = Pedagogy::where('course_code','=',$course->course_code)->count();
        $countoutcome = Course_Learning_Outcomes::where([
            ['course_code','=',$course->course_code],
            ['status','=','Active']
            ])->count();
        $number = $countoutcome + 1;
        // dd($course);
        $sum = Course_Learning_Outcomes::where([
            ['course_code','=',$course->course_code]
            ])->count();
        $countmapping = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->count();
        $mapping2 = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->first();
        if($countmapping != 0){
        $mappingtitle = Academic_Program::where('program_code','=', $mapping2->program_code)->first();
        $program2 = Academic_Program::where('program_code','!=',$mapping2->program_code)->get();
        $mappingplo = Program_Learning_Outcomes::where('program_code','=',$mapping2->program_code)->get();
        }
        $mapping = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->get();
        $clo = Course_Learning_Outcomes::where([
            ['course_code','=',$course->course_code]
            // ['status','=','Active']
            ])->get();
        $data = Course_Learning_Outcomes::where('course_code','=',$course->course_code)->first();
        $carbon = date('Y-m-d');
        $endterm = Term::where([
            ['term_code','!=', $course->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ])->get();
        $effectiveterm = Term::where([
            ['term_code','!=', $course->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        
        // dd($plo2);
        return view('course.edit.outcome',compact('course','textbook','hourdata','countpolicy','countspecial','countpedagogy','countoutcome','clo','program','plo','countmapping','mapping','mappingtitle','mappingplo','program2','plo2','endterm','effectiveterm','sum','number'));
    }

    public function outcomestore(Request $request)
    {
        $user = Auth::user();

        $course = Course::where('created_by', '=', $user->id)->first();
        $mapping = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->first();
        // if($request->text[2] != null){
            // dd(count($request->clo_code));
            for($i=0; $i < count($request->clo_code);$i++){
            DB::table('course_learning_outcomes')->where('clo_code',$request->clo_code[$i+1])->update([
                'clo_description' => $request->text,
                'effective_term_code' => $request->eff_term,
                'end_term_code' => $request->end_term,
                'sequence' => '2',
                // dd($user->id);
            ]);
            }
                return back()->with('alert-success','Success Input Data');
            // echo("yes");
        // }else{
            // $outcome = [];
            // for ($i = 1; $i < count($request->clo_code); $i++) {
            //     $outcome[] = [
            //         'clo_code' => $request->clo_code[$i],
            //         'clo_description' => $request->text[$i],
            //         'course_code' => $course->course_code,
            //         'effective_term_code' => $course->effective_term_code,
            //         'end_term_code' => $course->end_term_code
            //     ];
            // }
            // // dd($outcome);
            // DB::table('course_learning_outcomes')->insert($outcome);
            
            // return back()->with('alert-success','Success Input Data');

        //     $plo = count($request->plo);
        //     // dd($request->textt);
        //     // dd($plo);
        //     for ($i = 1; $i < $plo; $i++) {
        //     $mapping2 = $request->plo[$i].'_'.$request->clo_code;
        //     }
        //     if($mapping2 = $mapping->mapping_code){
        //         return back()->with('alert-danger','Cannot Input Same Mapping Code');
        //     }else{
        //     $mapping = [];
        //     for ($i = 0; $i < $plo; $i++) {
        //         $mapping[] = [
        //             'mapping_code' => $request->plo[$i].'_'.$request->clo_code,
        //             'course_code' => $course->course_code,
        //             'clo_code' => $request->clo_code,
        //             'program_code' => $request->program,
        //             'plo_code' => $request->plo[$i]
        //         ];
        //     }
        // // dd($outcome);
        //     DB::table('plo_clo_mappings')->insert($mapping);

        // }
    }

    public function outcomestatus(Request $request)
    {
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user->id)->first();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // dd($request->clo_code);
        DB::table('course_learning_outcomes')->where('clo_code',$request->clo_code)->update([
            'status' => "Inactive",
            // dd($user->id);
        ]);
        return redirect()->route('home.course')->with('alert-success','Request Already Submit');
    }

    
    public function choosehour()
    {
        $course = Course::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('course.edit.choosehour',compact('course'));
    }

    public function hour(Request $request)
    {
        $request->session()->push('course',$request->course);
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $user = Auth::user();
        $course = Course::where('course_code', '=', $coursecode3)->count();
        $course2 = Course::where('course_code', '=', $coursecode3)->first();
        $hour = Course::where('formula','!=','')->count();
        $hourdata = Course::where('course_code', '=', $coursecode3)->first();
        // dd($hour);
        return view('course.edit.hour',compact('course','hour','hourdata','course2'));
    }

    public function hourstore(Request $request)
    {
        $user = Auth::user();
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        // dd($coursecode2);
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $course = Course::where('course_code', '=', $coursecode[$i])->first();
        // dd($coursecodeold2);
        $sequence2 = Course::where('course_Code', '=', $coursecode[$i])->max('sequence');
        $sequence = $sequence2 + 1;
        if($request->credit == $course->credit_hours){
            return back()->with('alert-Error','Please Update The Credit Hour First !');
        }else if($request->eff_term == $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else if($request->eff_term > $request->end_term){
            return back()->with('alert-Error','End Term Must be Greater Than Effective Term');
        }else{

            DB::table('courses')->where('course_code', $coursecode2)->update([
                'end_term_code' => $request->eff_term,
                'status' => 'Inactive',
                // dd($user->id);
            ]);

            $data = New Course();
            $data->course_code = $course->course_code;
            $data->description = $course->description;
            $data->program_code_01 = $course->program_code_01;
            $data->college_code = $course->college_code;
            $data->grading_mode_code = $course->grading_mode_code;
            $data->course_type_code = $course->course_type_code;
            $data->long_title = $course->long_title;
            $data->short_title = $course->short_title;
            $data->course_prerequisites = $course->course_prerequisites;
            $data->course_content = $course->course_content;
            $data->course_weekly_outline = $course->course_weekly_outline;
            $data->course_assessment = $course->course_assessment;
            $data->course_learning_outcomes = $course->course_learning_outcomes;
            $data->effective_term_code = $course->effective_term_code;
            $data->end_term_code = $course->end_term_code;
            $data->credit_hours = $request->credit;
            $data->contact_hours = $course->contact_hours;
            $data->lecture_hours = $course->lecture_hours;
            $data->lab_hours = $course->lab_hours;
            $data->other_hours = $course->other_hours;
            $data->faculty_load_hours = $course->faculty_load_hours;
            $data->this_course_replace = $course->this_course_replace;
            $data->subject_code = $course->subject_code;
            $data->course_genre_code_01 = $course->course_genre_code_01;
            $data->policy = $course->policy;
            $data->honor = $course->honor;
            $data->attendance = $course->attendance;
            $data->disability = $course->disability;
            $data->pedagogy = $course->pedagogy;
            $data->course_level_code = $course->course_level_code;
            $data->role_level_code = $course->role_level_code;
            $data->sequence = $sequence;
            $data->created_by = $user->id;

            $data->save();

            DB::table('study_plans')->where('course_code',$course->course_code)->update([
                'credit_hour' => $request->credit,
            ]);
            return redirect()->route('course.description.association')->with('alert-Success','Saved Successfully');
        }
    }

    public function association()
    {
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $carbon = date('Y-m-d');
        $user = Auth::user();
        $program = Academic_Program::get();
        $coursecode3 = substr($coursecode[$i], 0, 6);
        $course = Course::where('course_code', '=', $coursecode3)->first();
        $course3 = Course::get();
        $program3 = Academic_Program::first();
        $course2 = Course::orderBy('course_code', 'asc')->get();
        $textbook = Course_Textbook::where('course_code','=',$course->course_code)->count();
        $hourdata = Course::where('course_code', '=', $coursecode3)->first();
        $countstudy = Study_Plan::where('course_code','=', $course->course_code)->count();
        $semester = Semester::get();
        $type = Course_Type::get();
        $plan = Academic_Program::get();
        $term2 = Term::where('term_code','=', $course->effective_term_code)->first();
        $term3 = Term::where('term_code','=', $course->end_term_code)->first();
        $endterm = Term::where([
            ['term_code','!=', $course->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ])->get();
        $effectiveterm = Term::where([
            ['term_code','!=', $course->effective_term_code],
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        if($countstudy != 0){
        $plans = Study_Plan::where('course_code','=', $coursecode3)->first();
        $study = Study_Plan::where('program_code','=', $plans->program_code)->get();
        $study2 = Study_Plan::where('course_code','=', $coursecode3)->first();
        $sum = Study_Plan::where('program_code','=', $study2->program_code)->sum('credit_hour');
        $sumcon = Study_Plan::where([
            ['program_code','=', $study2->program_code],
            ['course_type','=','CONC']
        ])->sum('credit_hour');
        $sumgen = Study_Plan::where([
            ['program_code','=', $study2->program_code],
            ['course_type','=','GEN']
        ])->sum('credit_hour');
        $sumcore = Study_Plan::where([
            ['program_code','=', $study2->program_code],
            ['course_type','=','Core']
        ])->sum('credit_hour');
        $sumelect = Study_Plan::where([
            ['program_code','=', $study2->program_code],
            ['course_type','=','Elect']
        ])->sum('credit_hour');
        $status = Academic_Program::where('program_code','=', $study2->program_code)->first();
        }
        // dd($plo2);
        return view('course.edit.association',compact('course','textbook','hourdata','countpolicy','countspecial','countpedagogy','countoutcome','clo','program','plo','countmapping','mapping','mappingtitle','mappingplo','program2','plo2','semester','course2','plan','type','program3','study','study2','countstudy','status','sum','sumcon','sumelect','sumcore','sumgen','course3','term2','term3','effectiveterm','endterm'));
    }

    public function associationstore(Request $request)
    {
        $study = Study_Plan::first();
        $user = Auth::user();
        $semester = [];
        $y = 2;
        for ($i = 1; $i <= count($request->semester); $i++) {

                if($request->type[$i] == "Core"){
                $semester[] = [
                    'program_code' => $request->program,
                    'semester_code' => $request->semester[$i],
                    'course_code' => $request->course[$i],
                    'course_genre' => $request->genre[$i],
                    'course_type' => $request->type[$i],
                    'credit_hour' => $request->creditcore[$i],
                    'created_by' => $user->id
                ];
                }elseif($request->type[$i] == "GEN"){
                    $semester[] = [
                        'program_code' => $request->program,
                        'semester_code' => $request->semester[$i],
                        'course_code' => $request->course[$i],
                        'course_genre' => $request->genre[$i],
                        'course_type' => $request->type[$i],
                        'credit_hour' => $request->creditgen[$i],
                        'created_by' => $user->id
                    ];                        
                }elseif($request->type[$i] == "ELECT"){
                    $semester[] = [
                        'program_code' => $request->program,
                        'semester_code' => $request->semester[$i],
                        'course_code' => $request->course[$i],
                        'course_genre' => $request->genre[$i],
                        'course_type' => $request->type[$i],
                        'credit_hour' => $request->creditelect[$i],
                        'created_by' => $user->id
                    ];                        
                }elseif($request->type[$i] == "CONC"){
                    $semester[] = [
                        'program_code' => $request->program,
                        'semester_code' => $request->semester[$i],
                        'course_code' => $request->course[$i],
                        'course_genre' => $request->genre[$i],
                        'course_type' => $request->type[$i],
                        'credit_hour' => $request->creditconc[$i],
                        'created_by' => $user->id
                    ];                        
                }
        }
        // dd($outcome);
        DB::table('study_plans')->insert($semester);
        
        return back()->with('alert-Success','Saved Successfully');
    }

    public function choosecoprerequisite()
    {
        $course = Course::where('status','=','Active')
                        ->orwhere('status','=','Approve')->get();
        return view('course.edit.choosecoandprerequisite',compact('course'));
    }

    
    public function coprerequisite(Request $request)
    {
        $carbon = date('Y-m-d');
        $user = Auth::user();
        $request->session()->push('course',$request->course);
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 6);
        // dd($coursecode2);
        $course = Course::where('course_code', '=', $coursecode3)->first();
        // dd($course);
        $coursetitle = Course::pluck('long_title','course_code')->first();
        $course2 = Course::where([
            ['course_code', '!=', $coursecode3],
            ['status','=','Active'],
            ])->orderBy('course_code', 'ASC')->get();
        // dd($course2);
        $courseactive = Course::get();
        $countcorequisite = Course_Corequisite::where('course_code', '=', $coursecode3)->count();
        $corequisite = Course_Corequisite::where([
            ['course_code', '=', $coursecode3],
            ['status','=','Active'],
            ])->get();
        $count = Course_Corequisite::where([
            ['course_code', '=', $coursecode3],
            ['status','=','Active'],
            ])->count();
        $countprerequisite = Course_Prerequisite::where('course_code', '=', $coursecode3)->count();
        $prerequisite = Course_Prerequisite::where([
            ['course_code', '=', $coursecode3],
            ['status','=','Active'],
            ])->get();
            // dd($count);
        $title2 = Course::where('course_code','=',$coursecode3)->first();
        // dd($coursetitle);
        $effectiveterm = Term::where([
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        $endterm = Term::where('term_start_date', '>=', $carbon)->get();
        return view('course.edit.coandprerequisite',compact('course','courseinactive','courseactive','countcorequisite','corequisite','title2','effectiveterm','endterm','course2','count','countprerequisite','prerequisite'));
    }

    // public function status(Request $request)
    // {
    //     $user = Auth::user();
    //     $course = Course::where('created_by', '=', $user->id)->first();
    //     $program = Academic_Program::where('created_by', '=', $user->id)->first();
    //     // dd($request->eff_term);
    //     if($course->effective_term_code == $request->eff_term){
    //         return back()->with('alert-danger','Please Change The Effective Term Before You Submit It');
    //     }else{
    //         DB::table('courses')->where('course_code',$course->course_code)->update([
    //             'status' => "Pending",
    //             'effective_term_code' => $request->eff_term,
    //             'end_term_code' => $request->end_term,
    //             // dd($user->id);
    //         ]);
    //         DB::table('academic_programs')->where('program_code',$program->program_code)->update([
    //             'status' => "Pending",
    //             // dd($user->id);
    //         ]);
    //     return redirect()->route('home.course')->with('alert-success','Request Already Submit');
    //     }
    // }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
