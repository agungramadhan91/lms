<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Academic_Program;
use App\College;

class CollegeViewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function choose()
    {
        $college = College::get();
        // dd($college);
        return view('college.view.choose', compact('college')); 
    }
    
    public function index(Request $request)
    {
        $request->session()->push('course',$request->course);
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        for ($i = count($coursecode) - 1; $i > 0 && $coursecode[$i] == null; $i--);
        $coursecode3 = substr($coursecode[$i], 0, 2);
        // dd($coursecode3);    
        $program = Academic_Program::where('college_code','=', $coursecode3)->get();
        // dd($program);
        return view('college.view.index', compact('program')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
