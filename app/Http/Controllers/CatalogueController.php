<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Campus;
use Carbon;
use App\College;
use App\Degree_Type;
use App\Degree_Level;
use App\Department;
use App\Term;
use App\Major;
use App\Concentration;
use App\Academic_Program;
use App\Program_Learning_Outcomes;
use App\Program_Objective;
use App\Program_Assessment;
use App\Program_Admission_Requirements;
use App\Program_Graduation_Requirement;
use App\Program_Facultyandstaff;
use App\Program_Fitnessalignment;
use App\Student;
use App\Research;
use App\Resource;
use App\Program_Supportive;
use App\Cost_Revenue;
use App\Community_Engagement;
use App\Public_Diclosure;
use App\Study_Plan;
use App\Delivery_Mode;
use App\Instructional_Language;
use App\Course;
use App\Course_Genre;
use App\Semester;
use App\Course_Type;
use App\PLO_CLO_Mapping;
use App\Course_Learning_Outcomes;
use Auth;
use DB;
use PDF;

class CatalogueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function catalogueIndex()
    {
        $college = College::where('status','!=','Inactive')->get();
        $effterm = Term::get();
        $endterm = Term::get();
        return view('catalogue', compact('college','effterm','endterm'));
    }

    public function index(Request $request)
    {
        $request->session()->push('program',$request->program);
        $cataloguecode = session('program');
        $cataloguecode2 = array_slice($cataloguecode, -1)[0];
        for ($i = count($cataloguecode) - 1; $i > 0 && $cataloguecode[$i] == null; $i--);
        $cataloguecode3 = substr($cataloguecode[$i], 0, 2);
        if($cataloguecode3 == 'Sh'){
        $college = College::where('status','!=','Inactive')->orderBy('college_code', 'asc')->get();
        $program = Academic_Program::where('status','!=','Inactive')->get();
        $course = Course::where('status','!=','Inactive')->get();
        $effterm = Term::get();
        $endterm = Term::get();
        $all = 1;
        }else{
        $college = College::where('college_code','=',$cataloguecode3)->first();
        $program = Academic_Program::where('college_code', '=', $cataloguecode3)->orderBy('program_code', 'asc')->get();
        $course = Course::where('college_code','=',$cataloguecode3)->get();
        $effterm = Term::where('term_code','=',$college->effective_term_code)->first();
        $endterm = Term::where('term_code','=',$college->end_term_code)->first();
        $all = 0;
        }
        $campus = Campus::where('status','!=','Inactive')->get();
        $type = Degree_Type::where('status','!=','Inactive')->get();
        $level = Degree_Level::where('status','!=','Inactive')->get();
        $par = Program_Admission_Requirements::where('status','!=','Inactive')->get();
        $grad = Program_Graduation_Requirement::where('status','!=','Inactive')->get();
        return view('catalogue.index', compact('college','program','course','campus','level','type','par','grad','all','effterm','endterm'));
    }

    public function pdf()
    {
        set_time_limit(9999);

        $cataloguecode = session('program');
        $cataloguecode2 = array_slice($cataloguecode, -1)[0];
        for ($i = count($cataloguecode) - 1; $i > 0 && $cataloguecode[$i] == null; $i--);
        $cataloguecode3 = substr($cataloguecode[$i], 0, 2);
        if($cataloguecode3 == 'Sh'){
            $college = College::where('status','!=','Inactive')->orderBy('college_code', 'asc')->get();
            $program = Academic_Program::where('status','!=','Inactive')->get();
            $course = Course::where('status','!=','Inactive')->get();
            $effterm = Term::get();
            $endterm = Term::get();
            $all = 1;
            $title = 'All';
        }else{
            $college = College::where('college_code','=',$cataloguecode3)->first();
            $program = Academic_Program::where('college_code', '=', $cataloguecode3)->orderBy('program_code', 'asc')->get();
            $course = Course::where('college_code','=',$cataloguecode3)->get();
            $effterm = Term::where('term_code','=',$college->effective_term_code)->first();
            $endterm = Term::where('term_code','=',$college->end_term_code)->first();
            $all = 0;
            $title = $college->title;
        }
        $campus = Campus::where('status','!=','Inactive')->get();
        $type = Degree_Type::where('status','!=','Inactive')->get();
        $level = Degree_Level::where('status','!=','Inactive')->get();
        $par = Program_Admission_Requirements::where('status','!=','Inactive')->get();
        $grad = Program_Graduation_Requirement::where('status','!=','Inactive')->get();
        $pdf = PDF::loadview('catalogue.pdf',['college'=>$college,'effterm'=>$effterm,
        'endterm'=>$endterm,'program'=>$program,'course'=>$course,'campus'=>$campus,'type'=>$type,'level'=>$level,
        'par'=>$par,'grad'=>$grad,'all'=>$all]);
        return $pdf->download($title.'_Catalogue.pdf');
    }

    public function word()
    {
        set_time_limit(9999);

        $cataloguecode = session('program');
        $cataloguecode2 = array_slice($cataloguecode, -1)[0];
        for ($i = count($cataloguecode) - 1; $i > 0 && $cataloguecode[$i] == null; $i--);
        $cataloguecode3 = substr($cataloguecode[$i], 0, 2);
        if($cataloguecode3 == 'Sh'){
            $college = College::where('status','!=','Inactive')->orderBy('college_code', 'asc')->get();
            $program = Academic_Program::where('status','!=','Inactive')->get();
            $course = Course::where('status','!=','Inactive')->get();
            $effterm = Term::get();
            $endterm = Term::get();
            $all = 1;
            $title = 'All';
        }else{
            $college = College::where('college_code','=',$cataloguecode3)->first();
            $program = Academic_Program::where('college_code', '=', $cataloguecode3)->orderBy('program_code', 'asc')->get();
            $course = Course::where('college_code','=',$cataloguecode3)->get();
            $effterm = Term::where('term_code','=',$college->effective_term_code)->first();
            $endterm = Term::where('term_code','=',$college->end_term_code)->first();
            $all = 0;
            $title = $college->title;
        }
        $campus = Campus::where('status','!=','Inactive')->get();
        $type = Degree_Type::where('status','!=','Inactive')->get();
        $level = Degree_Level::where('status','!=','Inactive')->get();
        $par = Program_Admission_Requirements::where('status','!=','Inactive')->get();
        $grad = Program_Graduation_Requirement::where('status','!=','Inactive')->get();
        $pdf = PDF::loadview('catalogue.pdf',['college'=>$college,'effterm'=>$effterm,
        'endterm'=>$endterm,'program'=>$program,'course'=>$course,'campus'=>$campus,'type'=>$type,'level'=>$level,
        'par'=>$par,'grad'=>$grad,'all'=>$all]);
        return $pdf->download($title.'_Catalogue.docx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
