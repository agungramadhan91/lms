<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon;
use App\Academic_Program;
use App\College;
use App\Course_Subject;
use App\Department;
use App\Course;
use App\Course_Prerequisite;
use App\Course_Corequisite;
use App\Course_Equivalency;
use App\Course_Replace;
use App\Course_Textbook;
use App\Course_Additional_Material;
use App\Course_Genre;
use App\Course_Level;
use App\Course_Type;
use App\Course_Policy;
use App\Course_Special_Requirement;
use App\Course_Learning_Outcomes;
use App\Course_Content;
use App\Course_Weekly_Outline;
use App\Course_Assessment;
use App\Program_Learning_Outcomes;
use App\Pedagogy;
use App\Term;
use App\Grading_Mode;
use App\Grading_Schema_Detail;
use App\Plo_Clo_Mapping;
use App\Semester;
use App\Study_Plan;
use Auth;
use DB;

class CourseViewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function choose()
    {
        $user = Auth::user();
        $course = Course::where('status','=','Active')->get();
        // dd($course);
        return view('course.view.choose', compact('course')); 
    }

    public function index(Request $request)
    {
        $subject = Course_Subject::get();
        $subject2 = Course_Subject::first();
        $college = College::get();
        $genre = Course_Genre::get();
        $type = Course_Type::get();
        $level = Course_Level::get();
        $department = Department::get();
        $carbon = date('Y-m-d');
        $endterm = Term::where('term_start_date', '>=', $carbon)->get();
        $effectiveterm = Term::where([
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        $user = Auth::user();
        $request->session()->push('course',$request->id);
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        $course = Course::where('course_code', '=', $coursecode2)->count();
        $coursedata = Course::where('course_code', '=', $coursecode2)->first();
        $number = preg_replace("/[^0-9]{1,4}/", '', $coursedata->course_code);
        // dd($number);
        $college2 = College::where('college_code','=',$coursedata->college_code)->first();
        $genre2 = Course_Genre::where('course_genre_code','=',$coursedata->course_genre_code_01)->first();
        $level2 = Course_Level::where('course_level_code','=',$coursedata->course_level_code)->first();
        $subject3 = Course_Subject::where('subject_code','=',$coursedata->subject_code)->first();
        $hour = Course::where('formula','!=','')->count();
        $term2 = Term::where('term_code','=', $coursedata->effective_term_code)->first();
        $term3 = Term::where('term_code','=', $coursedata->end_term_code)->first();
        // dd($term2);
        return view('course.view.course', compact('subject','subject2','college','genre','level','department','endterm','effectiveterm','type','course','coursedata','hour','term2','term3','college2','genre2','level2','subject3','number'));
    }

    public function hour()
    {
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        // dd($coursecode);
        $user = Auth::user();
        $course = Course::where('course_code', '=', $coursecode2)->count();
        $course2 = Course::where('course_code', '=', $coursecode2)->first();
        $hour = Course::where('formula','!=','')->count();
        $hourdata = Course::where('course_code', '=', $coursecode2)->first();
        // dd($hour);
        return view('course.view.hour',compact('course','hour','hourdata','course2'));
    }

    public function requisite()
    {
        $carbon = date('Y-m-d');
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        $user = Auth::user();
        $course = Course::where('course_code', '=', $coursecode2)->first();
        $coursetitle = Course::pluck('long_title','course_code')->first();
        $courseinactive = Course::where('status', '=', 'Inactive')->get();
        $courseactive = Course::get();
        $countreplace = Course_Replace::where('course_code', '=', $coursecode2)->count();
        $countprerequisite = Course_Prerequisite::where('course_code', '=', $coursecode2)->count();
        $countcorequisite = Course_Corequisite::where('course_code', '=', $coursecode2)->count();
        $countequivalency = Course_Equivalency::where('course_code', '=', $coursecode2)->count();
        $replace = Course_Replace::where([
            ['course_code', '=', $coursecode2],
            ['sequence','=','1'],
            ])->first(); 
        $replace2 = Course_Replace::where([
            ['course_code', '=', $coursecode2],
            ['sequence','=','2'],
            ])->first();
        if($countreplace != "0"){
        $title = Course::where('course_code','=',$replace)->first();
        }else{}
        $prerequisite = Course_Prerequisite::where([
            ['course_code', '=', $coursecode2],
            ['sequence','=','1'],
            ])->first(); 
        $prerequisite2 = Course_Prerequisite::where([
            ['course_code', '=', $coursecode2],
            ['sequence','=','2'],
            ])->first();
        // dd($prerequisite2);
        if($countprerequisite != "0"){
        $title2 = Course::where('course_code','=',$prerequisite->course_code)->first();
        }else{}
        $corequisite = Course_Corequisite::where([
            ['course_code', '=', $coursecode2],
            ['sequence','=','1'],
            ])->first(); 
        $corequisite2 = Course_Corequisite::where([
            ['course_code', '=', $coursecode2],
            ['sequence','=','2'],
            ])->first();
        if($countcorequisite != "0"){
        $title3 = Course::where('course_code','=',$corequisite->course_code)->first();
        }else{}
        $equivalency = Course_Equivalency::where([
            ['course_code', '=', $coursecode2],
            ['sequence','=','1'],
            ])->first(); 
        $equivalency2 = Course_Equivalency::where([
            ['course_code', '=', $coursecode2],
            ['sequence','=','2'],
            ])->first();
        if($countequivalency != "0"){
        $title4 = Course::where('course_code','=',$equivalency->course_code)->first();
        }else{}
        $effectiveterm = Term::where([
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        $endterm = Term::where('term_start_date', '>=', $carbon)->get();
        $textbook = Course_Textbook::where('course_code','=',$course->full_course_code)->count();
        $additional = Course_Additional_Material::where('course_code','=',$course->full_course_code)->count();
        $hourdata = Course::where('created_by', '=', $user['id'])->first();
        // dd($coursetitle);
        return view('course.view.requisite',compact('course','additional','textbook','courseinactive','courseactive','effectiveterm','endterm','coursetitle','countreplace','countprerequisite','countcorequisite','countequivalency','replace','replace2','title','prerequisite','prerequisite2','title2','corequisite','corequisite2','title3','equivalency','equivalency2','title4'));
    }

    public function association()
    {
        $user = Auth::user();
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        $program = Academic_Program::get();
        $course = Course::where('course_code', '=', $coursecode2)->first();
        // dd($course);
        $program3 = Academic_Program::first();
        $course2 = Course::orderBy('course_code', 'asc')->get();
        $textbook = Course_Textbook::where('course_code','=',$course->full_course_code)->count();
        $hourdata = Course::where('course_code', '=', $coursecode)->first();
        $countpolicy = Course_Policy::where('course_code','=',$course->course_code)->count();
        $countspecial = Course_Special_Requirement::where('course_code','=',$course->course_code)->count();
        $countpedagogy = Pedagogy::where('course_code','=',$course->course_code)->count();
        $countoutcome = Course_Learning_Outcomes::where('course_code','=',$course->course_code)->count();
        $countmapping = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->count();
        $semester = Semester::get();
        $type = Course_Type::get();
        $plan = Academic_Program::get();
        $status = Academic_Program::where('program_code','=', $course->program_code_01)->first();
        $study = Study_Plan::where('course_code','=', $coursecode2)->get();
        $study2 = Study_Plan::where('course_code','=', $coursecode2)->first();
        $sum = Study_Plan::where('course_code','=', $coursecode2)->sum('credit_hour');
        $sumcon = Study_Plan::where([
            ['course_code','=', $coursecode2],
            ['course_type','=','CONC']
        ])->sum('credit_hour');
        $sumgen = Study_Plan::where([
            ['course_code','=', $coursecode2],
            ['course_type','=','GEN']
        ])->sum('credit_hour');
        $sumcore = Study_Plan::where([
            ['course_code','=', $coursecode2],
            ['course_type','=','Core']
        ])->sum('credit_hour');
        $sumelect = Study_Plan::where([
            ['course_code','=', $coursecode2],
            ['course_type','=','Elect']
        ])->sum('credit_hour');
        $countstudy = Study_Plan::where('course_code','=', $course->course_code)->count();
        if($countstudy != 0){
        $status = Academic_Program::where('program_code','=', $study2->program_code)->first();
        }
        // dd($plo2);
        return view('course.view.association',compact('course','textbook','hourdata','countpolicy','countspecial','countpedagogy','countoutcome','clo','program','plo','countmapping','mapping','mappingtitle','mappingplo','program2','plo2','semester','course2','plan','type','program3','study','study2','countstudy','status','sum','sumcon','sumelect','sumcore','sumgen'));
    }

    public function outcome()
    {
        $user = Auth::user();
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        $program = Academic_Program::get();
        $course = Course::where('course_code', '=', $coursecode2)->first();
        $plo = Program_Learning_Outcomes::where('program_code','=', $course->program_code_01)->get();
        $plo2 = Program_Learning_Outcomes::where('program_code','=',$course->program_code_01)->get();
        $textbook = Course_Textbook::where('course_code','=',$course->full_course_code)->count();
        $hourdata = Course::where('course_code', '=', $coursecode)->first();
        $countpolicy = Course_Policy::where('course_code','=',$course->course_code)->count();
        $countspecial = Course_Special_Requirement::where('course_code','=',$course->course_code)->count();
        $countpedagogy = Pedagogy::where('course_code','=',$course->course_code)->count();
        $countoutcome = Course_Learning_Outcomes::where('course_code','=',$course->course_code)->get();
        $countmapping = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->count();
        // dd($countoutcome);
        $mapping2 = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->first();
        $mapping = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->get();
        if($countmapping != 0){
        $mappingtitle = Academic_Program::where('program_code','=', $mapping2->program_code)->first();
        $mappingplo = Program_Learning_Outcomes::where('program_code','=',$mapping2->program_code)->get();
        $program2 = Academic_Program::where('program_code','!=',$mapping2->program_code)->get();
        }else{}
        $clo = Course_Learning_Outcomes::where('course_code','=',$course->course_code)->get();
        // dd($plo2);
        return view('course.view.outcome',compact('course','textbook','hourdata','countpolicy','countspecial','countpedagogy','countoutcome','clo','program','plo','countmapping','mapping','mappingtitle','mappingplo','program2','plo2'));
    }

    public function assessment()
    {
        $carbon = date('Y-m-d');
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        $user = Auth::user();
        $course = Course::where('course_code', '=', $coursecode2)->first();
        $coursetitle = Course::pluck('long_title','course_code')->first();
        $courseinactive = Course::where('status', '=', 'Inactive')->get();
        $courseactive = Course::get();
        $countreplace = Course_Replace::where('course_code', '=', $coursecode2)->count();
        $countprerequisite = Course_Prerequisite::where('course_code', '=', $coursecode2)->count();
        $countcorequisite = Course_Corequisite::where('course_code', '=', $coursecode2)->count();
        $countequivalency = Course_Equivalency::where('course_code', '=', $coursecode2)->count();
        $countcontent = Course_Content::where('course_code','=', $course->course_code)->count();
        $countweekly = Course_Weekly_Outline::where('course_code','=', $course->course_code)->count();
        $countassessment = Course_Assessment::where('course_code','=', $course->course_code)->count();
        $plo = PLO_CLO_Mapping::get();
        // dd($coursetitle);
        return view('course.view.assessment',compact('course','additional','textbook','courseinactive','courseactive','effectiveterm','endterm','coursetitle','countreplace','countprerequisite','countcorequisite','countequivalency','replace','replace2','title','prerequisite','prerequisite2','title2','corequisite','corequisite2','title3','equivalency','equivalency2','title4','countcontent','countweekly','countassessment','plo'));
    }

    public function textbook()
    {
        $user = Auth::user();
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        $course = Course::where('course_code', '=', $coursecode2)->first();
        $textbook = Course_Textbook::where('course_code','=',$course->full_course_code)->count();
        $textbook2 = Course_Textbook::where('course_code','=',$course->full_course_code)->get();
        $hourdata = Course::where('course_code', '=', $coursecode2)->first();
        // dd($textbook);
        return view('course.view.textbook',compact('course','textbook','hourdata','textbook2'));
    }

    public function optional()
    {
        $user = Auth::user();
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        $course = Course::where('course_code', '=', $coursecode2)->first();
        $textbook = Course_Textbook::where('course_code','=',$course->full_course_code)->count();
        $additional = Course_Additional_Material::where('course_code','=',$course->full_course_code)->count();
        $hourdata = Course::where('course_code', '=', $coursecode2)->first();
        // dd($additional);
        return view('course.view.optional',compact('course','additional','textbook'));
    }

    public function grading()
    {
        $user = Auth::user();
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        $course = Course::where('course_code', '=', $coursecode2)->first();
        $grading = Grading_Mode::get();  
        $countgrading = Course::where([
            ['course_code', '=', $coursecode2],
            ['grading_mode_code','!=','']
            ])->count();
        $grading2 = Course::where([
            ['course_code', '=', $coursecode2],
            ['grading_mode_code','!=','']
            ])->first();
        $schema = Grading_Schema_Detail::where('grading_mode_code','=',$grading2->grading_mode_code)->get();
        $schema1 = Grading_Schema_Detail::where('grading_mode_code','=','ULET')->get();
        $schema2 = Grading_Schema_Detail::where('grading_mode_code','=','UPAF')->get();
        $schema3 = Grading_Schema_Detail::where('grading_mode_code','=','GLET')->get();
        return view('course.view.grading',compact('grading','schema1','schema2','schema3','countgrading','grading2','schema'));
    }

    public function policies()
    {
        $user = Auth::user();
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        $course = Course::where('course_code', '=', $coursecode2)->first();
        $textbook = Course_Textbook::where('course_code','=',$course->full_course_code)->count();
        $hourdata = Course::where('course_code', '=', $coursecode2)->first();
        $countpolicy = Course_Policy::where('course_code','=',$course->course_code)->count();
        $policy = Course_Policy::where('course_code','=',$course->course_code)->first();
        // dd($policy);
        return view('course.view.policies',compact('course','textbook','hourdata','countpolicy','policy'));
    }

    public function special()
    {
        $user = Auth::user();
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        $course = Course::where('course_code', '=', $coursecode2)->first();
        $textbook = Course_Textbook::where('course_code','=',$course->full_course_code)->count();
        $hourdata = Course::where('course_code', '=', $coursecode2)->first();
        $countpolicy = Course_Policy::where('course_code','=',$course->course_code)->count();
        $countspecial = Course_Special_Requirement::where('course_code','=',$course->course_code)->count();
        $special = Course_Special_Requirement::where('course_code','=',$course->course_code)->first();
        // dd($policy);
        return view('course.view.special',compact('course','textbook','hourdata','countpolicy','countspecial','special'));
    }

    public function pedagogy()
    {
        $user = Auth::user();
        $coursecode = session('course');
        $coursecode2 = array_slice($coursecode, -1)[0];
        $course = Course::where('course_code', '=', $coursecode2)->first();
        $textbook = Course_Textbook::where('course_code','=',$course->full_course_code)->count();
        $hourdata = Course::where('course_code', '=', $coursecode2)->first();
        $countpolicy = Course_Policy::where('course_code','=',$course->course_code)->count();
        $countspecial = Course_Special_Requirement::where('course_code','=',$course->course_code)->count();
        $countpedagogy = Pedagogy::where('course_code','=',$course->course_code)->count();

        $pedagogy = Pedagogy::where('course_code','=',$course->course_code)->first();
        // dd($countpedagogy);
        return view('course.view.pedagogy',compact('course','textbook','hourdata','countpolicy','countspecial','countpedagogy','pedagogy'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
