<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Carbon;
use App\Academic_Program;
use App\Course_Subject;
use App\Department;
use App\Campus;
use App\College;
use App\Course;
use App\Course_Restrictions;
use App\Course_Prerequisite;
use App\Course_Corequisite;
use App\Course_Equivalency;
use App\Course_Replace;
use App\Course_Textbook;
use App\Course_Additional_Material;
use App\Course_Genre;
use App\Course_Level;
use App\Course_Type;
use App\Course_Policy;
use App\Course_Special_Requirement;
use App\Course_Learning_Outcomes;
use App\Course_Content;
use App\Course_Weekly_Outline;
use App\Course_Assessment;
use App\Program_Learning_Outcomes;
use App\Pedagogy;
use App\Term;
use App\Grading_Mode;
use App\Grading_Schema_Detail;
use App\Plo_Clo_Mapping;
use App\Semester;
use App\Study_Plan;
use Auth;
use DB;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subject = Course_Subject::get();
        $subject2 = Course_Subject::first();
        $college = College::get();
        $genre = Course_Genre::get();
        $type = Course_Type::get();
        $level = Course_Level::get();
        $department = Department::get();
        $carbon = date('Y-m-d');
        $endterm = Term::where('term_start_date', '>=', $carbon)->get();
        $effectiveterm = Term::where([
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user['id'])->count();
        $coursedata = Course::where('created_by', '=', $user['id'])->first();
        $college2 = College::where('college_code','=',$coursedata->college_code)->first();
        $genre2 = Course_Genre::where('course_genre_code','=',$coursedata->course_genre_code_01)->first();
        $level2 = Course_Level::where('course_level_code','=',$coursedata->course_level_code)->first();
        $subject3 = Course_Subject::where('subject_code','=',$coursedata->subject_code)->first();
        $hour = Course::where('formula','!=','')->count();
        $term2 = Term::where('term_code','=', $coursedata->effective_term_code)->first();
        $term3 = Term::where('term_code','=', $coursedata->end_term_code)->first();
        // dd($term2);
        return view('course.course', compact('subject','subject2','college','genre','level','department','endterm','effectiveterm','type','course','coursedata','hour','term2','term3','college2','genre2','level2','subject3'));
    }

    public function hour()
    {
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user['id'])->count();
        $course2 = Course::where('created_by', '=', $user['id'])->first();
        $hour = Course::where('formula','!=','')->count();
        $hourdata = Course::where('created_by', '=', $user['id'])->first();
        // dd($hour);
        return view('course.hour',compact('course','hour','hourdata','course2'));
    }
    
    public function restriction()
    {
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user['id'])->first();
        $college = College::get();
        $campus = Campus::get();
        $program = Academic_Program::get();
        $carbon = date('Y-m-d');
        $endterm = Term::where('term_start_date', '>=', $carbon)->get();
        $effectiveterm = Term::where([
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        // dd($textbook);
        return view('course.restriction',compact('course','college','campus','program','endterm','effectiveterm'));
    }

    public function textbook()
    {
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user['id'])->first();
        $textbook = Course_Textbook::where('course_code','=',$course->full_course_code)->count();
        $hourdata = Course::where('created_by', '=', $user['id'])->first();
        // dd($textbook);
        return view('course.textbook',compact('course','textbook','hourdata'));
    }
    
    public function policies()
    {
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user['id'])->first();
        $textbook = Course_Textbook::where('course_code','=',$course->full_course_code)->count();
        $hourdata = Course::where('created_by', '=', $user['id'])->first();
        $countpolicy = Course_Policy::where('course_code','=',$course->course_code)->count();
        $policy = Course_Policy::where('course_code','=',$course->course_code)->first();
        // dd($policy);
        return view('course.policies',compact('course','textbook','hourdata','countpolicy','policy'));
    }

    public function special()
    {
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user['id'])->first();
        $textbook = Course_Textbook::where('course_code','=',$course->full_course_code)->count();
        $hourdata = Course::where('created_by', '=', $user['id'])->first();
        $countpolicy = Course_Policy::where('course_code','=',$course->course_code)->count();
        $countspecial = Course_Special_Requirement::where('course_code','=',$course->course_code)->count();
        $special = Course_Special_Requirement::where('course_code','=',$course->course_code)->first();
        // dd($policy);
        return view('course.special',compact('course','textbook','hourdata','countpolicy','countspecial','special'));
    }

    public function pedagogy()
    {
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user['id'])->first();
        $textbook = Course_Textbook::where('course_code','=',$course->full_course_code)->count();
        $hourdata = Course::where('created_by', '=', $user['id'])->first();
        $countpolicy = Course_Policy::where('course_code','=',$course->course_code)->count();
        $countspecial = Course_Special_Requirement::where('course_code','=',$course->course_code)->count();
        $countpedagogy = Pedagogy::where('course_code','=',$course->course_code)->count();

        $pedagogy = Pedagogy::where('course_code','=',$course->course_code)->first();
        // dd($countpedagogy);
        return view('course.pedagogy',compact('course','textbook','hourdata','countpolicy','countspecial','countpedagogy','pedagogy'));
    }

    public function outcome()
    {
        $user = Auth::user();
        $program = Academic_Program::get();
        $plo = Program_Learning_Outcomes::where('program_code','=','CB_BSACC')->get();
        $plo2 = Program_Learning_Outcomes::where('program_code','=','CB_BSMEN')->get();
        $course = Course::where('created_by', '=', $user['id'])->first();
        $textbook = Course_Textbook::where('course_code','=',$course->full_course_code)->count();
        $hourdata = Course::where('created_by', '=', $user['id'])->first();
        $countpolicy = Course_Policy::where('course_code','=',$course->course_code)->count();
        $countspecial = Course_Special_Requirement::where('course_code','=',$course->course_code)->count();
        $countpedagogy = Pedagogy::where('course_code','=',$course->course_code)->count();
        $countoutcome = Course_Learning_Outcomes::where('course_code','=',$course->course_code)->count();
        $countmapping = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->count();
        $mapping2 = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->first();
        $mappingtitle = Academic_Program::where('program_code','=', $mapping2->program_code)->first();
        $mapping = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->get();
        $mappingplo = Program_Learning_Outcomes::where('program_code','=',$mapping2->program_code)->get();
        $clo = Course_Learning_Outcomes::where('course_code','=',$course->course_code)->get();
        $program2 = Academic_Program::where('program_code','!=',$mapping2->program_code)->get();
        // dd($plo2);
        return view('course.outcome',compact('course','textbook','hourdata','countpolicy','countspecial','countpedagogy','countoutcome','clo','program','plo','countmapping','mapping','mappingtitle','mappingplo','program2','plo2'));
    }

    public function optional()
    {
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user['id'])->first();
        $textbook = Course_Textbook::where('course_code','=',$course->full_course_code)->count();
        $additional = Course_Additional_Material::where('course_code','=',$course->full_course_code)->count();
        $hourdata = Course::where('created_by', '=', $user['id'])->first();
        // dd($additional);
        return view('course.optional',compact('course','additional','textbook'));
    }

    

    public function requisite()
    {
        $carbon = date('Y-m-d');
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user['id'])->first();
        $coursetitle = Course::pluck('long_title','course_code')->first();
        $courseinactive = Course::where('status', '=', 'Inactive')->get();
        $courseactive = Course::get();
        $countreplace = Course_Replace::where('created_by', '=', $user->id)->count();
        $countprerequisite = Course_Prerequisite::where('created_by', '=', $user->id)->count();
        $countcorequisite = Course_Corequisite::where('created_by', '=', $user->id)->count();
        $countequivalency = Course_Equivalency::where('created_by', '=', $user->id)->count();
        $replace = Course_Replace::where([
            ['created_by', '=', $user->id],
            ['sequence','=','1'],
            ])->first(); 
        $replace2 = Course_Replace::where([
            ['created_by', '=', $user->id],
            ['sequence','=','2'],
            ])->first();
        if($countreplace != "0"){
        $title = Course::where('course_code','=',$replace->course_code)->first();
        }else{}
        $prerequisite = Course_Prerequisite::where([
            ['created_by', '=', $user->id],
            ['sequence','=','1'],
            ])->get();
        if($countprerequisite != "0"){
        foreach ($prerequisite as $object){
            $code = substr($object->course_prerequisite_code, 0, 6);
            $title2 = Course::where('course_code','=',$object->course_code)->first();
        }
        }else{}
        $corequisite = Course_Corequisite::where([
            ['created_by', '=', $user->id],
            ['sequence','=','1'],
            ])->first(); 
        $corequisite2 = Course_Corequisite::where([
            ['created_by', '=', $user->id],
            ['sequence','=','2'],
            ])->first();
        if($countcorequisite != "0"){
        $title3 = Course::where('course_code','=',$corequisite->course_code)->first();
        }else{}
        $equivalency = Course_Equivalency::where([
            ['created_by', '=', $user->id],
            ['sequence','=','1'],
            ])->first(); 
        $equivalency2 = Course_Equivalency::where([
            ['created_by', '=', $user->id],
            ['sequence','=','2'],
            ])->first();
        if($countequivalency != "0"){
        $title4 = Course::where('course_code','=',$equivalency->course_code)->first();
        }else{}
            // dd($equivalency2);
        $effectiveterm = Term::where([
            ['term_start_date', '>=', $carbon],
            ['term_code', '!=', '999999'],
            ])->get();
        $endterm = Term::where('term_start_date', '>=', $carbon)->get();
        $textbook = Course_Textbook::where('course_code','=',$course->full_course_code)->count();
        $additional = Course_Additional_Material::where('course_code','=',$course->full_course_code)->count();
        $hourdata = Course::where('created_by', '=', $user['id'])->first();
        // dd($coursetitle);
        return view('course.requisite',compact('course','additional','textbook','courseinactive','courseactive','effectiveterm','endterm','coursetitle','countreplace','countprerequisite','countcorequisite','countequivalency','replace','replace2','title','prerequisite','title2','corequisite','corequisite2','title3','equivalency','equivalency2','title4','code'));
    }

    public function association()
    {
        $user = Auth::user();
        $program = Academic_Program::get();
        $course = Course::where('created_by', '=', $user['id'])->first();
        $program3 = Academic_Program::first();
        $course2 = Course::orderBy('course_code', 'asc')->get();
        $textbook = Course_Textbook::where('course_code','=',$course->full_course_code)->count();
        $hourdata = Course::where('created_by', '=', $user['id'])->first();
        $countpolicy = Course_Policy::where('course_code','=',$course->course_code)->count();
        $countspecial = Course_Special_Requirement::where('course_code','=',$course->course_code)->count();
        $countpedagogy = Pedagogy::where('course_code','=',$course->course_code)->count();
        $countoutcome = Course_Learning_Outcomes::where('course_code','=',$course->course_code)->count();
        $countmapping = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->count();
        $semester = Semester::get();
        $type = Course_Type::get();
        $plan = Academic_Program::get();
        $status = Academic_Program::where('created_by','=', $user['id'])->first();
        $study = Study_Plan::where('program_code','=', $user['id'])->get();
        $study2 = Study_Plan::where('created_by','=', $user['id'])->first();
        $sum = Study_Plan::where('created_by','=', $user['id'])->sum('credit_hour');
        $sumcon = Study_Plan::where([
            ['created_by','=', $user['id']],
            ['course_type','=','CONC']
        ])->sum('credit_hour');
        $sumgen = Study_Plan::where([
            ['created_by','=', $user['id']],
            ['course_type','=','GEN']
        ])->sum('credit_hour');
        $sumcore = Study_Plan::where([
            ['created_by','=', $user['id']],
            ['course_type','=','Core']
        ])->sum('credit_hour');
        $sumelect = Study_Plan::where([
            ['created_by','=', $user['id']],
            ['course_type','=','Elect']
        ])->sum('credit_hour');
        $countstudy = Study_Plan::where('created_by','=', $user['id'])->count();
        if($countstudy != 0){
        $status = Academic_Program::where('program_code','=', $study2->program_code)->first();
        $study = Study_Plan::where('program_code','=', $status->program_code)->get();
        // dd($status)
        }
        // dd($plo2);
        return view('course.association',compact('course','textbook','hourdata','countpolicy','countspecial','countpedagogy','countoutcome','clo','program','plo','countmapping','mapping','mappingtitle','mappingplo','program2','plo2','semester','course2','plan','type','program3','study','study2','countstudy','status','sum','sumcon','sumelect','sumcore','sumgen'));
    }

    public function assessment()
    {
        $carbon = date('Y-m-d');
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user['id'])->first();
        $coursetitle = Course::pluck('long_title','course_code')->first();
        $courseinactive = Course::where('status', '=', 'Inactive')->get();
        $courseactive = Course::get();
        $countreplace = Course_Replace::where('created_by', '=', $user->id)->count();
        $countprerequisite = Course_Prerequisite::where('created_by', '=', $user->id)->count();
        $countcorequisite = Course_Corequisite::where('created_by', '=', $user->id)->count();
        $countequivalency = Course_Equivalency::where('created_by', '=', $user->id)->count();
        $countcontent = Course_Content::where('course_code','=', $course->course_code)->count();
        $countweekly = Course_Weekly_Outline::where('course_code','=', $course->course_code)->count();
        $countassessment = Course_Assessment::where('course_code','=', $course->course_code)->count();
        $plo = PLO_CLO_Mapping::get();
        // dd($coursetitle);
        return view('course.assessment',compact('course','additional','textbook','courseinactive','courseactive','effectiveterm','endterm','coursetitle','countreplace','countprerequisite','countcorequisite','countequivalency','replace','replace2','title','prerequisite','prerequisite2','title2','corequisite','corequisite2','title3','equivalency','equivalency2','title4','countcontent','countweekly','countassessment','plo'));
    }

    public function grading()
    {
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user['id'])->first();
        $grading = Grading_Mode::get();  
        $countgrading = Course::where([
            ['created_by', '=', $user['id']],
            ['grading_mode_code','!=','']
            ])->count();
        $grading2 = Course::where([
            ['created_by', '=', $user['id']],
            ['grading_mode_code','!=','']
            ])->first();
        $schema = Grading_Schema_Detail::where('grading_mode_code','=',$grading2->grading_mode_code)->get();
        $schema1 = Grading_Schema_Detail::where('grading_mode_code','=','ULET')->get();
        $schema2 = Grading_Schema_Detail::where('grading_mode_code','=','UPAF')->get();
        $schema3 = Grading_Schema_Detail::where('grading_mode_code','=','GLET')->get();
        return view('course.grading',compact('grading','schema1','schema2','schema3','countgrading','grading2','schema'));
    }

    public function policiesstore(Request $request)
    {
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user->id)->first();
        $term = Course::where('course_code','=',$request->idrep)->first();
        $data = new Course_Policy();
        $data->course_policy_code = $request->sequence;
        $data->course_code = $course->course_code;
        $data->policy_type = $request->type;
        $data->policy_title = $request->title;
        $data->policy_text = $request->text;
        $data->effective_term_code = $course->effective_term_code;
        $data->end_term_code = $course->end_term_code;

        $data->save();
        return back()->with('alert-success','Success Input Data');
    }

    public function specialstore(Request $request)
    {
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user->id)->first();
        $term = Course::where('course_code','=',$request->idrep)->first();
        $data = new Course_Special_Requirement();
        $data->course_policy_code = $request->sequence;
        $data->course_code = $course->course_code;
        $data->requirement_type = $request->type;
        $data->requirement_title = $request->title;
        $data->requirement_text = $request->text;
        $data->effective_term_code = $course->effective_term_code;
        $data->end_term_code = $course->end_term_code;

        $data->save();
        return back()->with('alert-success','Success Input Data');
    }

    
    public function pedagogystore(Request $request)
    {
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user->id)->first();
        $term = Course::where('course_code','=',$request->idrep)->first();
        $data = new Pedagogy();
        $data->pedagogy_code = $request->sequence;
        $data->course_code = $course->course_code;
        $data->pedagogy_title = $request->title;
        $data->pedagogy_text = $request->text;
        $data->effective_term_code = $course->effective_term_code;
        $data->end_term_code = $course->end_term_code;

        $data->save();
        return back()->with('alert-success','Success Input Data');
    }

    public function outcomestore(Request $request)
    {
        $user = Auth::user();

        $course = Course::where('created_by', '=', $user->id)->first();
        $mapping = Plo_Clo_Mapping::where('course_code','=', $course->course_code)->first();
        if($request->text[1] == null){
            $request->session()->flash('alert-danger', 'Fill at Least 1 Outcomes Learning Outcomes!!');
            return back();    
        }else{
            $outcome = [];
            for ($i = 1; $i <= count($request->clo_code); $i++) {
                $outcome[] = [
                    'clo_code' => $request->clo_code[$i],
                    'clo_description' => $request->text[$i],
                    'course_code' => $course->course_code,
                    'effective_term_code' => $course->effective_term_code,
                    'end_term_code' => $course->end_term_code
                ];
            }
            // dd($outcome);
            DB::table('course_learning_outcomes')->insert($outcome);
        }

        $plo = count($request->plo);
        // dd($request->textt);
        for ($i = 0; $i < $plo; $i++) {
        $mapping2 = $request->plo[$i].'_'.$request->clo_code;
        }
        if($mapping2 = $mapping->mapping_code){
            return back()->with('alert-danger','Cannot Input Same Mapping Code');
        }else{
        $mapping = [];
        for ($i = 0; $i < $plo; $i++) {
            $mapping[] = [
                'mapping_code' => $request->plo[$i].'_'.$request->clo_code,
                'course_code' => $course->course_code,
                'clo_code' => $request->clo_code,
                'program_code' => $request->program,
                'plo_code' => $request->plo[$i]
            ];
        }
        // dd($outcome);
        DB::table('plo_clo_mappings')->insert($mapping);

        return back()->with('alert-success','Success Input Data');
        }
    }

    public function requisitestore(Request $request)
    {
        if($request->effpre = "null" || $request->endpre = "null"){
            return back()->with('alert-Error','Please Input The Effective Term And The End Term');
        }else{
            $pre = count($request->idpre);
            // dd($pre);

            if($pre != "0"){
                $user = Auth::user();
                $course = Course::where('created_by', '=', $user->id)->first();
                for ($i = 1; $i <= count($request->idpre); $i++) {
                    $code = $request->idpre[$i].'_E_001';
                    $mapping[] = [
                        'course_prerequisite_code' => $code,
                        'course_code' => $course->course_code,
                        'effective_term_code' => $request->effpre,
                        'end_term_code' => $request->endpre,
                        'condition' => $request->condition[$i],
                        'status' => 'Draft',
                        'group_by' => $request->group[$i],
                        'created_by' => $user->id
                    ];
                }
                
                DB::table('course_prerequisites')->insert($mapping);
                
                if($request->continue == 1){
                    return redirect()->route('course.association')->with('alert-success','Success Input Data');
                }elseif($request->exit == 1){
                    return redirect()->route('home.course')->with('alert-Confirmation','Success Input Data');
                }
            }else{
            }
        }
        if($request->idrep == $request->idrep2){
            return back()->with('alert-danger','Can not use the same Course ID !');
        }

        if($request->idrep1 != ""){
            $user = Auth::user();
            $course = Course::where('created_by', '=', $user->id)->first();
            $term = Course::where('course_code','=',$request->idrep)->first();
            $rep = new Course_Replace();
            // dd($term);
            $rep->course_replace_code = $request->idrep1;
            $rep->course_code = $request->idrep;
            $rep->effective_term_code = $term->effective_term_code;
            $rep->end_term_code = $term->end_term_code;
            $rep->condition_sequence = '2';
            $rep->sequence='1';
            $rep->condition = $request->condition;
            $rep->created_by = $user->id;

            $rep->save();

            $rep2 = new Course_Replace();
            $rep2->course_replace_code = $request->idrep1;
            $rep2->course_code = $request->idrep2;
            $rep2->effective_term_code = $term->effective_term_code;
            $rep2->end_term_code = $term->end_term_code;
            $rep2->condition_sequence = '2';
            $rep2->sequence='2';
            $rep2->condition = $request->condition;
            $rep2->created_by = $user->id;

            $rep2->save();

        }else{
        }

        if($request->idco1 != ""){
            $user = Auth::user();
            $course = Course::where('created_by', '=', $user->id)->first();
            $co = new Course_Corequisite();
            $co->course_corequisite_code = $request->idco1;
            $co->course_code = $request->idco;
            $co->effective_term_code = $request->effco;
            $co->end_term_code = $request->endco;
            $co->condition_sequence = '2';
            $co->condition = $request->condition3;
            $co->created_by = $user->id;

            $co->save();

            $co2 = new Course_Corequisite();
            $co2->course_corequisite_code = $request->idco1;
            $co2->course_code = $request->idco2;
            $co2->effective_term_code = $request->effco2;
            $co2->end_term_code = $request->endco2;
            $co2->condition_sequence = '2';
            $co2->condition = $request->condition3;
            $co2->created_by = $user->id;
            $co2->save();
        }else{

        }

        if($request->idqui1 != ""){
            $user = Auth::user();
            $course = Course::where('created_by', '=', $user->id)->first();
            $qui = new Course_Equivalency();
            $qui->course_equivalency_code = $request->idqui1;
            $qui->course_code = $request->idqui;
            $qui->effective_term_code = $request->effqui;
            $qui->end_term_code = $request->endqui;
            $qui->condition_sequence = '2';
            $qui->condition = $request->condition4;
            $qui->created_by = $user->id;

            $qui->save();

            $qui2 = new Course_Equivalency();
            $qui2->course_equivalency_code = $request->idqui1;
            $qui2->course_code = $request->idqui2;
            $qui2->effective_term_code = $request->effqui2;
            $qui2->end_term_code = $request->endqui2;
            $qui2->condition_sequence = '2';
            $qui2->condition = $request->condition4;
            $qui2->created_by = $user->id;

            $qui2->save();
        }else{

        }
        // dd($request->exit);
            return redirect()->route('home.course')->with('alert-Confirmation','Success Input Data');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function textbookstore(Request $request)
    {
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user->id)->first();
        $sequnce = Course_Textbook::where('course_code','=',$course->full_course_code)->first();
        $sequence2 = $sequnce['sequence'] + 1;
        $data = New Course_Textbook();
        $data->course_textbook_code = $course->full_course_code.'_TB_'.$sequence2;
        $data->textbook = $request->title;
        $data->publisher = $request->publisher;
        $data->author = $request->author;
        $data->isbn = $request->isbn;
        $data->publication_type = $request->publication;
        $data->year = $request->year;
        $data->course_code = $course->full_course_code;
        $data->effective_term_code = $request->effective_term_code;
        $data->end_term_code = $request->end_term_code;

        $data->save();
        
        return back()->with('alert-success','Success Input Data');
    }

    public function optionalstore(Request $request)
    {
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user->id)->first();
        $sequnce = Course_Additional_Material::where('course_code','=',$course->full_course_code)->first();
        $sequence2 = $sequnce['sequence'] + 1;
        $data = New Course_Additional_Material();
        $data->course_additional_material_code = $course->full_course_code.'_OR_'.$sequence2;
        $data->additional_material = $request->title;
        $data->publisher = $request->publisher;
        $data->author = $request->author;
        $data->isbn = $request->isbn;
        $data->publication_type = $request->publication;
        $data->year = $request->year;
        $data->course_code = $course->full_course_code;
        $data->effective_term_code = $course->effective_term_code;
        $data->end_term_code = $course->end_term_code;

        $data->save();
        
        return back()->with('alert-success','Success Input Data');
    }

    public function assessmentstore(Request $request)
    {
        if($request->idcontent != ""){
            $user = Auth::user();
            $course = Course::where('created_by', '=', $user->id)->first();
            $term = Course::where('course_code','=',$request->idrep)->first();
            $rep = new Course_Content();
            // dd($term);
            $rep->course_content_code = $course->course_code.'CC'.$request->idcontent;
            $rep->course_code = $course->course_code;
            $rep->course_content_description = $request->topiccontent;
            $rep->effective_term_code = $course->effective_term_code;
            $rep->end_term_code = $course->end_term_code;

            $rep->save();

        }else{
        }
        
        if($request->idweekly != ""){
            $user = Auth::user();
            $course = Course::where('created_by', '=', $user->id)->first();
            $pre = new Course_Weekly_Outline();
            $pre->cwo_code = $course->course_code.'WO'.$request->idweekly;
            $pre->course_code = $course->course_code;
            $pre->cwo_topic = $request->topicweekly;
            $pre->effective_term_code = $course->effective_term_code;
            $pre->end_term_code = $course->end_term_code;
            $pre->cwo_week = $request->week;
            $pre->cwo_description = $request->activities;

            $pre->save();
        }else{

        }

        if($request->idassessment != ""){
            if($request->weight > 1000){
                return back()->with('alert-danger','Weight Total Must be 1000');
            }else{
            $user = Auth::user();
            $course = Course::where('created_by', '=', $user->id)->first();
            $co = new Course_Assessment();
            $co->course_assessment_code = $course->course_code.'WO'.$request->idassessment;
            $co->course_code = $course->course_code;
            $co->course_assessment_type = $request->type;
            $co->course_assessment_description = $request->description;
            $co->weight = $request->weight;
            $co->mapping = $request->mapping;
            $co->category = $request->category;
            $co->effective_term_code = $course->effective_term_code;
            $co->end_term_code = $course->end_term_code;

            $co->save();
            }

        }else{

        }

        return back()->with('alert-success','Success Input Data');
    }

    public function associationstore(Request $request)
    {
        $study = Study_Plan::first();
        $user = Auth::user();
        $semester = [];
        $y = 2;
        for ($i = 1; $i <= count($request->semester); $i++) {

                if($request->type[$i] == "Core"){
                $semester[] = [
                    'program_code' => $request->program,
                    'semester_code' => $request->semester[$i],
                    'course_code' => $request->course[$i],
                    'course_genre' => $request->genre[$i],
                    'course_type' => $request->type[$i],
                    'credit_hour' => $request->creditcore[$i],
                    'created_by' => $user->id
                ];
                }elseif($request->type[$i] == "GEN"){
                    $semester[] = [
                        'program_code' => $request->program,
                        'semester_code' => $request->semester[$i],
                        'course_code' => $request->course[$i],
                        'course_genre' => $request->genre[$i],
                        'course_type' => $request->type[$i],
                        'credit_hour' => $request->creditgen[$i],
                        'created_by' => $user->id
                    ];                        
                }elseif($request->type[$i] == "ELECT"){
                    $semester[] = [
                        'program_code' => $request->program,
                        'semester_code' => $request->semester[$i],
                        'course_code' => $request->course[$i],
                        'course_genre' => $request->genre[$i],
                        'course_type' => $request->type[$i],
                        'credit_hour' => $request->creditelect[$i],
                        'created_by' => $user->id
                    ];                        
                }elseif($request->type[$i] == "CONC"){
                    $semester[] = [
                        'program_code' => $request->program,
                        'semester_code' => $request->semester[$i],
                        'course_code' => $request->course[$i],
                        'course_genre' => $request->genre[$i],
                        'course_type' => $request->type[$i],
                        'credit_hour' => $request->creditconc[$i],
                        'created_by' => $user->id
                    ];                        
                }
        }
        // dd($outcome);
        DB::table('study_plans')->insert($semester);
        
        if($request->continue == 1){
            return redirect()->route('course.outcome')->with('alert-success','Success Input Data');
        }elseif($request->exit == 1){
            return redirect()->route('home.course')->with('alert-success','Success Input Data');
        }
    }

    public function gradingstore(Request $request){
        
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user->id)->first();
        $grading = (string)$request->grading;
        // dd($course);
        DB::table('courses')->where('course_code',$course->course_code)->update([
            'grading_mode_code' => $grading,
            // dd($user->id);
        ]);
        return back()->with('alert-success','Success Input Data');
    }

    
    public function restrictionstore(Request $request)
    {
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user->id)->first();
        $rest = Course_Restrictions::where('course_code','=',$course->course_code)->count();
        $seq = $rest + 1;
        $code = $course->course_code."_R_00".$seq;
        // dd($code);
        $data = new Course_Restrictions();
        $data->course_restrictions_code = $code;
        $data->course_code = $course->course_code;
        $data->program_code = $request->program;
        $data->college_code = $request->college;
        $data->campus_code = $request->campus;
        $data->other_pertinent_information = $request->other;
        $data->effective_term_code = $course->effective_term_code;
        $data->end_term_code = $course->end_term_code;

        $data->save();
        return back()->with('alert-success','Success Input Data');
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user['id'])->first();
        $course = Course::where('course_code','=',$request->course)->count();
        // dd($course);
        $level = $request->level;
        $number = $request->no;
        if($level = "UG" && $number > 499){
            return back()->with('alert-danger','The Number must Under 499');
        }elseif($level = "GR" && $number < 500){
            return back()->with('alert-danger','The Number must more than 500');
        }
        if($course == "0"){
        $data = New Course();
        $data->course_code = $request->course.$request->no;
        $data->program_code_01 = $program->program_code;
        $data->college_code = $request->college;
        $data->grading_mode_code = 'ULET';
        $data->course_type_code = 'GEN';
        $data->long_title = $request->long_title;
        $data->short_title = $request->short_title;
        $data->course_prerequisites = "";
        $data->course_content = "as per the instructor";
        $data->course_weekly_outline = "as per the instructor";
        $data->course_assessment = "as per the instructor";
        $data->course_learning_outcomes = "";
        $data->effective_term_code = $request->eff_term;
        $data->end_term_code = $request->end_term;
        $data->credit_hours = "0";
        $data->contact_hours = "0";
        $data->lecture_hours = "0";
        $data->lab_hours = "0";
        $data->other_hours = "0";
        $data->faculty_load_hours = "0";
        $data->this_course_replace = "";
        $data->subject_code = $request->course;
        $data->course_genre_code_01 = $request->genre;
        $data->policy = "yes";
        $data->honor = "yes";
        $data->attendance = "yes";
        $data->disability = "yes";
        $data->pedagogy = "yes";
        $data->course_level_code = $request->level;
        $data->role_level_code = "";
        $data->created_by = $user->id;

        $data->save();
        }
        // }else{
        //     $sequnce = Course::where('course_code','=',$request->code)->first();
        //     $sequence2 = $sequnce['sequence'] + 1;
        //     // dd($sequence2);
        // $data = New Course();
        // $data->course_code = $request->course.$request->no;
        // $data->program_code_01 = $program->program_code;
        // $data->college_code = $request->college;
        // $data->grading_mode_code = 'ULET';
        // $data->course_type_code = 'GEN';
        // $data->long_title = $request->long_title;
        // $data->short_title = $request->short_title;
        // $data->course_prerequisites = "";
        // $data->course_content = "as per the instructor";
        // $data->course_weekly_outline = "as per the instructor";
        // $data->course_assessment = "as per the instructor";
        // $data->course_learning_outcomes = "";
        // $data->effective_term_code = $request->eff_term;
        // $data->end_term_code = $request->end_term;
        // $data->credit_hours = "3";
        // $data->contact_hours = "3";
        // $data->lecture_hours = "3";
        // $data->lab_hours = "0";
        // $data->other_hours = "0";
        // $data->faculty_load_hours = "0";
        // $data->this_course_replace = "";
        // $data->subject_code = $request->course;
        // $data->course_genre_code_01 = $request->genre;
        // $data->policy = "yes";
        // $data->honor = "yes";
        // $data->attendance = "yes";
        // $data->disability = "yes";
        // $data->pedagogy = "yes";
        // $data->course_level_code = $request->level;
        // $data->role_level_code = "";
        // $data->sequence = $sequence;
        // $data->created_by = $user->id;
        // }
        if($request->continue == 1){
            return redirect()->route('course.hour')->with('alert-success','Success Input Data');
        }elseif($request->exit == 1){
            return redirect()->route('home.course')->with('alert-success','Success Input Data');
        }
    }
    public function hourstore(Request $request)
    {
        $user = Auth::user();
        $course = Course::where('created_by', '=', $user->id)->first();
        DB::table('courses')->where('course_code',$course->course_code)->update([
            'credit_hours' => $request->credit,
            'contact_hours' => $request->contact,
            'lecture_hours' => $request->lecture,
            'lab_hours' => $request->lab,
            'other_hours' => $request->other,
            'faculty_load_hours' => $request->faculty,
            // dd($user->id);
        ]);
        if($request->continue == 1){
            return redirect()->route('course.requisite')->with('alert-success','Success Input Data');
        }elseif($request->exit == 1){
            return redirect()->route('home.course')->with('alert-success','Success Input Data');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
