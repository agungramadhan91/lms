<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campus;
use App\College;
use App\Degree_Type;
use App\Degree_Level;
use App\Department;
use App\Term;
use App\Major;
use App\Academic_Program;
use App\Program_Learning_Outcomes;
use App\Program_Objective;
use App\Program_Assessment;
use App\Program_Facultyandstaff;
use App\Program_Fitnessalignment;
use App\Student;
use App\Research;
use App\Resource;
use App\Program_Supportive;
use App\Cost_Revenue;
use App\Community_Engagement;
use App\Public_Diclosure;
use App\Study_Plan;
use App\Delivery_Mode;
use App\Instructional_Language;
use App\Course;
use App\Course_Genre;
use App\Semester;
use App\Course_Type;
use Auth;
use DB;

class ProgramViewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function choose()
    {
        $user = Auth::user();
        $program = Academic_Program::where('status','=','Active')->get();
        // dd($program);
        return view('program.view.choose', compact('program')); 
    }

    public function index(Request $request)
    {
        // dd($request->id);
        $user = Auth::user();
        $program = Academic_Program::where('program_code', '=', $request->id)->first();
        $countprogram = Academic_Program::where('program_code', '=', $request->id)->count();
        $countmajor = Major::where('program_code','=', $program->program_code)->count();
        if($countmajor != 0){
        $major = Major::where('program_code','=', $program->program_code)->first();
        $major_name = Major::where('program_code','=', $program->program_code)->first();
        $assesment = Program_Assessment::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        }else{
        }
        // dd($outcome);
        $campus = Campus::get();
        $college = College::get();
        $type = Degree_Type::get();
        $level = Degree_Level::get();
        $department = Department::where('college_code','=',$program->college_code)->first();
        $term = Term::get();
        $outcome = Program_Learning_Outcomes::where('program_code','=',$program->program_code)->get();
        $campus2 = campus::where('campus_code', '=', $program->campus_code)->first();
        $college2 = College::where('college_code', '=', $program->college_code)->first();
        $type2 = Degree_Type::where('degree_type_code','=', $program->degree_type_code)->first();
        $level2 = Degree_Level::where('degree_level_code','=', $program->degree_level_code)->first();
        // $department2 = Department::where('department_code','');
        $effterm = Term::where('term_code','=', $program->effective_term_code)->first();
        $endterm = Term::where('term_code','=', $program->end_term_code)->first();
        return view('program.view.index', compact('campus', 'college', 'type','level','department','term','program','campus2','college2','level2','type2','effterm','endterm','major','major_name','outcome','assesment','faculty','fitness','student','countmajor')); 
    }

    public function outcome()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $outcome = Program_Learning_Outcomes::where('program_code','=',$program->program_code)->get();
        $assesment = Program_Assessment::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        // dd($outcome[0]->plo_code);
        $sequence2 = $sequnce['sequence'] + 1;

        return view('program.view.outcome', compact('program', 'sequence2','outcome','assesment','faculty','fitness','student'));
    }

    public function structureview()
    {
        $user = Auth::user();
        $program = Academic_Program::first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $countresearch = Research::where('program_code','=',$program->program_code)->count();
        $countresource = Resource::where('program_code','=',$program->program_code)->count();
        $countsupportive = Program_Supportive::where('program_code','=',$program->program_code)->count();
        $countcost = Cost_Revenue::where('program_code','=',$program->program_code)->count();
        $countcommunity = Community_Engagement::where('program_code','=',$program->program_code)->count();
        $countpublic = Public_Diclosure::where('program_code','=',$program->program_code)->count();
        $countstudy = Study_Plan::where('program_code','=',$program->program_code)->count();
        $delivery = Delivery_Mode::get();
        $language = Instructional_Language::get();
        $course = Course::all();
        // dd($countstudy);
        $genre = Course_Genre::get();
        $semester = Semester::get();
        $type = Course_Type::get();
        $study = Study_Plan::where('program_code','=',$program->program_code)->get();
        $sum = Study_Plan::where('created_by','=', $user['id'])->sum('credit_hour');
        $sumcon = Study_Plan::where([
            ['created_by','=', $user['id']],
            ['course_type','=','CONC']
        ])->sum('credit_hour');
        $sumgen = Study_Plan::where([
            ['created_by','=', $user['id']],
            ['course_type','=','GEN']
        ])->sum('credit_hour');
        $sumcore = Study_Plan::where([
            ['created_by','=', $user['id']],
            ['course_type','=','Core']
        ])->sum('credit_hour');
        $sumelect = Study_Plan::where([
            ['created_by','=', $user['id']],
            ['course_type','=','Elect']
        ])->sum('credit_hour');
        // dd($course2);
        return view('program.view.structure', compact('program', 'faculty', 'student','fitness','countresearch','countresource','countsupportive','countcost','countcommunity','countpublic','countstudy','delivery','language','course','genre','semester','type','study','sum','sumelect','sumcore','sumcon','sumgen'));
    }

    public function assesment()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $assesment = Program_Assessment::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        // dd($assesment);
        return view('program.view.assesment', compact('program', 'assesment', 'faculty','fitness','student'));
    }

    public function faculty()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $assesment = Program_Assessment::where('program_code','=',$program->program_code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        // dd($faculty);
        return view('program.view.faculty', compact('program', 'faculty', 'fitness','student', 'assesment'));   
    }

    public function fitness()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $assesment = Program_Assessment::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        // dd($assesment);
        return view('program.view.fitness', compact('program', 'assesment', 'fitness','student','faculty'));
    }

    public function student()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        // dd($faculty);
        return view('program.view.student', compact('program', 'faculty', 'student','fitness'));
    }

    public function research()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $countresearch = Research::where('program_code','=',$program->program_code)->count();
        $countresource = Resource::where('program_code','=',$program->program_code)->count();
        $countsupportive = Program_Supportive::where('program_code','=',$program->program_code)->count();
        $countcost = Cost_Revenue::where('program_code','=',$program->program_code)->count();
        $countcommunity = Community_Engagement::where('program_code','=',$program->program_code)->count();
        $countpublic = Public_Diclosure::where('program_code','=',$program->program_code)->count();
        // dd($faculty);
        return view('program.view.research', compact('program', 'faculty', 'student','fitness','countresearch','countresource','countsupportive','countcost','countcommunity','countpublic'));
    }

    public function resource()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $countresearch = Research::where('program_code','=',$program->program_code)->count();
        $countresource = Resource::where('program_code','=',$program->program_code)->count();
        $countsupportive = Program_Supportive::where('program_code','=',$program->program_code)->count();
        $countcost = Cost_Revenue::where('program_code','=',$program->program_code)->count();
        $countcommunity = Community_Engagement::where('program_code','=',$program->program_code)->count();
        $countpublic = Public_Diclosure::where('program_code','=',$program->program_code)->count();
        // dd($faculty);
        return view('program.view.resource', compact('program', 'faculty', 'student','fitness','countresearch','countresource','countsupportive','countcost','countcommunity','countpublic'));
    }

    public function supportive()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $countresearch = Research::where('program_code','=',$program->program_code)->count();
        $countresource = Resource::where('program_code','=',$program->program_code)->count();
        $countsupportive = Program_Supportive::where('program_code','=',$program->program_code)->count();
        $countcost = Cost_Revenue::where('program_code','=',$program->program_code)->count();
        $countcommunity = Community_Engagement::where('program_code','=',$program->program_code)->count();
        $countpublic = Public_Diclosure::where('program_code','=',$program->program_code)->count();
        // dd($faculty);
        return view('program.view.supportive', compact('program', 'faculty', 'student','fitness','countresearch','countresource','countsupportive','countcost','countcommunity','countpublic'));
    }

    public function cost()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $countresearch = Research::where('program_code','=',$program->program_code)->count();
        $countresource = Resource::where('program_code','=',$program->program_code)->count();
        $countsupportive = Program_Supportive::where('program_code','=',$program->program_code)->count();
        $countcost = Cost_Revenue::where('program_code','=',$program->program_code)->count();
        $countcommunity = Community_Engagement::where('program_code','=',$program->program_code)->count();
        $countpublic = Public_Diclosure::where('program_code','=',$program->program_code)->count();
        // dd($faculty);
        return view('program.view.cost', compact('program', 'faculty', 'student','fitness','countresearch','countresource','countsupportive','countcost','countcommunity','countpublic'));
    }

    public function community()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $countresearch = Research::where('program_code','=',$program->program_code)->count();
        $countresource = Resource::where('program_code','=',$program->program_code)->count();
        $countsupportive = Program_Supportive::where('program_code','=',$program->program_code)->count();
        $countcost = Cost_Revenue::where('program_code','=',$program->program_code)->count();
        $countcommunity = Community_Engagement::where('program_code','=',$program->program_code)->count();
        $countpublic = Public_Diclosure::where('program_code','=',$program->program_code)->count();
        // dd($faculty);
        return view('program.view.community', compact('program', 'faculty', 'student','fitness','countresearch','countresource','countsupportive','countcost','countcommunity','countpublic'));
    }

    public function public()
    {
        $user = Auth::user();
        $program = Academic_Program::where('created_by', '=', $user->id)->first();
        // $sequnce = Academic_program::where('program_code','=',$program->code)->first();
        $student = Student::where('program_code','=',$program->program_code)->first();
        $faculty = Program_Facultyandstaff::where('program_code','=',$program->program_code)->first();
        $fitness = Program_Fitnessalignment::where('program_code','=',$program->program_code)->first();
        $countresearch = Research::where('program_code','=',$program->program_code)->count();
        $countresource = Resource::where('program_code','=',$program->program_code)->count();
        $countsupportive = Program_Supportive::where('program_code','=',$program->program_code)->count();
        $countcost = Cost_Revenue::where('program_code','=',$program->program_code)->count();
        $countcommunity = Community_Engagement::where('program_code','=',$program->program_code)->count();
        $countpublic = Public_Diclosure::where('program_code','=',$program->program_code)->count();
        // dd($faculty);
        return view('program.view.public', compact('program', 'faculty', 'student','fitness','countresearch','countresource','countsupportive','countcost','countcommunity','countpublic'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
