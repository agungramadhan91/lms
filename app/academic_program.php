<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class academic_program extends Model
{
    protected $guarded = [];
    protected $table = "academic_programs";
    protected $primaryKey = "program_code"."_"."sequence";
    public $incrementing = false;
}
