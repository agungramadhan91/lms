<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampusProgram extends Model
{
    protected $table = "campus_program";
}
