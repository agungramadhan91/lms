<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class workflow extends Model
{
    protected $table = "workflows";
    protected $primaryKey = "workflow_code";
    public $incrementing = false;
}
