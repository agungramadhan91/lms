<?php
namespace App\Helper;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class StudyPlanHelper 
{
    public static function getByProgram($code)
    {
        // $program = ProgramHelper::first($code);

        $db     = DB::table('study_plans')->where('program_code', $code);
        $seq    = $db->max('sequence');
        $get    = $db->where('sequence', $seq)
                ->where('status','Active')
                ->get();

        return $get;
    }

    public static function first($code)
    {
        $db     = DB::table('study_plans')->where('course_code', $code);
        $seq    = $db->max('sequence');
        $first  = $db->where('sequence', $seq)->first();

        return $first;
    }
    
    public static function firstData($code)
    {
        $db     = DB::table('study_plans')->where('course_code', $code);
        $seq    = $db->max('sequence');
        $first  = $db->where('sequence', $seq);

        return $first;
    }

    public static function unique()
    {
        $study_plans    = DB::table('study_plans')
                        ->select('course_code')
                        ->groupBy('course_code')
                        ->get();

        foreach ($study_plans as $data) {
            $db     = DB::table('study_plans')->where('course_code', $data->course_code);
            $seq    = $db->max('sequence');
            $get    = $db->where('sequence', $seq)->first();
            
            $data->program_code         = $get->program_code;
            $data->course_code          = $get->course_code;
            $data->course_genre         = $get->course_genre;
            $data->course_type          = $get->course_type;
            $data->semester_code        = $get->semester_code;
            $data->credit_hour          = $get->credit_hour;
            $data->sequence             = $get->sequence;
            $data->status               = $get->status;
            $data->effective_term_code  = $get->effective_term_code;
            $data->end_term_code        = $get->end_term_code;
        }

        return $terms;
    }
}