<?php
namespace App\Helper;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SemesterHelper 
{
    public static function first($code)
    {
        $db     = DB::table('semesters')->where('semester_code', $code);
        $seq    = $db->max('sequence');
        $first  = $db->where('sequence', $seq)->first();

        return $first;
    }
    
    public static function firstData($code)
    {
        $db     = DB::table('semesters')->where('semester_code', $code);
        $seq    = $db->max('sequence');
        $first  = $db->where('sequence', $seq);

        return $first;
    }

    public static function unique()
    {
        $semesters  = DB::table('semesters')
                ->where('status', 'Approval')
                ->orWhere('status', 'Active')
                ->select('semester_code')
                ->groupBy('semester_code')
                ->get();

        foreach ($semesters as $data) {
            $db     = DB::table('semesters')->where('semester_code', $data->semester_code);
            $seq    = $db->max('sequence');
            $get    = $db->where('sequence', $seq)->first();
            
            $data->semester_code        = $get->semester_code;
            $data->semester_description = $get->semester_description;
            $data->effective_term_code  = $get->effective_term_code;
            $data->end_term_code        = $get->end_term_code;
        }

        return $semesters;
    }
}