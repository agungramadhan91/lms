<?php
namespace App\Helper;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class TermHelper 
{
    public static function first($code)
    {
        $db     = DB::table('terms')->where('term_code', $code);
        $seq    = $db->max('sequence');
        $first  = $db->where('sequence', $seq)->first();

        return $first;
    }
    
    public static function firstData($code)
    {
        $db     = DB::table('terms')->where('term_code', $code);
        $seq    = $db->max('sequence');
        $first  = $db->where('sequence', $seq);

        return $first;
    }

    public static function unique()
    {
        $terms  = DB::table('terms')
                ->where('calender_year','>', Carbon::now()->format('Y'))
                ->select('term_code')
                ->groupBy('term_code')
                ->get();

        foreach ($terms as $data) {
            $db     = DB::table('terms')->where('term_code', $data->term_code);
            $seq    = $db->max('sequence');
            $get    = $db->where('sequence', $seq)->first();
            
            $data->term_code         = $get->term_code;
            $data->term_description  = $get->term_description;
            $data->academic_year     = $get->academic_year;
            $data->calender_year     = $get->calender_year;
            $data->term_start_date   = $get->term_start_date;
            $data->term_end_date     = $get->term_end_date;
            $data->is_shown          = $get->is_shown;
            $data->sequence          = $get->sequence;
            $data->is_active         = $get->is_active;
        }

        return $terms;
    }
}