<?php
namespace App\Helper;

use Illuminate\Support\Facades\DB;

class CollegeHelper {

    public static function first($code) 
    {
        $db     = DB::table('colleges')->where('college_code', $code);
        $seq    = $db->max('sequence');
        $first  = $db->where('sequence', $seq)->first();

        return $first;
    }
    
    public static function firstData($code) 
    {
        $db     = DB::table('colleges')->where('college_code', $code);
        $seq    = $db->max('sequence');
        $first  = $db->where('sequence', $seq);

        return $first;
    }

    public static function unique()
    {
        $programs   = DB::table('colleges')
                    ->select('college_code')
                    ->groupBy('college_code')
                    ->get();

        foreach ($programs as $data) 
        {
            $helper = CollegeHelper::first($data->college_code);

            $data->college_code         = $helper->college_code;
            $data->title                = $helper->title;
            $data->effective_term_code  = $helper->effective_term_code;
            $data->end_term_code        = $helper->end_term_code;
        }

        return $programs;
    }

    public static function uniqueByCodes($code)
    {
        $programs = array();

        for ($i=0; $i < count($code); $i++) { 
            $first = CollegeHelper::first($code[$i]);
            array_push($programs, $first);
        }

        return $programs;
    }
}