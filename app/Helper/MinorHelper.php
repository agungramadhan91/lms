<?php
namespace App\Helper;

use Illuminate\Support\Facades\DB;

class MinorHelper {

    public static function first($code) 
    {
        $db     = DB::table('minors')->where('minor_code', $code);
        $seq    = $db->max('sequence');
        $first  = $db->where('sequence', $seq)->first();

        return $first;
    }
    
    public static function firstData($code) 
    {
        $db     = DB::table('minors')->where('minor_code', $code);
        $seq    = $db->max('sequence');
        $first  = $db->where('sequence', $seq);

        return $first;
    }

    public static function unique()
    {
        $programs   = DB::table('minors')
                    ->select('minor_code')
                    ->groupBy('minor_code')
                    ->get();

        foreach ($programs as $data) 
        {
            $helper = MinorHelper::first($data->minor_code);

            $data->minor_code           = $helper->minor_code;
            $data->title                = $helper->title;
            $data->college_code         = $helper->college_code;
            $data->campus_code          = $helper->campus_code;
            $data->sequence             = $helper->sequence;
            $data->effective_term_code  = $helper->effective_term_code;
            $data->end_term_code        = $helper->end_term_code;
        }

        return $programs;
    }

    public static function uniqueByCodes($code)
    {
        $programs = array();

        for ($i=0; $i < count($code); $i++) { 
            $first = ProgramHelper::first($code[$i]);
            array_push($programs, $first);
        }

        return $programs;
    }
}