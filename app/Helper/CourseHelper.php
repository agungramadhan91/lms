<?php
namespace App\Helper;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CourseHelper 
{
    public static function first($code)
    {
        $db     = DB::table('courses')->where('course_code', $code);
        $seq    = $db->max('sequence');
        $first  = $db->where('sequence', $seq)
                ->where('status','Active')
                ->first();

        return $first;
    }
    
    public static function firstData($code)
    {
        $db     = DB::table('courses')->where('course_code', $code);
        $seq    = $db->max('sequence');
        $first  = $db->where('sequence', $seq)->where('status','Active');

        return $first;
    }

    public static function unique()
    {
        $courses  = DB::table('courses')
                ->select('course_code')
                ->where(function($query){
                    for ($j=1; $j < 10; $j++) { 
                        $query->orWhere('program_code_0'.$j,'!=', NULL);
                    }
                    $query->orWhere('program_code_10','!=', NULL);
                    
                })
                ->where('status','Active')
                ->orWhere('status','Approve')
                ->groupBy('course_code')
                ->get();

        foreach ($courses as $data) {
            $get = CourseHelper::first($data->course_code);
            
            if(isset($get))
            {
                $data->course_code              = $get->course_code;
                $data->college_code             = $get->college_code;
                $data->grading_mode_code        = $get->grading_mode_code;
                $data->long_title               = $get->long_title;
                $data->short_title              = $get->short_title;
                $data->description              = $get->description;
                $data->course_prerequisites     = $get->course_prerequisites;
                $data->course_content           = $get->course_content;
                $data->course_weekly_outline    = $get->course_weekly_outline;
                $data->course_assessment        = $get->course_assessment;
                $data->course_learning_outcomes = $get->course_learning_outcomes;
                $data->effective_term_code      = $get->effective_term_code;
                $data->end_term_code            = $get->end_term_code;
                $data->sequence                 = $get->sequence;
                $data->credit_hours             = $get->credit_hours;
                $data->contact_hours            = $get->contact_hours;
                $data->lecture_hours            = $get->lecture_hours;
                $data->lab_hours                = $get->lab_hours;
                $data->other_hours              = $get->other_hours;
                $data->faculty_load_hours       = $get->faculty_load_hours;
                $data->this_course_replace      = $get->this_course_replace;
                $data->subject_code             = $get->subject_code;
                $data->program_code_01          = $get->program_code_01;
                $data->program_code_02          = $get->program_code_02;
                $data->program_code_03          = $get->program_code_03;
                $data->program_code_04          = $get->program_code_04;
                $data->program_code_05          = $get->program_code_05;
                $data->program_code_06          = $get->program_code_06;
                $data->program_code_07          = $get->program_code_07;
                $data->program_code_08          = $get->program_code_08;
                $data->program_code_09          = $get->program_code_09;
                $data->program_code_10          = $get->program_code_10;
                $data->course_type_code_01      = $get->course_type_code_01;
                $data->course_type_code_02      = $get->course_type_code_02;
                $data->course_type_code_03      = $get->course_type_code_03;
                $data->course_type_code_04      = $get->course_type_code_04;
                $data->course_type_code_05      = $get->course_type_code_05;
                $data->course_type_code_06      = $get->course_type_code_06;
                $data->course_type_code_07      = $get->course_type_code_07;
                $data->course_type_code_08      = $get->course_type_code_08;
                $data->course_type_code_09      = $get->course_type_code_09;
                $data->course_type_code_10      = $get->course_type_code_10;
                $data->semester_program_01      = $get->semester_program_01;
                $data->semester_program_02      = $get->semester_program_02;
                $data->semester_program_03      = $get->semester_program_03;
                $data->semester_program_04      = $get->semester_program_04;
                $data->semester_program_05      = $get->semester_program_05;
                $data->semester_program_06      = $get->semester_program_06;
                $data->semester_program_07      = $get->semester_program_07;
                $data->semester_program_08      = $get->semester_program_08;
                $data->semester_program_09      = $get->semester_program_09;
                $data->semester_program_10      = $get->semester_program_10;
                $data->course_genre_code_01     = $get->course_genre_code_01;
                $data->course_genre_code_02     = $get->course_genre_code_02;
                $data->course_genre_code_03     = $get->course_genre_code_03;
                $data->course_genre_code_04     = $get->course_genre_code_04;
                $data->course_genre_code_05     = $get->course_genre_code_05;
                $data->course_genre_code_06     = $get->course_genre_code_06;
                $data->course_genre_code_07     = $get->course_genre_code_07;
                $data->course_genre_code_08     = $get->course_genre_code_08;
                $data->course_genre_code_09     = $get->course_genre_code_09;
                $data->course_genre_code_10     = $get->course_genre_code_10;
                
                $data->status                   = $get->status;
                $data->policy                   = $get->policy;
                $data->honor                    = $get->honor;
                $data->attendance               = $get->attendance;
                $data->disability               = $get->disability;
                $data->pedagogy                 = $get->pedagogy;
                $data->course_level_code        = $get->course_level_code;
                $data->role_level_code          = $get->role_level_code;
                $data->full_course_code         = $get->full_course_code;
                $data->created_by               = $get->created_by;
                $data->formula                  = $get->formula;
                $data->additional_text          = $get->additional_text;
                $data->created_at               = $get->created_at;
                $data->updated_at               = $get->updated_at;
            }
        }

        return $courses;
    }

    public static function uniqueByCollege($code)
    {
        $courses = DB::table('courses')
                ->select('course_code')
                ->where('college_code', $code)
                ->groupBy('course_code')
                ->get();
        
        foreach ($courses as $data) {
            $get = CourseHelper::first($data->course_code);

            $data->course_code              = $get->course_code;
            $data->college_code             = $get->college_code;
            $data->grading_mode_code        = $get->grading_mode_code;
            $data->long_title               = $get->long_title;
            $data->short_title              = $get->short_title;
            $data->description              = $get->description;
            $data->course_prerequisites     = $get->course_prerequisites;
            $data->course_content           = $get->course_content;
            $data->course_weekly_outline    = $get->course_weekly_outline;
            $data->course_assessment        = $get->course_assessment;
            $data->course_learning_outcomes = $get->course_learning_outcomes;
            $data->effective_term_code      = $get->effective_term_code;
            $data->end_term_code            = $get->end_term_code;
            $data->sequence                 = $get->sequence;
            $data->credit_hours             = $get->credit_hours;
            $data->contact_hours            = $get->contact_hours;
            $data->lecture_hours            = $get->lecture_hours;
            $data->lab_hours                = $get->lab_hours;
            $data->other_hours              = $get->other_hours;
            $data->faculty_load_hours       = $get->faculty_load_hours;
            $data->this_course_replace      = $get->this_course_replace;
            $data->subject_code             = $get->subject_code;
            $data->program_code_01          = $get->program_code_01;
            $data->program_code_02          = $get->program_code_02;
            $data->program_code_03          = $get->program_code_03;
            $data->program_code_04          = $get->program_code_04;
            $data->program_code_05          = $get->program_code_05;
            $data->program_code_06          = $get->program_code_06;
            $data->program_code_07          = $get->program_code_07;
            $data->program_code_08          = $get->program_code_08;
            $data->program_code_09          = $get->program_code_09;
            $data->program_code_10          = $get->program_code_10;
            $data->course_type_code_01      = $get->course_type_code_01;
            $data->course_type_code_02      = $get->course_type_code_02;
            $data->course_type_code_03      = $get->course_type_code_03;
            $data->course_type_code_04      = $get->course_type_code_04;
            $data->course_type_code_05      = $get->course_type_code_05;
            $data->course_type_code_06      = $get->course_type_code_06;
            $data->course_type_code_07      = $get->course_type_code_07;
            $data->course_type_code_08      = $get->course_type_code_08;
            $data->course_type_code_09      = $get->course_type_code_09;
            $data->course_type_code_10      = $get->course_type_code_10;
            $data->semester_program_01      = $get->semester_program_01;
            $data->semester_program_02      = $get->semester_program_02;
            $data->semester_program_03      = $get->semester_program_03;
            $data->semester_program_04      = $get->semester_program_04;
            $data->semester_program_05      = $get->semester_program_05;
            $data->semester_program_06      = $get->semester_program_06;
            $data->semester_program_07      = $get->semester_program_07;
            $data->semester_program_08      = $get->semester_program_08;
            $data->semester_program_09      = $get->semester_program_09;
            $data->semester_program_10      = $get->semester_program_10;
            $data->course_genre_code_01     = $get->course_genre_code_01;
            $data->course_genre_code_02     = $get->course_genre_code_02;
            $data->course_genre_code_03     = $get->course_genre_code_03;
            $data->course_genre_code_04     = $get->course_genre_code_04;
            $data->course_genre_code_05     = $get->course_genre_code_05;
            $data->course_genre_code_06     = $get->course_genre_code_06;
            $data->course_genre_code_07     = $get->course_genre_code_07;
            $data->course_genre_code_08     = $get->course_genre_code_08;
            $data->course_genre_code_09     = $get->course_genre_code_09;
            $data->course_genre_code_10     = $get->course_genre_code_10;
            $data->status                   = $get->status;
            $data->policy                   = $get->policy;
            $data->honor                    = $get->honor;
            $data->attendance               = $get->attendance;
            $data->disability               = $get->disability;
            $data->pedagogy                 = $get->pedagogy;
            $data->course_level_code        = $get->course_level_code;
            $data->role_level_code          = $get->role_level_code;
            $data->full_course_code         = $get->full_course_code;
            $data->created_by               = $get->created_by;
            $data->formula                  = $get->formula;
            $data->additional_text          = $get->additional_text;
            $data->created_at               = $get->created_at;
            $data->updated_at               = $get->updated_at;
        }
        
        return $courses;
    }

    public static function uniqueByProgram($code)
    {
        $courses = DB::table('courses')
                ->select('course_code')
                ->where(function($query) use ($code){
                    for ($j=1; $j < 10; $j++) { 
                        $query->orWhere('program_code_0'.$j, $code);
                    }
                    $query->orWhere('program_code_10', $code);
                    
                })
                ->where('status','Active')
                ->groupBy('course_code')
                ->get();
        
        foreach ($courses as $data) {
            $get = CourseHelper::first($data->course_code);

            if(isset($get))
            {
                $data->course_code              = $get->course_code;
                $data->college_code             = $get->college_code;
                $data->grading_mode_code        = $get->grading_mode_code;
                $data->long_title               = $get->long_title;
                $data->short_title              = $get->short_title;
                $data->description              = $get->description;
                $data->course_prerequisites     = $get->course_prerequisites;
                $data->course_content           = $get->course_content;
                $data->course_weekly_outline    = $get->course_weekly_outline;
                $data->course_assessment        = $get->course_assessment;
                $data->course_learning_outcomes = $get->course_learning_outcomes;
                $data->effective_term_code      = $get->effective_term_code;
                $data->end_term_code            = $get->end_term_code;
                $data->sequence                 = $get->sequence;
                $data->credit_hours             = $get->credit_hours;
                $data->contact_hours            = $get->contact_hours;
                $data->lecture_hours            = $get->lecture_hours;
                $data->lab_hours                = $get->lab_hours;
                $data->other_hours              = $get->other_hours;
                $data->faculty_load_hours       = $get->faculty_load_hours;
                $data->this_course_replace      = $get->this_course_replace;
                $data->subject_code             = $get->subject_code;
                $data->program_code_01          = $get->program_code_01;
                $data->program_code_02          = $get->program_code_02;
                $data->program_code_03          = $get->program_code_03;
                $data->program_code_04          = $get->program_code_04;
                $data->program_code_05          = $get->program_code_05;
                $data->program_code_06          = $get->program_code_06;
                $data->program_code_07          = $get->program_code_07;
                $data->program_code_08          = $get->program_code_08;
                $data->program_code_09          = $get->program_code_09;
                $data->program_code_10          = $get->program_code_10;
                $data->course_type_code_01      = $get->course_type_code_01;
                $data->course_type_code_02      = $get->course_type_code_02;
                $data->course_type_code_03      = $get->course_type_code_03;
                $data->course_type_code_04      = $get->course_type_code_04;
                $data->course_type_code_05      = $get->course_type_code_05;
                $data->course_type_code_06      = $get->course_type_code_06;
                $data->course_type_code_07      = $get->course_type_code_07;
                $data->course_type_code_08      = $get->course_type_code_08;
                $data->course_type_code_09      = $get->course_type_code_09;
                $data->course_type_code_10      = $get->course_type_code_10;
                $data->semester_program_01      = $get->semester_program_01;
                $data->semester_program_02      = $get->semester_program_02;
                $data->semester_program_03      = $get->semester_program_03;
                $data->semester_program_04      = $get->semester_program_04;
                $data->semester_program_05      = $get->semester_program_05;
                $data->semester_program_06      = $get->semester_program_06;
                $data->semester_program_07      = $get->semester_program_07;
                $data->semester_program_08      = $get->semester_program_08;
                $data->semester_program_09      = $get->semester_program_09;
                $data->semester_program_10      = $get->semester_program_10;
                $data->course_genre_code_01     = $get->course_genre_code_01;
                $data->course_genre_code_02     = $get->course_genre_code_02;
                $data->course_genre_code_03     = $get->course_genre_code_03;
                $data->course_genre_code_04     = $get->course_genre_code_04;
                $data->course_genre_code_05     = $get->course_genre_code_05;
                $data->course_genre_code_06     = $get->course_genre_code_06;
                $data->course_genre_code_07     = $get->course_genre_code_07;
                $data->course_genre_code_08     = $get->course_genre_code_08;
                $data->course_genre_code_09     = $get->course_genre_code_09;
                $data->course_genre_code_10     = $get->course_genre_code_10;
                $data->status                   = $get->status;
                $data->policy                   = $get->policy;
                $data->honor                    = $get->honor;
                $data->attendance               = $get->attendance;
                $data->disability               = $get->disability;
                $data->pedagogy                 = $get->pedagogy;
                $data->course_level_code        = $get->course_level_code;
                $data->role_level_code          = $get->role_level_code;
                $data->full_course_code         = $get->full_course_code;
                $data->created_by               = $get->created_by;
                $data->formula                  = $get->formula;
                $data->additional_text          = $get->additional_text;
                $data->created_at               = $get->created_at;
                $data->updated_at               = $get->updated_at;
            }
        }
        
        return $courses;
    }

    public static function uniqueByOtherProgram($code)
    {
        $courses = DB::table('courses')
                ->select('course_code')
                ->where(function($query) use ($code){
                    for ($j=1; $j < 10; $j++) { 
                        $query->orWhere('program_code_0'.$j,'!=', $code);
                    }
                    $query->orWhere('program_code_10','!=', $code);
                    
                })
                ->where('status','Active')
                ->groupBy('course_code')
                ->get();
        
        foreach ($courses as $data) {
            $get = CourseHelper::first($data->course_code);

            $data->course_code              = $get->course_code;
            $data->college_code             = $get->college_code;
            $data->grading_mode_code        = $get->grading_mode_code;
            $data->long_title               = $get->long_title;
            $data->short_title              = $get->short_title;
            $data->description              = $get->description;
            $data->course_prerequisites     = $get->course_prerequisites;
            $data->course_content           = $get->course_content;
            $data->course_weekly_outline    = $get->course_weekly_outline;
            $data->course_assessment        = $get->course_assessment;
            $data->course_learning_outcomes = $get->course_learning_outcomes;
            $data->effective_term_code      = $get->effective_term_code;
            $data->end_term_code            = $get->end_term_code;
            $data->sequence                 = $get->sequence;
            $data->credit_hours             = $get->credit_hours;
            $data->contact_hours            = $get->contact_hours;
            $data->lecture_hours            = $get->lecture_hours;
            $data->lab_hours                = $get->lab_hours;
            $data->other_hours              = $get->other_hours;
            $data->faculty_load_hours       = $get->faculty_load_hours;
            $data->this_course_replace      = $get->this_course_replace;
            $data->subject_code             = $get->subject_code;
            $data->program_code_01          = $get->program_code_01;
            $data->program_code_02          = $get->program_code_02;
            $data->program_code_03          = $get->program_code_03;
            $data->program_code_04          = $get->program_code_04;
            $data->program_code_05          = $get->program_code_05;
            $data->program_code_06          = $get->program_code_06;
            $data->program_code_07          = $get->program_code_07;
            $data->program_code_08          = $get->program_code_08;
            $data->program_code_09          = $get->program_code_09;
            $data->program_code_10          = $get->program_code_10;
            $data->course_type_code_01      = $get->course_type_code_01;
            $data->course_type_code_02      = $get->course_type_code_02;
            $data->course_type_code_03      = $get->course_type_code_03;
            $data->course_type_code_04      = $get->course_type_code_04;
            $data->course_type_code_05      = $get->course_type_code_05;
            $data->course_type_code_06      = $get->course_type_code_06;
            $data->course_type_code_07      = $get->course_type_code_07;
            $data->course_type_code_08      = $get->course_type_code_08;
            $data->course_type_code_09      = $get->course_type_code_09;
            $data->course_type_code_10      = $get->course_type_code_10;
            $data->semester_program_01      = $get->semester_program_01;
            $data->semester_program_02      = $get->semester_program_02;
            $data->semester_program_03      = $get->semester_program_03;
            $data->semester_program_04      = $get->semester_program_04;
            $data->semester_program_05      = $get->semester_program_05;
            $data->semester_program_06      = $get->semester_program_06;
            $data->semester_program_07      = $get->semester_program_07;
            $data->semester_program_08      = $get->semester_program_08;
            $data->semester_program_09      = $get->semester_program_09;
            $data->semester_program_10      = $get->semester_program_10;
            $data->course_genre_code_01     = $get->course_genre_code_01;
            $data->course_genre_code_02     = $get->course_genre_code_02;
            $data->course_genre_code_03     = $get->course_genre_code_03;
            $data->course_genre_code_04     = $get->course_genre_code_04;
            $data->course_genre_code_05     = $get->course_genre_code_05;
            $data->course_genre_code_06     = $get->course_genre_code_06;
            $data->course_genre_code_07     = $get->course_genre_code_07;
            $data->course_genre_code_08     = $get->course_genre_code_08;
            $data->course_genre_code_09     = $get->course_genre_code_09;
            $data->course_genre_code_10     = $get->course_genre_code_10;
            $data->status                   = $get->status;
            $data->policy                   = $get->policy;
            $data->honor                    = $get->honor;
            $data->attendance               = $get->attendance;
            $data->disability               = $get->disability;
            $data->pedagogy                 = $get->pedagogy;
            $data->course_level_code        = $get->course_level_code;
            $data->role_level_code          = $get->role_level_code;
            $data->full_course_code         = $get->full_course_code;
            $data->created_by               = $get->created_by;
            $data->formula                  = $get->formula;
            $data->additional_text          = $get->additional_text;
            $data->created_at               = $get->created_at;
            $data->updated_at               = $get->updated_at;
            if(isset($get))
            {
            }
        }
        
        // return dd($courses);
        return $courses;
    }

    public static function getCreditByProgram($code, $type)
    {
        // $order = 0;
        $courses = DB::table('courses')
                ->select('course_code')
                ->where(function($query) use ($code){
                    for ($j=1; $j < 10; $j++) { 
                        $query->orWhere('program_code_0'.$j, $code);
                    }
                    $query->orWhere('program_code_10', $code);                    
                })
                ->where('status','Active')
                ->groupBy('course_code')
                ->get();
        
                foreach ($courses as $data) {
                    
                    $db     = DB::table('courses')->where('course_code', $data->course_code);
                    $seq    = $db->max('sequence');
                    $first  = $db->where('sequence', $seq)
                            ->where('status','Active')
                            ->first();
                    $q      = 1;
                    for ($i=1; $i < 10; $i++) { 
                        if ($first->{'program_code_0'.$i} == $code) {
                            $q = $i;
                            break;
                        }
                    }
                            
                    $get    = CourseHelper::firstData($data->course_code)
                            ->where('course_type_code_0'.$q, $type)
                            ->first();
                            
                    if(isset($get))
                    {
                        $data->course_code              = $get->course_code;
                        $data->credit_hours             = $get->credit_hours;
                        $data->course_type_code_01      = $get->course_type_code_01;
                        $data->course_type_code_02      = $get->course_type_code_02;
                        $data->course_type_code_03      = $get->course_type_code_03;
                        $data->course_type_code_04      = $get->course_type_code_04;
                        $data->course_type_code_05      = $get->course_type_code_05;
                        $data->course_type_code_06      = $get->course_type_code_06;
                        $data->course_type_code_07      = $get->course_type_code_07;
                        $data->course_type_code_08      = $get->course_type_code_08;
                        $data->course_type_code_09      = $get->course_type_code_09;
                        $data->course_type_code_10      = $get->course_type_code_10;
                    }
                }
                // $columns = Schema::getColumnListing('courses'); // users table
                // dd($columns[25]); // dump the result and die
        // return dd($courses);
        return $courses->sum('credit_hours');
    }
}
