<?php
namespace App\Helper;

use Illuminate\Support\Facades\DB;

class ProgramHelper {

    public static function first($code) 
    {
        $db     = DB::table('academic_programs')->where('program_code', $code);
        $seq    = $db->max('sequence');
        $first  = $db->where('sequence', $seq)->first();

        return $first;
    }
    
    public static function firstData($code) 
    {
        $db     = DB::table('academic_programs')->where('program_code', $code);
        $seq    = $db->max('sequence');
        $first  = $db->where('sequence', $seq);

        return $first;
    }

    public static function unique()
    {
        $programs   = DB::table('academic_programs')
                    ->select('program_code')
                    ->groupBy('program_code')
                    ->get();

        foreach ($programs as $data) 
        {
            $helper = ProgramHelper::first($data->program_code);

            $data->title                = $helper->title;
            $data->campus_code          = $helper->campus_code;
            $data->degree_type_code     = $helper->degree_type_code;
            $data->degree_level_code    = $helper->degree_level_code;
            $data->college_code         = $helper->college_code;
            $data->required_hours       = $helper->required_hours;
            $data->effective_term_code  = $helper->effective_term_code;
            $data->end_term_code        = $helper->end_term_code;
            $data->status               = $helper->status;
        }

        return $programs;
    }

    public static function uniqueByCodes($code)
    {
        $programs = array();

        for ($i=0; $i < count($code); $i++) { 
            $first = ProgramHelper::first($code[$i]);
            array_push($programs, $first);
        }

        return $programs;
    }
}