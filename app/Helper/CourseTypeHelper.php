<?php
namespace App\Helper;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CourseTypeHelper 
{
    public static function first($code)
    {
        $db     = DB::table('course_types')->where('course_type_code', $code);
        $seq    = $db->max('sequence');
        $first  = $db->where('sequence', $seq)->first();

        return $first;
    }
    
    public static function firstData($code)
    {
        $db     = DB::table('course_types')->where('course_type_code', $code);
        $seq    = $db->max('sequence');
        $first  = $db->where('sequence', $seq);

        return $first;
    }

    public static function unique()
    {
        $course_types  = DB::table('course_types')
                ->select('course_type_code')
                ->groupBy('course_type_code')
                ->get();

        foreach ($course_types as $data) {
            $db     = DB::table('course_types')->where('course_type_code', $data->course_type_code);
            $seq    = $db->max('sequence');
            $get    = $db->where('sequence', $seq)->first();
            
            $data->course_type_code     = $get->course_type_code;
            $data->type_description     = $get->type_description;
            $data->effective_term_code  = $get->effective_term_code;
            $data->end_term_code        = $get->end_term_code;
            $data->system_date          = $get->system_date;
            $data->sequence             = $get->sequence;
            $data->is_shown             = $get->is_shown;
            $data->is_active            = $get->is_active;
            $data->created_at           = $get->created_at;
            $data->updated_at           = $get->updated_at;
            $data->instance_code        = $get->instance_code;
        }

        return $course_types;
    }
}