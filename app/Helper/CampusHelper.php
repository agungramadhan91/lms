<?php
namespace App\Helper;

use Illuminate\Support\Facades\DB;

class CampusHelper {

    public static function first($code) 
    {
        $db     = DB::table('campuses')->where('campus_code', $code);
        $seq    = $db->max('sequence');
        $first  = $db->where('sequence', $seq)->first();

        return $first;
    }
    
    public static function firstData($code) 
    {
        $db     = DB::table('campuses')->where('campus_code', $code);
        $seq    = $db->max('sequence');
        $first  = $db->where('sequence', $seq);

        return $first;
    }

    public static function unique()
    {
        $programs   = DB::table('campuses')
                    ->select('campus_code')
                    ->groupBy('campus_code')
                    ->get();

        foreach ($programs as $data) 
        {
            $helper = CampusHelper::first($data->campus_code);

            $data->campus_code          = $helper->campus_code;
            $data->campus_description   = $helper->campus_description;
            $data->effective_term_code  = $helper->effective_term_code;
            $data->end_term_code        = $helper->end_term_code;
        }

        return $programs;
    }

    public static function uniqueByCodes($code)
    {
        $programs = array();

        for ($i=0; $i < count($code); $i++) { 
            $first = ProgramHelper::first($code[$i]);
            array_push($programs, $first);
        }

        return $programs;
    }
}