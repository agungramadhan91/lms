<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class role extends Model
{
    protected $table = 'roles';
    protected $primaryKey = "role_code";
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = ['role_code','role_level_code','role_description','is_approver','is_active','is_shown','department_code'];

    public function users()
    {
        return $this->belongsToMany(User::class,'role_user','user_id','role_code');
    }
}
