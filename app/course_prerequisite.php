<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class course_prerequisite extends Model
{
    protected $guarded = [];

    protected $casts = [
        'theme_setting' => 'array'
    ];
}
