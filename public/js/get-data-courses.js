
$(document).ready(function() {

    $('select[id="college_code"]').on('change', function(){
        var dl_code = $(this).val();
        if(dl_code) {
            $.ajax({
                url: '/course/college/'+dl_code,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                },

                success:function(data) {
                    // $('#select-course').append(`
                    //     <select multiple='multiple'
                    //             name="course_codes[]"
                    //             id="course_codes"
                    //             required>

                    //             <option value="">dari jquery</option>
                    //     </select>
                    // `);
                    
                    $('select[id="course_codes"]').empty();
                    $.each(data, function(key, value){
                        $('#course_codes').append(
                            '<option value="'+ key +'">'+ key +' - ' + value + '</option>'
                        );
                    });
                },
                complete: function(){
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $('select[id="course_codes"]').empty();
        }

    });

    // $('select[name="dl_code"]').on('change', function(){
    //     var dl_code = $(this).val();
    //     if(dl_code) {
    //         $.ajax({
    //             url: 'http://dapur.ragacorner.dvlp/api/categories',
    //             type:"GET",
    //             dataType:"json",
    //             beforeSend: function(){
    //                 $('#loader').css("visibility", "visible");
    //             },
        
    //             success:function(data) {
        
    //                 // $('select[name="category_id"]').empty();
    //                 $('select[name="category_id"]').append('<option value="0">SEMUA TEMPAT</option>');
    //                 $.each(data, function(key, value){
        
    //                     $('select[name="category_id"]').append('<option value="'+ key +'">' + value + '</option>');
        
    //                 });
    //             },
    //             complete: function(){
    //                 $('#loader').css("visibility", "hidden");
    //             }
    //         });
    //     } else {
    //         $('select[name="place_id"]').empty();
    //     }

    // });

});