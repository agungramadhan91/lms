$(document).ready(function() {

    $('select[name="dl_code"]').on('change', function(){
        var dl_code = $(this).val();
        if(dl_code) {
            $.ajax({
                url: '/maintenance/programs/degree/type/'+dl_code,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                },

                success:function(data) {

                    $('select[name="dt_code"]').empty();
                    // $('select[name="dt_code"]').append('<option value="0">SEMUA TEMPAT</option>');
                    $.each(data, function(key, value){

                        $('select[name="dt_code"]').append('<option value="'+ key +'">'+ key +' - ' + value + '</option>');

                    });
                },
                complete: function(){
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $('select[name="dt_code"]').empty();
        }

    });

    // $('select[name="dl_code"]').on('change', function(){
    //     var dl_code = $(this).val();
    //     if(dl_code) {
    //         $.ajax({
    //             url: 'http://dapur.ragacorner.dvlp/api/categories',
    //             type:"GET",
    //             dataType:"json",
    //             beforeSend: function(){
    //                 $('#loader').css("visibility", "visible");
    //             },
        
    //             success:function(data) {
        
    //                 // $('select[name="category_id"]').empty();
    //                 $('select[name="category_id"]').append('<option value="0">SEMUA TEMPAT</option>');
    //                 $.each(data, function(key, value){
        
    //                     $('select[name="category_id"]').append('<option value="'+ key +'">' + value + '</option>');
        
    //                 });
    //             },
    //             complete: function(){
    //                 $('#loader').css("visibility", "hidden");
    //             }
    //         });
    //     } else {
    //         $('select[name="place_id"]').empty();
    //     }

    // });

});