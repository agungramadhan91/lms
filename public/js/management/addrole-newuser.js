var max_fields      = 10; //maximum input boxes allowed
var add_role_field  = $(".add_role_field"); //Add button ID
var wrap_role_fields= $(".wrap_role_fields"); //Fields wrapper
var y = 1;

$(add_role_field).click(function(e){ //on add input button click
    e.preventDefault();
    y++; //text box increment
    $(wrap_role_fields).append(
        `
        <div class="form-group role_field_filled">
            <strong id="role_field_title`+ y +`">
                Role `+ y +`
            </strong>
            <button class="remove_role_field btn btn-xs btn-danger">-</button>
            <span class="text-danger">*</span>
            
            <select name="role_code[]" 
                    class="form-control role_field" 
                    onchange="checkDuplicateRole()" 
                    id="role_field`+ y +`"
                    required>

                <option value="">-----------</option>
                @foreach($roles as $role)
                    <option value="{{ $role->role_code }}">
                        {{ $role->role_code }} - {{ $role->role_description }}
                    </option>
                @endforeach
            </select>
        </div>
        `
    ); 
});

$(wrap_role_fields).on("click",".remove_role_field", function(e){ //user click on remove text
    e.preventDefault(); 
    $(this).parent('div').remove(); 
    y--;
});