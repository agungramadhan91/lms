var wf_add_role_field  = $(".wf_add_role_field"); //Add button ID
var wf_wrap_role_fields= $(".wf_wrap_role_fields"); //Fields wrapper
var x = 1; //initlal text box count

$(wf_add_role_field).click(function(e){ //on add input button click
    e.preventDefault();
    x++; //text box increment
    $(wf_wrap_role_fields).append(
        `
        <div class="form-group wf_role_field_filled">
            <strong id="wf_role_field_title`+ x +`">
                Role Level `+ x +`
            </strong>
            <button class="wf_remove_role_field btn btn-xs btn-danger">-</button>
            <span class="text-danger">*</span>
            
            <select name="role_code[]" 
                    class="form-control wf_role_field" 
                    onchange="checkDuplicateRole2()" 
                    id="wf_role_field`+ x +`"
                    required>

                <option value="">-----------</option>
                @foreach($roles as $role)
                    <option value="{{ $role->role_code }}">
                        {{ $role->role_code }} - {{ $role->role_description }}
                    </option>
                @endforeach
            </select>
        </div>
        `
    ); 
});

$(wf_wrap_role_fields).on("click",".wf_remove_role_field", function(e){ //user click on remove text
    e.preventDefault(); 
    $(this).parent('div').remove(); 
    x--;
});